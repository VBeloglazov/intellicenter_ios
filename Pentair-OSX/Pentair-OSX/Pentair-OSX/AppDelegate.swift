//
//  AppDelegate.swift
//  Pentair-OSX
//
//  Created by Dashon Howard on 3/17/15.
//  Copyright (c) 2015 Pentair. All rights reserved.
//

import Cocoa
import WebKit

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {
    
    @IBOutlet weak var webView: WebView!
    @IBOutlet weak var window: NSWindow!
    var controller: ViewController?
    
    
    func applicationDidFinishLaunching(aNotification: NSNotification) {
        controller = ViewController()
        controller!.webView = webView
        let view = controller!.view
        
        window.makeKeyAndOrderFront(nil)
    }
    
    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }
    
    
}

