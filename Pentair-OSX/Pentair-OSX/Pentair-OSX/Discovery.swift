//
//  Discovery.swift
//  Pentair OSX
//
//  Created by Dashon Howard on 3/2/15.
//  Copyright (c) 2015 Pentair. All rights reserved.
//

import Foundation

let UTDataUpdatedNotification: String! = "DataUpdated"

class Discovery: NSObject, NSNetServiceBrowserDelegate, NSNetServiceDelegate {
    let serviceType: String
    var serviceBrowser: NSNetServiceBrowser
    var serviceList: [NSNetService]
    var services: [LocalService]
    
    override init() {
        self.serviceType = "_http._tcp."
        self.serviceBrowser = NSNetServiceBrowser()
        self.serviceList = [NSNetService]()
        self.services = [LocalService]()
        super.init()
        self.serviceBrowser.delegate = self
    }
    
    func netServiceBrowser(aNetServiceBrowser: NSNetServiceBrowser, didFindService aNetService: NSNetService, moreComing: Bool) {
        serviceList.append(aNetService)
       // NSLog("Found: \(aNetService)")
        if !moreComing {
            update()
        }
    }
    
    func netServiceBrowser(aNetServiceBrowser: NSNetServiceBrowser, didNotSearch errorDict: [NSObject : AnyObject]) {
        NSLog("Search was not successful. Error code: \(errorDict[NSNetServicesErrorCode])")
    }
    
    func netServiceBrowser(aNetServiceBrowser: NSNetServiceBrowser, didRemoveService aNetService: NSNetService, moreComing: Bool) {
        let iService = find(serviceList, aNetService)
        if iService != nil {
            serviceList.removeAtIndex(iService!)
        }
        let subArray = services.filter({$0.netService == aNetService})
        for item in subArray {
            let iSharedPoint = find(services, item)
            if iSharedPoint != nil {
                services.removeAtIndex(iSharedPoint!)
            }
        }
        NSLog("Became unavailable: \(aNetService)")
        if !moreComing {
            update()
        }
    }
    
    func update() {
        for service in serviceList {
            service.delegate = self
            service.resolveWithTimeout(5)
        }
    }
    
    func netServiceDidResolveAddress(sender: NSNetService) {
        //service.addresses - array containing NSData objects, each of which contains an appropriate
        //sockaddr structure that you can use to connect to the socket
        for addressBytes in sender.addresses!
        {
            var inetAddress : sockaddr_in!
            var inetAddress6 : sockaddr_in6?
            //NSData’s bytes returns a read-only pointer (const void *) to the receiver’s contents.
            //var bytes: UnsafePointer<()> { get }
            var inetAddressPointer = UnsafePointer<sockaddr_in>(addressBytes.bytes)
            //Access the underlying raw memory
            inetAddress = inetAddressPointer.memory
            
            if inetAddress.sin_family == __uint8_t(AF_INET) //Note: explicit convertion (var AF_INET: Int32 { get } /* internetwork: UDP, TCP, etc. */)
            {
            }
            else
            {
                if inetAddress.sin_family == __uint8_t(AF_INET6) //var AF_INET6: Int32 { get } /* IPv6 */
                {
                    var inetAddressPointer6 = UnsafePointer<sockaddr_in6>(addressBytes.bytes)
                    inetAddress6 = inetAddressPointer6.memory
                    inetAddress = nil
                }
                else
                {
                    inetAddress = nil
                }
            }
            var ipString : UnsafePointer<Int8>?
            var portString = String()
            
            var ipStringBuffer = UnsafeMutablePointer<Int8>.alloc(Int(INET6_ADDRSTRLEN))
            if (inetAddress != nil)
            {
                var addr = inetAddress.sin_addr
                ipString = inet_ntop(Int32(inetAddress.sin_family),
                    &addr,
                    ipStringBuffer,
                    __uint32_t(INET6_ADDRSTRLEN))
                
                var port = UInt16(bigEndian: inetAddress.sin_port)
                portString = (Int(port) as NSNumber).stringValue
            } else {
                if (inetAddress6 != nil)
                {
                    var addr  = inetAddress6!.sin6_addr
                    ipString = inet_ntop(Int32(inetAddress6!.sin6_family),
                        &addr,
                        ipStringBuffer,
                        __uint32_t(INET6_ADDRSTRLEN))
                    var port = UInt16(bigEndian: inetAddress6!.sin6_port)
                    portString = (Int(port) as NSNumber).stringValue
                }
            }
            if (ipString != nil)
            {
                
                // Returns `nil` if there is a malformed service type or ip address
                var ip = String.fromCString(ipString!)
                if ip != nil {
                    NSLog("\(sender.name)(\(sender.type)) - \(ip!)")
                    if (sender.name.isEmpty)
                    {
                        continue
                    }
                    var serviceName = sender.name
                    var location = serviceName.rangeOfString(self.serviceForSearch)
                    if (location != nil)
                    {
                        // strip off the Pentair:
                        var name = serviceName.substringWithRange(Range<String.Index>(start: advance(serviceName.startIndex, 9), end: serviceName.endIndex))
                        var found = false;
                        // Is this a duplicate entry?
                        for var i = 0; i < services.count; i++ {
                            if (services[i].name == name) {
                                found = true;
                                continue
                            }
                        }
                        if (!found) {
                            services.append(LocalService(netService: sender, name: name, ipAddress: ip!, portValue: portString))
                            NSNotificationCenter.defaultCenter().postNotificationName(UTDataUpdatedNotification, object: self)
                        }
                    }
                }
            }
            // Deallocate the pointer memory.
            ipStringBuffer.dealloc(Int(INET6_ADDRSTRLEN))
        }
    }
    
    func netService(sender: NSNetService, didNotResolve errorDict: [NSObject : AnyObject]) {
        NSLog("\(sender.name) did not resolve: \(errorDict[NSNetServicesErrorCode]!)")
    }
    
    func searchForLocalProperties() {
        serviceBrowser.searchForServicesOfType(serviceType, inDomain: "")
    }
    
    func reset() {
        serviceBrowser.stop()
        for service in serviceList {
            service.stop()
        }
        serviceList.removeAll()
        services.removeAll()
    }
}

// Helper class
class LocalService: Serializable, Equatable {
    var netService: NSNetService
    var name: String
    var address: String
    var port: String
    
    init(netService: NSNetService, name: String, ipAddress: String, portValue: String) {
        self.netService = netService
        self.name = name
        self.address = ipAddress
        self.port = portValue
    }
}

func == (lhs: LocalService, rhs: LocalService) -> Bool {
    return (lhs.netService == rhs.netService) && (lhs.name == rhs.name) && (lhs.address == rhs.address) && (lhs.port == rhs.port)
}