//
//  PentairServer.swift
//  Swifter
//  Copyright (c) 2014 Damian Kołakowski. All rights reserved.
//

import Foundation

func PentairServer(publicDir: String?) -> HttpServer {
    let server = HttpServer()
    server["/?localConnections"] = { request in
        return .OK(.JSON(browser.services.toDictionary()))
    }
    
    server["/app/templates/(.+)"] = { request in
        var capturedGroup = ""
        for (index, group) in enumerate(request.capturedUrlGroups) {
            capturedGroup = group
        }
        
        switch request.method.uppercaseString {
        case "GET":
            var requestedTemplate = NSBundle.mainBundle().URLForResource(capturedGroup.stringByDeletingPathExtension, withExtension: capturedGroup.pathExtension, subdirectory: "www.client/App/Templates")
            if let url = requestedTemplate {
                if let rootDir = publicDir {
                    if let html = NSData(contentsOfURL:requestedTemplate!) {
                        return .RAW(200, html)
                    } else {
                        return .NotFound
                    }
                }
            }
            break;
        case "POST":
            break;
        default:
            return .NotFound
        }
        return .NotFound
    }
    
    server["/"] = { request in
        return .OK(.JSON(browser.services.toDictionary()))
    }
    return server
}