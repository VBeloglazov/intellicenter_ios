//
//  JsonSerializer.swift
//  Pentair OSX
//
//  Created by Dashon Howard on 12/22/14.
//  Copyright (c) 2014 Pentair Corporation. All rights reserved.
//
import Foundation

extension Array {
    func toDictionary() -> [NSDictionary!] {
        var err: NSError?
        var dictionaryArray=[NSDictionary]()
        for item in self{
            if item is Serializable{
                var sItem = item as? Serializable
                dictionaryArray.append(sItem!.toDictionary())
            }
        }
        return dictionaryArray
    }
}


class Serializable : NSObject{
    
    func toJson() -> NSData! {
        var err: NSError?
        
        return NSJSONSerialization.dataWithJSONObject(self.toDictionary(), options:NSJSONWritingOptions(0), error: &err)
    }
    
    func toJsonString() -> NSString! {
        return NSString(data: self.toJson(), encoding: NSUTF8StringEncoding)
    }
    
    func toDictionary() -> NSDictionary {
        var propertiesDictionary = NSMutableDictionary()
        
        for name in getNames() {
            var propValue : AnyObject! = self.valueForKey(name);
            
            if propValue is Serializable {
                propertiesDictionary.setValue((propValue as Serializable).toDictionary(), forKey: name)
            } else if propValue is Array<Serializable> {
                var subArray = Array<NSDictionary>()
                for item in (propValue as Array<Serializable>) {
                    subArray.append(item.toDictionary())
                }
                propertiesDictionary.setValue(subArray, forKey: name)
            } else if propValue is NSData {
                propertiesDictionary.setValue((propValue as NSData).base64EncodedStringWithOptions(nil), forKey: name)
            } else if propValue is Bool {
                propertiesDictionary.setValue((propValue as Bool).boolValue, forKey: name)
            } else if propValue is NSDate {
                var date = propValue as NSDate
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "Z"
                var dateString = NSString(format: "/Date(%.0f000%@)/", date.timeIntervalSince1970, dateFormatter.stringFromDate(date))
                propertiesDictionary.setValue(dateString, forKey: name)
            } else if (propValue is NSString || propValue is Int || propValue is Float){
                propertiesDictionary.setValue(propValue, forKey: name)
            }
        }
        return propertiesDictionary
    }
    
    
    func getNames() -> [String]{
        var target: AnyClass  = classForCoder
        var result:[String] = propertyNames(target)
        
        while target.superclass() != nil {
            target = target.superclass()!
            
            var clz: NSObject.Type = target as NSObject.Type
            var con = clz()
            if (con is Serializable){
                result = result + propertyNames(target)
            }
        }
        return result
    }
    
    func propertyNames(target:AnyClass!) -> [String] {
        var names: [String] = []
        var count: UInt32 = 0
        var count2: UInt32 = 0
        // Uses the Objc Runtime to get the property list
        var properties = class_copyPropertyList(target, &count)
        for var i = 0; i < Int(count); ++i {
            let property: objc_property_t = properties[i]
            let name: String = String.fromCString(property_getName(property))!
            names.append(name)
        }
        free(properties)
        return names
    }
    
    override init() { super.init()}
}
