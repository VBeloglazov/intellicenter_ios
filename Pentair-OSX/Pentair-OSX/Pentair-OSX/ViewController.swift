//
//  ViewController.swift
//  Pentair OSX
//
//  Created by Dashon Howard on 3/2/15.
//  Copyright (c) 2015 Pentair. All rights reserved.
//

import Cocoa
import WebKit
import Utility

var browser = Discovery("Pentair: ")

class ViewController: NSViewController {
    var webView:WebView?

    override func loadView() {
        self.view = webView!
        var localUrl = NSBundle.mainBundle().URLForResource("index2", withExtension: "html", subdirectory: "www.client")
        webView!.mainFrame!.loadRequest(NSURLRequest(URL: localUrl!))
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "update", name: UTDataUpdatedNotification, object: browser)
        
        let server = PentairServer(NSBundle.mainBundle().resourcePath)
        var error: NSError?
        
        if !server.start(error: &error) {
            println("Server start error: \(error)")
        } else {
            println("Server started !")
            //start searcher
            browser.reset()
            browser.searchForLocalProperties()
        }
    }
    
    
    func update() {
        println(browser.services)
    }
    
    
    override var representedObject: AnyObject? {
        didSet {
            // Update the view, if already loaded.
        }
    }
    
    
}

