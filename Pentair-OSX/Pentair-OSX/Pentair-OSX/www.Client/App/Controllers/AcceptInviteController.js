﻿'use strict';
var PWC = PWC || {};
PWC.controller('AcceptInviteController', [
    '$scope', '$location', '$routeParams', '$route', 'PropertyService', 'Enumerations', 'AppService', 'AuthService',
    function ($scope, $location, $routeParams, $route, propertyService, enumerations, appService, authService) {
        var inviteInfo = $route.current.locals.inviteInfo.data;
        var autoAcceptData = {
            Email: authService.authentication.userName,
            Password: "TokenProvided",
            ConfirmPassword: "TokenProvided",
            InviteId: $routeParams.inviteId
        };


        $scope.InvalidInvite = false;
        $scope.EulaRefused = false;
        $scope.ShowEula = false;
        $scope.ShowExistingAcct = false;

        if (inviteInfo.error) {
            $scope.InvalidInvite = true;
            $scope.InvaidInviteMessage = inviteInfo.error;
        }

        //AutoAccept if logged in as invited user
        if (authService.authentication.isAuth && inviteInfo.Email === authService.authentication.userName) {
            appService.PostData("Api/Account/AcceptInvite", autoAcceptData).then(function (inviteResponse) {
                propertyService.FetchInstallations();
                propertyService.SelectedInstallation = inviteResponse.data.InstallationId;
                propertyService.Connect();
            }).catch(function () {
                authService.logOut();
            });
        } else {
            authService.resetAuthData(true);
        }

        $scope.Property = propertyService;
        $scope.InviteError = false;
        $scope.ExistingInviteError = false;
        $scope.ExistingInviteLoginError = false;

        $scope.AcceptInviteData = {
            email: inviteInfo.Email,
            password: "",
            confirmPassword: "",
            propertyName: inviteInfo.PropertyName,
            inviteId: $routeParams.inviteId
        }

        $scope.LoginData = {
            email: "",
            password: "",
            inviteId: $routeParams.inviteId
        };

        $scope.ToggleEula = function () {
            $scope.ShowEula = !$scope.ShowEula;
        }

        $scope.ToggleExistingAcct = function () {
            $scope.ShowExistingAcct = !$scope.ShowExistingAcct;
        };

        $scope.RefusedEula = function () {
            $scope.ShowEula = false;
            $scope.EulaRefused = true;
        }


        $scope.AttachToExistingAcct = function () {
            var loginData = {
                userName: $scope.LoginData.email,
                password: $scope.LoginData.password,
                rememberMe: false
            };
            authService.login(loginData).then(function (loginResponse) {
                var data = {
                    Email: $scope.LoginData.email,
                    Password: $scope.LoginData.password,
                    ConfirmPassword: $scope.LoginData.password,
                    InviteId: $scope.LoginData.inviteId
                };
                appService.PostData("Api/Account/AcceptInvite", data).then(function (inviteResponse) {
                    propertyService.FetchInstallations();
                    propertyService.SelectedInstallation = inviteResponse.data.InstallationId;
                    propertyService.Connect();
                }).catch(function () {
                    $scope.ExistingInviteError = true;
                });
            }).catch(function () {
                $scope.ExistingInviteLoginError = true;
            });
        }

        $scope.AcceptInvite = function () {
            $scope.ShowEula = false;

            var data = {
                Email: $scope.AcceptInviteData.email,
                Password: $scope.AcceptInviteData.password,
                ConfirmPassword: $scope.AcceptInviteData.confirmPassword,
                InviteId: $scope.AcceptInviteData.inviteId
            };


            appService.PostData("Api/Account/AcceptInvite", data).then(function (inviteResponse) {
                authService.authentication.lastUserName = inviteResponse.data.UserName;
                authService.authentication.accountCreated = true;

                $location.path('/login');
            }).catch(function () {
                $scope.InviteError = true;
            });
        }

        $scope.validatePassword = function () {
            var re = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*)[\\w]{6,}$");
            var result = false;
            if ($scope.AcceptInviteData.password) {
                result = $scope.AcceptInviteData.password.match(re) ? true : false;
            }
            return result;
        }

        $scope.verifyConfirmationPassword = function () {
            return !$scope.AcceptInvitationForm.confirmPassword.$invalid &&
                    $scope.validatePassword() &&
                   $scope.AcceptInviteData.password === $scope.AcceptInviteData.confirmPassword;
        }

        $scope.validateForm = function () {
            return $scope.validatePassword() &&
                $scope.verifyConfirmationPassword();
        }

    }
]);