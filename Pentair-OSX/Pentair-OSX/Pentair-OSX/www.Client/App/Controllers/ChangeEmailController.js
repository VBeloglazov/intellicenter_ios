﻿'use strict';
var PWC = PWC || {};

PWC.controller('ChangeEmailController', [
   '$scope', '$location', 'AppService', function ($scope, $location, appService) {
       $scope.changeEmailData = {
           oldEmail: "",
           newEmail: "",
           confirmEmail: "",
           error: false,
           success: false
       }

       $scope.GoToDashboard = function () {
           $location.path("/property");
       }

       $scope.validateOldEmail = false;

       $scope.ChangeEmail = function () {
           var changeEmail = {
               oldEmail: $scope.changeEmailData.oldEmail,
               newEmail: $scope.changeEmailData.newEmail,
               confirmEmail: $scope.changeEmailData.confirmEmail,
           }
           appService.PostData("Api/Account/ChangeEmail", changeEmail).then(function () {
               $scope.changeEmailData.error = false;
               $scope.changeEmailData.success = true;
           }).catch(function () {
               $scope.changeEmailData.error = true;
               $scope.changeEmailData.success = false;
           }
           ).finally(function () {
               $scope.validateOldEmail = false;
               $scope.changeEmailData.oldEmail = "";
               $scope.changeEmailData.newEmail = "";
               $scope.changeEmailData.confirmEmail = "";
           });

       };


       $scope.validateEmailForm = function () {
           if ($scope.ChangeEmailForm)
               return $scope.ChangeEmailForm && $scope.verifyEmail && !$scope.ChangeEmailForm.newEmail.$error.email && $scope.changeEmailData.newEmail &&
                   $scope.verifyConfirmationEmail();
           return false;
       };


       $scope.verifyEmail = function () {
           var data = {
               Email: $scope.changeEmailData.oldEmail
           }
           appService.PostData("Api/Account/VerifyEmail", data).then(function () {
               $scope.validateOldEmail = true;
           }).catch(function (err) {
               $scope.validateOldEmail = false;
           });
       }


       $scope.verifyConfirmationEmail = function () {
           return !$scope.ChangeEmailForm.confirmEmail.$invalid &&
               $scope.changeEmailData.newEmail == $scope.changeEmailData.confirmEmail;
       }

       $scope.clearOldEmail = function () {
           $scope.changeEmailData.oldEmail = "";
       }

   }
]);