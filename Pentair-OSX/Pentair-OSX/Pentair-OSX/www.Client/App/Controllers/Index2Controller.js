﻿'use strict';
var PWC = PWC || {};

PWC.controller('Index2Controller', ['$scope', '$location',
    function ($scope, $location) {
        $scope.DisplayAccountMenu = true;


        $scope.LogOut = function () {
            $scope.ToggleAccountMenu();
        }


        $scope.ToggleAccountMenu = function () {
            $scope.DisplayAccountMenu = !$scope.DisplayAccountMenu;
        }

    }]);