﻿'use strict';
var PWC = PWC || {};
PWC.controller('InstallationsController', [
    '$scope', '$location', '$route', '$filter', '$q', 'PropertyService', 'LocalStorageService', 'SocketService', 'PropertyModel','AppService',
    function ($scope, $location, $route, $filter, $q, propertyService, localStorageService, socketService, propertyModel,appService) {

        $scope.Property = propertyService;
        $scope.Model = propertyModel;

        $scope.HidePropertySelector = function () {
            var routesToHideFrom = ['InstallationsController', 'LoginController', 'AcceptInviteController','InstallationGridController'];
            return ($route.current && routesToHideFrom.indexOf($route.current.controller) > -1) || propertyModel.Installations.length == 0;
        }

        //Private Methods

        function init() {
            if (propertyModel.Ready) {
                $location.path('/property');
            } else {
                loadProperty();
            }
        }


        function loadProperty() {
            propertyModel.Clear(true);

            if ($route.current)
                propertyModel.Installations = $route.current.locals.installations.data;

            reloadFromCache();
            if (PWC.isEmptyObject(socketService.socket))
                autoConnectToSingleProperty();
        }


        function reloadFromCache() {
            if (sessionStorage.selectedInstallation) {
                propertyModel.SelectedInstallation = JSON.parse(sessionStorage.selectedInstallation);
            }
        }


        function autoConnectToSingleProperty() {
            if (1 === propertyModel.Installations.length) {
                propertyModel.SelectedInstallation = propertyModel.Installations[0].InstallationId;
                propertyService.Connect();
            }
        }

        //END


        //Init
        init();
        //END

    }
]);