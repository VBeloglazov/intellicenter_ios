﻿'use strict';
var PWC = PWC || {};
PWC.controller('LightShowController', [
    '$scope', '$location', 'PropertyService', 'PropertyModel',
    function ($scope, $location, propertyService, propertyModel) {

        $scope.Property = propertyService;
        $scope.Model = propertyModel;

        $scope.IntelliHelp = false;
        $scope.MagicHelp = false;


        $scope.ToggleIntelliHelp = function () {
            $scope.IntelliHelp = !$scope.IntelliHelp;
        };


        $scope.ToggleMagicHelp = function () {
            $scope.MagicHelp = !$scope.MagicHelp;
        };


        $scope.GoToDashboard = function () {
            $location.path("/property");
        }
    }
]);