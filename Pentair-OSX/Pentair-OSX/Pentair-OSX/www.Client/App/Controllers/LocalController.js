'use strict';
var PWC = PWC || {};
PWC.controller('LocalController', [
    '$scope', '$location', '$interval', 'AppService',
    function ($scope, $location, $interval, AppService) {

        //SCOPE vars
        $scope.Busy = false;
        $scope.Properties = [];
        var pull = $interval(function() {
         var serviceUrl = 'http://localhost:12001/?localConnections';
            AppService.GetLocalData(serviceUrl).then(function (message) {
                if (message) {
                    var connections = message.data;
                    if (connections) {
                        $scope.Properties = [];
                        for (var i = 0; i < connections.length; i++) {
                        var connection = connections[i];
                                                     
                            var newConnection = {
                                name: connection.name,
                                address: connection.address,
                                port: connection.port
                            }
                            $scope.Properties.push(newConnection);
                        }
                    }
                }
            });
        }, 2000);
        //END

        //SCOPE Methods 
        $scope.$on('$destroy', function() {
            if (pull) {
                $interval.cancel(pull);
            }
        });

        //END
    }
]);