﻿'use strict';
var PWC = PWC || {};
PWC.controller('PropertyController', [
    '$scope', '$location', 'PropertyService','PropertyModel',
    function ($scope, $location, propertyService,propertyModel) {

        //SCOPE vars
        $scope.Busy = false;
        $scope.AdminSecurity = false;
        $scope.Property = propertyService;
        $scope.Model = propertyModel;
        //END

        //SCOPE Methods 
        $scope.GoToLightsPage = function () {
            $location.path("/lightShow");
        }


        $scope.GoToManageUserPage = function () {
            $location.path("/manageUsers");
        }


        $scope.GoToSchedulesPage = function () {
            $location.path("/schedules");
        }

        
        $scope.GoToAddProgramPage = function () {
            $location.path("/addProgram");
        }
        
        $scope.GoToConfigPage = function () {
            $location.path("/configure");
        }
        //END
    }
]);