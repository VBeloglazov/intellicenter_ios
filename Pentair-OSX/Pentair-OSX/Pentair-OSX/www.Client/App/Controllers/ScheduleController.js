﻿'use strict';
var PWC = PWC || {};
PWC.controller('ScheduleController', [
    '$scope', '$location', '$filter', 'PropertyService', 'AuthService', 'Enumerations', 'PropertyModel',
    function ($scope, $location, $filter, propertyService, authService, enumerations, propertyModel) {
        var stopTimePristine = true;
        $scope.InitialTimeFormat = getTimeFormat;
        $scope.Model = propertyModel;
        $scope.Property = propertyService;
        $scope.Authentication = authService;
        $scope.Enumerations = enumerations;
        $scope.Startup = PWC.convertToDate(propertyModel.Objects["_C10C"].MIN, propertyModel.Objects["_C10C"].TIMZON);
        $scope.ProgrammableCircuits = [];
        $scope.Modal = '';

        $scope.Schedule = {
            ObjNam: '',
            Body: {},
            Circuit: '',
            Type: '',
            Group: '1',
            Status: 'ON',
            Days: [],
            StartTime: $scope.Startup,
            StopTime: PWC.addTwohours($scope.Startup),
            StartMode: 'ABSTIM',
            StopMode: 'ABSTIM',
            UseCurrentHeatMode: true,
            UseCurrentTemp: true
        }

        $scope.CircuitSelected = function (selection) {
            if (selection) {
                $scope.Schedule.Body = {};
                var obj = propertyModel.Objects[selection];

                if (obj.OBJTYP == 'BODY')
                    angular.copy(obj, $scope.Schedule.Body);
            }
        }

        $scope.HeatModeSelected = function (body, heatMode) {
            if (!PWC.isEmptyObject(body) && heatMode)
                body.HEATER = $filter('modeToHeater')(body, heatMode);
        }

        $scope.TypeChanged = function () {
            $scope.Schedule.Days = [];
        }

        $scope.PopulateProgrammableCircuits = function () {
            $scope.ProgrammableCircuits = $filter('programmableCircuits')(propertyModel.Objects);
        }

        $scope.PopulateProgrammableCircuits();

        $scope.GoToDashboard = function () {
            propertyService.ReleaseTimeUpdates();
            $location.path("/property");
        }

        $scope.DeleteSchedule = function (schedule) {
            if (schedule && schedule.OBJNAM) {
                $scope.Schedule.ObjNam = schedule.OBJNAM;
                $scope.Modal = 'DELETE';
            }
        }

        $scope.ConfirmDeleteSchedule = function () {
            if ($scope.Schedule.ObjNam) {
                propertyService.SetValue($scope.Schedule.ObjNam, 'STATUS', 'DSTROY');
                propertyService.GetSchedules();
                $scope.Modal = '';
            }
        }


        $scope.CreateNewSchedule = function () {
            resetSchedule();
            $scope.Schedule.UseCurrentHeatMode = true;
            $scope.Schedule.UseCurrentTemp = true;
            $scope.Modal = 'ADD';
        }

        $scope.EditSchedule = function (schedule) {
            resetSchedule();
            var obj = propertyModel.Objects[schedule.CIRCUIT];
            if (obj) {
                $scope.Schedule.ObjNam = schedule.OBJNAM;
                $scope.Schedule.Circuit = schedule.CIRCUIT;
                $scope.Schedule.Days = schedule.DAY.split('');
                $scope.Schedule.Type = schedule.SINGLE == 'ON' ? 1 : 0; 
                $scope.Schedule.StartTime = PWC.convertToDate(schedule.TIME, propertyModel.Objects["_C10C"].TIMZON);
                $scope.Schedule.StopTime = PWC.convertToDate(schedule.TIMOUT, propertyModel.Objects["_C10C"].TIMZON);
                $scope.Schedule.StartMode = schedule.START;
                $scope.Schedule.StopMode = schedule.STOP;
                $scope.Schedule.Group = schedule.GROUP;
                $scope.Schedule.Status = schedule.STATUS;

                if (obj.OBJTYP === 'BODY') {
                    angular.copy(obj, $scope.Schedule.Body);
                    $scope.Schedule.Body.HEATER = schedule.HEATER;

                    if (schedule.HEATER !== '000FF') {
                        $scope.Schedule.Body.LOTMP = schedule.LOTMP;
                        $scope.Schedule.Body.HITMP = schedule.HITMP;
                    }

                    $scope.Schedule.UseCurrentHeatMode = false;
                    $scope.Schedule.UseCurrentTemp = false;
                }

                $scope.Modal = "EDIT";
            }
        }

        $scope.checkAllDays = function () {
            $scope.Schedule.Days = enumerations.DaysOfWeek.map(function (item) { return item.id; });
        };

        $scope.SetTemp = function (obj, prop, value) {
            if (!PWC.isEmptyObject($scope.Schedule.Body)) {
                $scope.Schedule.Body[prop] = value;
            }
        }


        $scope.IncreaseTemp = function (obj, prop, max) {
            if (PWC.isValidObject(obj, [prop])) {
                var target = obj[prop];
                var value = Number(target) || Number(target.replace(/\D/g, ''));
                value++;
                if (value <= max)
                    obj[prop] = value;
            }
        }


        $scope.DecreaseTemp = function (obj, prop, min) {
            if (PWC.isValidObject(obj, [prop])) {
                var target = obj[prop];
                var value = Number(target) || Number(target.replace(/\D/g, ''));
                value--;
                if (value >= min)
                    obj[prop] = value;
            }
        }

        $scope.SaveSchedule = function (mode) {
            $scope.Modal = '';
            switch (mode) {
                case 'ADD':
                    propertyService.CreateScheduleObject().then(function (message) {
                        saveSchedule(message.objectList[0].objnam);
                    }).catch(function () {
                        $scope.ScheduleError = true;
                    });
                    break;
                case 'EDIT':
                    saveSchedule($scope.Schedule.ObjNam);
            }
        }

        $scope.SyncStopTime = function (value) {
            if (stopTimePristine || isNaN($scope.Schedule.StopTime)) {
                $scope.Schedule.StopTime = PWC.addTwohours(value);
            }
        }

        $scope.StopTimeChanged = function () {
            stopTimePristine = false;
        }

        $scope.ValidateNewSchedule = function () {
            return ($scope.Schedule.Days.length && $scope.Schedule.Circuit) ? true : false;
        }

        $scope.ToggleUseCurrent = function (type) {
            switch (type) {
                case 'HeatMode':
                    if ($scope.Schedule.UseCurrentHeatMode)
                        $scope.Schedule.Body.HEATER = propertyModel.Objects[$scope.Schedule.Circuit].HEATER;
                    break;
                case 'Temp':
                    if ($scope.Schedule.UseCurrentTemp)
                        $scope.Schedule.Body.LOTMP = propertyModel.Objects[$scope.Schedule.Circuit].LOTMP;
            }
        }

        function getTimeFormat() {
            var system = propertyModel.Objects["_C10C"];
            var mode = ((system) ? system.CLK24A : "AMPM");

            return (mode == "AMPM") ? "h:mm a" : "HH:mm";
        }

        function resetSchedule() {
            $scope.Schedule = {
                ObjNam: '',
                Body: {},
                Circuit: '',
                Type: '',
                Group: '1',
                Status: 'ON',
                Days: [],
                StartTime: PWC.convertToDate(propertyModel.Objects["_C10C"].MIN, propertyModel.Objects["_C10C"].TIMZON),
                StopTime: PWC.addTwohours(PWC.convertToDate(propertyModel.Objects["_C10C"].MIN, propertyModel.Objects["_C10C"].TIMZON)),
                StartMode: 'ABSTIM',
                StopMode: 'ABSTIM',
                UseCurrentHeatMode: true,
                UseCurrentTemp: true
            }
        }

        function saveSchedule(objNam) {
            if (!objNam) {
                $scope.ScheduleError = true;
                return;
            }
            var params = {
                CIRCUIT: $scope.Schedule.Circuit,
                DAY: getDayMsgString($scope.Schedule.Days),
                SINGLE: $scope.Schedule.Type ? 'ON' : 'OFF',
                START: $scope.Schedule.StartMode,
                TIME: $filter('date')($scope.Schedule.StartTime, 'HH:mm'),
                STOP: $scope.Schedule.StopMode,
                TIMOUT: $filter('date')($scope.Schedule.StopTime, 'HH:mm'),
                GROUP: $scope.Schedule.Group,
                STATUS: $scope.Schedule.Status,
                HEATER: $scope.Schedule.Body.HEATER
            }
            if (params.HEATER !== '000FF') {
                params.HITMP = $scope.Schedule.Body.HITMP;
                params.LOTMP = $scope.Schedule.Body.LOTMP;
            }
            propertyService.SaveSchedule(objNam, params);
        }

        function getDayMsgString(days) {
            return (angular.isArray(days) ? days.join('') : days);
        }
    }
]);