﻿'use strict';
var PWC = PWC || {};
PWC.controller('SystemInformationController', [
    '$scope', '$location', '$filter', 'Enumerations', 'PropertyService', 'PropertyModel',
    function ($scope, $location, $filter, Enumerations, propertyService, propertyModel) {
        $scope.Property = propertyService;
        $scope.Model = propertyModel;
        $scope.EditLocation = false;
        $scope.EditTime = false;
        $scope.EditTimeComponents = false;
        $scope.EditPreferences = false;
        $scope.savedClockMode = propertyModel.Objects["_C10C"].CLK24A;
        $scope.savedSource = propertyModel.Objects["_C105"].SOURCE;
        $scope.savedMode = propertyModel.Objects["_5451"].MODE;
        $scope.PropertyName = propertyService.GetInstallationObject(propertyModel.SelectedInstallation).PoolName;
        $scope.timeZones = [];
        $scope.AdminUsers = [];
        $scope.Address = propertyModel.SystemInfo[$scope.Model.SelectedInstallation].ADDRESS.valueOf();
        $scope.selectOwner = [];
        $scope.Owner = [];


        (function () {
            var zones = [];
            for (var i = 0; i < 25; i++) {
                var zone = "ZT";
                if (i < 12) {
                    zone += '-' + Math.abs(i - 12);
                } else if (i > 12) {
                    zone += '+' + (i - 12);
                }
                var newObject = {
                    ZONE: i+1,
                    VALUE: $filter("translate")(zone)
                }
                zones[i] = newObject;
            }
            $scope.timeZones = zones;

            var adminUserList = $filter('userAdmin')($scope.Model.Users);
            $scope.selectOwner = { name: $filter('translate')('SELECT'), id: 0 };
            $scope.Owner = $filter('selectedUser')(adminUserList, $scope.Model.SystemInfo[$scope.Model.SelectedInstallation].OWNER);
        }());

        $scope.ownerName = function (userId) {
            $scope.locationData.Owner = $filter('selectedUser')($scope.AdminUsers, userId).id;
        }

        $scope.locationData = {
            LOCX: "",
            LOCY: "",
            address: "",
            city: "",
            state: "",
            zip: "",
            owner: "",
        }

        $scope.timeData = {
            CLK24A: propertyModel.Objects["_C10C"].CLK24A,
            TIMZON: propertyModel.Objects["_C10C"].TIMZON,
            DLSTIM: propertyModel.Objects["_C10C"].DLSTIM == 'ON' ? true : false,
            OFFSET: "",
            DAY: "",
            TIME: "",
            SOURCE: propertyModel.Objects["_C105"].SOURCE,
            TimeZoneValue: Number(propertyModel.Objects["_C10C"].TIMZON) + 13,
            TimeZone: $scope.timeZones[Number(propertyModel.Objects["_C10C"].TIMZON) + 12].VALUE
        }

        $scope.PreferenceData = {
            MODE: propertyModel.Objects["_5451"].MODE
        }

        $scope.GoToConfigPage = function () {
            propertyService.ReleaseTimeUpdates();
            $location.path("/configure");
        }

        $scope.EditEnable = function () {
            return !($scope.EditLocation |
                $scope.EditTime |
                $scope.EditPreferences);
        }

        // Location Editor
        $scope.EditLocationMode = function () {
            $scope.locationData.LOCX = $scope.Model.Objects["_C10C"].LOCX.valueOf();
            $scope.locationData.LOCY = $scope.Model.Objects["_C10C"].LOCY.valueOf();
            $scope.locationData.city = $scope.Model.Objects["_C10C"].ViewModel.City.valueOf();
            $scope.locationData.state = $scope.Model.Objects["_C10C"].ViewModel.State.valueOf();
            $scope.locationData.zip = $scope.Model.Objects["_C10C"].ZIP.valueOf();
            $scope.locationData.address = $scope.Model.SystemInfo[$scope.Model.SelectedInstallation].ADDRESS.valueOf();
            $scope.AdminUsers = $filter('userAdmin')($scope.Model.Users);
            $scope.locationData.owner = $scope.Owner == null ? null : $scope.Owner.id;
            $scope.EditLocation = true;
        }


        $scope.ValidateLocationForm = function () {
            return $scope.validateLongitude() &&
                $scope.validateLatitude() &&
                $scope.validateZip() &&
                $scope.validateCity() &&
                $scope.validateAddress() &&
                $scope.validateState();
        }

        $scope.SaveLocationSettings = function () {
            var params = {
                LOCX: $scope.locationData.LOCX,
                LOCY: $scope.locationData.LOCY,
                SNAME: $scope.locationData.city + "," + $scope.locationData.state,
                ZIP: $scope.locationData.zip
            }
            propertyService.SetParams("_C10C", params);

            var info = {
                INSTALLATION: $scope.Model.SelectedInstallation,
                OWNER: $scope.locationData.owner,
                ADDRESS: $scope.locationData.address
            }
            propertyService.SetSystemInfo($scope.Model.SelectedInstallation, info);
            $scope.Owner = $filter('selectedUser')($scope.AdminUsers, $scope.locationData.owner);
            $scope.Address = $scope.locationData.address;
            $scope.EditLocation = false;
        }


        $scope.CancelLocationEdit = function () {
            $scope.EditLocation = false;
        }

        $scope.clearLongitude = function () {
            $scope.locationData.LOCX = "";
        }

        $scope.validateLongitude = function () {
            var result = false;
            if ($scope.locationData.LOCX) {
                result = validDecimal($scope.locationData.LOCX);
            }
            return result;
        }

        $scope.clearLatitude = function () {
            $scope.locationData.LOCY = "";
        }

        $scope.validateLatitude = function () {
            var result = false;
            if ($scope.locationData.LOCY) {
                result = validDecimal($scope.locationData.LOCY);
            }
            return result;
        }

        $scope.clearZip = function () {
            $scope.locationData.zip = "";
        }

        $scope.validateZip = function () {
            var result = false;
            if ($scope.locationData.zip) {
                if ($scope.locationData.zip.length == 5) {
                    result = validWholeNumber($scope.locationData.zip);
                }
            }
            return result;
        }

        $scope.clearOwner = function () {
            $scope.locationData.owner = "";
        }

        $scope.clearAddress = function () {
            $scope.locationData.address = "";
        }

        $scope.validateAddress = function () {
            if ($scope.locationData.address) {
                return ($scope.locationData.city.length > 0);
            }
            return false;
        }

        $scope.clearCity = function () {
            $scope.locationData.city = "";
        }

        $scope.validateCity = function () {
            if ($scope.locationData.city) {
                return ($scope.locationData.city.length > 0);
            }
            return false;
        }

        $scope.clearState = function () {
            $scope.locationData.state = "";
        }

        $scope.validateState = function () {
            if ($scope.locationData.state) {
                return ($scope.locationData.state.length > 0);
            }
            return false;
        }


        // Time settings Editor
        $scope.EditTimeMode = function () {
            $scope.timeData.CLK24A = $scope.Model.Objects["_C10C"].CLK24A.valueOf();
            $scope.timeData.SOURCE = $scope.Model.Objects["_C105"].SOURCE.valueOf();
            $scope.timeData.TIMZON = $scope.Model.Objects["_C10C"].TIMZON.valueOf();
            $scope.timeData.DAY = $scope.Model.Objects["_C10C"].DAY.valueOf();
            $scope.timeData.TIME = $scope.Model.Objects["_C10C"].CLK24A == "AMPM" ?
                convertToTwelveHour($scope.Model.Objects["_C10C"].MIN.valueOf()) :
                convertToTwentyFourHour($scope.Model.Objects["_C10C"].MIN.valueOf());
            $scope.timeData.DLSTIM = $scope.Model.Objects["_C10C"].DLSTIM.valueOf() === 'ON' ? true : false;
            $scope.EditTime = true;
            $scope.EditTimeComponents = $scope.Model.Objects["_C105"].SOURCE == 'LOCAL';
        }

        $scope.GetDropdown = function (mode) {
            return $filter('dropdownSelections')(mode);
        };


        $scope.validateTime = function () {
            if($scope.timeData.TIME) {
                return validTime($scope.timeData.TIME);
            }
            return false;
        }

        $scope.SaveTimeSettings = function () {
            var params = {
                CLK24A: $scope.timeData.CLK24A,
                TIMZON: $scope.timeData.TIMZON,
                DLSTIM: $scope.timeData.DLSTIM ? 'ON' : 'OFF',
                OFFSET: $scope.timeData.TIMZON,
                DAY: $scope.timeData.DAY,
                MIN: convertToTwentyFourHour($scope.timeData.TIME),
                TIME: convertToTwentyFourHour($scope.timeData.TIME)
            }

            propertyService.SetParams("_C10C", params);

            var clockparms = {
                SOURCE: $scope.timeData.SOURCE
            }

            propertyService.SetParams("_C105", clockparms);
            $scope.EditTime = false;
            $scope.EditTimeComponents = false;
        }


        $scope.CancelTimeSettings = function() {
            $scope.timeData.CLK24A = $scope.Model.Objects["_C10C"].CLK24A.valueOf();
            $scope.timeData.SOURCE = $scope.Model.Objects["_C105"].SOURCE.valueOf();
            $scope.timeData.TIMZON = $scope.Model.Objects["_C10C"].TIMZON.valueOf();
            $scope.timeData.DAY = $scope.Model.Objects["_C10C"].DAY.valueOf();
            $scope.timeData.TIME = $scope.Model.Objects["_C10C"].CLK24A == "AMPM" ?
                convertToTwelveHour($scope.Model.Objects["_C10C"].MIN.valueOf()) :
                convertToTwentyFourHour($scope.Model.Objects["_C10C"].MIN.valueOf());
            $scope.timeData.DLSTIM = $scope.Model.Objects["_C10C"].DLSTIM.valueOf() === 'ON' ? true : false;
            $scope.timeData.TimeZoneValue = Number($scope.Model.Objects["_C10C"].TIMZON) + 13;
            $scope.timeData.TimeZone = $scope.timeZones[Number($scope.Model.Objects["_C10C"].TIMZON) + 12].VALUE;
            $scope.EditTimeComponents = false;
            $scope.EditTime = false;
        }

        $scope.clockSourceSelect = function (value) {
            if($scope.EditTimeMode) {
                $scope.timeData.SOURCE = value;
                $scope.EditTimeComponents = value == "LOCAL";
            }
        }

        $scope.clockModeSelect = function (value) {
            if($scope.EditTimeMode) {
                $scope.timeData.CLK24A = value;
            }
        }

        $scope.timezoneSelected = function (value) {
            $scope.timeData.TIMZON = value - 13;
            $scope.timeData.TimeZoneValue = value;
            $scope.timeData.TimeZone = $scope.timeZones[Number(value)-1].VALUE;
        }

        $scope.clearDate = function () {
            $scope.timeData.DAY = "";
        }

        $scope.validateDate = function () {
            var result = false;
            if ($scope.timeData.DAY) {
                result = validDate($scope.timeData.DAY);
            }
            return result;
        }

        $scope.clearTime = function () {
            $scope.timeData.TIME = "";
        }

        $scope.validateTime = function () {
            var result = false;
            if ($scope.timeData.TIME) {
                result = validTime($scope.timeData.TIME);
            }
            return result;
        }

        $scope.validateTimeSettingsForm = function () {
            return $scope.timeData.CLK24A != "" &&
                $scope.timeData.MODE != "" &&
                $scope.validateTime() &&
                $scope.validateDate();
        }

        // Preferences
        $scope.EditPreferencesMode = function () {
            $scope.PreferenceData.MODE = $scope.Model.Objects["_5451"].MODE;
            $scope.EditPreferences = true;
            $scope.savedMode = $scope.PreferenceData.MODE;
        }


        $scope.SavePreferenceSettings = function () {
            $scope.EditPreferences = false;
            var params = {
                MODE: $scope.PreferenceData.MODE
            }
            propertyService.SetParams("_5451", params);
        }

        $scope.preferencesSelect = function (value) {
            if ($scope.EditTimeMode) {
                $scope.PreferenceData.MODE = value;
            }
        }


        $scope.CancelPreferencesEdit = function () {
            $scope.PreferenceData.MODE = $scope.savedMode;
            $scope.EditPreferences = false;
        }


        $scope.validatePreferencesForm = function () {
            return $scope.PreferenceData.MODE == "METRIC" ||
                $scope.PreferenceData.MODE == "ENGLISH";
        }

        $scope.$watch('Model.Objects["_C10C"].TIMZON', function(newVal) {
            if (newVal) {
                $scope.timeData.TimeZoneValue = Number(propertyModel.Objects["_C10C"].TIMZON) + 13;
                $scope.timeData.timeZones = [Number(propertyModel.Objects["_C10C"].TIMZON + 12)].VALUE;
            }
        });

        $scope.$watch('Model.Objects["_C10C"].SNAME', function (newVal) {
            if(newVal) {
                var locale = $filter('locationCityState')(newVal);
                propertyModel.Objects["_C10C"].ViewModel.City = locale.City;
                propertyModel.Objects["_C10C"].ViewModel.State = locale.State;
            }
        });

        // Local functions
        function validDecimal(value) {
            var decimal = /^[-+]?[0-9]+\.[0-9]+$/;
            return value.match(decimal) != null;
        }

        function validWholeNumber(value) {
            var wn = /^-?[0-9]+$/;
            return value.match(wn) != null;
        }

        function validDate(value) {
            var matches = value.match(/(\d{1,2})[- \/](\d{1,2})[- \/](\d{4})/);
            if (!matches) return false;

            // parse each piece and see if it makes a valid date object
            var month = parseInt(matches[1], 10);
            var day = parseInt(matches[2], 10);
            var year = parseInt(matches[3], 10);
            var date = new Date(year, month - 1, day);
            if (!date || !date.getTime()) return false;

            // make sure we have no funny rollovers that the date object sometimes accepts
            // month > 12, day > what's allowed for the month
            if (date.getMonth() + 1 != month ||
                date.getFullYear() != year ||
                date.getDate() != day) {
                return false;
            }
            return true;
        }

        function validTime(value) {
            var ts = /(1[012]|[1-9]):[0-5][0-9](\s)?(am|pm)?/;
            if ($scope.timeData.CLK24A == 'HR24') {
                ts = /([1-2][0123]|[1-9]):[0-5][0-9]/;
            }
            return value.match(ts) != null;
        }

        function convertToTwelveHour(value) {
            var ampm = 'am';
            var hours = Number(value.match(/^(\d+)/)[1]);
            var minutes = Number(value.match(/:(\d+)/)[1]);
            if (hours > 12) {
                hours = hours - 12;
                ampm = "pm";
            }
            var minuteString = minutes < 10 ? '0' + minutes.toString() : minutes.toString();
            return hours.toString() + ':' + minuteString + ' ' + ampm;
        }

        function convertToTwentyFourHour(value) {
            var hours = Number(value.match(/^(\d+)/)[1]);
            var minutes = Number(value.match(/:(\d+)/)[1]);
            if (value.match(/\s(.*)$/)) {
                var ampm = value.match(/\s(.*)$/)[1];
                if (ampm == "pm" && hours < 12) hours = hours + 12;
                if (ampm == "am" && hours > 12) hours = hours - 12;
                var minuteString = minutes < 10 ? '0' + minutes.toString : minutes.toString();
                return hours.toString() + ':' + minuteString;
            }
            return value;
        }
    }
]);