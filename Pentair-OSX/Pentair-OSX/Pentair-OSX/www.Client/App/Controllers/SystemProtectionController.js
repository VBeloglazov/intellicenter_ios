﻿'use strict';
var PWC = PWC || {};
PWC.controller('SystemProtectionController', [
    '$scope', '$location', 'PropertyService', 'PropertyModel',
    function ($scope, $location, propertyService, propertyModel) {
        $scope.ShowPassWrdBox = {
            show: false,
            password: ''
        }
        $scope.SecurityValue = propertyModel.SecurityEnabled ? 'ON' : 'OFF';
        $scope.Model = propertyModel;

        $scope.GoToConfigPage = function () {
            $location.path("/configure");
        }

        $scope.ToggleSecurity = function (value) {
            $scope.ShowPassWrdBox.show = value == 'OFF' ? false : true;
            $scope.ShowPassWrdBox.password = '';
            if (value == 'OFF' && propertyModel.SecurityEnabled)
                propertyService.SetValue('uFFFE', 'PASSWRD', '');
        }

        $scope.$watch('Model.SecurityEnabled', function (newValue, oldValue) {
            if (!$scope.ShowPassWrdBox.show) {
                $scope.SecurityValue = newValue ? 'ON' : 'OFF';
            }
        });

        $scope.SetSecurity = function () {
            propertyService.SetValue('uFFFE', 'PASSWRD', $scope.ShowPassWrdBox.password);
            $scope.ShowPassWrdBox.show = false;
        }
    }
]);