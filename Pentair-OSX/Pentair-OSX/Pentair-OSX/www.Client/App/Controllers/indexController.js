﻿'use strict';
var PWC = PWC || {};

PWC.controller('IndexController', ['$scope', '$location', 'AuthService', 'SocketService', 'PropertyService', 'PropertyModel',
    function ($scope, $location, authService, socketService, propertyService, propertyModel) {
        $scope.Model = propertyModel;
        $scope.Authentication = authService.authentication;
        $scope.DisplayAccountMenu = true;
                                   $scope.debugString = window.location.hostname;


        $scope.LogOut = function () {
            socketService.closeSocket();
            authService.logOut();
            propertyService.Reset(true);
            $scope.ToggleAccountMenu();
        }


        $scope.ToggleAccountMenu = function () {
            $scope.DisplayAccountMenu = !$scope.DisplayAccountMenu;
        }

        $scope.GoToChangePasswordPage = function () {
            $scope.ToggleAccountMenu();
            $location.path("/changePassword");
        }

        $scope.GoToRemovePoolPage = function () {
            $scope.ToggleAccountMenu();
            $location.path("/manageProperties");
        }

        $scope.GoToChangeEmailPage = function () {
            $scope.ToggleAccountMenu();
            $location.path("/changeEmail");
        }

        $scope.GoToChangeInstallationsGrid = function () {
            $location.path("/installationGrid");
        }

    }]);