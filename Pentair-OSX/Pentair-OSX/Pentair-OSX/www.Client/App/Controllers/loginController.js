﻿'use strict';
var PWC = PWC || {};

PWC.controller('LoginController', ['$scope', '$location', '$route', 'AuthService', 'AppService', 'PropertyService',
    function ($scope, $location, $route, authService, appService, propertyService) {

        $scope.AuthService = authService;

        $scope.message = "";
        $scope.validateOldPassword = false;

        $scope.submitUserName = false;
        $scope.changePasswordFlag = true;

        $scope.loginData = {
            userName: "",
            password: "",
            rememberMe: true
        };

        $scope.changePasswordData = {
            user: "",
            code: "",
            oldPassword: "",
            newPassword: "",
            confirmPassword: "",
            error: false
        }

        $scope.forgotPasswordData = {
            email: ""
        }

      
        if ($route.current && $route.current.locals && $route.current.locals.resetModel) {
            $scope.changePasswordData.code = $route.current.locals.resetModel.Code;
            $scope.changePasswordData.user = $route.current.locals.resetModel.User;
            $scope.changePasswordFlag = false;
        }

        $scope.validateNewPassword = function () {
            var re = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*)[\\w]{6,}$");
            var result = false;
            if ($scope.changePasswordData.newPassword) {
                result = $scope.changePasswordData.newPassword.match(re) ? true : false;
            }
            return result;
        }


        $scope.clearOldPassword = function () {
            $scope.changePasswordData.oldPassword = "";
            $scope.validateOldPassword = false;
        }

        $scope.verifyConfirmationPassword = function () {
            return !$scope.ChangePasswordForm.confirmPassword.$invalid &&
                $scope.validateNewPassword() &&
                $scope.changePasswordData.newPassword == $scope.changePasswordData.confirmPassword;
        }


        $scope.verifyPassword = function () {
            var data = {
                oldPassword: $scope.changePasswordData.oldPassword
            }
            appService.PostData("Api/Account/ValidatePassword", data).then(function () {
                $scope.validateOldPassword = true;
            }).catch(function (err) {
                $scope.validateOldPassword = false;
            });
        }

        $scope.validateForm = function () {
            if ($scope.changePasswordFlag == true) {
                return $scope.validateNewPassword() &&
                    $scope.validateOldPassword &&
                    $scope.verifyConfirmationPassword();
            } else {
                return $scope.validateNewPassword() &&
                    $scope.verifyConfirmationPassword();
            }
        };

        $scope.login = function () {
            authService.login($scope.loginData).then(function () {
                $location.path('/installations');
            },
                function (err) {
                    $scope.message = err.error_description;
                    $scope.loginData.password = "";
                });
            //clear url params
            $location.search({});
        };


        $scope.ChangePassword = function () {
            if ($scope.changePasswordFlag) {
                var changePassword = {
                    oldPassword: $scope.changePasswordData.oldPassword,
                    newPassword: $scope.changePasswordData.newPassword,
                    confirmPassword: $scope.changePasswordData.confirmPassword
                }
                appService.PostData("Api/Account/ChangePassword", changePassword).then(function () {
                    propertyService.Reset();
                    authService.authentication.passwordChanged = true;
                    authService.authentication.lastUserName = authService.authentication.userName;
                    authService.logOut();
                }).catch(function () {
                    $scope.changePasswordData.error = true;
                }
                );
            } else {
                var resetPassword = {
                    email: $scope.changePasswordData.user,
                    password: $scope.changePasswordData.newPassword,
                    confirmPassword: $scope.changePasswordData.confirmPassword,
                    code: $scope.changePasswordData.code
                }
                appService.PostData("Api/Account/ResetPassword", resetPassword).then(function () {
                    propertyService.Reset();
                    authService.authentication.passwordChanged = true;
                    authService.authentication.lastUserName = $scope.changePasswordData.user;
                    authService.logOut();
                }).catch(function () {
                    $scope.changePasswordData.error = true;
                }
                );
            }
        };
        $scope.forgotPassword = function () {
            $location.path('/passwordAssistance');
            $scope.PasswordAssistanceFrom.$setPristine();
        };


        $scope.passwordAssistance = function () {
            appService.PostData("Api/Account/ForgotPassword", $scope.forgotPasswordData).then(function () {
                $scope.submitUserName = true;
                $scope.changePasswordData.error = false;
            }).catch(function () {
                $scope.forgotPasswordData.email = '';
                $scope.PasswordAssistanceFrom.$setPristine();
                $scope.changePasswordData.error = true;
            });
        };

        if (authService.authentication.lastUserName) {
            $scope.loginData.userName = authService.authentication.lastUserName;
        }
    }
]);