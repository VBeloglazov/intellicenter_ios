﻿'use strict';
var PWC = PWC || {};
PWC.controller('ManageUsersController', [
    '$scope', '$location', 'PropertyService', 'AuthService', 'PropertyModel', 'uiGridConstants',
    function ($scope, $location, propertyService, authService, propertyModel, uiGridConstants) {
        var sortInfo = null;
        var currentPage = 0;
        var pageSize = 20;

        propertyService.DeleteUserError = false;
        propertyService.GetSecurityTokens();
        propertyService.EditSession = false;
        $scope.SortDirection = "asc";
        $scope.SortField = "email";
        $scope.Filter = "all";
        $scope.UserList = [];
        $scope.Model = propertyModel;
        $scope.Property = propertyService;
        $scope.Authentication = authService;
        $scope.Security = propertyService.Security;
        $scope.Property = propertyService;
        $scope.EditRow = null;
        $scope.DeleteRow = null;
        $scope.Notify = false;
        $scope.EditFilters = false;
        $scope.SecurityObject = "";

     
        $scope.IgnoreEdit = false;

        var navigateTo = "";

        fetchUsers();
        $scope.GoToDashboard = function () {
            if ($scope.Property.EditSession) {
                $scope.IgnoreEdit = true;
                navigateTo = "/property";
            } else {
                $location.path("/property");
            }
        }

        $scope.GridViewModel = {
            UserIsLogin: function (user) {
                if (user) {
                    if (user.name === $scope.Authentication.authentication.userName) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            },
            EditUser: function (user) {
                if ($scope.Model.EditSession === false) {
                    for (var i = 0; i < $scope.Model.Security.length; i++) {
                        if ($scope.Model.Security[i].name === user.level) {
                            $scope.SecurityObject = $scope.Model.Security[i].value;
                        }
                    }
                    $scope.EditRow = user;
                    $scope.Model.EditSession = true;
                }
            },
            EditSave: function (user) {
                $scope.EditRow = null;
                $scope.Model.EditSession = false;
                $scope.Property.EditUser(user.email, $scope.SecurityObject).then(function() {
                     fetchUsers(currentPage);
                });
            },
            CancelEdit: function () {
                $scope.EditRow = null;
                $scope.Model.EditSession = false;
            },
            RemoveUser: function (user, notify) {
                $scope.DeleteRow = null;
                $scope.EditRow = null;
                $scope.Model.EditSession = false;
                $scope.Notify = false;
                $scope.Property.DeleteUser(user.email, user.status, notify).then(function() {
                     fetchUsers(currentPage);
                });
            },
            DeleteButton: function (user) {
                $scope.DeleteRow = user;
            },
            CancelDeleteButton: function () {
                $scope.DeleteRow = null;
                $scope.Notify = false;
            },
            NotifyButton: function () {
                $scope.Notify = true;
            },
            Edit: function (user) {
                if ($scope.EditRow) {
                    return user.email === $scope.EditRow.email;
                }
                return false;
            },
            Delete: function (user) {
                if (user && $scope.DeleteRow) {
                    return user.email === $scope.DeleteRow.email && !$scope.Notify;
                }
                return false;
            },
            DeleteNotify: function (user) {
                if (user && $scope.DeleteRow) {
                    return user.email === $scope.DeleteRow.email && $scope.Notify;
                }
                return false;
            },
            SecurityObject: function () {
                return $scope.SecurityObject;
            },
            SecurityReference: function (value) {
                if (value) {
                    $scope.SecurityObject = value;
                }
            },
            SecurityModel: function () {
                return $scope.Model.Security;
            }
        };


        $scope.GoToAddUsersPage = function () {
            if ($scope.Property.EditSession) {
                $scope.IgnoreEdit = true;
                navigateTo = "/inviteUser";
            } else {
                $location.path("/inviteUser");
            }
        }


        $scope.ContinueNavigate = function () {
            $location.path(navigateTo);
        }


        $scope.gridOptions = {
            useExternalSorting: true,
            enableHorizontalScrollbar: false,
            enableVerticalScrollbar: false,
            pagingPageSizes: [20, 50, 75],
            pagingPageSize: pageSize,
            useExternalPaging: true,
            enableColumnMenus: false,
            rowHeight: 30,
            columnDefs: [
                {
                    name: "Email", field: "email", cellTemplate: '<div style="white-space: nowrap; text-overflow: ellipsis; width: 240px"><img src="Content/img/blueDot.png" ng-show=row.entity.status==="ACCEPTED">' +
                         '<img src="Content/img/redDot.png" ng-show=row.entity.status==="BOUNCED">' +
                         '<img src="Content/img/yellowDot.png" ng-show=row.entity.status==="EXPIRED">' +
                         '<img src="Content/img/grayDot.png" ng-show=row.entity.status==="UNKNOWN">&nbsp;{{COL_FIELD}}</div>'
                },
                { name: "Name", field: "name", cellTemplate: '<div style="white-space: nowrap; text-overflow: ellipsis; width: 240px">{{COL_FIELD}}</div>' },
                {
                    name: "Level", field: "level", cellTemplate: '<div ng-hide="getExternalScopes().Edit(row.entity)">{{COL_FIELD}}</div>' +
                        '<div id="SecurityLevel" style="width: 180px;" ng-show="getExternalScopes().Edit(row.entity)" default-value="getExternalScopes().SecurityObject()" dropdown-menu="getExternalScopes().SecurityModel()" dropdown-model="getExternalScopes().SecurityObject()" dropdown-onchange="getExternalScopes().SecurityReference(selection)" value-field="value" label-field="name"></div>'
                },
                {
                    name: "Status", field: "status", cellTemplate: '<div><div id="ConfirmDeleteModal" class="modal" ng-show="getExternalScopes().Delete(row.entity)">' +
                        '<div class="modal-dialog-sm"><div class="modal-content-sm"><div class="modal-header-sm"><button id="ConfirmXBtn" style="width: 22px; float:right;" ng-click="getExternalScopes().DeleteButton(row.entity)">x</button></div>' +
                        '<div class="modal-body"><h2>{{"DELETECONFIRM" | translate}}</h2></div><div class="modal-footer">' +
                        '<button id="DelCancelBtn" style="float:left" ng-click="getExternalScopes().CancelDeleteButton()">{{"CANCEL" | translate}}</button>' +
                        '<button id="DelConfirmBtn" style="float:right" ng-click="getExternalScopes().NotifyButton()">{{"DELETE" | translate}}</button></div></div></div></div>' +
                        '<div id="DelNotifyModal" class="modal" ng-show="getExternalScopes().DeleteNotify(row.entity)"><div class="modal-dialog-sm"><div class="modal-content-sm">' +
                        '<div class="modal-header-sm"><button id=DelNotifyXBtn" style="width: 22px; float:right;" ng-click="getExternalScopes().RemoveUser(row.entity, false)">x</button></div>' +
                        '<div class="modal-body"><div style="padding-bottom:1em;"><h2>{{"NOTIFYUSER" | translate}}</h2></div><span>{{"DELETEUSERSENDEMAIL" | translate}}</span></div>' +
                        '<div class="modal-footer"><button id="DelNotifyBtn" style="float:left" ng-click="getExternalScopes().RemoveUser(row.entity, true)">{{"NOTIFY" | translate}}</button>' +
                        '<button id="DelDontNotifyBtn" style="float:right" ng-click="getExternalScopes().RemoveUser(row.entity, false)">{{"DONOTNOTIFY" | translate}}</button></div></div></div></div>{{COL_FIELD}}</div>'
                },
                {
                    name: "Actions", field: "email", cellTemplate: '<div external-scopes="$scope" ng-hide="getExternalScopes().UserIsLogin(row.entity)">' +
                      '<a id="editBtn" type="button" class="button-link" ng-click="getExternalScopes().EditSave(row.entity)" ng-show="getExternalScopes().Edit(row.entity)">{{"SAVE" | translate}}</a>' +
                      '<a id="editBtn" type="button" class="button-link" ng-click="getExternalScopes().EditUser(row.entity)" ng-hide="getExternalScopes().Edit(row.entity)">{{"EDIT" | translate}}</a> | <a id="deleteBtn" type="button" class="button-link" ng-click="getExternalScopes().DeleteButton(row.entity)" ng-hide="getExternalScopes().Edit(row.entity)">{{"DELETE" | translate}}</a>' +
                      '<a id="editBtn" type="button" class="button-link" ng-click="getExternalScopes().CancelEdit(row.entity)" ng-show="getExternalScopes().Edit(row.entity)">{{"CANCEL" | translate}}</a>' +
                      '</div>', enableSorting: false
                }
            ],
            onRegisterApi: function (gridApi) {
                $scope.gridApi = gridApi;
                $scope.gridApi.core.on.sortChanged($scope, function (grid, sortColumns) {
                    sortInfo = sortColumns[0];
                    fetchUsers();
                });
                $scope.gridApi.paging.on.pagingChanged($scope, function (newPage) {
                   fetchUsers(newPage);
                });
            }
        };

        $scope.ShowFilters = function () {
            $scope.EditFilters = true;
            event.stopPropagation();
        }

        $scope.Search = function () {
            $scope.FilterUsers();
        }

        $scope.ClearSearchTerm = function () {
            $scope.SearchTerm = "";
            $scope.FilterUsers();
        }

        $scope.SelectFilter = function (filter) {
            $scope.Filter = filter;
            $scope.FilterUsers();
            $scope.EditFilters = false;
        }

        $scope.FilterUsers = function () {
            fetchUsers().then(function (result) {
                $scope.gridOptions.pagingCurrentPage = 0;
            });
        }

        function fetchUsers(page) {
            currentPage = page || 1;
            if (sortInfo) {
               return propertyService.GetUserList(pageSize, currentPage, $scope.Filter, $scope.SearchTerm, sortInfo.field, sortInfo.sort.direction).then(function () {
                   $scope.UserList = [];
                   $scope.gridOptions.totalItems = propertyModel.ListCount;
                       angular.forEach(propertyModel.Users, function (user) {
                        $scope.UserList.push(user);
                    });
                    $scope.gridOptions.data = $scope.UserList;
                });
            } else {
               return propertyService.GetUserList(pageSize, currentPage, $scope.Filter, $scope.SearchTerm).then(function () {
                   $scope.UserList = [];
                   $scope.gridOptions.totalItems = propertyModel.ListCount;
                      angular.forEach(propertyModel.Users, function (user) {
                        $scope.UserList.push(user);
                    });
                    $scope.gridOptions.data = $scope.UserList;
                });
            }
        }

    }
]);