﻿var PWC = PWC || {};
PWC.directive('connectorDropdownMenu', [function () {
    return {
        restrict: 'A',
        replace: true,
        scope: {
            connectorDropdownMenu: '=',
            connectorDropdownModel: '=',
            connectorDropdownOnchange: '&',
            defaultValue: '=',
            disabled: '='
        },
        controller: [
            '$scope', '$element', '$attrs', '$window', '$location', function (scope, $element, $attrs, $window, $location) {
                var bodyEl = angular.element($window.document);

                scope.labelField = $attrs.labelField || 'name';
                scope.valueField = $attrs.valueField || 'value';
                scope.limitTo = $attrs.limitTo || 20;
                scope.showAlternativeSelector = $attrs.showAlternativeSelector || false;
                scope.selected = scope.defaultValue;

                //Update the UI value if model changes
                scope.$watch('connectorDropdownModel + connectorDropdownMenu', function () {
                    //if more than 7 Connections show Alternative Selector
                    if (scope.connectorDropdownMenu.length > scope.limitTo) {
                        scope.showAlternativeSelector = true;
                    }
                    for (var i = 0; i < scope.connectorDropdownMenu.length; i++) {
                        switch (scope.connectorDropdownModel || '0') {
                            case scope.connectorDropdownMenu[i][scope.valueField]:
                                scope.selected = scope.connectorDropdownMenu[i];
                                break;
                            case '0':
                                scope.selected = scope.defaultValue;
                                break;
                        }
                    }
                });

                scope.GoToChangeConnectionsGrid = function () {
                    $location.path("/connectionGrid");
                }
                this.select = function (selected) {
                    scope.connectorDropdownModel = selected[scope.valueField];
                    if (scope.connectorDropdownOnchange)
                        scope.connectorDropdownOnchange({ selection: scope.connectorDropdownModel });
                };


                $element.bind('click', elemClick);


                function elemClick(event) {
                    if (event)
                        event.stopPropagation();

                    if (!scope.disabled)
                        $element.toggleClass('active');

                    if ($element.hasClass('active')) {
                        bodyEl.on('click', onBodyClick);
                    } else {
                        bodyEl.off('click', onBodyClick);
                    }
                }


                function onBodyClick(evt) {
                    if (evt.target === $element[0]) return;
                    return evt.target !== $element[0] && elemClick();
                }
            }
        ],
        template: "<div class='wrap-dd-select'>  <span class='selected'>{{selected[labelField]}}</span>   <ul class='dropdown'>  <li ng-if='showAlternativeSelector'><a ng-click='GoToChangeConnectionsGrid()'  href=''>Search/sort pool systems </a></li>   <li ng-repeat='item in connectorDropdownMenu |limitTo:limitTo' class='dropdown-item' connector-dropdown-menu-item='item'  dropdown-item-label='labelField'>       </li>    </ul></div>"
    };
}
]).directive('connectorDropdownMenuItem', [
    function () {
        return {
            require: '^connectorDropdownMenu',
            replace: true,
            scope: {
                connectorDropdownItemLabel: '=',
                connectorDropdownMenuItem: '=',
            },
            link: function (scope, element, attrs, dropdownMenuCtrl) {
                scope.selectItem = function () {
                    dropdownMenuCtrl.select(scope.connectorDropdownMenuItem);
                };

                scope.dropdownItemClass = scope.connectorDropdownMenuItem.OnLine == false ? 'notConnected' : 'connected';
            },
            template: "<li ng-class='{divider: connectorDropdownMenuItem.divider}'>\n    <a href='' class='connector-dropdown-item {{dropdownItemClass}}'\n        ng-if='!connectorDropdownMenuItem.divider'\n        ng-href='{{connectorDropdownMenuItem.href}}'\n        ng-click='selectItem()'>\n        {{connectorDropdownMenuItem[dropdownItemLabel]}}\n    </a>\n</li>"
        };
    }
]);