'use strict';
var PWC = PWC || {};
PWC.directive('heaterDropdown', ['$filter', function ($filter) {
    return {
        restrict: 'A',
        replace: true,
        scope: {
            bodyObject: '=',
            dropdownOnchange: '&',
            disabled: '='
        },
        controller: [
            '$scope', '$element', '$attrs', '$window', function (scope, $element, $attrs, $window) {
                var bodyEl = angular.element($window.document);

                if (!$attrs.disabled)
                    $attrs.disabled = false;

                scope.labelField = $attrs.labelField || 'name';
                scope.valueField = $attrs.valueField || 'value';
                scope.selected = {};
                scope.heatModes = $filter('heatModes')(scope.bodyObject);

                //Update the UI value if body.HEATERS changes
                scope.$watch('bodyObject', function () {
                    if (!PWC.isEmptyObject(scope.bodyObject))
                        scope.selected = $filter('currentHeatMode')(scope.bodyObject, scope.valueField, scope.heatModes);
                }, true);

                this.select = function (selected) {
                    if (selected[scope.valueField] !== scope.bodyObject) {
                        var params = {
                            body: scope.bodyObject.OBJNAM,
                            heatMode: selected[scope.valueField]
                        };

                        scope.dropdownOnchange(params);
                    }
                };

                $element.bind('click', elemClick);


                function elemClick(event) {
                    if (event)
                        event.stopPropagation();

                    if (!scope.disabled)
                        $element.toggleClass('active');

                    if ($element.hasClass('active')) {
                        bodyEl.on('click', onBodyClick);
                    } else {
                        bodyEl.off('click', onBodyClick);
                    }
                }


                function onBodyClick(evt) {
                    if (evt.target === $element[0]) return;
                    return evt.target !== $element[0] && elemClick();
                }
            }
        ],
        template: "<div class='wrap-dd-select'>\n    <span class='selected truncate'>{{selected[labelField]}}</span>\n    <ul class='dropdown'>\n        <li ng-repeat='item in heatModes'\n            class='dropdown-item'\n            heater-dropdown-menu-item='item'\n            heater-dropdown-item-label='labelField'>\n        </li>\n    </ul>\n</div>"
    };
}]).directive('heaterDropdownMenuItem', [
    function () {
        return {
            require: '^heaterDropdown',
            replace: true,
            scope: {
                heaterDropdownItemLabel: '=',
                heaterDropdownMenuItem: '=',
            },
            link: function (scope, element, attrs, dropdownMenuCtrl) {
                scope.selectItem = function () {
                    dropdownMenuCtrl.select(scope.heaterDropdownMenuItem);
                };
            },
            template: "<li ng-class='{divider: heaterDropdownMenuItem.divider}'>\n    <a href='' class='dropdown-item'\n        ng-if='!heaterDropdownMenuItem.divider'\n        ng-href='{{heaterDropdownMenuItem.href}}'\n        ng-click='selectItem()'>\n        {{heaterDropdownMenuItem[heaterDropdownItemLabel]}}\n    </a>\n</li>"
        };
    }
]);
