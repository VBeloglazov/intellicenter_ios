﻿var PWC = PWC || {};
PWC.directive('ngNumbersOnly', function () {
    return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {

            modelCtrl.$parsers.push(function (inputValue) {
                if (!inputValue) return '';
                var transformedInput = inputValue;

                //ignore String values
                if (typeof transformedInput == "string")
                    transformedInput = inputValue.replace(/[^0-9]/g, '');

                //update displayValue
                if (transformedInput != inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                    modelCtrl.temp = transformedInput;
                }
                return transformedInput;
            });
        }
    };
});
