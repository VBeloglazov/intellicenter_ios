'use strict';
var PWC = PWC || {};


PWC.directive('toggleCircuit', function () {
    return {
        restrict: 'EA',
        replace: true,
        scope: {
            targetCircuit: '=',
            disabled: '@',
            toggleOnclick: '&'
        },
        template: '<div><div ng-show="useSwitch" class="switch-animate {{value}}"></div><span ng-hide="useSwitch" ng-class="value.toLowerCase()">{{value}}</span></div>',
        link: function (scope, element, attrs) {
            if (PWC.isEmptyObject(scope.targetCircuit) || scope.targetCircuit.STATUS === undefined)
                return;

            if (!attrs.disabled) {
                attrs.disabled = false;
            }

            validateSwitchValue();
            scope.value = getClassName();


            element.on('click', function () {
                if (!scope.disabled) {
                    var newValue = getValue();
                    var params = { obj: scope.targetCircuit, value: newValue };

                    scope.toggleOnclick(params);
                }
            });

            //Update UI Value when model Changes
            scope.$watch('targetCircuit.STATUS', function (newVal, oldVal) {
                validateSwitchValue();
                if (newVal && newVal !== oldVal)
                    scope.value = getClassName(newVal);
            });


            function getClassName(status) {
                if (scope.useSwitch)
                    return (scope.targetCircuit.STATUS === 'ON' || scope.targetCircuit.STATUS === 'DLYOFF') ? "switch-on" : "switch-off";

                return scope.targetCircuit.STATUS;
            }

            function getValue() {
                if (scope.useSwitch)
                    return scope.value === "switch-off" ? 'ON' : 'OFF';

                return scope.value || 'ON';
            }

            function validateSwitchValue() {
                scope.useSwitch = (scope.targetCircuit.STATUS === 'ON' || scope.targetCircuit.STATUS === 'OFF' ||
                               scope.targetCircuit.STATUS === 'DLYON' || scope.targetCircuit.STATUS === 'DLYOFF');
            }
        }
    };
});

