﻿var PWC = PWC || {};
PWC.directive('whenscrollends', function () {
    return {
        restrict: "A",
        transclude: true,
        scope: {
            whenscrollends: '&'
        },
        template: '<div ng-transclude></div>',
        link: function (scope, element, attrs) {
            var container = element[0];
            element.bind('scroll', function () {
                if (container.scrollTop + container.offsetHeight >= container.scrollHeight) {
                    scope.$apply(doAction());
                }
            });

            function doAction() {
                if (attrs.whenscrollends) {
                    scope.whenscrollends();
                }
            }
        }
    };
});