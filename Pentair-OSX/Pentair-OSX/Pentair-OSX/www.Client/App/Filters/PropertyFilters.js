﻿var PWC = PWC || {};


PWC.filter('circuitsByName', [function () {
    return function (circuits, objnam) {
        if (!angular.isUndefined(circuits) && !angular.isUndefined(objnam)) {
            var filteredCircuits = [];
            angular.forEach(circuits, function (circuit) {
                if (angular.isDefined(objnam) && circuit.OBJNAM == objnam) {
                    filteredCircuits.push(circuit);
                }
            });
            return filteredCircuits;
        } else {
            return circuits;
        }
    };
}]);


PWC.filter('moduleByParent', [
    function () {
        return function (modules, parentName) {
            if (!angular.isUndefined(modules) && !angular.isUndefined(parentName)) {
                var filteredModules = [];
                angular.forEach(modules, function (module) {
                    if (angular.isDefined(parentName) && module.PARENT == parentName) {
                        filteredModules.push(module);
                    }
                });
                return filteredModules;
            } else {
                return modules;
            }
        };
    }
]);


PWC.filter('circuitsBySection', [function () {
    return function (circuits, section) {
        if (!angular.isUndefined(circuits) && !angular.isUndefined(section)) {
            var filteredCircuits = [];
            angular.forEach(circuits, function (circuit) {
                if (circuit.SHOMNU && circuit.SHOMNU.toLowerCase().indexOf(section) > -1 && circuit.SUBTYP !== 'BODY')
                    filteredCircuits.push(circuit);
            });
            return filteredCircuits;
        } else {
            return circuits;
        }
    };

}]);


PWC.filter('programmableCircuits', [function () {
    return function (circuits) {
        if (!PWC.isEmptyObject(circuits)) {
            var filteredCircuits = [];
            angular.forEach(circuits, function (circuit) {
                if (circuit.OBJTYP === 'BODY' || (circuit.SUBTYP !== 'BODY' && circuit.SHOMNU && circuit.SHOMNU.toLowerCase().indexOf('e') > -1))
                    filteredCircuits.push(circuit);
            });
            return filteredCircuits;
        } else {
            return circuits;
        }
    }
}]);


PWC.filter('circuitsBySectionAndSubType', [function () {
    return function (circuits, section, subTypes) {
        if (circuits && section) {
            var filteredCircuits = [];
            angular.forEach(circuits, function (circuit) {

                if (circuit.SHOMNU && circuit.SHOMNU.toLowerCase().indexOf(section) > -1 && subTypes.indexOf(circuit.SUBTYP) !== -1)
                    filteredCircuits.push(circuit);
            });
            return filteredCircuits;
        } else {
            return circuits;
        }
    };
}]);


PWC.filter('translateTemperature', ["PropertyModel", function (PropertyModel) {
    return function (temp, UoM) {
        var result = {
            NUMBER: NaN,
            TEMPERATURE: NaN,
            UNIT: 'C',
            ALL: ''
        }

        var preference = PropertyModel.Objects["_5451"];

        UoM = UoM || ((preference) ? preference.MODE : "METRIC");

        if (temp) {
            var numeric = Number(temp) || Number(temp.replace(/\D/g, ''));

            if (UoM.toUpperCase() == "ENGLISH") {
                result.NUMBER = Math.round((numeric * 9) / 5 + 32);
                result.TEMPERATURE = result.NUMBER + '°';
                result.UNIT = 'F';
            } else {
                result.NUMBER = Math.round(numeric);
                result.TEMPERATURE = result.NUMBER + '°';
            }
        }

        if (result.TEMPERATURE && result.UNIT)
            result.ALL = result.TEMPERATURE + result.UNIT;

        return result;
    };
}]);


PWC.filter('connectionStatus', [function () {
    return function (code) {
        switch (code) {
            case 0:
                return "Connecting";
            case 1:
                return "Connected";
            case 2:
                return "Closing";
            case 3:
                return "Closed";
            default:
                return "";
        }
    };
}]);

PWC.filter('scheduleDays', [function () {
    return function (days) {
        if (days) {
            days = days.replace('1', '');
            if (angular.equals(days.split('').sort(), ["A", "F", "M", "R", "T", "U", "W"]))
                return "ALL";
            var ordered = "";
            if (days.indexOf('M') > -1) { ordered = ordered + 'M'; }
            if (days.indexOf('T') > -1) { ordered = ordered + 'T'; }
            if (days.indexOf('W') > -1) { ordered = ordered + 'W'; }
            if (days.indexOf('R') > -1) { ordered = ordered + 'R'; }
            if (days.indexOf('F') > -1) { ordered = ordered + 'F'; }
            if (days.indexOf('A') > -1) { ordered = ordered + 'A'; }
            if (days.indexOf('U') > -1) { ordered = ordered + 'U'; }
            ordered = ordered.split('').join(" ").toString();
            ordered = ordered.replace('A', 'Sa');
            ordered = ordered.replace('U', 'Su');
            ordered = ordered.replace('R', 'Th');
            return ordered;
        }
        return "";
    }
}]);

PWC.filter('scheduleType', [function () {
    return function (days) {
        var value = '';
        if (days) {
            switch (days) {
                case 'ON':
                    value = 'One Time Only';
                    break;
                case 'OFF':
                    value = 'Weekly';
                    break;
            }
        }
        return value;
    }
}]);


PWC.filter('translateTime', ['PropertyModel', function (PropertyModel) {
    return function (time) {
        if (time) {
            var system = PropertyModel.Objects["_C10C"];
            var mode = ((system) ? system.CLK24A : "AMPM");

            if (mode == "AMPM") {
                time = time.replace(':', '');
                var hours24 = parseInt(time.substring(0, 2));
                var hours = ((hours24 + 11) % 12) + 1;
                var amPm = hours24 > 11 ? 'pm' : 'am';
                var minutes = time.substring(2);

                return hours + ':' + minutes + amPm;
            } else {
                return time;
            }
        }
        return '--';
    }
}]);

PWC.filter('locationCityState', [function () {
    return function (locale) {
        var result = { City: '', State: '' };
        if (locale) {
            var parms = locale.split(',');
            result.City = parms[0] || '';
            result.State = parms[1] || '';
        }
        return result;
    }
}]);

PWC.filter('userAdmin', [function() {
    return function(users) {
        if (!PWC.isEmptyObject(users)) {
            var filteredUsers = [];
            angular.forEach(users, function(user) {
                if (user.level == 'ADMIN') {
                    filteredUsers.push(user);
                }
            });
            return filteredUsers;
        } else {
            return {};
        }
    }
}]);


PWC.filter('selectedUser', [function() {
    return function(users, userId) {
        if (!PWC.isEmptyObject(users)) {
            var selectedUser = null;
            for (var i = 0; i < users.length; i++) {
                var item = users[i];
                if (item.id == userId) {
                    selectedUser = item;
                }
            };
            return selectedUser;
        } else {
            return null;
        }
    }
}]);


PWC.filter('dropdownSelections', [
    'Enumerations', function(enumerations) {
    return function(list) {
        if (list) {
            return enumerations.CLOCK_LISTS[list];
        } else {
            
        }
    }
}]);


PWC.filter('heatModes', ['Enumerations', function (enumerations) {
    return function (body) {
        if (!PWC.isEmptyObject(body)) {
            if (!PWC.isEmptyObject(body.HEATERS.SECONDARY.HEATER)) {
                return enumerations.HEAT_MODES[body.HEATERS.SECONDARY.HEATER.SUBTYP];
            } else {
                return enumerations.HEAT_MODES.GENERIC;
            }
        }
        return [];
    }
}]);


PWC.filter('currentHeatMode', ['$filter', function ($filter) {
    return function (body, valueField, heatModes) {
        heatModes = heatModes || $filter('heatModes')(body);
        var status = "";
        if (!PWC.isEmptyObject(body) && heatModes) {

            switch (body.HEATER) {
                case body.HEATERS.GENERIC.HEATER.OBJNAM:
                    status = "GENERIC";
                    break;
                case body.HEATERS.SECONDARY.HEATER.OBJNAM:
                    status = "ONLY";
                    break;
                case body.HEATERS.VIRTUAL.OBJNAM:
                    if (1 == body.HEATERS.SECONDARY.HCOMBO.LISTORD && 2 == body.HEATERS.GENERIC.HCOMBO.LISTORD)
                        status = "PREF";
                    break;
                case "00000":
                case "000FF":
                    status = 'NONE';
                    break;
            }

            for (var i = 0; i < heatModes.length; i++) {
                if (status === heatModes[i][valueField || 'value'])
                    return heatModes[i];
            }
        }
        return null;
    }
}]);


PWC.filter('modeToHeater', [function () {
    return function (body, heatMode) {
        var heater = "";
        if (!PWC.isEmptyObject(body) && heatMode) {
            switch (heatMode) {
                case "NONE":
                    heater = '000FF';
                    break;
                case "GENERIC":
                    heater = body.HEATERS.GENERIC.HEATER.OBJNAM;
                    break;
                case "PREF":
                    heater = body.HEATERS.VIRTUAL.OBJNAM;
                    break;
                case "ONLY":
                    heater = body.HEATERS.SECONDARY.HEATER.OBJNAM;
                    break;
            }
        }
        return heater;

    }
}]);




PWC.filter('intToWord', [function () {
    return function (numericValue) {
        var units = new Array("Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen");
        var tens = new Array("Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety");
        var theword = "";
        var started;
        if (numericValue > 999) return "Lots";
        if (numericValue == 0) return units[0];
        for (var i = 9; i >= 1; i--) {
            if (numericValue >= i * 100) {
                theword += units[i];
                started = 1;
                theword += " hundred";
                if (numericValue != i * 100) theword += " and ";
                numericValue -= i * 100;
                i = 0;
            }
        };

        for (var i = 9; i >= 2; i--) {
            if (numericValue >= i * 10) {
                theword += (started ? tens[i - 2].toLowerCase() : tens[i - 2]);
                started = 1;
                if (numericValue != i * 10) theword += "-";
                numericValue -= i * 10;
                i = 0;
            }
        };

        for (var i = 1; i < 20; i++) {
            if (numericValue == i) {
                theword += (started ? units[i].toLowerCase() : units[i]);
            }
        };
        return theword;
    }
}]);