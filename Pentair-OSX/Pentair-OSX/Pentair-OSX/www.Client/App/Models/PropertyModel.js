﻿'use strict';
var PWC = PWC || {};

PWC.factory('PropertyModel', ['Enumerations', function (enumerations) {
    var property = {
        Objects: {},
        Panels: [],
        Modules: [],
        Relays: [],
        Bodies: [],
        CircuitGroups: [],
        Circuits: [],
        Heaters: [],
        Valves: [],
        Pumps: [],
        HeaterCombos: [],
        AirSensors: [],
        Singleton: {},
        ACL: {},
        Users: {},
        UserNotifications: {},
        SecurityEnabled: true,
        subscriptionMessage: {
            command: "RequestParamList",
            objectList: []
        },
        SystemInfo: {},
        Schedules: {},
        Security: [],
        Installations: [],
        ConnectionStatus: 'Not Connected',
        Ready: false,
        SelectedInstallation: enumerations.DefaultInstallation.InstallationId,
        DefaultInstallation: enumerations.DefaultInstallation,
        Busy: false,
        InviteStatus: {},
        EditSession: false,
        DeleteUserError: false,
        ListCount: 0
    };

    property.Clear = function (resetSelectedInstallation) {
        property.Panels = [];
        property.Modules = [];
        property.Relays = [];
        property.Bodies = [];
        property.Circuits = [];
        property.Objects = {};
        property.Heaters = [];
        property.AirSensors = [];
        property.HeaterCombos = [];
        property.CircuitGroups = [];
        property.Security = [];
        property.ACL = [];
        property.Users = [];
        property.UserNotifications = [];
        property.Schedules = [];
        property.subscriptionMessage.objectList = [];
        property.Ready = false;
        property.SecurityEnabled = true;
        if (resetSelectedInstallation)
            property.SelectedInstallation = enumerations.DefaultInstallation.InstallationId;
        property.Busy = false;


        property.EditSession = false;
        property.DeleteUserError = false;
    }

    return property;

}]);