﻿'use strict';
var PWC = PWC || {};

PWC.factory('AppService', [
    '$http', '$q', function ($http, $q) {
        var serviceFactory = {};

        var hostedEndpoint = '/service/';
        var hostname = window.location.hostname;
        //var serviceBase = 'https://' + ((hostname === 'localhost') ? 'localhost:44300/' : hostname + hostedEndpoint);
        var serviceBase = 'http://pentairdev-01.cloudapp.net' + hostedEndpoint;

        serviceFactory.GetData = function (endpoint) {
            var d = $q.defer();
            if (endpoint) {
                $http.get(serviceBase + endpoint).success(function (data, status, hdrs, config) {
                    var response = { data: data, status: status, headers: hdrs, config: config }
                    d.resolve(response);
                }).error(function (err, status, hdrs, config) {
                    var response = { error: err, status: status, headers: hdrs, config: config }
                    d.reject(response);
                });
            }
            return d.promise;
        }


        serviceFactory.GetLocalData = function(endpoint) {
            var d = $q.defer();
            if (endpoint) {
                $http.get(endpoint).success(function (data, status, hdrs, config) {
                    var response = { data: data, status: status, headers: hdrs, config: config }
                    d.resolve(response);
                }).error(function (err, status, hdrs, config) {
                    var response = { error: err, status: status, headers: hdrs, config: config }
                    d.reject(response);
                });
            }
            return d.promise;
        }


        serviceFactory.PostData = function (endpoint, data, headers) {
            var d = $q.defer();
            if (endpoint) {
                $http.post(serviceBase + endpoint, data, { headers: headers || {} }).success(function (data, status, hdrs, config) {
                    var response = { data: data, status: status, headers: hdrs, config: config }
                    d.resolve(response);
                }).error(function (err, status, hdrs, config) {
                    var response = { error: err, status: status, headers: hdrs, config: config }
                    d.reject(response);
                });
            }
            return d.promise;
        }

        serviceFactory.Installations = function (pageSize, page, term, sortCol, descend) {
            var params = buildParams({ pageSize: pageSize, page: page, sort: sortCol, term: term, descend: descend });
            return serviceFactory.GetData('Api/Installations' + params);
        }
          serviceFactory.InstallationsWithDetails = function (pageSize, page, term, filter) {
            var params = buildParams({ pageSize: pageSize, page: page,  term: term, filter: filter });
            return serviceFactory.GetData('Api/Installations/withDeails' + params);
        }

        function buildParams(params) {
            return Object.keys(params).filter(function (key) {
                return (params[key]);
            }).map(function (key, index) {
                var val = index == 0 ? "?" : "";
                return val + [key, params[key]].map(encodeURIComponent).join("=");
            }).join("&");
        }

        serviceFactory.InviteInfo = function (inviteId) {
            return serviceFactory.GetData('Api/Account/InviteInfo?id=' + inviteId);
        }

        serviceFactory.validateEmail = function (user, code) {
            return serviceFactory.PostData('Api/Account/ValidateEmail', { User: user, Code: code });
        }

        return serviceFactory;

    }]);