'use strict';
var PWC = PWC || {};

PWC.factory('MessageProcessor', [
    '$filter', 'PropertyModel', 'Enumerations', 'SocketService', function ($filter, propertyModel, enumerations, socketService) {
        var factory = {};

        factory.ProcessSchedules = function (message) {
            var schedules = message.objectList;

            propertyModel.Schedules = {};

            if (schedules) {
                for (var i = 0; i < schedules.length; i++) {
                    var schedule = schedules[i];
                    factory.ProcessSchedule(schedule);
                }
            }

        };

        factory.ProcessSchedule = function (schedule) {
            if (schedule.objnam && schedule.params) {

                var scheduledCircuit = {};
                angular.copy(propertyModel.Objects[schedule.params.CIRCUIT], scheduledCircuit);

                var newObject = {
                    OBJNAM: schedule.objnam,
                    OBJTYP: schedule.params.OBJTYP,
                    SNAME: schedule.params.SNAME,
                    LISTORD: schedule.params.LISTORD,
                    CIRCUIT: schedule.params.CIRCUIT,
                    HNAME: schedule.params.HNAME,
                    DAY: schedule.params.DAY,
                    START: schedule.params.START,
                    TIME: schedule.params.TIME,
                    STOP: schedule.params.STOP,
                    SINGLE: schedule.params.SINGLE,
                    TIMOUT: schedule.params.TIMOUT,
                    GROUP: schedule.params.GROUP,
                    STATUS: schedule.params.STATUS,
                    HEATER: schedule.params.HEATER,
                    HITMP: schedule.params.HITMP,
                    LOTMP: schedule.params.LOTMP,
                    ViewModel: {
                        Circuit: scheduledCircuit.SNAME || 'NO CIRCUIT FOUND',
                        Day: $filter('scheduleDays')(schedule.params.DAY),
                        Type: $filter('scheduleType')(schedule.params.SINGLE),
                        //  Time: $filter('translateTime')(schedule.params.TIME) + " - " + $filter('translateTime')(schedule.params.TIMOUT),
                        Settings: ''
                    }
                };

                //Get Heat Mode
                if (!PWC.isEmptyObject(scheduledCircuit) && scheduledCircuit.OBJTYP === "BODY") {
                    scheduledCircuit.HEATER = newObject.HEATER;
                    var heatMode = $filter('currentHeatMode')(scheduledCircuit);
                    heatMode = heatMode.name || "INVALID";
                    switch (newObject.HEATER) {
                        case '000FF':
                        case '00000':
                            newObject.ViewModel.Settings = heatMode;
                            break;
                        default:
                            newObject.ViewModel.Settings = $filter('translateTemperature')(newObject.LOTMP).ALL + '  ' + heatMode;
                    }
                }
                propertyModel.Schedules[newObject.OBJNAM] = newObject;
            }
        };

        factory.ProcessSystemInfo = function (message) {
            var info = message.objectList;
            propertyModel.SystemInfo = {};
            if (info) {
                for (var i = 0; i < info.length; i++) {
                    var item = info[i];
                    var newObject = {
                        INSTALLATION: item.params.INSTALLATION,
                        OWNER: item.params.OWNER,
                        ADDRESS: item.params.ADDRESS
                    }
                    propertyModel.SystemInfo[newObject.INSTALLATION] = newObject;
                }
            }
        }


        factory.ProcessManageUserMessage = function (message) {
            if (!PWC.isEmptyObject(message) && message.status === 'OK') {
                propertyModel.DeleteUserError = false;
            } else {
                propertyModel.DeleteUserError = (message.status === "NOTFOUND") ? $filter('translate')('USERNOTFOUNDERROR')
                    : $filter('translate')('LASTADMINDELETEERROR');
            }
        };


        factory.ProcessUserListMessage = function (message) {
            var users = message.objectList;
            propertyModel.ListCount = message.totalCount;
            propertyModel.Users = {};
            if (users) {
                for (var i = 0; i < users.length; i++) {
                    var user = users[i];
                    var newUser = {
                        email: user.objnam,
                        id: user.params.id,
                        name: user.params.name,
                        level: user.params.level,
                        status: user.params.status,
                    };
                    propertyModel.Users[newUser.name] = newUser;
                }
            }
        };

        factory.ProcessUserNotificationListMessage = function (message) {
            var notifications = message.objectList;

            propertyModel.UserNotifications = {};
            if (notifications) {
                for (var i = 0; i < notifications.length; i++) {
                    var notification = notifications[i];
                    var newNotification = {
                        name: notification.objnam,
                        id: notification.params.id,
                        duration: Number(notification.params.duration),
                        enabled: notification.params.duration === "0" ? "OFF" : "ON",
                        valid: true,
                        type: notification.params.type,
                    };
                    propertyModel.UserNotifications[newNotification.name] = newNotification;
                }
            }
        };

        factory.ProcessNotifications = function (message) {
            if (!PWC.isEmptyObject(message)) {
                if (message.command === "NOTIFYLIST" || message.command === "WRITEPARAMLIST") {
                    for (var i = 0; i < message.objectList.length; i++) {
                        var notification = message.objectList[i];
                        if (notification && notification.params) {

                            //security Object
                            if (notification.objnam === "uFFFE" && notification.params["STATUS"]) {
                                propertyModel.SecurityEnabled = notification.params["STATUS"] === 'ON';
                            }

                            //deleted object
                            if (notification.params["STATUS"] && notification.params["STATUS"] == "DSTROY") {
                                if (propertyModel.Schedules[notification.objnam]) {
                                    delete propertyModel.Schedules[notification.objnam];
                                }
                                else if (propertyModel.Objects[notification.objnam]) {
                                    delete propertyModel.Objects[notification.objnam];
                                }
                            }

                                //update or add
                            else {
                                var match = propertyModel.Objects[notification.objnam];
                                if (match) {
                                    angular.extend(match, notification.params);
                                    if (match.OBJTYP == "CIRCUIT") {
                                        factory.ProcessDelayState(match, "", "");
                                    }
                                    //update Units displays
                                    if (notification.objnam === "_5451" && notification.params["MODE"]) {

                                    }
                                } else if (message.command === 'WRITEPARAMLIST') {
                                 
                                    print("notification.params.OBJTYP is: \(notification.params.OBJTYP)")
                                 
                                    switch (notification.params.OBJTYP) {
                                        case 'BODY':
                                            factory.ProcessBodies(message);
                                            break;
                                        case 'CIRCUIT':
                                            factory.ProcessCircuits(message);
                                            break;
                                        case 'AIR':
                                            factory.ProcessAirSensor(message);
                                            break;
                                        case 'HEATER':
                                            factory.ProcessHeaters(message);
                                            break;
                                        case 'HCOMBO':
                                            factory.ProcessHeaterCombos(message);
                                            break;
                                        case 'SCHED':
                                            factory.ProcessSchedule(message);
                                            var schedules = message.objectList;
                                            if (schedules) {
                                                for (var i = 0; i < schedules.length; i++) {
                                                    var schedule = schedules[i];
                                                    factory.ProcessSchedule(schedule);
                                                }
                                            }
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        };


        factory.ProcessPanels = function (message) {
            var panels = message.objectList;
            propertyModel.Panels = [];
            if (panels) {
                for (var i = 0; i < panels.length; i++) {
                    var panel = panels[i];
                    var newObject = {
                        OBJNAM: panel.objnam,
                        LISTORD: panel.params.LISTORD,
                        SUBTYP: panel.params.SUBTYP,
                        HNAME: panel.params.HNAME,
                        SNAME: panel.params.SNAME
                    };
                    storeObject("Panels", newObject);
                }
            }
        };


        factory.ProcessModules = function (message) {
            var modules = message.objectList;
            propertyModel.Modules = [];
            if (modules) {
                for (var i = 0; i < modules.length; i++) {
                    var module = modules[i];
                    var newObject =
                    {
                        OBJNAM: module.objnam,
                        OBJTYP: module.params.OBJTYP,
                        SUBTYP: module.params.SUBTYP,
                        SNAME: module.params.SNAME,
                        LISTORD: module.params.LISTORD,
                        PORT: module.params.PORT,
                        PARENT: module.params.PARENT,
                        READY: module.params.READY
                    };

                    storeObject("Modules", newObject);
                }
            }
        };


        factory.ProcessRelays = function (message) {
            var relays = message.objectList;
            propertyModel.Relays = [];
            for (var i = 0; i < relays.length; i++) {
                var relay = relays[i];
                var newObject =
                {
                    OBJNAM: relay.objnam,
                    OBJTYP: relay.params.OBJTYP,
                    SUBTYP: relay.params.SUBTYP,
                    SNAME: relay.params.SNAME,
                    PANEL: relay.params.PANEL,
                    PARENT: relay.params.PARENT,
                    RUNON: relay.params.RUNON,
                    STATUS: relay.params.STATUS,
                    SOURCE: relay.params.SOURCE,
                    LISTORD: relay.params.LISTORD
                };

                storeObject("Relays", newObject);
            }
        };


        factory.ProcessSecurityTokens = function (message) {
            var tokens = message.objectList;

            propertyModel.Security = [];
            for (var i = 0; i < tokens.length; i++) {
                var token = tokens[i];
                var newObject =
                {
                    value: token.objnam,
                    name: token.params.SNAME
                };
                storeObject("Security", newObject);
            }
        };


        factory.ProcessBodies = function (message) {
            var bodies = message.objectList;
            if (bodies) {
                for (var i = 0; i < bodies.length; i++) {
                    var body = bodies[i];

                    var newObject =
                    {
                        OBJNAM: body.objnam,
                        SNAME: body.params.SNAME,
                        FILTER: body.params.FILTER,
                        TEMP: body.params.TEMP,
                        HITMP: body.params.HITMP,
                        LOTMP: body.params.LOTMP,
                        OBJTYP: body.params.OBJTYP,
                        SUBTYP: body.params.SUBTYP,
                        HEATER: body.params.HEATER,
                        HEATERS: {
                            VIRTUAL: {
                            },
                            GENERIC: {
                                HEATER: {
                                },
                                HCOMBO: {}
                            },
                            SECONDARY: {
                                HEATER: {
                                },
                                HCOMBO: {}
                            }
                        }
                    };
                    storeObject("Bodies", newObject);

                    //Subscribe to body
                    propertyModel.subscriptionMessage.objectList.push({
                        objnam: newObject.OBJNAM,
                        keys: ["TEMP", "HITMP", "LOTMP", "HEATER"]
                    });
                }
            }
        };


        factory.ProcessCircuits = function (message) {
            var circuits = message.objectList;

            if (circuits) {
                for (var i = 0; i < circuits.length; i++) {
                    var circuit = circuits[i];

                    var newObject = {
                        OBJNAM: circuit.objnam,
                        OBJTYP: circuit.params.OBJTYP,
                        SNAME: circuit.params.SNAME,
                        SHOMNU: circuit.params.SHOMNU,
                        STATUS: circuit.params.STATUS,
                        SUBTYP: circuit.params.SUBTYP,
                        SOURCE: circuit.params.SOURCE,
                        TIMOUT: circuit.params.TIMOUT,
                        Delay: {
                            VISIBLE: false,
                            MESSAGE: ""
                        }
                    };

                    if (newObject.SUBTYP === "CIRCGRP") {
                        //Do not store All Lights Object
                        if (newObject.OBJNAM !== enumerations.AllLightsObj) {
                            storeObject("CircuitGroups", newObject);
                        }
                    } else {
                        storeObject("Circuits", newObject);
                    }

                    propertyModel.subscriptionMessage.objectList.push({
                        objnam: newObject.OBJNAM,
                        keys: ["STATUS", "MODE"]
                    });
                }
            }
        };


        factory.ProcessAirSensor = function (message) {
            var airSensors = message.objectList;

            if (airSensors) {
                for (var i = 0; i < airSensors.length; i++) {
                    var airSensor = airSensors[i];

                    var newObject = {
                        OBJNAM: airSensor.objnam,
                        STATUS: airSensor.params.STATUS,
                        PROBE: airSensor.params.PROBE,
                        SUBTYP: airSensor.params.SUBTYP
                    };

                    storeObject("AirSensors", newObject);

                    propertyModel.subscriptionMessage.objectList.push({
                        objnam: newObject.OBJNAM,
                        keys: ["STATUS", "PROBE"]
                    });
                }
            }
        };


        factory.ProcessHeaters = function (message) {
            var heaters = message.objectList;

            if (heaters) {
                for (var i = 0; i < heaters.length; i++) {
                    var heater = heaters[i];

                    var newObject = {
                        OBJNAM: heater.objnam,
                        OBJTYP: heater.params.OBJTYP,
                        SNAME: heater.params.SNAME,
                        PERMIT: heater.params.PERMIT,
                        STATUS: heater.params.STATUS,
                        SUBTYP: heater.params.SUBTYP,
                        READY: heater.params.READY,
                        BODY: heater.params.BODY,
                        SHARE: heater.params.SHARE
                    };

                    storeObject("Heaters", newObject);

                    // Link HEATER to BODY OBJECT
                    linkHeaterToBody(newObject);

                    propertyModel.subscriptionMessage.objectList.push({
                        objnam: newObject.OBJNAM,
                        keys: ["STATUS", "SUBTYP", "PERMIT", "TIMOUT", "READY"]
                    });
                }
            }
        };


        function linkHeaterToBody(newObject) {
            if (newObject) {
                switch (newObject.SUBTYP) {
                    case "HCOMBO":
                        if (propertyModel.Objects[newObject.BODY])
                            propertyModel.Objects[newObject.BODY].HEATERS.VIRTUAL = newObject;
                        if (propertyModel.Objects[newObject.SHARE])
                            propertyModel.Objects[newObject.SHARE].HEATERS.VIRTUAL = newObject;
                        break;
                    case "HTPMP":
                    case "SOLAR":
                        if (propertyModel.Objects[newObject.BODY])
                            propertyModel.Objects[newObject.BODY].HEATERS.SECONDARY.HEATER = newObject;
                        if (propertyModel.Objects[newObject.SHARE])
                            propertyModel.Objects[newObject.SHARE].HEATERS.SECONDARY.HEATER = newObject;
                        break;
                    case "GENERIC":
                        if (propertyModel.Objects[newObject.BODY])
                            propertyModel.Objects[newObject.BODY].HEATERS.GENERIC.HEATER = newObject;
                        if (propertyModel.Objects[newObject.SHARE])
                            propertyModel.Objects[newObject.SHARE].HEATERS.GENERIC.HEATER = newObject;
                        break;
                }
            }
        }


        factory.ProcessHeaterCombos = function (message) {
            var heaterCombos = message.objectList;

            if (heaterCombos) {
                for (var i = 0; i < heaterCombos.length; i++) {
                    var heaterCombo = heaterCombos[i];

                    var newObject = {
                        OBJNAM: heaterCombo.objnam,
                        USE: heaterCombo.params.USE,
                        ACT: heaterCombo.params.ACT,
                        HEATER: heaterCombo.params.HEATER,
                        LISTORD: heaterCombo.params.LISTORD
                    };

                    storeObject("HeaterCombos", newObject);

                    linkHComboToBody(newObject);

                    propertyModel.subscriptionMessage.objectList.push({
                        objnam: newObject.OBJNAM,
                        keys: ["LISTORD"]
                    });
                }
            }

        };

        factory.ProcessValves = function (message) {
            var valves = message.objectList;

            if (valves) {
                for (var i = 0; i < valves.length; i++) {
                    var valve = valves[i];

                    var newObject = {
                        OBJNAM: valve.objnam,
                        OBJTYP: valve.params.OBJTYP,
                        SUBTYP: valve.params.SUBTYP,
                    };

                    storeObject("Valves", newObject);
                }
            }
        };

           factory.ProcessPumps = function (message) {
            var pumps = message.objectList;

            if (pumps) {
                for (var i = 0; i < pumps.length; i++) {
                    var pump = pumps[i];

                    var newObject = {
                        OBJNAM: pump.objnam,
                        OBJTYP: pump.params.OBJTYP,
                        SUBTYP: pump.params.SUBTYP,
                    };

                    storeObject("Pumps", newObject);
                }
            }
        };
                                 
                                 
     factory.ProcessPumpEffects = function (message) {
          var pumpeffects = message.objectList;
                                 
                if (pumpeffects) {
                    for (var i = 0; i < pumpeffects.length; i++) {
                    var pumpeffect = pumpeffects[i];
                                 
                    var newObject = {
                        OBJNAM: pumpeffect.objnam,
                        OBJTYP: pumpeffect.OBJTYP,
                        PARENT: pumpeffect.params.PARENT,
              };
                                 
               storeObject("PumpEffects", newObject);
             }
          }
      };
                                 
                                 
    function linkHComboToBody(newObject) {
            if (newObject) {
                for (var j = 0; j < propertyModel.Bodies.length; j++) {
                    var body = propertyModel.Objects[propertyModel.Bodies[j].OBJNAM];

                    switch (newObject.USE) {
                        case body.HEATERS.GENERIC.HEATER.OBJNAM:
                            body.HEATERS.GENERIC.HCOMBO = newObject;
                            break;
                        case body.HEATERS.SECONDARY.HEATER.OBJNAM:
                            body.HEATERS.SECONDARY.HCOMBO = newObject;
                            break;
                    }
                }
            }
        }


        factory.ProcessPreferences = function (message) {
            var preferences = message.objectList;

            if (preferences) {
                var newObject = {
                    OBJNAM: preferences[0].objnam,
                    OBJTYP: preferences[0].params.OBJTYP,
                    MODE: preferences[0].params.MODE
                }
                propertyModel.Objects[newObject.OBJNAM] = newObject;
            }
        }


        factory.ProcessClock = function (message) {
            var clock = message.objectList;

            if (clock) {
                var newObject = {
                    OBJNAM: clock[0].objnam,
                    OBJTYP: clock[0].params.OBJTYP,
                    SOURCE: clock[0].params.SOURCE
                }
                propertyModel.Objects[newObject.OBJNAM] = newObject;
            }
        }


        factory.ProcessLocation = function (message) {
            var location = message.objectList;

            if (location) {
                var locale = $filter('locationCityState')(location[0].params.SNAME);
                var newObject = {
                    OBJNAM: location[0].objnam,
                    OBJTYP: location[0].params.OBJTYP,
                    ACCEL: location[0].params.ACCEL,
                    ZIP: location[0].params.ZIP,
                    LOCX: location[0].params.LOCX,
                    LOCY: location[0].params.LOCY,
                    SNAME: location[0].params.SNAME,
                    DLSTIM: location[0].params.DLSTIM,
                    TIMZON: location[0].params.TIMZON,
                    OFFSET: location[0].params.OFFSET,
                    CLK24A: location[0].params.CLK24A,
                    TIME: location[0].params.TIME,
                    MIN: location[0].params.MIN,
                    DAY: location[0].params.DAY,
                    SRIS: location[0].params.SRIS,
                    SSET: location[0].params.SSET,
                    ViewModel: {
                        City: locale.City,
                        State: locale.State
                    }
                }
                propertyModel.Objects[newObject.OBJNAM] = newObject;
            }
        }


        factory.ProcessInviteMessage = function (message) {
            propertyModel.InviteStatus = {
                Status: message.status, Message: message.messageDetail
            };
        };


        //TODO: is there a better way to do this?
        factory.ProcessDelayMessage = function (message) {
            var delayMsg = message.objectList;

            if (delayMsg) {
                var notification = delayMsg[0];
                var match = propertyModel.Objects[notification.objnam];
                if (match && notification && notification.params) {
                    angular.extend(match, notification.params);
                    factory.ProcessDelayState(match, match.TIMOUT, match.SOURCE);
                }
            }
        };


        //TODO: is there a better way to do this?
        factory.ProcessDelayState = function (circuit, timout, source) {
            if (circuit.STATUS == "DLYON" || circuit.STATUS == "DLYOFF") {
                if (!circuit.Delay.VISIBLE) {
                    getDelayByOBJNAM(circuit.OBJNAM).then(factory.ProcessDelayMessage);
                }
                circuit.Delay.VISIBLE = true;
                if (timout && source) {
                    var delay = Number(timout);
                    var units = "seconds";
                    if (delay > 60) {
                        units = "minutes";
                        delay = Math.round(delay / 60);
                    }
                    var value = $filter('intToWord')(delay).toLowerCase();
                    switch (propertyModel.Objects[source].OBJTYP) {
                        case "HEATER":
                            circuit.Delay.MESSAGE = "The pump will turn off after a delay of " + value + " (" + delay + ") " + units + " to ensure the heater has cooled sufficiently to avoid damage to equipment.";
                            break;
                        case "VALVE":
                            circuit.Delay.MESSAGE = "The pump is off for a period of " + value + " (" + delay + ") " + units + " while valves are repositioned.";
                            break;
                        case "PUMP":
                            circuit.Delay.MESSAGE = "The Iflo pump is running at a higher speed for up to " + value + " (" + delay + ") " + units + " in order to prime it for normal operation.";
                            break;
                        default:
                            circuit.Delay.MESSAGE = "The cleaner will start after a delay of " + value + " (" + delay + ") " + units + " to ensure pipes are properly filled.";
                            break;
                    }

                } else {
                    circuit.Delay.MESSAGE = "Loading ...";
                }
            } else {
                circuit.Delay.VISIBLE = false;
            }
        };


        function storeObject(listName, object) {
            if (object) {
                propertyModel[listName].push(object);
                propertyModel.Objects[object.OBJNAM] = object;
            }
        }


        function getDelayByOBJNAM(objName) {
            if (objName) {
                var message = {
                    command: "getParamList",
                    objectList: [
                        {
                            objnam: objName,
                            keys: ["TIMOUT : SOURCE"]
                        }
                    ]
                };
                return socketService.sendMessage(message);
            }
        }


        return factory;
    }]);