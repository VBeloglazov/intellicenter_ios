﻿'use strict';
var PWC = PWC || {};

PWC.factory('PropertyService', [
      '$filter', '$location', '$q', '$rootScope', '$timeout', 'SocketService', 'Enumerations', 'LocalStorageService', 'AppService', 'PropertyModel', 'MessageProcessor', 'AuthService',
      function ($filter, $location, $q, $rootScope, $timeout, socketService, enumerations, localStorageService, appService, propertyModel, messageProcessor, authService) {
          var factory = {}
          var socket = socketService.socket;
          var reconnectAttempts = 0;
          var retry = true;
          //public methods

          factory.ToggleCircuit = function (circuit, value) {
              if (circuit && value)
                  factory.SetValue(circuit.OBJNAM, 'STATUS', value);
          }


          factory.IncreaseValue = function (obj, prop, max) {
              if (PWC.isValidObject(obj, [prop, 'OBJNAM'])) {
                  var target = obj[prop];
                  var value = Number(target) || Number(target.replace(/\D/g, ''));
                  value++;
                  if (value <= max)
                      factory.SetValue(obj.OBJNAM, prop, value);
              }
          }


          factory.DecreaseValue = function (obj, prop, min) {
              if (PWC.isValidObject(obj, [prop, 'OBJNAM'])) {
                  var target = obj[prop];
                  var value = Number(target) || Number(target.replace(/\D/g, ''));
                  value--;
                  if (value >= min)
                      factory.SetValue(obj.OBJNAM, prop, value);
              }
          }


          factory.SetValue = function (objName, prop, value) {
              if (objName && prop) {
                  return sendSetParamList(objName, prop, value);
              }
          }


          factory.GetHeatModes = function (body) {
              return $filter('heatModes')(body);
          };


          factory.SetHeatModes = function (objNam, heatMode) {
              var body = propertyModel.Objects[objNam];
              if (!PWC.isEmptyObject(body) && heatMode) {
                  return sendSetParamList(objNam, "HEATER", $filter('modeToHeater')(body, heatMode));
              }
          }


          factory.SetAllLights = function (status) {
              if (status)
                  return sendSetParamList(enumerations.AllLightsObj, "STATUS", status);
          }


          factory.ClearSubscriptions = function () {
              var message = {
                  command: "ClearParam",
                  messageID: Math.uuidCompact()
              };
              return socketService.sendMessage(message);
          }


          factory.GetParamListByOBJTYP = function (params, objType) {
              if (params && objType) {
                  var message = {
                      command: "getParamList",
                      objectList: [
                          {
                              objnam: "ALL",
                              keys: [params + " ? OBJTYP=" + objType]
                          }
                      ]
                  };

                  return socketService.sendMessage(message);
              }
          }


          factory.GetParamListBySUBTYP = function (params, objType, subType) {
              if (params && objType && subType) {
                  var message = {
                      command: "getParamList",
                      objectList: [
                          {
                              objnam: "ALL",
                              keys: [params + " ? OBJTYP=" + objType + " & " + "SUBTYP=" + subType]
                          }
                      ]
                  };
                  return socketService.sendMessage(message);
              }
          }


          factory.GetParamListByObject = function (params, objName) {
              if (params && objName) {
                  var message = {
                      command: "getParamList",
                      objectList: [
                          {
                              objnam: objName,
                              keys: [params]
                          }
                      ]
                  };
                  return socketService.sendMessage(message);
              }
          }


          factory.SetLightMode = function (lightTypes, mode) {
              if (typeof lightTypes === 'object' && lightTypes.length > 0 && mode) {
                  var message = {
                      command: "SetParamList",
                      objectList: []
                  }
                  for (var i = 0; i < propertyModel.Circuits.length; i++) {
                      var circuit = propertyModel.Circuits[i];
                      if (lightTypes.indexOf(circuit.SUBTYP) != -1) {

                          var newObj = {
                              objnam: circuit.OBJNAM,
                              params: { ACT: mode }
                          }

                          message.objectList.push(newObj);
                      }
                  }

                  if (message.objectList.length > 0)
                      return socketService.sendMessage(message);
              }
          }


          factory.CancelDelay = function (objNam) {
              if (objNam) {
                  return sendSetParamList(objNam, 'DLYCNCL', '');
              }
          }


          factory.GetDelayByOBJNAM = function (objName) {
              if (objName) {
                  var message = {
                      command: "getParamList",
                      objectList: [
                          {
                              objnam: objName,
                              keys: ["TIMOUT : SOURCE"]
                          }
                      ]
                  };
                  return socketService.sendMessage(message);
              }
          }


          factory.InviteUser = function (email, group) {
              if (email && group) {
                  var message = {
                      command: "INVITEUSER",
                      params: {
                          EMAIL: email,
                          PERMISSIONGROUP: group
                      }
                  }
                  return socketService.sendMessage(message).then(messageProcessor.ProcessInviteMessage);
              }
          }


          factory.CreateScheduleObject = function () {
              var message = {
                  command: "CREATEOBJECTFROMPARAM",
                  objectList: [
                      {
                          objtyp: 'SCHED',
                          keys: []
                      }
                  ]
              };
              return socketService.sendMessage(message);
          }


          factory.SetParams = function (objNam, params) {
              if (objNam && !PWC.isEmptyObject(params)) {
                  var message = {
                      command: "SetParamList",
                      objectList: [{
                          objnam: objNam,
                          params: params
                      }],
                  }
                  return socketService.sendMessage(message);
              }
          }


          factory.DeleteUser = function (email, status, notify) {
              if (email && status && notify != null) {
                  var message = {
                      command: "DELETEUSER",
                      params: {
                          EMAIL: email,
                          STATUS: status,
                          NOTIFY: notify
                      }
                  }
                  return socketService.sendMessage(message).then(messageProcessor.ProcessManageUserMessage);
              }
          }


          factory.EditUser = function (email, level) {
              if (email && level) {
                  var message = {
                      command: "EDITUSER",
                      params: {
                          EMAIL: email,
                          PERMISSIONGROUP: level
                      }
                  }
                  return socketService.sendMessage(message).then(messageProcessor.ProcessManageUserMessage);
              }
          }


          factory.GetUserList = function (pageSize, page, filter, searchTerm, sortField, sortDirection) {
              var message = {
                  command: "USERLIST",
                  messageID: Math.uuidCompact(),
                  params: {
                      PAGESIZE: pageSize,
                      PAGE: page,
                      FILTER: filter,
                      SORTFIELD: sortField,
                      SORTDIRECTION: sortDirection,
                      SEARCHTERM: searchTerm
                  }
              }
              return socketService.sendMessage(message).then(messageProcessor.ProcessUserListMessage);
          }


          factory.GetSecurityTokens = function () {
              factory.GetParamListByOBJTYP("OBJNAM : SNAME", "PERMIT").then(messageProcessor.ProcessSecurityTokens);
          }


          factory.GetSystemInformation = function () {
              return $q.all([
              factory.GetSystemInfo(),
              factory.GetUserList(),
              factory.GetParamListByObject("OBJNAM : OBJTYP : SOURCE ", "_C105").then(messageProcessor.ProcessClock),
              factory.GetParamListByObject("OBJNAM : OBJTYP : ACCEL : ZIP : LOCX : LOCY : SNAME : DLSTIM : TIMZON : OFFSET : CLK24A : TIME : MIN : DAY : SRIS : SSET", "_C10C").then(messageProcessor.ProcessLocation),
              ]).then(function () {
                  var subscription = [
                      {
                          objnam: "_C10C",
                          keys: ["DAY", "MIN", "OFFSET", "SNAME", "TIMZON", "SRIS", "SSET"]
                      },
                      {
                          objnam: "_C105",
                          keys: ["SOURCE"]
                      }
                  ];
                  factory.Subscribe(subscription);
              });
          }


          factory.ReleaseTimeUpdates = function () {
              var subscription = [
                  {
                      objnam: "_C10C",
                      keys: ["DAY", "MIN", "OFFSET", "SNAME", "TIMZON", "SRIS", "SSET"]
                  },
                  {
                      objnam: "_C105",
                      keys: ["SOURCE"]
                  }
              ];
              factory.UnSubscribe(subscription);
          }


          factory.GetPanels = function () {
              return $q.all([
                  factory.GetParamListByOBJTYP("OBJNAM : LISTORD : SUBTYP : HNAME : SNAME", "PANEL").then(messageProcessor.ProcessPanels),
                  factory.GetParamListByOBJTYP("OBJNAM : OBJTYP : SUBTYP : SNAME : LISTORD : PORT : PARENT : READY", "MODULE").then(messageProcessor.ProcessModules),
                  factory.GetParamListByOBJTYP("OBJNAM : OBJTYP : SUBTYP : SNAME : PANEL : PARENT : RUNON : STATUS : SOURCE : LISTORD", "RLY").then(messageProcessor.ProcessRelays)
              ]);
          }

          factory.GetUserNotifications = function () {
              var message = {
                  command: "USERNOTIFICATIONLIST",
                  messageID: Math.uuidCompact()
              }
              return socketService.sendMessage(message).then(messageProcessor.ProcessUserNotificationListMessage);
          }

          factory.SaveUserNotification = function(obj,value) {
            var params = {
                    id: obj.id,
                duration: value,
                type: obj.type
            };
              var message = {
                  command: "EDITNOTIFICATION",
                  messageID: Math.uuidCompact(),
                  objectList: [{
                      objnam: obj.name,
                      params: params
                  }]
              }
              socketService.sendMessage(message);
              propertyModel.UserNotifications[obj.name].duration = value;
          }

          factory.FetchInstallations = function () {
              var deferred = $q.defer();
              appService.Installations().then(function (result) {
                  propertyModel.Installations = result.data;
                  deferred.resolve();
              });
              return deferred.promise;
          }

          factory.GetSchedules = function () {
              return $q.all([
              factory.GetParamListByOBJTYP("OBJNAM : OBJTYP : LISTORD : CIRCUIT : HNAME : SNAME : DAY : SINGLE : START : TIME : STOP : TIMOUT : GROUP : STATUS : HEATER : HITMP : LOTMP", "SCHED").then(messageProcessor.ProcessSchedules),
              factory.GetParamListByObject("OBJNAM : OBJTYP : ACCEL : ZIP : LOCX : LOCY : SNAME : DLSTIM : TIMZON : OFFSET : CLK24A : TIME : MIN : DAY : SRIS : SSET", "_C10C").then(messageProcessor.ProcessLocation)
              ]).then(function () {
                  var subscription = [
                      {
                          objnam: "_C10C",
                          keys: ["MIN", "OFFSET", "SNAME", "SRIS", "SSET"]
                      }
                  ];
                  factory.Subscribe(subscription);
              });
          }

          factory.GetSystemInfo = function() {
              var message = {
                  command: "SYSTEMINFO",
                  messageID: Math.uuidCompact()
              }
              return socketService.sendMessage(message).then(messageProcessor.ProcessSystemInfo);
          }

          factory.SetSystemInfo = function(installation, params) {
              var message = {
                  command: "SAVESYSTEMINFO",
                  messageID: Math.uuidCompact(),
                  objectList: [
                      {
                          objnam: installation,
                          params: params
                      }]
              }
              socketService.sendMessage(message);
          }

          factory.GetClockSingleton = function () {
              factory.GetParamListByObject("OBJNAM : OBJTYP : SOURCE ", "_C105").then(messageProcessor.ProcessSchedules);
          }

          factory.Reset = function () {
              if (!PWC.isEmptyObject(socket) && socket.readyState == 1) {
                  //factory.ClearSubscriptions().then(function () {
                  //    socket.close();
                  //});
                  factory.ClearSubscriptions();
                  socket.close();
              } else {
                  propertyModel.Clear();
              }
          }


          factory.ChangeConnection = function (selection) {
              factory.Reset();
              factory.Connect(selection);
          }


          factory.Connect = function (selection) {
              if (selection)
                  propertyModel.SelectedInstallation = selection;
              if (propertyModel.SelectedInstallation && '0' !== propertyModel.SelectedInstallation) {

                  propertyModel.Busy = true;

                  factory.Reset();

                  if (reconnectAttempts == 0)
                      propertyModel.ConnectionStatus = 'Connecting...';

                  //clear existing subscriptions and wait for close event if socket is connected
                  if (!PWC.isEmptyObject(socket) && socket.readyState < 3) {
                      socket.addEventListener('close', function (event) {
                          connectToProperty();
                      });

                  } else {
                      connectToProperty();
                  }
              }
          }

          factory.Subscribe = function (subscriptionRequest) {
              for (var i = 0; i < subscriptionRequest.length; i++) {
                  var request = subscriptionRequest[i];
                  propertyModel.subscriptionMessage.objectList.push({
                      objnam: request.objnam,
                      keys: request.keys
                  });
              }
              return submitPendingSubscriptionRequest();
          }

          factory.UnSubscribe = function (unsubscribeRequest) {
              var message = {
                  command: "ReleaseParamList",
                  messageID: Math.uuidCompact(),
                  objectList: []
              };

              for (var i = 0; i < unsubscribeRequest.length; i++) {
                  var request = unsubscribeRequest[i];
                  message.objectList.push({
                      objnam: request.objnam,
                      keys: request.keys
                  });
              }
              return socketService.sendMessage(message);
          }

          factory.Disconnect = function () {
              if (socket.close) {
                  retry = false;
                  socket.close();
              }
          }

          factory.SaveSchedule = function(objNam, params) {
              factory.SetParams(objNam, params);
          };
          //END


          // Private Methods
          function getInstallationObject(selectedInstallation) {
              for (var i = 0; i < propertyModel.Installations.length; i++) {
                  if (propertyModel.Installations[i].InstallationId === selectedInstallation) {
                      return propertyModel.Installations[i];
                  }
              }
              return null;
          }

          function sendSetParamList(objNam, prop, value) {
              if (objNam && prop && !angular.isUndefined(value)) {

                  var newParams = {
                  }
                  newParams[prop] = value.toString();

                  var newObj = {
                      objnam: objNam,
                      params: newParams
                  }

                  var message = {
                      command: "SetParamList",
                      objectList: [newObj]
                  }
                  return socketService.sendMessage(message);
              }
          }


          function submitPendingSubscriptionRequest() {
              if (propertyModel.subscriptionMessage.objectList.length > 0) {
                  return socketService.sendMessage(propertyModel.subscriptionMessage).then(function () {
                      propertyModel.subscriptionMessage.objectList = [];
                  });
              }
              return $q.reject('No Notifications Found');
          }


          function selectedInstallationIsValid() {
              return (propertyModel.SelectedInstallation && 0 !== propertyModel.SelectedInstallation);
          }


          function connectToProperty() {
              saveCookies();
              if (selectedInstallationIsValid()) {
                  socket = socketService.newSocket(propertyModel.SelectedInstallation);
                  wireUpSocketEvents();
              }
              return socket;
          }


          function saveCookies() {
              sessionStorage.selectedInstallation = JSON.stringify(propertyModel.SelectedInstallation || '0');
              if (localStorageService.get('saveSession'))
                  localStorageService.set('selectedInstallation', sessionStorage.selectedInstallation);
          }

          function handleMessage(message) {
              if (message) {
                  var msgObj = JSON.parse(message);
                  if (PWC.isValidObject(msgObj, ['command'])) {
                      switch (msgObj.command) {
                          case "WRITEPARAMLIST":
                          case "NOTIFYLIST":
                              messageProcessor.ProcessNotifications(msgObj);
                              break;
                      }
                  } else {
                      console.log("Failed to convert JSON to Object");
                      debugger;
                  }
              }
          }

          function wireUpSocketEvents() {
              socket.addEventListener('open', function (e) {
                  reconnectAttempts = 0;
                  propertyModel.ConnectionStatus = 'Downloading...';

                  //save user security token
                  propertyModel.ACL = {};
                  var installation = getInstallationObject(propertyModel.SelectedInstallation);
                  if (installation) {
                      var newObject = {
                          OBJNAM: installation.AccessToken,
                          SHOMNU: ""
                      }
                      propertyModel.ACL = newObject;
                      propertyModel.Objects[newObject.OBJNAM] = newObject;
                  }

                  factory.ClearSubscriptions();
                  $q.all([
                      factory.GetParamListByOBJTYP("OBJNAM : OBJTYP : SNAME : FILTER : TEMP : HITMP : LOTMP : SUBTYP : HEATER : SOURCE", "BODY").then(messageProcessor.ProcessBodies),
                      factory.GetParamListByOBJTYP("OBJNAM : OBJTYP : SNAME : SHOMNU : STATUS : SUBTYP", "CIRCUIT").then(messageProcessor.ProcessCircuits),
                      factory.GetParamListBySUBTYP("OBJNAM : OBJTYP : STATUS : PROBE : SUBTYP", "SENSE", "AIR").then(messageProcessor.ProcessAirSensor),
                      factory.GetParamListByOBJTYP("OBJNAM : OBJTYP : SNAME : PERMIT : STATUS : SUBTYP : READY : BODY : SHARE", "HEATER").then(messageProcessor.ProcessHeaters),
                      factory.GetParamListByOBJTYP("OBJNAM : OBJTYP : USE : ACT : HEATER : LISTORD", "HCOMBO").then(messageProcessor.ProcessHeaterCombos),
                      factory.GetParamListByOBJTYP("OBJNAM : OBJTYP : SUBTYP", "VALVE").then(messageProcessor.ProcessValves),
                      factory.GetParamListByOBJTYP("OBJNAM : OBJTYP : SUBTYP", "PUMP").then(messageProcessor.ProcessPumps),
                      factory.GetParamListByOBJTYP("OBJNAM : OBJTYP : SUBTYP", "STAMSG").then(messageProcessor.ProcessUserNotificationListMessage),
                      factory.GetParamListByObject("OBJNAM : OBJTYP : MODE ", "_5451").then(messageProcessor.ProcessPreferences)
                  ]).then(function () {
                      submitPendingSubscriptionRequest().then(function () {
                          propertyModel.Ready = true;
                          propertyModel.ConnectionStatus = 'Connected';
                      });
                  }).catch(function (err) {
                      propertyModel.ConnectionStatus = 'Download Error-' + JSON.stringify(err);
                      reconnect();
                  });

                  // Subscribe to security enable
                  propertyModel.subscriptionMessage.objectList.push({
                      objnam: 'uFFFE',
                      keys: ['STATUS']
                  });

                  // Subscribe to users security token.
                  propertyModel.subscriptionMessage.objectList.push({
                      objnam: propertyModel.ACL.OBJNAM,
                      keys: ['SHOMNU']
                  });

                  $location.path('/property');
                  propertyModel.Busy = false;
              });

              socket.addEventListener('error', function (e) {
                  propertyModel.Clear();
                  $rootScope.$apply();
                  reconnect();
              });

              socket.addEventListener('close', function (e) {
                  propertyModel.ConnectionStatus = 'Not Connected';
                  if (retry)
                      reconnect();

                  propertyModel.Clear(!retry);
                  $rootScope.$apply();
                  retry = true;
              });

              socket.addEventListener('message', function (e) {
                  handleMessage(e.data);
              });
          }

          function reconnect() {
              if (propertyModel.SelectedInstallation && reconnectAttempts < 500 && authService.authentication.isAuth) {

                  if (getInstallationObject(propertyModel.SelectedInstallation).OnLine) {
                      propertyModel.ConnectionStatus = 'Connection Error';
                  } else {
                      propertyModel.ConnectionStatus = 'Property OffLine!';
                  }
                  propertyModel.ConnectionStatus = propertyModel.ConnectionStatus + ' Attempting to Re-Connect(' + reconnectAttempts + ')...';

                  if (PWC.isEmptyObject(socket) || socket.readyState == 3) {
                      $timeout(function () {
                          if (PWC.isEmptyObject(socket) || socket.readyState == 3)
                              factory.Connect();
                      }, 500);
                      reconnectAttempts += 1;
                  }
              } else {
                  propertyModel.ConnectionStatus = 'Not Connected';
              }
          }
          //END

          factory.GetInstallationObject = getInstallationObject;
          return factory;
      }]);