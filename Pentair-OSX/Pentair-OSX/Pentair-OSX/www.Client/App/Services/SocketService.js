﻿'use strict';
var PWC = PWC || {};

PWC.factory('SocketService', ['$http', '$q', '$window', '$rootScope', 'Logger', 'PropertyModel',
    function ($http, $q, $window, $rootScope, logger, propertyModel) {
        var factory = {};
        var propertyId;
        var socket = {};
        var messageQueue = {};

        var hostedEndpoint = '/service/';
        var hostname = window.location.hostname;
      //  var serviceBase = ((hostname === 'localhost') ? 'localhost:44300' : hostname + hostedEndpoint);
        var serviceBase = 'http://pentairdev-01.cloudapp.net' + hostedEndpoint;
                              
        var sendMessage = function (message) {
            if (propertyModel.Ready)
                propertyModel.ConnectionStatus = 'Sending...';

            var defer = $q.defer();
            message.messageID = Math.uuidCompact();
            var messageJson = JSON.stringify(message);

            if (PWC.isEmptyObject(socket)) {
                defer.reject("No Socket");
            } else {
                socket.send(messageJson);
                logger.logMessage("Client Send", messageJson, propertyId);
                console.log("Sending: '" + messageJson + "' : " + new Date());

                messageQueue[message.messageID] = {
                    callBack: defer,
                    time: new Date(),
                    message: message
                };
            }
            return defer.promise;
        }

        function handler(message) {
            var queuedMsg = messageQueue[message.messageID];
            if (queuedMsg) {
                if (message.command.indexOf('ERROR') > -1) {
                    queuedMsg.callBack.reject(message);
                } else {
                    queuedMsg.callBack.resolve(message);
                }

                delete messageQueue[message.callbackID];
                $rootScope.$apply();
            }
            if (propertyModel.Ready) {
                propertyModel.ConnectionStatus = 'Connected';
            }
        }

        var newSocket = function (propId) {
            var accessToken = "";

            //copy to parent Scope
            propertyId = propId;

            //close if socket is open
            if (socket.readyState < 2) {
                socket.close();
            }

            if (sessionStorage.token)
                accessToken = '?access-token=' + sessionStorage.token;
                              
                              console.log('ws://' + serviceBase + '/api/websocket/' + propertyId + accessToken);
            socket = new WebSocket('ws://' + serviceBase + 'api/websocket/' + propertyId + accessToken);

            socket.onclose = function (event) {
                socket = {};
                messageQueue = {};

                console.log("Connection Terminated : " + new Date());
            }

            socket.onerror = function (err) {
                logger.logMessage("Client Error", err.data, propertyId);
                console.log('Error: ' + new Date() + ' ---' + err.data);
            }

            socket.onopen = function (event) {
                console.log("Connection Established : " + new Date());
            };

            socket.onmessage = function (message) {
                handler(JSON.parse(message.data));

                logger.logMessage("Client Receive", message.data, propertyId);
                console.log('Receiving: ' + message.data + ': ' + new Date());
            };
            return socket;
        };


        factory.socket = socket;
        factory.newSocket = newSocket;
        factory.sendMessage = sendMessage;
        factory.closeSocket = function () {
            if (!PWC.isEmptyObject(socket) && socket.readyState < 2)
                socket.close();
        }
        factory.messageQueue = function () {
            return messageQueue;
        }

        return factory;
    }
]);