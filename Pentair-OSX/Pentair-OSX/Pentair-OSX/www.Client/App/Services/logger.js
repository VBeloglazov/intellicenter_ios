﻿'use strict';
var PWC = PWC || {};

PWC.factory('Logger', ['AppService', function (appService) {
    return {
        logMessage: function (action, message, id) {
            var data = {
                action: action,
                message: message,
                timestamp: new Date(),
                hardwareId: id
            };
            appService.PostData('api/messageLogger', data);
        }
    };
}]);