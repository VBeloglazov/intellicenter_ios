﻿var PWC = PWC || {};
PWC.factory('Enumerations', function () {
    var factory = {};

    factory.DefaultInstallation = { PoolName: 'Select A Installation', InstallationId: '0' };
    factory.DefaultScheduledCircuit = { SNAME: 'Select A Circuit', OBJNAM: '0' };
    factory.DefaultSelectedTimezone = { ZONE: 0, VALUE: 'Greenwich Mean Time' };

    factory.CLOCK_LISTS = {
        MODE: [
            { name: '12-hour clock', id: 'AMPM' },
            { name: '24-hour clock', id: 'HR24' }
        ],
        SOURCE: [
            { name: 'Manual', id: 'LOCAL' },
            { name: 'Internet', id: 'URL' }
        ],
        PREFERENCES: [
            { name: 'English', id: 'ENGLISH' },
            { name: 'Meric', id: 'METRIC' }
        ]
    };

    factory.HEAT_MODES = {
        GENERIC: [
            {
                name: 'Heat Off',
                value: 'NONE'
            },
            {
                name: 'Heater',
                value: 'GENERIC'
            }
        ],
        HTPMP: [
            {
                name: 'Heat Off',
                value: 'NONE'
            },
            {
                name: 'Heater',
                value: 'GENERIC'
            },
            {
                name: 'Heat Pump Pref.',
                value: 'PREF'
            },
            {
                name: 'Heat Pump Only',
                value: 'ONLY'
            }
        ],
        SOLAR: [
            {
                name: 'Heat Off',
                value: 'NONE'
            },
            {
                name: 'Heater',
                value: 'GENERIC'
            },
            {
                name: 'Solar Preferred',
                value: 'PREF'
            },
            {
                name: 'Solar Only',
                value: 'ONLY'
            }
        ]
    };

    factory.AllLightsObj = "C0A11";

    factory.DaysOfWeek = [
        { id: 'M', text: 'Monday' },
        { id: 'T', text: 'Tuesday' },
        { id: 'W', text: 'Wednesday' },
        { id: 'R', text: 'Thursday' },
        { id: 'F', text: 'Friday' },
        { id: 'A', text: 'Saturday' },
        { id: 'U', text: 'Sunday' }
    ];
    return factory;
});