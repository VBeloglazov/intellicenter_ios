﻿(function () {
    PWC.isEmptyObject = function (obj) {
        for (var name in obj) {
            return false;
        }
        return true;
    };


    PWC.isValidObject = function (obj, props) {
        if (!obj)
            return false;

        var result = true;
        if (props) {
            for (var i = 0; i < props.length; i++) {
                if (!obj.hasOwnProperty(props[i]))
                    result = false;
            }
        }
        return result;
    }


    PWC.addTwohours = function (dateTime) {
        var result = new Date(dateTime);
        result.setHours(result.getHours() + 2);
        return result;
    }


    PWC.convertToDate = function (HHmmSS, timeZone) {
        var tz = timeZone > 0 ?
            timeZone > 9 ? timeZone.toString() : '0' + timeZone.toString() :
            timeZone < -9 ? '-' + Math.abs(timeZone).toString() : '-0' + Math.abs(timeZone).toString();
        tz = tz + '00';
        return new Date(Date.parse('2014-10-08T' + HHmmSS + tz));
    }
}());

