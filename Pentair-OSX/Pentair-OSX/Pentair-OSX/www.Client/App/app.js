﻿var PWC = angular.module('PentairWebClient', ['ngRoute', 'LocalStorageModule', 'angular-loading-bar'
                                            , 'ngCookies', 'pascalprecht.translate', 'checklist-model', 'dnTimepicker', 'ui.bootstrap', 'ui.grid', 'ui.grid.paging', 'ui.grid.cellNav', 'ui.grid.pagination']);

PWC.provider('translator', myTranslateProvider);
var hostedEndpoint = '/client/';
var hostname = window.location.hostname;
var endpoint = (function (hostname) {
                return (hostname === 'localhost') ? '' : hostedEndpoint;
                })(hostname);

endpoint = "/Users/Dashon/Development/Projects/pentair-osx/app.nextgen.alpha/app.nextgen.alpha/www.Client"
PWC.config(function ($routeProvider, translatorProvider, $translateProvider, $httpProvider) {

   
           
    //Set up routes object
    PWC.routes =
    {
        "/login": {
            controller: "LoginController",
            templateUrl: endpoint + "/app/templates/login.html",
            requireLogin: false
        },
        "/changePassword": {
            controller: "LoginController",
            templateUrl: endpoint + "/app/templates/changePassword.html",
            requireLogin: true
        },
        "/passwordAssistance": {
            controller: "LoginController",
            templateUrl: endpoint + "/app/templates/passwordAssistance.html",
            requireLogin: false
        },
        "/retrievePassword/:user": {
            controller: "LoginController",
            templateUrl: endpoint + "/app/templates/changePassword.html",
            requireLogin: false,
            resolve: {
                resetModel: function ($location, $route, AppService) {
                    return AppService.validateEmail($route.current.params.user, $route.current.params.code).then(function (result) {
                        return result.data;
                    }).catch(function () {
                        $location.path("login");
                    });
                }
            }
        },
        "/manageProperties": {
            controller: "ManagePropertiesController",
            templateUrl: endpoint + "/app/templates/manageProperties.html",
            requireLogin: true,
            resolve: {
                installations: function ($route, AppService) {
                    return AppService.InstallationsWithDetails(15);
                }
            }
        },
        "/property": {
            controller: "PropertyController",
            templateUrl: endpoint + "/app/templates/property.html",
            requireLogin: true
        },

        "/installations": {
            controller: "InstallationsController",
            templateUrl: endpoint + "/app/templates/installations.html",
            requireLogin: true,
            resolve: {
                installations: function ($route, AppService) {
                    return AppService.Installations();
                }
            }
        },
        "/installationGrid": {
            controller: "InstallationsGridController",
            templateUrl: endpoint + "/app/templates/installationsTable.html",
            requireLogin: true,
            resolve: {
                installations: function ($route, AppService) {
                    return AppService.Installations();
                }
            }
        },
        "/lightShow": {
            controller: "LightShowController",
            templateUrl: endpoint + "/app/templates/lightShow.html",
            requireLogin: true
        },

        "/manageUsers": {
            controller: "ManageUsersController",
            templateUrl: endpoint + "/app/templates/manageUsers.html",
            requireLogin: true
        },

        "/schedules": {
            controller: "ScheduleController",
            templateUrl: endpoint + "/app/templates/schedules.html",
            requireLogin: true,
            resolve: {
                schedules: function (PropertyService) {
                    return PropertyService.GetSchedules();
                }
            }
        },

        "/inviteUser": {
            controller: "InviteController",
            templateUrl: endpoint + "/app/templates/inviteUser.html",
            requireLogin: true
        },

        "/invitation/:inviteId": {
            controller: "AcceptInviteController",
            templateUrl: endpoint + "/app/templates/acceptInvite.html",
            requireLogin: false,
            resolve: {
                inviteInfo: function ($route, AppService) {
                    return AppService.InviteInfo($route.current.params.inviteId).catch(function (msg) {
                        var err = { error: "Error!" };
                        if (msg.error) {
                            switch (msg.error.Message) {
                                case "Used":
                                    err = { error: "The invitation you have responded to has been used. Please contact the pool owner or administrator if you are having difficulty." };
                                    break;
                                case "Expired":
                                    err = { error: "The invitation you have responded to has expired. Please contact the pool owner or administrator to be re-invited." };
                                    break;
                                default:
                                    err = { error: "Error!" }
                                    break;
                            }
                        }
                        return {data: err}
                    });
                }
            }
        },
        "/configure": {
            controller: "ConfigController",
            templateUrl: endpoint + "/app/templates/configure.html",
            requireLogin: true
        },
        "/systemProtection": {
            controller: "SystemProtectionController",
            templateUrl: endpoint + "/app/templates/systemProtection.html",
            requireLogin: true,
            resolve: {
                SecurityTokens: function (PropertyService) {
                    return PropertyService.GetSecurityTokens();
                }
            }
        },
        "/userNotifications": {
            controller: "UserNotificationController",
            templateUrl: endpoint + "app/templates/userNotifications.html",
            requireLogin: true,
            resolve: {
                UserNotifications: function(PropertyService) {
                    return PropertyService.GetUserNotifications();
                }
            }
        },
        "/systemInformation": {
            controller: "SystemInformationController",
            templateUrl: endpoint + "/app/templates/systemInformation.html",
            requireLogin: true,
            resolve: {
                CurrentTime: function (PropertyService) {
                    return PropertyService.GetSystemInformation();
                }
            }
        },
        "/systemPersonality": {
            controller: "SystemPersonalityController",
            templateUrl: endpoint + "/app/templates/systemPersonality.html",
            requireLogin: true,
            resolve: {
                Configuration: function (PropertyService) {
                    return PropertyService.GetPanels();
                }
            }
        },
        "/changeEmail": {
            controller: "ChangeEmailController",
            templateUrl: endpoint + "/app/templates/changeEmail.html",
            requireLogin: true
        },
    };

    //add routes to RouteProvider
    for (var path in PWC.routes) {
        $routeProvider.when(path, PWC.routes[path]);
    }

    //Default Page
    $routeProvider.otherwise({ redirectTo: "/login" });

    //Http Interceptors
    $httpProvider.interceptors.push('AuthInterceptorService');

    //I18N translation provider
    translatorProvider.init($translateProvider);
});


PWC.run([
    'AuthService', '$location', '$rootScope', function (AuthService, $location, $rootScope) {

        // restore session data and fill authentication object
        AuthService.restorePreviousSession();

        // intercept location changes to enforce required login
        $rootScope.$on("$locationChangeStart", function (event, next, current) {
            for (var i in PWC.routes) {
                if (next.indexOf(i) != -1) {
                    if (PWC.routes[i].requireLogin && !AuthService.authentication.isAuth) {
                        $location.path("/login");
                        event.preventDefault();
                    }
                }
            }
        });
        // redirect to properties page if authenticated on app start

        if (AuthService.authentication.isAuth && $location.path().indexOf('/invitation/') < 0) {
            $location.path('/installations');
        }
    }
]);
