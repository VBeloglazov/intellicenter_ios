//
//  Checkbox.swift
//  Pentair-iOs
//
//  Created by pentair developer on 5/13/16.
//  Copyright © 2016 Pentair Corporation. All rights reserved.
//

import UIKit

protocol CheckboxViewDelegate: class {
    /// Loads the data for the view, returns the marshalled data object from the server
    func valueChanged(view: Checkbox, value: Bool)
}
class Checkbox: UIButton{
    // Images
    
    weak var delegate: CheckboxViewDelegate?
    let checkedImage = UIImage(named: "checkbox_on")! as UIImage
    let uncheckedImage = UIImage(named: "checkbox_off")! as UIImage
    
    // Bool property
    var isChecked: Bool = false {
        didSet{
            if isChecked == true {
                self.setImage(checkedImage, forState: .Normal)
            } else {
                self.setImage(uncheckedImage, forState: .Normal)
            }
        }
    }
    
    override func awakeFromNib() {
        
        self.addTarget(self, action: "buttonClicked:", forControlEvents: UIControlEvents.TouchUpInside)
        self.isChecked = false
    }
    
    func buttonClicked(sender: UIButton) {
        if sender == self {
            if isChecked == true {
                isChecked = false
            } else {
                isChecked = true
            }
        }
        delegate?.valueChanged(self, value: isChecked)
    }
}
