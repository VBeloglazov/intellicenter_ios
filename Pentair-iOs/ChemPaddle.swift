//
//  ChemPaddle.swift
//  Pentair-iOs
//
//  Created by Dashon Howard on 2/4/16.
//  Copyright © 2016 Pentair Corporation. All rights reserved.
//

import Foundation
@IBDesignable
class ChemPaddle: UIButton {
    
    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        self.layer.cornerRadius = 0.5 * self.bounds.size.width
        self.layer.masksToBounds = true
    }
    
}