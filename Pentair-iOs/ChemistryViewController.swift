//
//  ChemistryViewController.swift
//  Pentair-iOs
//
//  Created by Dashon Howard on 2/3/16.
//  Copyright © 2016 Pentair Corporation. All rights reserved.
//

import Foundation
import UIKit
import Pentair

public class BodyOfWater {
    var property: Property!
    private var parent: NSMutableDictionary?
    private var child: NSMutableDictionary?
    private var  listOrder: String?
    private var  intellichem: [String : AnyObject]?
    private var  intellichlor: [String : AnyObject]?
    private var name: String?
    private var shared: Bool?
    
    init(body:NSMutableDictionary?, share:NSMutableDictionary?, property:Property) {
        if (body == nil) {return}
        if (share == nil) {
            shared = false
            parent = body
            self.name = body!["SNAME"] as? String
        } else {
            shared = true
            self.parent = nil
            self.child = nil
            if let parentListOrd = body!["LISTORD"] as? String {
                if let childListOrd = share!["LISTORD"] as? String {
                    if (parentListOrd < childListOrd) {
                        self.parent = body
                        self.child = share
                    }
                    else {
                        self.parent = share
                        self.child = body
                    }
                }
            }
            self.name = parent!["SNAME"] as! String + " / " + (child!["SNAME"] as! String)
        }
        self.property = property
        
        intellichem = self.property.configurationModel.getIntelliChem(parent!["OBJNAM"] as! String) as? [String : AnyObject]
        intellichlor = self.property.configurationModel.getIntelliChlor(parent!["OBJNAM"] as! String) as? [String : AnyObject]
        
        listOrder = parent!["LISTORD"] as? String
    }
    
    func getParent() -> NSMutableDictionary?{
        
        return self.parent
    }
    
    func getChild() -> NSMutableDictionary? {
        return self.child
    }
    
    func getListOrder() -> String {
        return self.listOrder!
    }
    
    func getIntellichem() -> [String : AnyObject]? {
        return self.intellichem
    }
    
    func getIntellichlor() -> [String : AnyObject]? {
        return self.intellichlor
    }
    
    func getName() -> String {
        return self.name!
    }
    
    func containsBody(bodyName:String) -> Bool {
        if let parent = getParent()!["OBJNAM"] as? String,
            let child = getChild()!["OBJNAM"] as? String {
                return (parent == bodyName || child == bodyName)
        }
        return false
    }
    
    func isShared() -> Bool {
        return self.shared!
    }
}



class ChemistryViewController: UIViewController {
    
    var property: Property!
    var bodiesOfWater = [BodyOfWater?]()
    var selectedBodyOfWater: BodyOfWater?
    @IBOutlet weak var bodySelectorButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBAction func BodySelectorTouched(sender: UIButton) {
        if (bodiesOfWater.count > 0){
            ActionSheetStringPicker(title: "Bodies of Water", rows: self.bodiesOfWater.map({$0!.name!}), initialSelection: 0, target: self, successAction: #selector(ChemistryViewController.bodyOfWaterSelected(_:)), cancelAction: nil, origin: sender).showActionSheetPicker()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadDropdown()
        
        bodyOfWaterSelected(0)
    }
    
    func loadDropdown(){
        for body in property.configurationModel.getAllBodies(){
            let shared = property.configurationModel.getItem(body["SHARE"] as! String)
            let newBody = BodyOfWater(body:body,share:shared,property:property)
            if !self.bodiesOfWater.contains({$0!.name == newBody.name! }){
                self.bodiesOfWater.append(newBody)
            }
        }
        
        if (self.bodiesOfWater.count < 2){
            bodySelectorButton.removeFromSuperview()
        }
    }
    
    func bodyOfWaterSelected(selectedIndex: NSNumber){
        if (bodiesOfWater.count > 0){
            selectedBodyOfWater = bodiesOfWater[selectedIndex.integerValue]
            bodySelectorButton.setTitle(selectedBodyOfWater?.name , forState: .Normal)
        }
        buildView()
    }
    
    func buildView(){
        if selectedBodyOfWater == nil {return}
        
        let intelliChemView = (storyboard?.instantiateViewControllerWithIdentifier("IntelliChemViewController"))! as! IntelliChemViewController
        let intelliChlorView = (storyboard?.instantiateViewControllerWithIdentifier("IntelliChlorViewController"))! as! IntelliChlorViewController
        
        // INTELLICHEM
        if  let intelliChemObj = selectedBodyOfWater?.getIntellichem() {
            intelliChemView.property  = self.property
            intelliChemView.objNam = intelliChemObj["objnam"] as? String
            
            self.addChildViewController(intelliChemView)
            intelliChemView.view.frame.size.width = scrollView.bounds.width
            scrollView.addSubview(intelliChemView.view)
            intelliChemView.didMoveToParentViewController(self)
            
            scrollView.contentSize.height = intelliChemView.IntelleChemViewContainer.bounds.height
        }
        
        //INTELLICHLOR
        if let intelliChlorObj = selectedBodyOfWater?.getIntellichlor() {
            intelliChlorView.property  = self.property
            intelliChlorView.objNam = intelliChlorObj["objnam"] as? String
            intelliChlorView.bodyOfWater = selectedBodyOfWater
            
            if(selectedBodyOfWater?.getIntellichem() != nil){
                intelliChemView.addChildViewController(intelliChlorView)
                intelliChemView.iChlorContainer.frame = intelliChlorView.view.bounds
                intelliChemView.iChlorContainer.addSubview(intelliChlorView.view)
                intelliChlorView.didMoveToParentViewController(intelliChemView)
                
                intelliChemView.view.frame.size.height = intelliChlorView.IntelliChlorContainer.bounds.height + intelliChemView.IntelleChemViewContainer.bounds.height
                scrollView.contentSize.height = intelliChemView.view.bounds.height
            } else {
                self.addChildViewController(intelliChlorView)
                scrollView.addSubview(intelliChlorView.view)
                intelliChlorView.didMoveToParentViewController(self)
                scrollView.contentSize.height = intelliChlorView.IntelliChlorContainer.bounds.height
            }
        }
    }
}