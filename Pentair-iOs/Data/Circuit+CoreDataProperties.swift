//
//  Circuit+CoreDataProperties.swift
//  Pentair-iOs
//
//  Created by pentair developer on 8/4/16.
//  Copyright © 2016 Pentair Corporation. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Circuit {

    @NSManaged var identifier: String?
    @NSManaged var listOrder: String?
    @NSManaged var objectName: String?
    @NSManaged var objectType: String?
    @NSManaged var shoMenu: String?
    @NSManaged var sName: String?
    @NSManaged var statusFlag: String?
    @NSManaged var subType: String?
    @NSManaged var timOut: String?
    @NSManaged var pump: Pump?
    @NSManaged var pumpEffect: PumpEffect?

}
