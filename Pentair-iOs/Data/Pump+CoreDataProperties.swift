//
//  Pump+CoreDataProperties.swift
//  Pentair-iOs
//
//  Created by pentair developer on 8/5/16.
//  Copyright © 2016 Pentair Corporation. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Pump {

    @NSManaged var circuitName: String?
    @NSManaged var displayName: String?
    @NSManaged var gpm: String?
    @NSManaged var identifier: String?
    @NSManaged var listOrder: String?
    @NSManaged var objectName: String?
    @NSManaged var pumpType: String?
    @NSManaged var pwr: String?
    @NSManaged var rpm: String?
    @NSManaged var state: String?
    @NSManaged var circuit: Circuit?
    @NSManaged var effects: NSSet

}
