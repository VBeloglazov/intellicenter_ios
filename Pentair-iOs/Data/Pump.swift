//
//  Pump.swift
//  Pentair-iOs
//
//  Created by pentair developer on 8/5/16.
//  Copyright © 2016 Pentair Corporation. All rights reserved.
//

import Foundation
import CoreData

@objc(Pump)
class Pump: NSManagedObject {

// Insert code here to add functionality to your managed object subclass
    @NSManaged func addPumpEffectsObject(tag: PumpEffect)
    @NSManaged func removePumpEffectsObject(tag: PumpEffect)
    @NSManaged func addPumpEffects(effects: NSSet)
    @NSManaged func removePumpEffects(effects: NSSet)
}
