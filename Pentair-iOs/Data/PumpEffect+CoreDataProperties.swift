//
//  PumpEffect+CoreDataProperties.swift
//  Pentair-iOs
//
//  Created by pentair developer on 8/4/16.
//  Copyright © 2016 Pentair Corporation. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension PumpEffect {

    @NSManaged var boost: String?
    @NSManaged var identifier: String?
    @NSManaged var listOrder: String?
    @NSManaged var objectName: String?
    @NSManaged var objectType: String?
    @NSManaged var parentObject: String?
    @NSManaged var select: String?
    @NSManaged var speed: String?
    @NSManaged var staticFlag: String?
    @NSManaged var circuit: Circuit?
    @NSManaged var circuitName: String?
    @NSManaged var pump: Pump?
    
}
