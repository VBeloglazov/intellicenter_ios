//
//  StatusMessage+CoreDataProperties.swift
//  Pentair-iOs
//
//  Created by pentair developer on 8/17/16.
//  Copyright © 2016 Pentair Corporation. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension StatusMessage {

    @NSManaged var alarm: String?
    @NSManaged var count: String?
    @NSManaged var identifier: String?
    @NSManaged var mode: String?
    @NSManaged var objectName: String?
    @NSManaged var objectRevision: String?
    @NSManaged var objectType: String?
    @NSManaged var parent: String?
    @NSManaged var party: String?
    @NSManaged var sindex: String?
    @NSManaged var sname: String?
    @NSManaged var time: String?

}
