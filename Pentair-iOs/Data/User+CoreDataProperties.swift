//
//  User+CoreDataProperties.swift
//  Pentair-iOs
//
//  Created by pentair developer on 8/4/16.
//  Copyright © 2016 Pentair Corporation. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension User {

    @NSManaged var email: String?
    @NSManaged var identifier: String?
    @NSManaged var level: String?
    @NSManaged var name: String?
    @NSManaged var security: String?
    @NSManaged var status: String?

}
