//
//  UserNotification+CoreDataProperties.swift
//  Pentair-iOs
//
//  Created by pentair developer on 8/4/16.
//  Copyright © 2016 Pentair Corporation. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension UserNotification {

    @NSManaged var identifier: String?
    @NSManaged var objectName: String?
    @NSManaged var objectRevised: String?
    @NSManaged var objectType: String?
    @NSManaged var parent: String?
    @NSManaged var mode: String?
    @NSManaged var sname: String?
    @NSManaged var count: String?
    @NSManaged var party: String?
    @NSManaged var sindex: String?
    @NSManaged var time: String?
    @NSManaged var alarm: String?
    @NSManaged var shomenu: String?

}
