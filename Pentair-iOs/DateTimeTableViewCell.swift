//
//  DateTimeTableViewCell.swift
//  Pentair-iOs
//
//  Created by pentair developer on 6/27/16.
//  Copyright © 2016 Pentair Corporation. All rights reserved.
//

import UIKit

class DateTimeTableViewCell: UITableViewCell {
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
