//
//  EffectsTableViewCell.swift
//  Pentair-iOs
//
//  Created by pentair developer on 7/11/16.
//  Copyright © 2016 Pentair Corporation. All rights reserved.
//

import UIKit
import Pentair

protocol EffectsTableViewCellViewDelegate: class {
    
    func stepperAction(sender: AnyObject)
}


class EffectsTableViewCell: UITableViewCell {
    weak var delegate: NumberStepperViewDelegate?
    var minSimplePumpType:Float = 0    // Dual, Single
    var maxSimplePumpType:Float = 100  // Dual, Single
    var minNextGenPumpType:Float = 134 // VS/VF/Vx Only
    var maxNextGenPumpType:Float = 3450 // VS/VF/Vx Only
    var increment:Float = 1
    var property: Property?
    var objNam:String?
    var param:String!
    var controlObject: NSMutableDictionary?
    var pumpType:String!
    var oldValue:Double!

    @IBOutlet weak var effectsNameLabel: UILabel!
    @IBOutlet weak var SpeedTextLabel: UILabel!
   
    @IBOutlet weak var backingView: UIView!
    @IBOutlet weak var stepper: UIStepper!
    @IBOutlet weak var stepperValue: UILabel!
    @IBOutlet weak var vsfPumpTypeView: UIView!
    @IBOutlet weak var RPMGPMSwitch: UISwitch!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 8
        }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func stepperAction(sender: UIStepper) {
        if (sender.value > oldValue) {
           
            self.incrementCount(sender)
        } else {
            
            self.decrementCount(sender)
        }
        
        sendUpdate((Float(totalCount())))
    }
    
    func incrementCount(stepper: UIStepper) -> Void{
         oldValue=oldValue+1
        stepperValue.text = "\(totalCount() + Int(increment))"
    }
    
    func decrementCount(stepper: UIStepper) -> Void{
        oldValue=oldValue-1
        stepperValue.text = "\(totalCount() - Int(increment))" //stepper.value
        
        //resetStepper(stepper)
    }

    func totalCount() -> Int{
        return Int(stepperValue.text!)!
    }
    
    //@ not used currently
    func resetStepper(stepper: UIStepper) -> Void{
        stepper.value = 0
    }
    
    
    private func sendUpdate(value: Float){
        
        if (self.pumpType != nil) {
            var newValue:Float = 0
            switch(self.pumpType) {
                
//            case "SINGLE": //
//                newValue = checkRangeSimple(value)
//                break;
//            case "DUAL": //
//                newValue = checkRangeSimple(value)
//                break;
            case "FLOW": //
                newValue = checkRangeNextGen(value)
                break;
            case "SPEED": //
                newValue = checkRangeNextGen(value)
                break;
            case "VSF": //
                newValue = checkRangeNextGen(value)
                break;
                
            default:
                //newValue = checkRangeSimple(value) (?)
                break;
            }
        
            let integerValue = Int(newValue)
            let keyValues: [String: String] = ["SPEED": String(integerValue)]
            print("keyValues \(keyValues)")
            
            let parameters = [KeyValue().setParametersObject(self.objNam!, keyValues: keyValues)]

            property!.sendRequest(setParamList().buildSetParameters(parameters))
        }
    }
    
    private func checkRangeSimple(value: Float) -> Float{
        var newValue = value
        if (value < minSimplePumpType) {
            newValue = minSimplePumpType
        }
        if (value > maxSimplePumpType) {
            newValue = maxSimplePumpType
        } else {
            newValue = Float(stepperValue.text!)!
            newValue = roundToIncrement(newValue)
        }
        return newValue
    }
    
    private func checkRangeNextGen(value: Float) -> Float{
        var newValue = value
        if (value < minNextGenPumpType) {
            newValue = minNextGenPumpType
        }
        if (value > maxNextGenPumpType) {
            newValue = maxNextGenPumpType
        } else{
            newValue = roundToIncrement(value)
        }
        
        return newValue
    }


    
    private func roundToIncrement(value: Float) -> Float{
        return round(value / increment) * increment
    }


}
