//
//  GradientLevelView.swift
//  Pentair-iOs
//
//  Created by Dashon Howard on 1/30/16.
//  Copyright © 2016 Pentair Corporation. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
public class GradientLevelView : UIView {
    
    @IBInspectable public var gradientColors : [UIColor] = [UIColor.redColor(),UIColor.greenColor(),UIColor.redColor()]
    
    @IBInspectable public var currentLevel: Float = 0.0
    
    let triangle = TriangleView(frame:CGRectMake(0, 0, 20, 10) )
    
    let gradient = CALayer()
    
    @IBInspectable var min:Float = 0.0
    @IBInspectable var max:Float = 100.0
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        triangle.transform = CGAffineTransformMakeRotation((180.0 * CGFloat(M_PI)) / 180.0)
        triangle.backgroundColor = UIColor.clearColor()
        self.addSubview(triangle)
    }
    override public func drawRect(rect: CGRect) {
        super.drawRect(rect)
        self.setLayer()
    }
    
    public override func prepareForInterfaceBuilder() {
    }
    
    public func setLayer(){
        self.layer.masksToBounds = true
        let gradientLayer : CAGradientLayer = CAGradientLayer()
        gradientLayer.frame.size = self.frame.size
        gradientLayer.frame.size.height = self.frame.size.height - triangle.frame.size.height + 5
        gradientLayer.frame.origin = CGPointMake(0.0,triangle.frame.size.height + 5)
        gradientLayer.colors = gradientColors.map{$0.CGColor}
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        self.gradient.insertSublayer(gradientLayer, atIndex: 0)
        self.layer.insertSublayer(self.gradient, atIndex: 0)
        
        positionArrow(self.getXPosition())
    }
    
    func getXPosition() -> CGFloat {
        return CGFloat((currentLevel - min) / (max - min)) *  self.frame.size.width
    }
    
    func positionArrow(position: CGFloat) {
        
        UIView.animateWithDuration(1, animations: {
            self.triangle.frame.origin.x = position -  (self.triangle.frame.size.width / 2)
            if(self.triangle.frame.origin.x < 0 ){
                self.triangle.frame.origin.x = 0
            }
            
            if(self.triangle.frame.origin.x > self.frame.size.width - self.triangle.frame.size.width   ){
                self.triangle.frame.origin.x = self.frame.size.width - self.triangle.frame.size.width
            }
            
        })
    }
    
    public func setChemLevel( value: Float){
        if value < min {
            self.currentLevel = min
        }else if value > max {
            self.currentLevel = max
        }else{
            self.currentLevel = value
        }
        
        positionArrow(self.getXPosition())
    }
    
    
    
}