//
//  Helpers.swift
//  Pentair-iOs
//
//  Created by Dashon Howard on 1/29/15.
//  Copyright (c) 2015 Pentair Corporation. All rights reserved.
//

import Foundation

public func isValidEmail(testStr:String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
    
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    let result = emailTest.evaluateWithObject(testStr)
    return result
}

public func getIndex(list:[String], find:String) -> Int?{
    for (index, value) in list.enumerate() {
        if (value.lowercaseString == find.lowercaseString){
            return index
        }
    }
    return nil
}
public func getStatusImage (status:String) -> UIImage?{
    switch (status){
    case UserInvitationStatus.Accepted.rawValue:
        return UIImage(named: "blueDot")
    case UserInvitationStatus.Unknown.rawValue:
        return UIImage(named: "whiteDot")
    case UserInvitationStatus.Expired.rawValue:
        return UIImage(named: "yellowDot")
    case UserInvitationStatus.Bounced.rawValue:
        return UIImage(named: "redDot")
    default:
        return UIImage(named: "greenDot")
    }
}

public func getStatusImage (status:Bool) -> UIImage?{
    return status ? UIImage(named: "greenDot") : UIImage(named: "whiteDot")
}
public func getStringOrDefault(value:String?)->String{
    var result = "--"
    if let value = value{
        if !value.isEmpty{
            result = value
        }
    }
    return result
}


public func setConnectBtnState(button:UIButton, isOnline:Bool){
    if (isOnline){
        button.backgroundColor = UIColor(red:0.42, green:0.70, blue:0.24, alpha:1.0)
        button.setTitle("Connect", forState: .Normal)
        button.enabled = true
    }else{
        button.backgroundColor = UIColor(red:0.70, green:0.24, blue:0.24, alpha:1.0)
        button.setTitle("Offline", forState: .Normal)
        button.enabled = false
    }
}


public func setDeleteBtnState(button:UIButton, canDelete:Bool){
    if (canDelete){
        button.backgroundColor = UIColor(red:0.70, green:0.24, blue:0.24, alpha:1.0)
        button.enabled = true
    }else{
        button.backgroundColor = UIColor(red:0.85, green:0.62, blue:0.62, alpha:1.0)
        button.enabled = false
    }
}

extension String
{
    func trim() -> String
    {
        return self.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
    }
}