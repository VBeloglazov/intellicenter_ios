//
//  HistoryViewController.swift
//  Pentair-iOs
//
//  Created by pentair developer on 7/12/16.
//  Copyright © 2016 Pentair Corporation. All rights reserved.
//


import UIKit
import QuartzCore
import Pentair
import Charts

// note: we have two different sets of data here -- Chemistry and Temperature
// there is a bar, point, and combo, view for each of them

class HistoryViewController: UIViewController, SSRadioButtonControllerDelegate, ChartViewDelegate {
    
    @IBOutlet weak var lineButton: UIButton!
    @IBOutlet weak var plotButton: UIButton!
    @IBOutlet weak var graphButton: UIButton!
    
    var objNam: String!
    var chartType: String!
    //var activeChartView: String!
    var property: Property?
    var radioButtonController: SSRadioButtonsController?
    var dataEntriesBarChart: [BarChartDataEntry] = []
    var dataEntriesPointChart: [ChartDataEntry] = []
    let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
    let temps = [100.0, 90.0, 80.0, 90.0, 70.0, 100.0, 60.0, 70.0, 80.0, 100.0, 80.0, 70.0]
    let historyTypeSelections = ["Temperature / Usage", "Chemistry / Feeds"]

    
    @IBOutlet weak var barChartView: HorizontalBarChartView!
    @IBOutlet weak var lineChartView: LineChartView!
    @IBOutlet weak var historyTypeSelectionView: UIView!
    @IBOutlet weak var chemistryView: UIView!
    @IBOutlet weak var temperatureView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()

        radioButtonController = SSRadioButtonsController(buttons: lineButton, plotButton, graphButton)
        radioButtonController!.delegate = self
        radioButtonController!.shouldLetDeSelect = true
        barChartView.delegate = self
        lineChartView.delegate = self
        self.chartType = "Bar"
        self.lineChartView.hidden = true
        setBarChart(months, values: temps)
    }
    
    func setBarChart(dataPoints: [String], values: [Double]) {
        barChartView.noDataText = "You need to provide data for the chart."
        barChartView.noDataTextDescription = "GIVE REASON"
       // barChartView.
        
        for i in 0..<dataPoints.count {
            let dataEntry = BarChartDataEntry(value: values[i], xIndex: i)
            dataEntriesBarChart.append(dataEntry)
        }
        
        let chartDataSet = BarChartDataSet(yVals: dataEntriesBarChart, label: "Temperature")
        let chartData = BarChartData(xVals: months, dataSet: chartDataSet)
        barChartView.data = chartData
        barChartView.drawValueAboveBarEnabled = true
        barChartView.xAxis.labelPosition = .Bottom
        barChartView.leftAxis.startAtZeroEnabled = false

        barChartView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0)
        //barChartView.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: .EaseInBounce)
    }
    
    func setLineChart(dataPoints: [String], values: [Double]) {
        lineChartView.noDataText = "You need to provide data for the chart."
        lineChartView.noDataTextDescription = "GIVE REASON"
        
        for i in 0..<dataPoints.count {
            let dataEntry = ChartDataEntry(value: values[i], xIndex: i)
            dataEntriesPointChart.append(dataEntry)
        }
        
        let lineChartDataSet = LineChartDataSet(yVals: dataEntriesPointChart, label: "Chemistry")
        let lineChartData = LineChartData(xVals: dataPoints, dataSet: lineChartDataSet)
        lineChartView.data = lineChartData
    }
    
    func changeChartViews(chartStr:String){
        print("Chart String is \(chartStr)")
        
        switch chartStr {
        case "Line":
            self.barChartView.hidden = false
            self.lineChartView.hidden = true
            setBarChart(months, values: temps)
            break
        case "Points":
            print("Type is point / line")
            self.barChartView.hidden = true
            self.lineChartView.hidden = false
            setLineChart(months, values: temps)
            break
        case "Both":
            print("Type is graph/both")
            break
        default:
            print("Type is something else")
            break
        }
        
         setBarChart(months, values: temps)
         setLineChart(months, values: temps)
    }

    @IBAction func historyTypeDropdownButtonPressed(sender: UIButton?) {
        self.showhideHistoryTypeDropdown()
    }
    
    @IBAction func showTemperatureCharts(){
        barChartView.hidden = false
        print("showing temperature")
        // show labels for Temperature and set charts to Temperature type
    }
    
    
    @IBAction func showChemistryCharts(){
        self.barChartView.hidden = false
         print("showing chemistry")
        // show labels for Chemistry and set charts to Chemistry type
    }
    

    func showhideHistoryTypeDropdown() {
        self.historyTypeSelectionView.hidden = false
        
       // if(self.chemistryViewIsDisplayed)
      //  {
          //  self.temperatureView.hidden = true
      //  }
        
     //   if (self.temperatureViewIsDisplayed) {
            // self.hideView()
     //   } else {
        //    self.chemistryView.hidden = true()
      //  }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didSelectButton(aButton: UIButton?) {
        self.chartType = (aButton?.titleLabel!.text)
        changeChartViews(self.chartType)
    }
    
    func chartValueSelected(chartView: ChartViewBase, entry: ChartDataEntry, dataSetIndex: Int, highlight: ChartHighlight) {
        print("\(entry.value) in \(months[entry.xIndex])")
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
