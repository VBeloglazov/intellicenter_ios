//
//  HomeBodyTableViewCell.swift
//  Pentair-iOs
//
//  Created by Dashon Howard on 5/18/16.
//  Copyright © 2016 Pentair Corporation. All rights reserved.
//

import UIKit

class HomeBodyTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var FreezeProtectionLabel: UILabel!
    @IBOutlet weak var LastTempLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
