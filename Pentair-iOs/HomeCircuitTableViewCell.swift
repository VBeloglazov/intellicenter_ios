//
//  HomeCircuitTableViewCell.swift
//  Pentair-iOs
//
//  Created by Dashon Howard on 5/18/16.
//  Copyright © 2016 Pentair Corporation. All rights reserved.
//

import UIKit
class HomeCircuitTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var countLabel: UILabel!
    
    @IBOutlet weak var countView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
