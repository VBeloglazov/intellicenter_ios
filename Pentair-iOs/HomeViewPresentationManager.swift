//
//  HomeViewPresentationManager
//  Pentair-iOs
//
//  Created by Jim Hewitt on 1/9/16.
//  Copyright © 2016 Pentair Corporation. All rights reserved.
//

import Foundation
import UIKit

class HomeViewPresentationManager: UIPercentDrivenInteractiveTransition, UIViewControllerAnimatedTransitioning, UIViewControllerTransitioningDelegate {

    private var presenting = false
    private var interactive = false
    private var enterPanGesture: UIScreenEdgePanGestureRecognizer!
    private var statusBarBackground: UIView!
    
    var sourceViewController: UIViewController! {
        didSet {
            self.enterPanGesture = UIScreenEdgePanGestureRecognizer()
            self.enterPanGesture.addTarget(self, action: #selector(HomeViewPresentationManager.handleOnstagePan(_:)))
            self.enterPanGesture.edges = UIRectEdge.Bottom
            self.sourceViewController .view.addGestureRecognizer(self.enterPanGesture)
            
            // create view behind status bar
            self.statusBarBackground = UIView()
            self.statusBarBackground.frame = CGRect(x: 0, y: 0, width: self.sourceViewController.view.frame.width, height: 20)
            self.statusBarBackground.backgroundColor = self.sourceViewController.view.backgroundColor
            
            // add to window instead of view controller
            UIApplication.sharedApplication().keyWindow!.addSubview(self.statusBarBackground)
        }
    }
    
    private var exitPanGesture: UIPanGestureRecognizer!
    
    var menuViewController: UIViewController! {
        didSet {
            self.exitPanGesture = UIPanGestureRecognizer()
          //  self.exitPanGesture.addTarget(self, action: Selector("handelOffstagePan:"))
            self.menuViewController.view.addGestureRecognizer(self.exitPanGesture)
        }
    }
    
    func handleOnstagePan(pan: UIPanGestureRecognizer) {
        let transition = pan.translationInView(pan.view!)
        let d = transition.x / CGRectGetHeight(pan.view!.bounds) * 0.5
        switch (pan.state) {
            
        case UIGestureRecognizerState.Began:
            self.interactive = true
            self.sourceViewController.performSegueWithIdentifier("presentWeather", sender: self)
            break
            
        case UIGestureRecognizerState.Changed:
            self.updateInteractiveTransition(d)
            break
            
        default:
            self.interactive = false
            if (d > 0.2) {
                self.finishInteractiveTransition()
            } else {
                self.cancelInteractiveTransition()
            }
        }
    }
    
    func handleOffstagePan(pan: UIPanGestureRecognizer) {
        let transition = pan.translationInView(pan.view!)
        let d = transition.x / CGRectGetHeight(pan.view!.bounds) * -0.5
        
        switch (pan.state) {
            
        case UIGestureRecognizerState.Began:
            self.interactive = true
            self.menuViewController.performSegueWithIdentifier("dismissWeather", sender: self)
            break
            
        case UIGestureRecognizerState.Changed:
            self.updateInteractiveTransition(d)
            break
            
        default:
            self.interactive = false
            if (d > 0.1) {
                self.finishInteractiveTransition()
            } else {
                self.cancelInteractiveTransition()
            }
        }
    }
    
    // MARK: UIViewControlerAnimatedTransitioning protocol methods
    
    // animage a change between viewControllers
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        // get a reference to the from view, the to view and the container view to perform the transition on
        let container = transitionContext.containerView()
        
        // create a tuple of our screens
        let screens : (from:UIViewController, to:UIViewController) = (transitionContext.viewControllerForKey (UITransitionContextFromViewControllerKey)!, transitionContext.viewControllerForKey (UITransitionContextToViewControllerKey)!)
        
        // assign references to the menu view controller and the 'bottom' view controller from the tuple
        // the menuViewControler will alternate between the from and two view contoller depending on the presenting / dismissing state
        let menuViewController = !self.presenting ? screens.from as! MenuViewController : screens.to as! MenuViewController
        let topViewController = !self.presenting ? screens.to as UIViewController : screens.from as UIViewController
        let menuView = menuViewController.view
        let topView = topViewController.view
        
        // prepare menu items to slide
        if (self.presenting) {
            self.offStageMenuControllerInteractive(menuViewController)
            topView.transform = self.offStage(290)
        }
        
        // add both views to our view controller
        container!.addSubview(menuView)
        container!.addSubview(topView)
        container!.addSubview(self.statusBarBackground)
        
        let duration = self.transitionDuration(transitionContext)
        
        // perform the animation
        UIView.animateWithDuration(duration, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.8, options: UIViewAnimationOptions.AllowUserInteraction, animations: {
            if (self.presenting) {
                self.onStageMenuController(menuViewController)
                topView.transform = self.offStage(290)
            } else {
                topView.transform = CGAffineTransformIdentity
                self.offStageMenuControllerInteractive(menuViewController)
            }
            }, completion: { finished in
                
                // tell the transitionContext object that we are done
                if (transitionContext.transitionWasCancelled()) {
                    transitionContext.completeTransition(false)
                    UIApplication.sharedApplication().keyWindow!.addSubview(screens.to.view)
                }
                UIApplication.sharedApplication().keyWindow!.addSubview(self.statusBarBackground)
        })
    }
    
    func offStage(amount: CGFloat) -> CGAffineTransform {
        return CGAffineTransformMakeTranslation(amount, 0)
    }

    func offStageMenuControllerInteractive(viewController: MenuViewController) {
        menuViewController.view.alpha = 0
        self.statusBarBackground.backgroundColor = self.sourceViewController.view.backgroundColor
        
        // set up 2D transitions for animation
        let offstageOffset: CGFloat = -200
        
        for view in menuViewController.view.subviews {
            view.transform = self.offStage(offstageOffset)
        }
    }
    
    func onStageMenuController(menuViewController: MenuViewController){
        
        // prepare menu to fade in
        menuViewController.view.alpha = 1
        self.statusBarBackground.backgroundColor = UIColor.blackColor()
        
        for view in menuViewController.view.subviews {
                view.transform = CGAffineTransformIdentity
        }
    }
    
    // Return the time the transition animation will take
    func transitionDuration (transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return 0.5
    }
    
    // MARK: UIViewControllerTransitioningDelegate protocol methods
    
    // return the animator when presenting a viewcontroller
    // this must conform to UIViewControllerAnimatedTransitioning protocol
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.presenting = true
        return self
    }
    
    // return the animator used when dismissing from the viewcontroller
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        self.presenting = false
        return self
    }
    
    func interactionContollerForPresentation(animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return self.interactive ? self : nil
    }
    
    func interactionControllerForDismissal(animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return self.interactive ? self : nil
    }
}

