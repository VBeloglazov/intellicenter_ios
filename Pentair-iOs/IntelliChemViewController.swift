//
//  IntelliChemViewController.swift
//  Pentair-iOs
//
//  Created by Dashon Howard on 1/26/16.
//  Copyright © 2016 Pentair Corporation. All rights reserved.
//

import Foundation

import UIKit
import Pentair

class IntelliChemViewController: UIViewController {
    
    var property: Property?
    var objNam:String!
    
    @IBOutlet weak var orpTankValue: UILabel!
    @IBOutlet weak var orpLevelValue: UILabel!
    @IBOutlet weak var orpTank: VerticalProgressView!
    @IBOutlet weak var orpLevel: GradientLevelView!
    
    @IBOutlet weak var pHTankValue: UILabel!
    @IBOutlet weak var pHLevelValue: UILabel!
    @IBOutlet weak var pHTank: VerticalProgressView!
    @IBOutlet weak var PhLevel: GradientLevelView!
    
    @IBOutlet weak var saltLevelValue: UILabel!
    @IBOutlet weak var saltLevelWarning: UILabel!
    @IBOutlet weak var saltLevelWarningIcon: UIImageView!
    
    @IBOutlet weak var waterBalance: UILabel!
    
    @IBOutlet weak var iChlorContainer: UIView!
    @IBOutlet weak var IntelleChemViewContainer: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(IntelliChemViewController.statusChange(_:)), name: "ConfigurationUpdate", object: property)
    }
    override func viewDidAppear(animated: Bool) {
        setValues()
    }
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func setValues(){
        if let controlObject = property?.configurationModel.getItem(objNam){
            
            if let oLevel = controlObject["ORPVAL"] as? NSString{
                orpLevelValue.text = oLevel.description
                
                orpLevel.max = 999
                orpLevel.min = 0
                orpLevel.setChemLevel(oLevel.floatValue)
            }
            if let oTankVal = controlObject["ORPTNK"] as? NSString{
                orpTankValue.text = oTankVal.description + "%"
                orpTank.setProgressLevel(oTankVal.integerValue)
            }
            if let pLevel = controlObject["PHVAL"] as? NSString{
                pHLevelValue.text = pLevel.description
                
                PhLevel.max = 10
                PhLevel.min = 4
                PhLevel.setChemLevel(pLevel.floatValue)
            }
            if let pTankVal = controlObject["PHTNK"] as? NSString{
                pHTankValue.text = pTankVal.description + "%"
                pHTank.setProgressLevel(pTankVal.integerValue)
            }
            
            if let saltVal = controlObject["SALT"] as? NSString{
                saltLevelValue.text = saltVal.description
                saltLevelWarning.text = "Very\nLow"
                saltLevelWarningIcon.hidden = false
            }
        }
    }
    
    func statusChange(notification: NSNotification) {
        setValues()
    }
    
    // Hide the keyboard when not needed
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (objNam == nil) { return }
        
        if let identifier = segue.identifier {
            switch identifier {
            case "alkStepper":
                let controller = segue.destinationViewController as! NumberStepperViewController
                controller.property = self.property
                controller.setUp(objNam, param: "ALK", min: 0, max: 999)
                break
            case "cyanStepper":
                let controller = segue.destinationViewController as! NumberStepperViewController
                controller.property = self.property
                controller.setUp(objNam, param: "CYACID", min: 0, max: 999)
                break
            case "phStepper":
                let controller = segue.destinationViewController as! NumberStepperViewController
                controller.property = self.property
                controller.setUp(objNam, param: "PHSET", min: 4, max: 10, increment: 0.1)
                break
            case "orpStepper":
                let controller = segue.destinationViewController as! NumberStepperViewController
                controller.property = self.property
                controller.setUp(objNam, param: "ORPSET", min: 0, max: 999, increment: 10)
                break
            case "calciumStepper":
                let controller = segue.destinationViewController as! NumberStepperViewController
                controller.property = self.property
                controller.setUp(objNam, param: "CALC", min: 0, max: 999)
                break
            default:
                break
            }
        }
    }
}