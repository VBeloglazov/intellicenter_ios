//
//  IntelliChlorViewController.swift
//  Pentair-iOs
//
//  Created by Dashon Howard on 2/2/16.
//  Copyright © 2016 Pentair Corporation. All rights reserved.
//

import Foundation
import UIKit
import Pentair

class IntelliChlorViewController: UIViewController {
    
    var property: Property?
    var objNam:String!
    var bodyOfWater:BodyOfWater!
    @IBOutlet weak var IntelliChlorContainer: UIView!
    @IBOutlet weak var saltLevelValue: UILabel!
    @IBOutlet weak var saltLevelWarning: UILabel!
    @IBOutlet weak var saltLevelWarningIcon: UIImageView!
    
    @IBOutlet weak var superChlorSwitch: UISwitch!
    @IBOutlet weak var parent_bodyImage: UIImageView!
    @IBOutlet weak var parent_label: UILabel!
    @IBOutlet weak var child_label: UILabel!
    @IBOutlet weak var child_bodyImage: UIImageView!
    
    @IBOutlet weak var childContainer: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
         NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(IntelliChlorViewController.statusChange(_:)), name: "ConfigurationUpdate", object: property)
    }
    override func viewDidAppear(animated: Bool) {
        setValues()
    }
    
    func setValues(){
        if let controlObject = property?.configurationModel.getItem(objNam){
            setBody(parent_bodyImage, body: bodyOfWater.getParent(), label: parent_label)
            setBody(child_bodyImage, body: bodyOfWater.getChild(), label: child_label)
            
            if let saltVal = controlObject["SALT"] as? String{
                saltLevelValue.text = saltVal
            }
            superChlorSwitch.on = isSuperChlorOn()
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func statusChange(notification: NSNotification) {
        setValues()
    }
    
    func setBody(imageView:UIImageView, body:NSMutableDictionary?, label:UILabel){
        if let finalBody = body{
            if let image = (finalBody["SUBTYP"] as? String) {
                imageView.image = UIImage(named: image.capitalizedString)
            }
            if let name = (finalBody["SNAME"] as? String) {
                label.text = "Output: " + name
            }
        }else{
            //remove child container
            if(body == bodyOfWater.getChild()){
                childContainer.removeFromSuperview()
            }
        }
    }
    
    // Hide the keyboard when not needed
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    @IBAction func switchToggled(sender: UISwitch) {
        let keyValues: [String: String] = ["SUPER": sender.on ? "ON" : "OFF"]
        let parameters = [KeyValue().setParametersObject(objNam, keyValues: keyValues)]
        
        self.property!.sendRequest(setParamList().buildSetParameters(parameters))
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (objNam == nil) { return }
        
        if let identifier = segue.identifier {
            switch identifier {
            case "parent_ichlor_stepper":
                let controller = segue.destinationViewController as! NumberStepperViewController
                controller.property = self.property
                controller.setUp(objNam, param: "PRIM", min: 0, max: 100)
                break
            case "child_ichlor_stepper":
                let controller = segue.destinationViewController as! NumberStepperViewController
                controller.property = self.property
                controller.setUp(objNam, param: "SEC", min: 0, max: 100)
                break
            case "superchlor_stepper":
                let controller = segue.destinationViewController as! NumberStepperViewController
                controller.property = self.property
                controller.setUp(objNam, param: "TIMOUT", min: 1, max: 72, increment: 1)
                break
            default:
                break
            }
        }
    }
    
    private func isSuperChlorOn() -> Bool {
        let status = property!.configurationModel.getParam(objNam, param: "SUPER")
        return status == "ON"
    }
}