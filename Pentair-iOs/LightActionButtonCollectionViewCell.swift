//
//  LightActionButtonCollectionViewCell.swift
//  Pentair-iOs
//
//  Created by Dashon Howard on 2/16/16.
//  Copyright © 2016 Pentair Corporation. All rights reserved.
//

import Foundation
import Pentair
protocol LightActionButtonCollectionViewDelegate: class {
    /// Loads the data for the view, returns the marshalled data object from the server
    func didPressAction(lightCode: LightCode)
}
class LightActionButtonCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var button: UIButton!
    weak var delegate: LightActionButtonCollectionViewDelegate?
    private var lightCode:LightCode?
    
    @IBAction func didTouch(sender: UIButton) {
        delegate?.didPressAction(lightCode!)
    }
    
    func setLightAction(code:LightCode){
        self.lightCode = code
        button.setTitle(lightCode?.rawValue, forState: .Normal)
    }
    
    func getLightCode()->LightCode?{
        return lightCode
    }
    
}