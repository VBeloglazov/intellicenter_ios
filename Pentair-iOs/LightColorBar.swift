//
//  LightColorBar.swift
//  Pentair-iOs
//
//  Created by Dashon Howard on 2/6/16.
//  Copyright © 2016 Pentair Corporation. All rights reserved.
//

import Foundation
import Pentair
import UIKit


public class LightColorBar : NibDesignable {
    
    @IBOutlet weak var colorBar: UIView!
    @IBOutlet weak var rightImage: UIImageView!
    @IBOutlet weak var leftImage: UIImageView!
    @IBInspectable public var gradientColors =  [UIColor.blueColor(),UIColor.brownColor()]
    
    @IBOutlet weak var colorLabel: UILabel!
    
    var gradient = CALayer()
    private var currentLightMode = LightMode(lightCode: .NONE)
    
    private var cornerRadius:CGFloat = 20.0
    private var secondaryColor = UIColor.whiteColor()
    private var borderColor = UIColor.clearColor()
  
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        self.backgroundColor = UIColor.clearColor()
        self.colorBar.backgroundColor = UIColor.clearColor()
        setLayer()
    }
    
    override public func prepareForInterfaceBuilder() {
        setLightMode(currentLightMode)
    }
    
    public func setLayer(){
        self.rightImage.tintColor = self.secondaryColor
        self.leftImage.tintColor = self.secondaryColor
        self.colorLabel.textColor = self.secondaryColor
        
        self.layer.masksToBounds = true
        gradient = CALayer()
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bounds
        gradientLayer.borderColor = self.borderColor.CGColor
        gradientLayer.borderWidth = 1
        gradientLayer.frame.origin = CGPointMake(0.0,0.0)
        gradientLayer.colors = self.gradientColors.map{$0.CGColor}
        gradientLayer.cornerRadius = self.cornerRadius
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        //TODO: Remove the explicit layer, not simply the first one
        if(self.colorBar.layer.sublayers?.count == 4){
            self.colorBar.layer.sublayers![0].removeFromSuperlayer()
        }
        self.gradient.insertSublayer(gradientLayer, atIndex: 0)
        self.colorBar.layer.insertSublayer(self.gradient, atIndex: 0)
    }
    
    public func setLightMode(newLightMode:LightMode, withRoundedCorners:Bool = true){
        self.currentLightMode = newLightMode
        if newLightMode.colors.count == 1{
            newLightMode.colors.append(newLightMode.colors[0])
        }
        self.gradientColors = newLightMode.colors
        self.secondaryColor = newLightMode.secondaryColor
        self.borderColor = newLightMode.boarderColor
        self.colorLabel.text = newLightMode.code.rawValue
        
        self.cornerRadius = withRoundedCorners ? 20.0:0.0
        
        setLayer()
    }
    public func setDimmer(value:Int?){
        setLightMode(LightMode(lightCode:LightCode.DIMMER))
        if let value = value{
        self.colorLabel.text = self.colorLabel.text! + value.description + "%"
        }else{
            self.colorLabel.text = self.colorLabel.text! + " --%"
        }
    }
    public func getLightMode()->LightMode?{
        return currentLightMode
    }
    
}