//
//  LightColorListViewController.swift
//  Pentair-iOs
//
//  Created by Dashon Howard on 2/12/16.
//  Copyright © 2016 Pentair Corporation. All rights reserved.
//

import Foundation
import UIKit
import QuartzCore
import Pentair

class LightColorListViewController: UITableViewController {
    var property: Property?
    var objNam: String!
    
    var lightColors:[LightMode]?
    var lights:[AnyObject]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LightColorListViewController.statusChange(_:)), name: "ConfigurationUpdate", object: property)
    }
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 50
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lightColors!.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let lightCell: LightColorTableViewCell = tableView.dequeueReusableCellWithIdentifier("LightColorTableViewCell", forIndexPath: indexPath) as! LightColorTableViewCell
        lightCell.lightColorBar.leftImage.image = UIImage(named:"check")
        
        lightCell.lightColorBar.rightImage.hidden = true
        lightCell.lightColorBar.leftImage.hidden = true
        
        lightCell.lightColorBar.setLightMode(lightColors![indexPath.row],withRoundedCorners: false)
        
        let actParam = self.property!.configurationModel.getParam(objNam, param: "ACT")
        let lightCode = LightCode(enumKey:actParam)
        
        if (lightColors![indexPath.row].code == lightCode){
            lightCell.lightColorBar.leftImage.hidden = false
        }
        
        return lightCell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let lightCode = lightColors![indexPath.row].code {
            let keyValues: [String: String] = ["ACT": String(lightCode)]
            let parameters = [KeyValue().setParametersObject(objNam!, keyValues: keyValues)]
            
            self.property!.sendRequest(setParamList().buildSetParameters(parameters))
            navigationController?.popViewControllerAnimated(true)
        }
    }
    
    @IBAction func didTouchCancel(sender: AnyObject) {
        navigationController?.popViewControllerAnimated(true)
    }
    
    func statusChange(notification: NSNotification) {
        self.tableView.reloadData()
    }
    
}
