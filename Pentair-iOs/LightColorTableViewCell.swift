//
//  LightColorTableViewCell.swift
//  Pentair-iOs
//
//  Created by Dashon Howard on 2/16/16.
//  Copyright © 2016 Pentair Corporation. All rights reserved.
//

import Foundation
import UIKit

class LightColorTableViewCell: UITableViewCell {
    var objnam: String?
    
    @IBOutlet weak var lightColorBar: LightColorBar!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
