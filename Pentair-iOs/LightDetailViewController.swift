//
//  LightDetailViewController.swift
//  Pentair-iOs
//
//  Created by Dashon Howard on 2/16/16.
//  Copyright © 2016 Pentair Corporation. All rights reserved.
//

import Foundation
import Pentair

class LightDetailViewController: UIViewController,  UICollectionViewDelegate, UICollectionViewDataSource, LightActionButtonCollectionViewDelegate{
    var property: Property?
    var objNam: String?
    
    var syncSwimActions = [LightMode]()
    var lightActions = [LightMode]()
    var lightColors = [LightMode]()
    @IBOutlet weak var leftDimmerImage: UIImageView!
    @IBOutlet weak var dimmerControlContainer: UIView!
    @IBOutlet weak var dimmerControl: TGPDiscreteSlider!
    @IBOutlet weak var rightDimmerImage: UIImageView!
    @IBOutlet weak var groupLightTableView: UITableView!
    @IBOutlet weak var lightColorBar: LightColorBar!
    @IBOutlet weak var syncSwimCollectionView: UICollectionView!
    @IBOutlet weak var lightActionCollectionView: UICollectionView!
    
    @IBAction func lightClicked(sender: UITapGestureRecognizer) {
        performSegueWithIdentifier("showLightColorOptions", sender: self)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        setModes()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(groupLightTableView != nil){
            groupLightTableView.removeFromSuperview()
        }
        rightDimmerImage.tintColor = UIColor.lightGrayColor()
        leftDimmerImage.tintColor = UIColor.lightGrayColor()
        dimmerControl.addTarget(self, action: #selector(LightDetailViewController.valueChanged(_:event:)), forControlEvents: [.TouchUpInside , .TouchUpOutside])

        setModes()
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LightDetailViewController.statusChange(_:)), name: "ConfigurationUpdate", object: property)
    }
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    // MARK: - UICollectionViewDataSource
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var count = 0
        if (collectionView.isEqual(syncSwimCollectionView)){
            count = syncSwimActions.count
        }else if (collectionView.isEqual(lightActionCollectionView)){
            count = lightActions.count
            collectionView.frame.size.height = 105
        }
        
        return count
    }
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell: LightActionButtonCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("actionButtonCell", forIndexPath: indexPath) as! LightActionButtonCollectionViewCell
        cell.delegate = self
        var lightAction:LightMode?
        
        if (collectionView.isEqual(syncSwimCollectionView)){
            lightAction = syncSwimActions[indexPath.row]
        }else if (collectionView.isEqual(lightActionCollectionView)){
            lightAction = lightActions[indexPath.row]
        }
        
        cell.setLightAction(lightAction!.code!)
        
        return cell
    }
    
    private func setModes(){
        lightColors.removeAll()
        lightActions.removeAll()
        syncSwimActions.removeAll()
        if let light = property?.configurationModel.getItem(objNam!){
            self.title = light["SNAME"] as? String
            if let usageFlags = light["USAGE"] as? String{
                for flag in usageFlags.characters {
                    let lightMode = LightMode(key:flag)
                    if(flag == "c" || flag == "s"){
                        syncSwimActions.append(lightMode)
                    }else if(lightMode.isColor){
                        lightColors.append(lightMode)
                    }else if(LightCode.DIMMER != lightMode.code){
                        lightActions.append(lightMode)
                    }
                }
            }
            if (property!.configurationModel.hasColorCapabilities(objNam!)){
                if let actValue = light["USE"] as? String{
                    let lightCode = LightCode(enumKey: actValue)
                    lightColorBar.setLightMode(LightMode(lightCode: lightCode!))
                }else{
                    lightColorBar.setLightMode(LightMode(lightCode: .NONE))
                }
                
                if(dimmerControlContainer != nil){
                    dimmerControlContainer.removeFromSuperview()
                }
            }else if (property!.configurationModel.isDimmable(objNam!)){
                if(lightColorBar != nil){
                    lightColorBar.removeFromSuperview()
                }
                
                if let subType = light["SUBTYP"] as? String{
                    switch subType{
                    case "DIMMER":
                        dimmerControl.minimumValue = 30
                        dimmerControl.incrementValue = 10
                        dimmerControl.tickCount = 8
                        break
                    case "GLOWT":
                        dimmerControl.minimumValue = 50
                        dimmerControl.incrementValue = 25
                        dimmerControl.tickCount = 3
                        break
                    default:
                        dimmerControl.minimumValue = 50
                        dimmerControl.incrementValue = 25
                        dimmerControl.tickCount = 3
                        break
                    }
                }
                if let dimmerValue = light["LIMIT"] as? NSString{
                    dimmerControl.value = CGFloat(dimmerValue.floatValue)
                }else{
                    dimmerControl.value = dimmerControl.minimumValue
                }
            }else{
                
                if(lightColorBar != nil){
                    lightColorBar.removeFromSuperview()
                }
                if(dimmerControlContainer != nil){
                    dimmerControlContainer.removeFromSuperview()
                }
            }
        }
    }
    
    func didPressAction(lightCode: LightCode){
        let keyValues: [String: String] = ["ACT": String(lightCode)]
        let parameters = [KeyValue().setParametersObject(objNam!, keyValues: keyValues)]
        
        self.property!.sendRequest(setParamList().buildSetParameters(parameters))
    }
    
    func statusChange(notification: NSNotification) {
        setModes()
        lightActionCollectionView.reloadData()
        syncSwimCollectionView.reloadData()
        if (groupLightTableView != nil){
            groupLightTableView.reloadData()
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "showLightColorOptions"){
            let controller = segue.destinationViewController as! LightColorListViewController
            controller.property = self.property
            controller.lightColors = self.lightColors
            controller.objNam = self.objNam
        }
    }
    
    func valueChanged(sender: TGPDiscreteSlider, event:UIEvent) {
        
        let keyValues: [String: String] = ["LIMIT": String(Int(sender.value))]
        let parameters = [KeyValue().setParametersObject(objNam!, keyValues: keyValues)]
        
        self.property!.sendRequest(setParamList().buildSetParameters(parameters))
    }
    
}