//
//  LightTableViewCellProtocol.swift
//  Pentair-iOs
//
//  Created by Dashon Howard on 2/17/16.
//  Copyright © 2016 Pentair Corporation. All rights reserved.
//

import Foundation
import Pentair

protocol LightTableViewCellProtocol {
    var objNam:String? { get set }
    
    func getLightMode() -> LightMode?
    func setLightMode(mode:LightMode)
}