//
//  NavigationController.swift
//  Pentair-iOS
//
//  Created by Jim Hewitt on 12/12/14.
//  Copyright (c) 2014 Pentair Corporation. All rights reserved.
//

import UIKit
import CoreData

class NavigationController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
