//
//  NotificationsViewController.swift
//  Pentair-iOs
//
//  Created by pentair developer on 7/20/16.
//  Copyright © 2016 Pentair Corporation. All rights reserved.
//

import UIKit
import Pentair
import CoreData

class NotificationsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var property:Property?
    var notificationsArray = [UserNotification]()
    var messagesArray = [StatusMessage]()
    var filteredMessagesArray = [StatusMessage]()
    var statusMessages :[AnyObject]?
    var configModel:ConfigurationModel?
    
    @IBOutlet weak var notificationsTableView: UITableView!
    
    var notificationArray: [NSDictionary] = []

    override func viewDidLoad() {
        super.viewDidLoad()
         notificationArray = property!.configurationModel.StatusMessages
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(NotificationsViewController.refresh(_:)), name: "ConfigurationUpdate", object: property)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(NotificationsViewController.statusChange(_:)), name: "refresh", object: property)
    }
    
    override func viewDidAppear(animated: Bool) {
         property?.getStatusMessages()
    }
    
    
    func refresh(notification: NSNotification){
        print("received the notification of refresh")
        print("check for status messages")
    }
    
    func fetchMessageWithName(targetMessageName:String) -> StatusMessage {
        var fetchedObject:StatusMessage
        print("Target Message Name is \(targetMessageName)")
        
        let tmpStatusMessages = self.fetchMessagesFromLocalCache()
        
        if let i = tmpStatusMessages.indexOf({$0.objectName == targetMessageName}) {
            fetchedObject = tmpStatusMessages[i]
        } else {
           fetchedObject = StatusMessage.MR_createEntity() as! StatusMessage
        }
        
        return fetchedObject
    }

        func fetchMessagesFromLocalCache() -> [StatusMessage] {
            var tmpMessagesArray = [StatusMessage]()
            let managedObjectContext = NSManagedObjectContext .MR_defaultContext()
            let fetchRequest = NSFetchRequest(entityName: "StatusMessage")
            
            do {
                let results = try managedObjectContext.executeFetchRequest(fetchRequest)
                tmpMessagesArray = results as! [StatusMessage]
                //log debug
                for message in tmpMessagesArray {
                    print("message \(message.sname)")
                }
            } catch let error as NSError {
                print("could not fetch local message objects")
            }
            return tmpMessagesArray
        }
    
//    func fetchStatusMessages() {
//        statusMessages = (property?.configurationModel.getAllStatusMessages())!
//        print("statusMessages are:  \(statusMessages)")
//
//        
//        //property!.sendRequest(setParamList().buildSetParameters(parameters))
//    }
    
        
    // TableView delegate methods
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell:UITableViewCell?
        
        cell = tableView.dequeueReusableCellWithIdentifier("statusMessageCell", forIndexPath: indexPath)
        
        let thisMessage = self.notificationArray[indexPath.row]
        
        cell!.textLabel!.font = UIFont(name:"Ariel", size:22)
        cell!.textLabel!.text = thisMessage["SNAME"] as? String

        return cell!
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notificationArray.count
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
    
    func statusChange(notification: NSNotification) {
            print("received the notification of a change in config")
        let userinfo = notification.userInfo
        let message  = userinfo!["statusMessages"] as? String
    }
    
   
   func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == UITableViewCellEditingStyle.Delete {
            self.notificationArray.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        
    }
    
    
//    func fetchStatusMessagesArray() -> [StatusMessage]{
//        var tmpMessages:[AnyObject]
//        var messagesArray:[StatusMessage] = []
//        var cachedMessages:[StatusMessage]
//        
//        cachedMessages = fetchMessagesFromLocalCache()
//        if(cachedMessages.count > 0){
//            messagesArray = cachedMessages
//        } else {
//            tmpMessages = (property?.configurationModel.getAllStatusMessages())!
//            //print("tmpCircuits (pull from the socket) count is: \(tmpCircuits.count)")
//            //print("dump of tmpCircuits from config model \(dump(tmpCircuits))")
//            
//            for message in tmpMessages {
//                
//                let thisMessage = StatusMessage.MR_createEntity() as! StatusMessage
//                
//                thisMessage.objectName = message["OBJNAM"] as? String
//                thisMessage.objectRevision = message["OBJREV"] as? String
//                thisMessage.objectType = message["OBJTYP"] as? String
//                thisMessage.parent = message["PARENT"] as? String
//                thisMessage.mode = message["MODE"] as? String
//                thisMessage.sname = message["SNAME"] as? String
//                thisMessage.count = message["COUNT"] as? String
//                thisMessage.party = message["PARTY"] as? String
//                thisMessage.sindex = message["SINDEX"] as? String
//                thisMessage.time = message["TIME"] as? String
//                thisMessage.alarm = message["ALARM"] as? String
//                //thisMessage.shomnu = message["SHOMNU"] as? String
//                //not currently in model, add if needed
//          
//            NSManagedObjectContext .MR_defaultContext() .MR_saveToPersistentStoreAndWait()
//                messagesArray.append(thisMessage)
//            }
//        }
//        return messagesArray
//    }
}

