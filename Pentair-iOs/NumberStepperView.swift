//
//  NumberStepperView.swift
//  Pentair-iOs
//
//  Created by Dashon Howard on 2/2/16.
//  Copyright © 2016 Pentair Corporation. All rights reserved.
//

import Foundation
import Pentair

@IBDesignable
class NumberStepperView: UIView {
    weak var delegate: NumberStepperViewDelegate?
    var min:Float = 0
    var max:Float = 100
    var increment:Float = 1
    var isSuperChlor = false
    var superChlorRemaining = 0
    
    var property: Property?
    var objNam:String?
    var param:String?
    var buttonColor = UIColor(red: 0.102, green: 0.298, blue: 0.553, alpha: 1) /*#1a4c8d*/
    var buttonLabelColor = UIColor.whiteColor()
    var labelColor = UIColor.blackColor()
    var superChlorTimer = NSTimer()
    
    @IBOutlet weak var label: UIButton!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var plusButton: UIButton!
    @IBOutlet weak var minusButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(NumberStepperView.statusChange(_:)), name: "ConfigurationUpdate", object: property)
        
    }
    func start(){
        label.hidden = false
        textField.hidden = true
        
        //setColors
        label.setTitleColor(labelColor, forState: .Normal)
        textField.textColor = labelColor
        plusButton.setTitleColor(buttonLabelColor, forState: .Normal)
        minusButton.setTitleColor(buttonLabelColor, forState: .Normal)
        plusButton.backgroundColor = buttonColor
        minusButton.backgroundColor = buttonColor
        
        addDoneButton()
        if let controlObject = property!.configurationModel.getItem(self.objNam!){
            if let subType = controlObject["SUBTYP"] as? String {
                if (param == "TIMOUT" && subType == "ICHLOR"){
                    isSuperChlor = true
                }
            }
        }
        
        setValue(getVal())
    }
    
    func statusChange(notification: NSNotification) {
        setValue(getVal())
    }
    
    func getVal() -> String{
        return property!.configurationModel.getParam(objNam!, param: param!)
    }
    
    func setValue(value: String) {
        if let num = Double(value){
            if (isSuperChlor) {
                if (isSuperChlorOn()) {
                    setSuperChlorTimer(Double(Int(num)))
                } else {
                    let hour = (Int(num) / (60 * 60))
                    label.setTitle(hour.description + " HOURS", forState: .Normal)
                    textField.text = hour.description
                    stopTimer()
                }
            } else if (param == "PRIM" || param == "SEC") {
                label.setTitle(Int(num).description + " %", forState: .Normal)
                textField.text = Int(num).description
            } else if (param == "PHSET") {
                let formattedNam = String.localizedStringWithFormat("%.01f", num)
                label.setTitle(formattedNam, forState: .Normal)
                textField.text = formattedNam
            } else {
                textField.text = Int(num).description
                label.setTitle(Int(num).description, forState: .Normal)
            }
        }else{
            textField.text = "--"
            label.setTitle("--", forState: .Normal)
        }
    }
    internal func timerTick(){
        if (isSuperChlorOn()) {
            superChlorRemaining -= 1
            if (superChlorRemaining <= 0){timeFinished()}
            label.setTitle(getCountDownString(superChlorRemaining), forState: .Normal)
            setEnabled(false)//TODO:is this redundant?
        } else {
            stopTimer()
        }
        
    }
    private func stopTimer(){
        superChlorTimer.invalidate()
        setEnabled(true)
        superChlorRemaining = 0
    }
    private func timeFinished() {
        if (isSuperChlorOn()) {
            label.setTitle(superChlorTimer.userInfo as? String, forState: .Normal)
            setEnabled(false)//TODO:is this redundant?
        }
        stopTimer()
        
    }
    private func setSuperChlorTimer(startValue:Double) {
        label.hidden = false
        textField.resignFirstResponder()
        textField.hidden = true
        
        stopTimer()
        superChlorRemaining = Int(startValue)
        superChlorTimer = NSTimer(timeInterval: 1.0, target: self, selector: #selector(NumberStepperView.timerTick), userInfo: "Complete", repeats: true)
        
        NSRunLoop.currentRunLoop().addTimer(superChlorTimer, forMode: NSRunLoopCommonModes)
    }
    
    func getCountDownString(milliseconds:Int) -> String {
        // let ms = Int((milliseconds % 1) * 1000)
        let seconds = milliseconds % 60
        let minutes = (milliseconds / 60) % 60
        let hours = (milliseconds / 3600)
        
        return String(format: "%0.2dH %0.2dM %0.2dS",hours,minutes,seconds)
    }
    
    private func isSuperChlorOn() -> Bool {
        let status = property!.configurationModel.getParam(objNam!, param: "SUPER")
        return status == "ON"
    }
    
    func setEnabled(enabled:Bool){
        self.plusButton.enabled = enabled
        self.label.enabled = enabled
        self.minusButton.enabled = enabled
        self.userInteractionEnabled = enabled
    }
    
    @IBAction func plusButtonTouched(sender: UIButton) {
        if let currentVal = Float(textField.text!){
            let rounded = roundToIncrement(currentVal)
            sendUpdate(rounded + increment)
        }
    }
    
    @IBAction func minusButtonTouched(sender: UIButton) {
        if let currentVal = Float(textField.text!){
            let rounded = roundToIncrement(currentVal)
            sendUpdate(rounded - increment)
        }
    }
    
    @IBAction func editingDidEnd(sender: UITextField) {
        textField.resignFirstResponder()
        
        if var currentVal = Float(textField.text!){
            currentVal =  roundToIncrement(currentVal)
            sendUpdate(currentVal)
        }
        label.hidden = false
        textField.hidden = true
    }
    
    @IBAction func labelClicked(sender: UIButton) {
        label.hidden = true
        textField.hidden = false
        textField.becomeFirstResponder()
    }
    private func sendUpdate(value: Float){
        var newValue = checkRange(value)
        
        if (isSuperChlor) {
            newValue = newValue * (60 * 60)
            if (isSuperChlorOn()){return}
        }
        
        delegate?.numberStepperValueChanged(self,value: newValue.description)
    }
    
    private func checkRange(value: Float) -> Float{
        var newValue = value
        if (value < min) {
            newValue = min
        }
        if (value > max) {
            newValue = max
        }
        return newValue
    }
    
    private func roundToIncrement(value: Float) -> Float{
        return round(value / increment) * increment
    }
    
    func addDoneButton() {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .FlexibleSpace,
            target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(barButtonSystemItem: .Done,
            target: self, action: #selector(NumberStepperView.editingDidEnd(_:)))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        textField.inputAccessoryView = keyboardToolbar
    }
}