//
//  NumberStepperViewController.swift
//  Pentair-iOs
//
//  Created by Dashon Howard on 1/26/16.
//  Copyright © 2016 Pentair Corporation. All rights reserved.
//

import Foundation

import UIKit
import Pentair
typealias DoneBlock = (data: AnyObject) -> Void
protocol NumberStepperViewDelegate: class {
    /// Loads the data for the view, returns the marshalled data object from the server
    func numberStepperValueChanged(view: NumberStepperView, value: String)
}
@IBDesignable
class NumberStepperViewController: UIViewController,NumberStepperViewDelegate {
    
    var property: Property?
    var objNam:String?
    var param:String!
    var controlObject: NSMutableDictionary?
    
    
    var numberStepperView: NumberStepperView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
  
    func setUp(objNam:String, param:String, min:Float = 0, max:Float = 100, increment:Float = 1, whiteTheme:Bool=false){
        self.objNam = objNam
        self.param = param
        
        controlObject = (property!.configurationModel.getItem(self.objNam!))
        numberStepperView = self.view as? NumberStepperView
        
        numberStepperView.delegate = self
        numberStepperView.min = min
        numberStepperView.max = max
        numberStepperView.increment = increment
        numberStepperView.objNam = objNam
        numberStepperView.property = property
        numberStepperView.param = param
        
        if (whiteTheme){
            numberStepperView.buttonColor = UIColor.whiteColor()
            numberStepperView.buttonLabelColor = UIColor(red: 0.102, green: 0.298, blue: 0.553, alpha: 1) /*#1a4c8d*/
            numberStepperView.labelColor = UIColor.whiteColor()
        }
        
        numberStepperView.start()
    }
    
    func sendUpdate(newValue:String){
        let keyValues: [String: String] = [param: newValue]
        let parameters = [KeyValue().setParametersObject(objNam!, keyValues: keyValues)]
        
        property!.sendRequest(setParamList().buildSetParameters(parameters))
    }
    func numberStepperValueChanged(view: NumberStepperView, value: String) {
        sendUpdate(value)
    }
}