//
//  ObjectName.swift
//  Pentair-iOS
//
//  Created by Dashon Howard on 12/22/14.
//  Copyright (c) 2014 Pentair Corporation. All rights reserved.
//

import Foundation
enum ObjectName :String{
    case OBJECT_NAME_ALL = "ALL"
    case OBJECT_NAME_SYS_PREFS = "_5451"
}