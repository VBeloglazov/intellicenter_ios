//
//  PentairButton.swift
//  Pentair-iOs
//
//  Created by Dashon Howard on 2/5/16.
//  Copyright © 2016 Pentair Corporation. All rights reserved.
//

import Foundation

@IBDesignable
class PentairButton : UIButton {
    
    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        self.layer.cornerRadius = 5
        self.layer.masksToBounds = true
    }
    
}