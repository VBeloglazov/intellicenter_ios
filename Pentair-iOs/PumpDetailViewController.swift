//
//  PumpDetailViewController.swift
//  Pentair-iOs
//
//  Created by CS on 05/27/16.
//  Copyright © 2016 Pentair Corporation. All rights reserved.
//

import Foundation
import Pentair

// not currently being used

class PumpDetailViewController: UIViewController,  UICollectionViewDelegate, UICollectionViewDataSource {
    var property: Property?
    var objNam: String?
    
    @IBOutlet weak var pumpActionCollectionView: UICollectionView!
    
    
    @IBAction func pumpClicked(sender: UITapGestureRecognizer) {
        performSegueWithIdentifier("showPumpOptions", sender: self)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pumpActionCollectionView.registerClass(UICollectionViewCell.self, forCellWithReuseIdentifier: "Cell")
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        //@todo
       // NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PumpDetailViewController.statusChange(_:)), name: "ConfigurationUpdate", object: property)
    }
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    // MARK: - UICollectionViewDataSource
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var count = 0
        return count
    }
    
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath)
        var pumpAction:PumpMode?
        
        return cell
    }
    
    private func setModes(){
        
    }
    
    func didPressAction(lightCode: LightCode){
        
    }
    
    func statusChange(notification: NSNotification) {
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
    }
    
    func valueChanged(sender: TGPDiscreteSlider, event:UIEvent) {
        
       
    }
    
}