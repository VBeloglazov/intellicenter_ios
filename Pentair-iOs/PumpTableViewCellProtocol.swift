//
//  PumpTableViewCellProtocol.swift
//  Pentair-iOs
//
//  Created by CS on 05/27/16.
//  Copyright © 2016 Pentair Corporation. All rights reserved.
//

import Foundation
import Pentair

protocol PumpTableViewCellProtocol {
    var objNam:String? { get set }
    
    func getPumpMode() -> PumpMode?
    func setPumpMode(mode:PumpMode)
}