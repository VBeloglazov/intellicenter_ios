//
//  ScheduleViewController.swift
//  Pentair-iOs
//
//  Created by pentair developer on 7/27/16.
//  Copyright © 2016 Pentair Corporation. All rights reserved.
//

import UIKit
import Pentair

class ScheduleViewController: UIViewController {
    
    var property:Property?
    var activeTextField:String?
    var objNam: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

//@todo - Notes
    
//    Methods necessary for editing of schedules:
//    EditSchedule
//    SaveSchedule
//    Delete / Confirm Delete
//    Cancel
//    
//    HeatMode
//    Temp (temperature)
//    
//    getTimeFormat
//    
//    resetSchedule
//    
//    stopTimeChanged
//    startTimeChanged
//    
//    decreaseTemp
//    increaseTemp
//    SetTemp
//    
//    
//    CIRCUIT
//    TIME
//    DAYS
//    PROGRAMTYPE
//    SETTINGS
//    ACTIONS
//    
//    AddSchedule
//    CreateNewSchedule
//    
//    ManageSchedules
//    
//    ValidateNewSchedule
//    
//    ToggleUseCurrent   (Heat mode and Temp)
//    
//    $scope.ToggleUseCurrent = function (type) {
//    switch (type) {
//    case 'HeatMode':
//    if ($scope.Schedule.UseCurrentHeatMode)
//    $scope.Schedule.Body.HEATER = propertyModel.Objects[$scope.Schedule.Circuit].HEATER;
//    break;
//    case 'Temp':
//    if ($scope.Schedule.UseCurrentTemp)
//    $scope.Schedule.Body.LOTMP = propertyModel.Objects[$scope.Schedule.Circuit].LOTMP;
//    }
//    }
    
}
