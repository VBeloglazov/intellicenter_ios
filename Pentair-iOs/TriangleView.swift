//
//  TriangleView.swift
//  Pentair-iOs
//
//  Created by Dashon Howard on 1/30/16.
//  Copyright © 2016 Pentair Corporation. All rights reserved.
//

import Foundation
@IBDesignable
public class TriangleView: UIView {
    
    public override func drawRect(rect: CGRect) {
        let ctx : CGContextRef = UIGraphicsGetCurrentContext()!
        
        CGContextBeginPath(ctx)
        CGContextMoveToPoint(ctx, CGRectGetMinX(rect), CGRectGetMaxY(rect))
        CGContextAddLineToPoint(ctx, CGRectGetMaxX(rect), CGRectGetMaxY(rect))
        CGContextAddLineToPoint(ctx, (CGRectGetMaxX(rect)/2.0), CGRectGetMinY(rect))
        CGContextClosePath(ctx)
        
        CGContextSetRGBFillColor(ctx, 1.0, 0.5, 0.0, 0.60)
        CGContextFillPath(ctx)
        CGContextSetFillColorWithColor(ctx, UIColor.orangeColor().CGColor)
    }
    
}