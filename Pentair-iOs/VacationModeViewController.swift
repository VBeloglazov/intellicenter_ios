//
//  VacationModeViewController.swift
//  Pentair-iOs
//
//  Created by pentair developer on 7/20/16.
//  Copyright © 2016 Pentair Corporation. All rights reserved.
//

import UIKit
import Pentair
import EPCalendarPicker

enum SaveError: ErrorType {
    case NetworkFailure
    case DataMissing
    case Unknown
}

class VacationModeViewController: UIViewController, EPCalendarPickerDelegate, CheckboxViewDelegate {
    var property:Property?
    var activeTextField:String?
    var selectedInstallation:AnyObject!
    var scheduleViewIsDisplayed: Bool = false
    var installationObject:AnyObject!
    var bodies:[AnyObject]?
    
    @IBOutlet weak var startDateTextField: UITextField!
    @IBOutlet weak var endDateTextField: UITextField!
    @IBOutlet weak var vacationModeSwitch: UISwitch!
    @IBOutlet weak var vacationScheduleView: UIView!
    @IBOutlet weak var scheduleCheckBox: Checkbox!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var scheduleViewConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // there are three states that need to be accomodated
        // 1. the VacMode is on
        // 2. the VacMode is off
        // 3. the VacMode has changed
        
        
        self.activeTextField = "startDate"
        
       //@ todo vacation mode - we are setting this for a system,
        
        if let installationObjectDict = self.property!.configurationModel.ControlObjects["_5451"] {
            
            // list all values
            for (key, value) in installationObjectDict {
                print(key, value)
            }
            
            var vacationMode = installationObject.objectForKey("VACFLO") as! String
            
            // @debug
            //vacationMode = "ON"
            
            if vacationMode == "ON" {
                vacationModeSwitch .setOn(true, animated: true)
            } else {
                vacationModeSwitch .setOn(false, animated: true)
            }
            
//            let keyValues: [String: String] = ["VACFLO": vacationModeSwitch.on ? "ON" : "OFF"]
//            let parameters = [KeyValue().setParametersObject(vacationMode!, keyValues: keyValues)]
//            
//            //installationObject.send
//            
//            self.property!.sendRequest(setParamList().buildSetParameters(parameters))
        
        
        }
        
        // not a body?
       //self.bodies = (property?.configurationModel.getAllBodies())!  //any object
        //pumps.sortInPlace({$0.getType().rawValue < $1.getType().rawValue})
        
        // @todo
        // get the active bodies from the config
        
        
        // check each body for the vacflo being on or off
        
        
        // set the switch state to reflect the vacflo
        
        
        
        // get paramlist
        
        
        initCheckbox()
       //print("Selected Installation is:  \(selectedInstallation)")
        self.saveButton.layer.cornerRadius = 5
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Vacation mode notes:
    // If VACFLO is currently OFF, setting VACFLO to ON turns it on right
    // now and forever until it is manually turned to OFF.
    
    // If VACFLO is OFF and VACTIM is ON, then the START and STOP times
    // will cause the THW to change the state of VACFLO to ON when the
    // start time is reached and to OFF when the STOP time is reached.
    
    // If VACFLO in ON, it may be manually set to OFF which will cause the
    //THW to turn off Vacation mode.
    
    
    @IBAction func turnVacationModeOnOff(sender: UISwitch) {
        let keyValues: [String: String] = ["VACFLO": sender.on ? "ON" : "OFF"]
        
                if sender.on {
                    print("Switch is on")
                    sender.setOn(true, animated:true)
                } else {
                    print("Switch is off")
                    sender.setOn(false, animated:true)
                }
        //objNam here will be S### 
        //objTyp here will be SCHED
        // we will need to know what the objNam for the selected body (or install?) is
        
        
        let parameters = [KeyValue().setParametersObject("S###", keyValues: keyValues)]
        self.property!.sendRequest(setParamList().buildSetParameters(parameters))
    }

    func rememberCheckboxState(){
        if scheduleCheckBox.isChecked {
            self.showScheduleView()
        } else {
            self.hideScheduleView()
        }
    }
    
    func valueChanged(view: Checkbox, value: Bool) {
        self.showHideScheduleView()
    }
    
    func showHideScheduleView() {
        if(self.scheduleViewIsDisplayed)
        {
            self.hideScheduleView()
        } else {
            self.showScheduleView()
            UIView.animateWithDuration(5) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func hideScheduleView() {
        self.scheduleViewIsDisplayed = false
        self.vacationScheduleView.hidden = true
    }
    
    func showScheduleView() {
        self.scheduleViewIsDisplayed = true
        self.vacationScheduleView.hidden = false
        
    }
    
    @IBAction func saveVactionSettings(sender:UIButton){
        print("saving the vacation settings")
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        let startDate = dateFormatter.dateFromString(self.startDateTextField.text!)
        let endDate = dateFormatter.dateFromString(self.endDateTextField.text!)
        
        if(self.startDateTextField.text!.isEmpty || self.endDateTextField.text!.isEmpty) {
            showUserMessage("One or more of the text fields are empty", title: "Error")
        } else if startDate!.timeIntervalSinceDate(NSDate()).isSignMinus {
            showUserMessage("Start date must be later than today", title: "Error")
        } else {
            if endDate!.compare(startDate!) == NSComparisonResult.OrderedDescending  {
                saveTheDates()
            } else {
                showUserMessage("Start date must be before the end date", title: "Error")
            }
        }
    }
    
    func showUserMessage(message:String, title:String){
        let alertController = UIAlertController(title: title, message:
            message, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func saveTheDates() {
        _ = true
        do {
            //guard saveState == true else { throw SaveError.Unknown}
            //try to save with futures, etc.
            //callback
            
            
            showUserMessage("Vacation dates have been saved", title: "Success")
        } catch SaveError.NetworkFailure {
            showUserMessage("Unable to save, not connected to the network", title: "Error")
        } catch SaveError.DataMissing {
            showUserMessage("Unable to save, problem with data format", title: "Error")
        } catch {
            showUserMessage("Unable to save, unknown error", title: "Error")
        }
    }
    
    @IBAction func dismissKeyboard(sender:UIButton){
        print("dismiss keyboard")
       // todo resignFirstResponder()
    }
    
  
    @IBAction func calendarButtonPressed(sender:UIButton){
        print("calendar Button Pressed")
        
        let calendarPicker = EPCalendarPicker(startYear: 2016, endYear: 2017, multiSelection: true, selectedDates: nil)
        calendarPicker.calendarDelegate = self
        calendarPicker.startDate = NSDate()
        calendarPicker.multiSelectEnabled = false
        calendarPicker.dateSelectionColor = UIColor .redColor()
        
        if sender.tag == 0 {
            self.activeTextField = "startDate"
        } else {
             self.activeTextField = "endDate"
        }
        
        let navigationController = UINavigationController(rootViewController: calendarPicker)
        self.presentViewController(navigationController, animated: true, completion: nil)
    }
    
     // Checkbox delegate methods
    
    func initCheckbox(){
        self.scheduleCheckBox.delegate = self
    }
    
    // EPCalendarPicker delegate methods

    func epCalendarPicker(_: EPCalendarPicker, didCancel error : NSError) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func epCalendarPicker(_: EPCalendarPicker, didSelectDate date : NSDate) {
        print("Date Selected is: \(date)")

        //@todo - make the selected date highlight when press down
        var formatter = NSDateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        let stringDate: String = formatter.stringFromDate(date)
        
        if(self.activeTextField == "startDate") {
            self.activeTextField == "startDate"
            self.startDateTextField.text = stringDate
        } else {
            self.activeTextField == "endDate"
            self.endDateTextField.text = stringDate
        }
        
        print("Active Text field is: \(self.activeTextField)")
    }
    
    func epCalendarPicker(_: EPCalendarPicker, didSelectMultipleDate dates : [NSDate]){
        
    }

}
