//
//  VerticalProgressView.swift
//  Pentair-iOs
//
//  Created by Dashon Howard on 1/30/16.
//  Copyright © 2016 Pentair Corporation. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
public class VerticalProgressView : UIView {
    
    @IBInspectable public var fillColor : UIColor = UIColor(red:0, green:0.769, blue:0.702, alpha:1)
    @IBInspectable public var cornerRadius: CGFloat = 5
    
    @IBInspectable public var totalProgress: Int = 0
    
    public var progressContainer = CALayer()
    public var container = CALayer()
    let triangle = TriangleView(frame:CGRectMake(0, 5, 10, 5) )
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        triangle.transform = CGAffineTransformMakeRotation(CGFloat(M_PI_2))
        triangle.backgroundColor = UIColor.clearColor()
        triangle.frame.origin.y = self.frame.size.height - self.triangle.frame.size.height
        
        container.borderColor = UIColor.blackColor().CGColor
        container.borderWidth = 1.0
        container.cornerRadius = self.cornerRadius
        container.frame = self.bounds
        container.frame.origin = CGPointMake(triangle.frame.size.height,0)
        container.frame.size.width = self.frame.size.width - triangle.frame.size.height
        container.frame.size.height = self.frame.size.height
        container.masksToBounds = true
        
        progressContainer.backgroundColor = fillColor.CGColor
        progressContainer.frame = self.container.bounds
        progressContainer.frame.origin = CGPointMake(0.0,0.0)
        
        self.layer.addSublayer(container)
        container.addSublayer(progressContainer)
        self.addSubview(triangle)
        
        fillContainer(self.getYPosition())
    }
    
    override public func drawRect(rect: CGRect) {
        super.drawRect(rect)
        let filledHeight = rect.size.height * getProgressPercentage()
        let y = self.frame.size.height - filledHeight
        progressContainer.frame = CGRectMake(0, y, rect.size.width, rect.size.height)
    }
    
    public override func prepareForInterfaceBuilder() {
        //  setProgressLevel(totalProgress)
    }
    
    private func getProgressPercentage() -> CGFloat {
        return CGFloat(Float(totalProgress) / 100)
    }
    
    func getYPosition() -> CGFloat {
        let filledHeight = self.frame.size.height * getProgressPercentage()
        let position = self.frame.size.height - filledHeight
        return position
    }
    
    func fillContainer(position: CGFloat) {
        CATransaction.begin()
        CATransaction.setAnimationDuration(1)
        
        UIView.animateWithDuration(0.75, animations: {
            self.triangle.frame.origin.y = position - (self.triangle.frame.size.height / 2)
            
            if( self.triangle.frame.origin.y < 0  ){
                self.triangle.frame.origin.y = 0
            }
            if(self.triangle.frame.origin.y > self.frame.size.height - self.triangle.frame.size.height ){
                self.triangle.frame.origin.y = self.frame.size.height - self.triangle.frame.size.height
            }
        })
        
        progressContainer.frame.origin.y = position
        CATransaction.commit()
    }
    
    public func setProgressLevel(value: Int){
        
        if value < 0 {
            totalProgress = 0
        } else if value > 100 {
            totalProgress = 100
        }else{
            totalProgress = value
        }
        
        fillContainer(self.getYPosition())
    }
}