//
//  AddUserViewController.swift
//  Pentair-iOs
//
//  Created by Dashon Howard on 1/20/15.
//  Copyright (c) 2015 Pentair Corporation. All rights reserved.
//

import Foundation
import Pentair

//@todo place into a constants file
public let Green: UIColor = UIColor(red: 0.01, green: 0.66, blue: 0.40, alpha: 1.0)
public let Black: UIColor = UIColor.blackColor()


class AddUserViewController: UIViewController{
    
    @IBOutlet weak var PermissionLevel: UIButton!
    @IBOutlet weak var InviteButton: UIButton!
    @IBOutlet weak var EmailAddressField: UITextField!
    @IBOutlet weak var ErrorMessage: UILabel!
    
    var securityTokens :[AnyObject]?
    var selectedToken : AnyObject?
    var errorMsgString : String?
    var property:Property!
    var userPermissionPicker:ActionSheetStringPicker!

    
    override func viewDidLoad() {
        InviteButton.layer.cornerRadius = 5
        InviteButton.backgroundColor = Green
        InviteButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState())
        InviteButton.titleLabel!.text = "Select security level"
        
        PermissionLevel.layer.cornerRadius = 5
        PermissionLevel.backgroundColor = Green
        PermissionLevel.setTitleColor(UIColor.whiteColor(), forState: UIControlState())
        
        //self.securityTokenNames = Array(self.property!.securityTokens.keys)
        
        setAccessibilityIdentifiers()
        
        //fetchSecurityTokens()
        
        selectedToken = self.property.securityTokens["GUEST"]
        ErrorMessage.hidden = true
        
        view.accessibilityIdentifier = "InviteUserView"
    }
    
    func setAccessibilityIdentifiers(){
        PermissionLevel.accessibilityIdentifier = "PermissionLevelDropDown"
        EmailAddressField.accessibilityIdentifier = "EmailAddressField"
        InviteButton.accessibilityIdentifier = "InviteButton"
    }
    
    @IBAction func cancel(sender: AnyObject) {
        if((self.presentingViewController) != nil){
            self.dismissViewControllerAnimated(true, completion: nil)
            print("cancel")
        }
    }
    
    @IBAction func showDropDown(sender: UIButton) {
        showPickerView(sender)
        //Hide Keyboard
        EmailAddressField.resignFirstResponder()
    }
    
    func savePermGroup(selectedIndex: NSNumber){
        selectedToken = self.securityTokens?[selectedIndex.integerValue]
        if let selectedName = selectedToken?["SNAME"] as? String{
            PermissionLevel.setTitle(selectedName, forState: UIControlState())
        }
    }
    
    
    func showPickerView(sender:UIButton){
        self.property.getSecurityTokens().onComplete{results in
            self.securityTokens = self.property.configurationModel.getAllSecurityTokens()
            var tokenNames = [String]()
          
            var selectedIndex = 0
            for (index,securityToken) in self.securityTokens!.enumerate()
            {
                let tokenName = securityToken["SNAME"] as! String
                tokenNames.append(tokenName)
                if tokenName == self.PermissionLevel.titleLabel!.text!{
                    selectedIndex = index
                }
                
                  print("tokenNames \(tokenNames)")
                
            }
            ActionSheetStringPicker(title: "Permission Level", rows: tokenNames, initialSelection: selectedIndex, target: self, successAction: #selector(AddUserViewController.savePermGroup(_:)), cancelAction: nil, origin: sender).showActionSheetPicker()
        }
    }

    
//    func fetchSecurityTokens(){
//        self.property.getSecurityTokens().onComplete{results in
//            self.securityTokenNames = Array(self.property!.securityTokens.keys)
//        }
//    }
    
    func getIndex(list:[String], find:String) -> Int?{
        for (index, value) in list.enumerate() {
            if (value == find){
                return index
            }
        }
        return nil
    }
    
    
    @IBAction func InviteUser(sender: UIButton) {
        //Hide Keyboard
        EmailAddressField.resignFirstResponder()
        
        // var errorMsg = validateForm()
        // @todo strip white space from email address before checking
        // also make a seperate error message for an empty field, etc.
        
        if let address = EmailAddressField.text where !address.isEmpty && (isValidEmail(EmailAddressField.text!))
        {
            print("email address is valid")
            address.trim()
            
            if let selectedObjName = selectedToken?["OBJNAM"] as? String{
                self.property!.inviteUser(address, permGroup: selectedObjName)
                NSNotificationCenter.defaultCenter().postNotificationName("UserInvited", object: self)
                dismissViewControllerAnimated(true, completion: nil)
                
                //self.navigationController?.popViewControllerAnimated(true)
            }
            
        } else {
            //@todo - create conditional error message
            // is the default permGroup 'Guest'?
            ErrorMessage.hidden = false
        }
    }
}
