//
//  BodyViewController.swift
//  Pentair-iOs
//
//  Created by Path Finder on 12/16/15.
//  Copyright © 2015 Pentair Corporation. All rights reserved.
//

import UIKit
import Pentair

class BodyViewController: UIViewController {
    var property: Property?
    var bucket:Bucket!
    var body:NSMutableDictionary!
    var poolHeaters = [AnyObject]()
    
    var objNam: String!
    
    @IBOutlet weak var FreezeProtectionLabelViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var HeatTempControlViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var PoolControlViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var PoolTempDividerConstraint: NSLayoutConstraint!
    @IBOutlet weak var objnamLabel: UILabel!
    @IBOutlet weak var objectTypeImageView: UIImageView!
    @IBOutlet weak var onOffSwitch: UISwitch!
    @IBOutlet weak var heaterSelectorButton: UIButton!
    
    @IBOutlet weak var poolTempLabel: UILabel!
    @IBOutlet weak var lastTempLabel: UILabel!
    @IBOutlet weak var freezeProtectionLabel: UILabel!
    @IBOutlet weak var coolTempControlView: UIView!
    @IBOutlet weak var heatTempControlView: UIView!
    
    @IBOutlet weak var heatingIconImageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        body = (property?.configurationModel.getItem(self.objNam))
        
        self.objNam = self.body["OBJNAM"] as? String
        buildView()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(BodyViewController.statusChange(_:)), name: "ConfigurationUpdate", object: property)
    }
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    private func buildView() {
        setUpBodyView()
        setupHeaterView()
    }
    
    private func setUpBodyView(){
        PoolTempDividerConstraint.constant = 8
        FreezeProtectionLabelViewConstraint.constant = 8
        //body Name
        if let sname = body["SNAME"] as? String {
            self.objnamLabel.text = sname
        }
        //body image
        let image = (body["SUBTYP"] as! String).capitalizedString
        self.objectTypeImageView.image = UIImage(named: image)
        
        //power switch
        if (property!.configurationModel.isBodyOff(self.body)){
            self.onOffSwitch.on = false
        } else {
            self.onOffSwitch.on = true
        }
        
        setUpBodyTemp()
        setUpFreezeProtection()
    }
    
    private func setUpBodyTemp(){
        poolTempLabel.text = "--"
        lastTempLabel.hidden = true
        
        if (property!.configurationModel.isBodyOff(body)){
            if (property!.configurationModel.isLastKnownTempEnabled()){
                lastTempLabel.hidden = false
                PoolTempDividerConstraint.constant += lastTempLabel.frame.size.height + 8
                FreezeProtectionLabelViewConstraint.constant = lastTempLabel.frame.size.height + 16
                if let lastTemp = body["LSTTMP"] as? String where !lastTemp.isEmpty {
                    poolTempLabel.text = lastTemp
                }
            }
        }else if let temp = body["TEMP"] as? String where !temp.isEmpty {
            poolTempLabel.text = temp
        }
    }

    private func setUpFreezeProtection(){
        freezeProtectionLabel.hidden = true
        if (property!.configurationModel.isBodyOn(self.body)){
            if let freezeObjects = property?.configurationModel.getObjectsBySubType("FRZ"){
                for freezeObject in freezeObjects {
                    if let status = freezeObject["STATUS"] as? String{
                        if (status == "ON"){
                            freezeProtectionLabel.hidden = false
                            PoolTempDividerConstraint.constant += freezeProtectionLabel.frame.size.height + 8
                        }
                    }
                }
                
            }
        }
    }
    
    private func setupHeaterView(){
        PoolControlViewConstraint.constant = 8
        HeatTempControlViewConstraint.constant = 16
        poolHeaters = property!.configurationModel.getAllHeaters(self.objNam)
        //get heaters from shared body if non found
        if (poolHeaters.count < 1 ){
            if let sharedObjnam = body["SHARE"] as? String{
                poolHeaters = property!.configurationModel.getAllHeaters(sharedObjnam)
            }
        }
        
        
        //set up heater controls
        let currentHeater = getCurrentHeater();
        
        if (!currentHeater.isEmpty){
            if(!property!.configurationModel.hasCoolingCapabilities(currentHeater)){
                coolTempControlView.hidden = true
            }else{
                coolTempControlView.hidden = false
                PoolControlViewConstraint.constant = coolTempControlView.frame.size.height
                HeatTempControlViewConstraint.constant += coolTempControlView.frame.size.height + 16
            }
            
            if(!property!.configurationModel.hasHeatingCapabilities(currentHeater)){
                heatTempControlView.hidden = true
            }else{
                heatTempControlView.hidden = false
            }
            
        }else{
            coolTempControlView.hidden = true
            heatTempControlView.hidden = true
            PoolControlViewConstraint.constant = 8
        }
        PoolControlViewConstraint.constant +=  PoolTempDividerConstraint.constant
        
        // set current heater
        for heater:AnyObject in poolHeaters {
            if (heater["objnam"] as? String == currentHeater){
                let heaterParams = heater["params"] as! [String:AnyObject]
                heaterSelectorButton.setTitle((heaterParams["SNAME"]! as! String), forState: .Normal)
            }
        }
    }
    
    @IBAction func heaterSelectorTouched(sender: UIButton) {
        if (poolHeaters.count > 0){
            var heaterNames = [String]()
            
            var selectedIndex = 0
            for (index,heater) in self.poolHeaters.enumerate()
            {
                
                let heaterParams = heater["params"] as! [String:AnyObject]
                let heaterName = heaterParams["SNAME"] as! String
                heaterNames.append(heaterName)
                if heaterName == self.heaterSelectorButton.titleLabel!.text!{
                    selectedIndex = index
                }
                
                print("tokenNames \(heaterNames)")
                
            }
            
            ActionSheetStringPicker(title: "Bodies of Water", rows: heaterNames, initialSelection: selectedIndex, target: self, successAction: #selector(BodyViewController.heaterSelected(_:)), cancelAction: nil, origin: sender).showActionSheetPicker()
        }
    }
    
    func heaterSelected(selectedIndex: NSNumber){
        if (poolHeaters.count > 0){
            if let selectedHeater = (poolHeaters[selectedIndex.integerValue]["objnam"] as? String){
                heaterSelectorButton.setTitle(selectedHeater , forState: .Normal)
                
                let keyValues: [String: String] = ["HEATER": selectedHeater]
                let parameters = [KeyValue().setParametersObject(self.objNam!, keyValues: keyValues)]
                
                self.property!.sendRequest(setParamList().buildSetParameters(parameters))
            }
        }
        buildView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func turnBodyOnOff(sender: UISwitch!) {
        let keyValues: [String: String] = [self.objNam!: sender.on ? "ON" : "OFF"]
        self.property!.sendRequest(commandExecute().buildExecuteCommand(CommandName.SET_BODYSTATE.rawValue, objects: keyValues))
    }
    
    func statusChange(notification: NSNotification) {
        buildView()
    }
    
    func getCurrentHeater() -> String{
        var currentHeater = ""
        if let heatSource = body["HTSRC"] as? String{
            if heatSource != "000FF" && !heatSource.isEmpty{
                currentHeater = heatSource
            }
        }
        if (currentHeater.isEmpty){
            if let heaterValue = body["HEATER"] as? String{
                currentHeater = heaterValue
            }
        }
        return currentHeater
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (objNam == nil) { return }
        
        if let identifier = segue.identifier {
            switch identifier {
            case "coolTempStepper":
                let numberStepper = segue.destinationViewController as! NumberStepperViewController
                numberStepper.property = self.property
                numberStepper.setUp(objNam, param: "HITMP", min: 0, max: 999, whiteTheme: true)
                break
            case "heatTempStepper":
                let numberStepper = segue.destinationViewController as! NumberStepperViewController
                numberStepper.property = self.property
                numberStepper.setUp(objNam, param: "LOTMP", min: 0, max: 999,whiteTheme: true)
                break
            default:
                break
            }
        }
    }
}
