//
//  EditUserViewController.swift
//  Pentair-iOs
//
//  Created by Dashon Howard on 1/27/15.
//  Copyright (c) 2015 Pentair Corporation. All rights reserved.
//

import Foundation
import Pentair

class EditUserViewController : UIViewController {
    var user:AnyObject!
    let httpService = HttpService()

    @IBOutlet weak var CancelButton: UIButton!
    @IBOutlet weak var DoneButton: UIButton!
    @IBOutlet weak var statusIcon: UIImageView!
    @IBOutlet weak var UserNameField: UILabel!
    @IBOutlet weak var PermissionLevelButton: UIButton!
    @IBOutlet weak var InviteStatusMessage: UILabel!
    
    @IBOutlet weak var detailContainer: UIView!
    
    var securityTokenNames :[String]?
    var selectedToken : AnyObject?
    var property: Property?
    var securityTokens :[AnyObject]?
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // fetchSecurityTokens()
        self.securityTokenNames = Array(self.property!.securityTokens.keys)
        
        PermissionLevelButton.setTitle((user["level"] as? String), forState: UIControlState())
        UserNameField.text = user["name"] as? String
        setInviteStatus()
        setStyles()
        setAccessibilityIdentifiers()
        
        selectedToken = self.property!.securityTokens[user["level"] as! String]
        
        let deleteUser = UIBarButtonItem(barButtonSystemItem:  UIBarButtonSystemItem.Trash, target: self, action:#selector(EditUserViewController.didPressDelete(_:)))
        deleteUser.accessibilityIdentifier = "DeleteButton"
        
        if (self.httpService.authorizedUserName() == user["name"] as? String){
            CancelButton.hidden = true
            PermissionLevelButton.enabled = false
        }
        else{
            self.navigationItem.rightBarButtonItem = deleteUser
        }
        
        view.accessibilityIdentifier = "EditUserView"
    }
    
    func setInviteStatus(){
        statusIcon.layer.cornerRadius = statusIcon.frame.size.width / 2
        
        switch (user["status"] as! String){
        case "ACCEPTED" :
            statusIcon.image = UIImage(named: "greenDot")
            InviteStatusMessage.text = "This invite has been accepted."
        case "BOUNCED" :
            statusIcon.image = UIImage(named: "redDot")
            InviteStatusMessage.text = "This invite has bounced."
        case "UNKNOWN" :
            statusIcon.image = UIImage(named: "blueDot")
            InviteStatusMessage.text = "This invite has no response."
        case "EXPIRED" :
            statusIcon.image = UIImage(named: "yellowDot")
            InviteStatusMessage.text = "This invite has expired."
        default :
            statusIcon.image = UIImage(named: "whiteDot")
            InviteStatusMessage.text = ""
        }
        
        statusIcon.clipsToBounds = true
    }
    
    func setAccessibilityIdentifiers(){
        CancelButton.accessibilityIdentifier = "CancelButton"
        // DoneButton.accessibilityIdentifier = "DoneButton"
        statusIcon.accessibilityIdentifier = "statusIcon"
        UserNameField.accessibilityIdentifier = "userNameField"
        PermissionLevelButton.accessibilityIdentifier = "PermissionLevelDropDown"
        InviteStatusMessage.accessibilityIdentifier = "InviteStatusMessage"
    }
    
    func setStyles(){
        //DoneButton.setTitleColor(Green, forState: UIControlState())
        
        //detailContainer.backgroundColor=UIColor(white: 0, alpha: 0.2)
        detailContainer.backgroundColor=UIColor.whiteColor()

        detailContainer.layer.borderColor = UIColor.grayColor().CGColor
        detailContainer.layer.borderWidth = 1
        detailContainer.clipsToBounds = true
        
        PermissionLevelButton.layer.cornerRadius = 5
        PermissionLevelButton.backgroundColor = Green
    }
    
    
    @IBAction func showDropDown(sender: UIButton) {
        showPickerView(sender)
        //Hide Keyboard
        //@todo
       // EmailAddressField.resignFirstResponder()
    }
    
    
    func showPickerView(sender:UIButton){
        self.property!.getSecurityTokens().onComplete{results in
            self.securityTokens = self.property!.configurationModel.getAllSecurityTokens()
            var tokenNames = [String]()
            
            var selectedIndex = 0
            for (index,securityToken) in self.securityTokens!.enumerate()
            {
                let tokenName = securityToken["SNAME"] as! String
                tokenNames.append(tokenName)
                if tokenName == self.PermissionLevelButton.titleLabel!.text!{
                    selectedIndex = index
                }
                
                print("tokenNames \(tokenNames)")
                
            }
            ActionSheetStringPicker(title: "Permission Level", rows: tokenNames, initialSelection: selectedIndex, target: self, successAction: #selector(AddUserViewController.savePermGroup(_:)), cancelAction: nil, origin: sender).showActionSheetPicker()
        }
    }

    
    @IBAction func didPressCancel(sender: UIButton) {
        goBack()
    }
    
    func didPressDelete(sender: UIButton) {
        let DeleteAlert = UIAlertController(title: "Delete?", message: "This Cannont Be Undone!", preferredStyle: UIAlertControllerStyle.Alert)
        let NotifyAlert = UIAlertController(title: "Notify?", message: "Would you like an e-mail notice sent to this account to let them know they have been removed from this pool?", preferredStyle: UIAlertControllerStyle.Alert)
        
        
        DeleteAlert.addAction(UIAlertAction (title: "Ok", style: .Default, handler: { action in
            self.presentViewController(NotifyAlert, animated: true, completion: nil)
        }))
        
        DeleteAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { action in
        }))
        
        NotifyAlert.addAction(UIAlertAction(title: "Notify", style: .Default, handler: { action in
            self.sendDeleteMessage(true)
        }))
        
        NotifyAlert.addAction(UIAlertAction(title: "Do Not Notify", style: .Default, handler: { action in
            self.sendDeleteMessage(false)
        }))
        
        self.presentViewController(DeleteAlert, animated: true, completion: nil)
        
    }
    
    func sendDeleteMessage(notify:Bool){
        self.property!.deleteUser(self.user["name"] as! String, status: self.user["status"] as! String, notify: notify)
        self.goBack()
    }
    
    @IBAction func DidPressDone(sender: UIButton) {
        if (self.httpService.authorizedUserName() != user["name"] as? String){
            //@todo
            self.property!.editUser(self.user["name"] as! String, permGroup: self.selectedToken!["OBJNAM"] as! String)
        }
        self.goBack()
    }
    
    
    func goBack(){
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func savePermGroup(selectedIndex: NSNumber){
        selectedToken = self.securityTokens?[selectedIndex.integerValue]
        if let selectedName = selectedToken?["SNAME"] as? String{
            PermissionLevelButton.setTitle(selectedName, forState: UIControlState())
        }
    }
    
//    func savePermGroup(selectedIndex: NSNumber){
//        let selectedValue : String = securityTokenNames![selectedIndex.integerValue]
//        selectedToken = self.property!.securityTokens[selectedValue]
//        
//        PermissionLevelButton.setTitle(selectedValue, forState: UIControlState())
//    }
    
//    func fetchSecurityTokens(){
//        self.property!.getSecurityTokens().onComplete{results in
//            self.securityTokenNames = Array(self.property!.securityTokens.keys)
//        }
//    }
    
}