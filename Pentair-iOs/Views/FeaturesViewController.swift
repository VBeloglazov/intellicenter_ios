//
//  FeaturesViewController.swift
//  Pentair-iOs
//
//  Created by Path Finder on 12/18/15.
//  Copyright © 2015 Pentair Corporation. All rights reserved.
//

import UIKit
import Pentair

class FeaturesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var property: Property?
    var features:[AnyObject]!
    var featuresHardware:[AnyObject]!
    
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        features = (property?.configurationModel.getAllFeatures())!
        
        self.view.backgroundColor = UIColor(colorLiteralRed: 208/255, green: 208/255, blue: 208/255, alpha: 1.0)
        self.tableView.backgroundColor = UIColor.clearColor()
        
        let grayView = UIView()
        grayView.backgroundColor = UIColor(colorLiteralRed: 208/255, green: 208/255, blue: 208/255, alpha: 1.0)
        self.tableView.tableFooterView = grayView
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(FeaturesViewController.statusChange(_:)), name: "ConfigurationUpdate", object: property)
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.features.count
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 65
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: FeatureTableViewCell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! FeatureTableViewCell
        
        
        let feature: [String : AnyObject] = self.features![indexPath.row] as! [String : AnyObject]
        
        if let objnam: String = feature["OBJNAM"] as? String {
            cell.objnam = objnam
            if let status: String = feature["STATUS"] as? String {
                cell.onOffSwitch.on = (status == "ON") ? true : false
            }
        }
        
        // Configure the cell...
        cell.titleLabel!.text = feature["SNAME"] as? String
        cell.onOffSwitch.tag = indexPath.row
        
        return cell
    }
    
    @IBAction func turnLightOnOff(sender: UISwitch) {
        let cell: FeatureTableViewCell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: sender.tag, inSection: 0)) as! FeatureTableViewCell
        
        let keyValues: [String: String] = ["STATUS": sender.on ? "ON" : "OFF"]
        let parameters = [KeyValue().setParametersObject(cell.objnam!, keyValues: keyValues)]
        
        self.property!.sendRequest(setParamList().buildSetParameters(parameters))
    }
    
    // MARK: - Notification processing
    func statusChange(notification: NSNotification) {
        self.tableView.reloadData()
    }
}
