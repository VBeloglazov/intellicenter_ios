//
//  HomeTableViewController.swift
//  Pentair-iOs
//
//  Created by Path Finder on 11/28/15.
//  Copyright © 2015 Pentair Corporation. All rights reserved.
//

import UIKit
import Pentair

enum BucketType: Int{
    case Air = 0
    case Pool
    case Spa
    case Lights
    case Features
    case Chemistry
    case Pump
    case Schedule
    case History
}

class Bucket {
    private var children:[String]
    private var type:BucketType
    private var title:String
    
    init(children:[String], type:BucketType, title:String){
        self.children = children
        self.type = type
        self.title = title
    }
    func getTitle() -> String{
        return title
    }
    
    func getChildren() -> [String]{
        return children
    }
    
    func getType() ->BucketType{
        return type
    }
    func addChild(childName:String){
        children.append(childName)
    }
}

class HomeTableViewController: UITableViewController {
    
    var Green = UIColor(hue: 0.35, saturation: 1, brightness: 0.62, alpha: 1.0) /* #009e0f */
    var BrightGreen = UIColor(hue: 0.3806, saturation: 0.18, brightness: 1, alpha: 1.0) /* #d1ffde */
    
    var property: Property!
    var httpService: HttpService!
    var buckets: [Bucket]!
    var configurationReceived: Bool!
    var weatherReceived: Bool!
    var indicator: UIActivityIndicatorView!
    var lastZip = ""
    var fontName = "Avenir"
    var currentSelection: NSNumber!
    var shouldScrollUp:Bool! = false
    var weatherViewHeight = 600.0 as CGFloat
    var itemViewHeight = 60.0 as CGFloat
    var weatherViewController: WeatherViewController?
    var localTemp:NSString!
    var selectedPoolName:String!
    var selectedInstallation:AnyObject!
    var installationAccessType:NSNumber!

     //@IBOutlet var activityIndicator: UIActivityIndicatorView!
    // @IBOutlet weak var countLabelView: UIView!
    
    let transitionManager = HomeViewPresentationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.Bordered, target: self, action: "back:")
       // self.navigationItem.leftBarButtonItem = newBackButton;
        let image = UIImage(named: "ic_bell")
        
        let alarmButton = UIBarButtonItem(image: image, style: UIBarButtonItemStyle.Plain, target:self, action:"alarm:")
    
        self.title = self.selectedPoolName
        self.buckets = [Bucket]()
        self.configurationReceived = false
        self.weatherReceived = false
        self.startActivityIndicator()
        
        let barButtonBackStr = " " + "5" + " "
        let attributedBarButtonAlertString = NSMutableAttributedString(string: barButtonBackStr as String)
        attributedBarButtonAlertString.addAttribute(NSFontAttributeName,
                                                value: UIFont(
                                                    name: "AmericanTypewriter-Bold",
                                                    size: 18.0)!,
                                                range: NSRange(
                                                    location:0,
                                                    length:1))
        attributedBarButtonAlertString.addAttribute(NSForegroundColorAttributeName, value: UIColor.whiteColor(), range: NSMakeRange(0, attributedBarButtonAlertString.length))

        let label = UILabel()
        label.backgroundColor = UIColor .orangeColor()
        label.attributedText = attributedBarButtonAlertString
        label.layer.cornerRadius = 5
        let shadowPath = UIBezierPath(rect: label.bounds)
        label.layer.shadowOffset = CGSize(width: 0, height: 0.5)
        label.layer.shadowOpacity = 1
        label.layer.shadowPath = shadowPath.CGPath
        label.layer.masksToBounds = false
        //label.
        label.sizeToFit()
        let newButton = UIBarButtonItem(customView: label)
        
        self.navigationItem.leftBarButtonItems = [newBackButton, alarmButton, newButton]
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "hamburger"), style: UIBarButtonItemStyle.Plain, target: self, action:#selector(HomeTableViewController.showmenu(_:)))
        self.navigationItem.rightBarButtonItem!.accessibilityIdentifier = "MenuButton"
        let menu = self.frostedViewController.menuViewController as! MenuViewController
        menu.property = property!
        menu.httpService = httpService!
        menu.selectedInstallation = self.selectedInstallation
        view.accessibilityIdentifier = "DashBoardView"
        self.tableView.setContentOffset(CGPointMake(0,-200), animated: false)
        
        // important: do not use self here
        print("Selected installation is: \(selectedInstallation)")
        
//      @todo revisit this - needs to be moved to the table view cell
//      self.countLabelView.layer.cornerRadius =  self.countLabelView.frame.size.width/2
//      self.countLabelView.clipsToBounds = true
//      self.countLabelView.layer.borderColor = UIColor.grayColor().CGColor
//      self.countLabelView.layer.borderWidth = 5.0
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(HomeTableViewController.configurationLoaded(_:)), name: "ConfigurationLoaded", object: property)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(HomeTableViewController.statusChange(_:)), name: "ConfigurationUpdate", object: property)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(HomeTableViewController.weatherForecastReceived(_:)), name: "WeatherForecastAvailable", object: property)
        
        //print("Selected Installation is:  \(selectedInstallation)")
        
        updateTableAppearance()
    }
    
    override func viewDidAppear(animated: Bool) {
        
        self.navigationController!.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
        
        if installationAccessType == 0 {    //local mode
            self.navigationController!.navigationBar.barTintColor = UIColor(hue: 0.5694, saturation: 1, brightness: 1.0, alpha: 0.5) /* #0090f7 */
        } else {  //remote mode
//            self.navigationController!.navigationBar.barTintColor = UIColor(hue: 0.4139, saturation: 1, brightness: 0.68, alpha: 0.5) /* #00ad53 */
            
            self.navigationController!.navigationBar.barTintColor = Green
        }

    }
    
    func back(sender: UIBarButtonItem) {
        // Perform your custom actions
        // ...
        // Go back to the previous ViewController
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    func alarm(sender: UIBarButtonItem) {
        // Show alerts / notifications
        let notificationsViewController = self.storyboard!.instantiateViewControllerWithIdentifier("viewNotificationsController") as! NotificationsViewController
        notificationsViewController.property = property!
        self.navigationController!.pushViewController(notificationsViewController, animated: true)
    }
    
    func updateTableAppearance() {
        self.tableView.reloadData()
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
    }
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        var sections = 1
        
        if self.configurationReceived == true {
            sections = 2
        }
        
        return sections
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count = 1
        
        if self.configurationReceived == true {
            switch section {
            case 0:
                count = buckets!.count
                break
                
            case 1:
                count = 1
                break
                
            default:
                break
            }
        }
        return count
    }
    
    
    func getTime() -> (hour: Int, minute: Int,second: Int) {
        let hour = 1
        let minute = 2
        let second = 3
        return ( hour, minute, second)
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if self.configurationReceived == false {
            let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
            cell.accessoryType = .None
            cell.textLabel?.text = ""
            return cell
        } else {
            let bucket: Bucket = buckets![indexPath.row]
            var controlObject:NSMutableDictionary?
            if let firstChild = bucket.getChildren().first {
                if let o = property.configurationModel.getItem(firstChild) {
                    controlObject = o
                }
            }
            
            if indexPath.section == 1 {
                let cell: WeatherTableViewCell = tableView.dequeueReusableCellWithIdentifier("WeatherCell", forIndexPath: indexPath) as! WeatherTableViewCell
                cell.hidden = true
                if self.weatherReceived == true {
                    cell.backgroundView = weatherViewController!.view
                    cell.hidden = false
                }
                cell.userInteractionEnabled = true
                return cell
            } else {
                var imageName = bucket.getTitle()
                switch bucket.getType() {
                case BucketType.Air:
                    let cell = tableView.dequeueReusableCellWithIdentifier("HeaderCell", forIndexPath: indexPath) as! DateTimeTableViewCell
                    
                    if (controlObject!["PROBE"] != nil) {
                        localTemp = controlObject!["PROBE"] as! NSString
                    } else {
                        if controlObject!["TEMP"] != nil {
                            localTemp = controlObject!["TEMP"] as! NSString
                        }
                    }
                    
                    let currentDate = NSDate()
                    let calendar = NSCalendar.currentCalendar()
                    let components = calendar.components([.Day , .Month , .Year], fromDate: currentDate)
                    let dayOfWeekFormatter = NSDateFormatter()
                    let dateFormatter = NSDateFormatter()
                    let timeFormatter = NSDateFormatter()
                   
                    dayOfWeekFormatter.dateFormat = "EEEE"
                    dateFormatter.dateFormat = "yyyyMMdd"
                    timeFormatter.timeStyle = .ShortStyle
                    timeFormatter.dateStyle = .NoStyle
                    
                    let dayOfWeekString = dayOfWeekFormatter.stringFromDate(currentDate) +  ","  + " "
                    let monthName = NSDateFormatter().monthSymbols[components.month - 1]
                    let day = components.day
                    let monthAndDay = monthName + " " + String(day) + " "
                    let time = "    " + timeFormatter.stringFromDate(currentDate) + " "
                    let zone =  NSTimeZone.localTimeZone().localizedName(.ShortStandard,  locale: NSLocale.currentLocale()) ?? "" + " "
                    let headerString = dayOfWeekString + monthAndDay + time + zone + "    " + (localTemp! as String) +  "\u{00B0} F"
                    cell.dateLabel.text = headerString
                    cell.dateLabel!.font = UIFont(name:fontName, size:16)
                    cell.userInteractionEnabled = false
                    return cell
                case BucketType.Spa:
                    fallthrough
                case BucketType.Pool:
                    let cell = tableView.dequeueReusableCellWithIdentifier("HomeBodyCell", forIndexPath: indexPath) as! HomeBodyTableViewCell
                    setUpBodyTemp(cell, body: controlObject!)
                    setUpFreezeProtection(cell, body: controlObject!)
                    if let objNam = controlObject?["OBJNAM"] as? String{
                        imageName = property.configurationModel.getParam(objNam, param: "SUBTYP").capitalizedString
                    }
                    cell.titleLabel.text = bucket.getTitle()
                    cell.titleLabel!.font = UIFont(name:fontName, size:22)
                    cell.imageView?.image = UIImage(named: imageName)!.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
                    cell.selectionStyle = .None
                    break
                    //return cell
                case BucketType.Features:
                    fallthrough
                case BucketType.Lights:
                    let cell : HomeCircuitTableViewCell = tableView.dequeueReusableCellWithIdentifier("HomeCircuitCell", forIndexPath: indexPath) as! HomeCircuitTableViewCell
                    
                    //@todo - what about light groups?  are they counted?
                    // previously we showed the count of all possible lights
                    //cell.countLabel.text = bucket.getChildren().count.description
                    //now we only show which lights are on

                    
                    var lightGroups = (property?.configurationModel.getAllLightGroups())!
                    var lights = (property?.configurationModel.getAllLights())!
                    
                    lights.sortInPlace({($0["LISTORD"] as! String) < ($1["LISTORD"] as! String)})
                    lightGroups.sortInPlace({($0["LISTORD"] as! String) < ($1["LISTORD"] as! String)})
                    
                    var lightCounter = 0
                    
                    for light in lights {
                        let status = light["STATUS"]! as! String
                        print("light status \(status)")
                        if status == "ON" {
                            lightCounter = lightCounter + 1
                            print("number of lights that are on is: \(lightCounter)")
                        }
                    }
                    
                    cell.countLabel.text = String(lightCounter)
                    cell.titleLabel?.text = bucket.getTitle()
                    cell.titleLabel!.font = UIFont(name:fontName, size:22)
                    cell.imageView?.image = UIImage(named: imageName)!.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
                    cell.selectionStyle = .None
                    return cell
                case BucketType.Chemistry:
                    let cell  = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
                    cell.detailTextLabel?.text = ""
                    cell.textLabel?.text = bucket.getTitle()
                    cell.textLabel!.font = UIFont(name:fontName, size:22)
                    cell.imageView?.image = UIImage(named: imageName)!.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
                    cell.selectionStyle = .None
                    return cell
                case BucketType.Pump:
                    let cell  = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
                    cell.detailTextLabel?.text = ""
                    cell.textLabel?.text = bucket.getTitle()
                    cell.textLabel!.font = UIFont(name:fontName, size:22)
                    //@todo
                    cell.imageView?.image = UIImage(named: "pump-icon-purple")!.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
                    cell.selectionStyle = .None
                    return cell
                    
                case BucketType.Schedule:
                    let cell  = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
                    cell.detailTextLabel?.text = ""
                    cell.textLabel?.text = bucket.getTitle()
                    cell.textLabel!.font = UIFont(name:fontName, size:22)
                    cell.imageView?.image = UIImage(named:"clock")!.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
                    cell.selectionStyle = .None
                    return cell
                    
                case BucketType.History:
                    let cell  = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
                    cell.detailTextLabel?.text = ""
                    cell.textLabel?.text = bucket.getTitle()
                    cell.textLabel!.font = UIFont(name:fontName, size:22)
                    cell.imageView?.image = UIImage(named: "ic_chart")!.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
                    cell.selectionStyle = .None
                    return cell
                }
            }
        }
        let cell  = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        cell.selectionStyle = .None
        //break
        return cell
    }
    
    
    private func setUpBodyTemp(cell:HomeBodyTableViewCell, body:NSMutableDictionary){
        cell.tempLabel.text = "--"
        cell.LastTempLabel.hidden = true
        if (property!.configurationModel.isBodyOff(body)){
            if (property!.configurationModel.isLastKnownTempEnabled()){
                if let lastTemp = body["LSTTMP"] as? String  where !lastTemp.isEmpty{
                    cell.tempLabel.text = lastTemp
                    cell.LastTempLabel.hidden = false
                }
            }
        }else if let temp = body["TEMP"] as? String  where !temp.isEmpty{
            cell.tempLabel.text = temp
        }
    }
    private func setUpFreezeProtection(cell:HomeBodyTableViewCell, body:NSMutableDictionary){
        cell.FreezeProtectionLabel.hidden = true
        if (property!.configurationModel.isBodyOn(body)){
            if let freezeObjects = property?.configurationModel.getObjectsBySubType("FRZ"){
                for freezeObject in freezeObjects {
                    if let status = freezeObject["STATUS"] as? String{
                        if (status == "ON"){
                            cell.FreezeProtectionLabel.hidden = false
                        }
                    }
                }
                
            }
        }
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        var height = 44.0 as CGFloat
        
        switch indexPath.section {
        case 1:
            height = self.weatherViewHeight
            break
            
        default:
            height = self.itemViewHeight
            break
        }
        
        return height
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if indexPath.section == 1 {
            if(shouldScrollUp == false)
            {
                scrollWeatherViewUp()
                shouldScrollUp = true
            } else {
                scrollWeatherViewDown()
                shouldScrollUp = false
            }
            
        } else {
            self.performSegueWithIdentifier("showTabs2", sender: self)
        }
    }
    
    func scrollWeatherViewUp() {
        self.tableView.setContentOffset(CGPointMake(0, 300), animated: true)
        self.weatherViewController?.rotateArrowUp()
    }
    
    func scrollWeatherViewDown() {
        self.tableView.setContentOffset(CGPointMake(0, -100), animated: true)
        self.weatherViewController?.rotateArrowDown()
    }
    
    // MARK: - Navigation
    
    // On 0 rows, do not transition to the detail view controller
    override func shouldPerformSegueWithIdentifier(identifier: String, sender: AnyObject!) -> Bool {
        var actionOPerformed: Bool = true
        
        let indexPath: NSIndexPath = self.tableView.indexPathForSelectedRow!
        if indexPath.row != 0 {
            actionOPerformed = true
        } else {
            NSLog("No segue action")
            actionOPerformed = false
        }
        return actionOPerformed
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let indexPath: NSIndexPath = self.tableView.indexPathForSelectedRow!
        let cell = self.tableView.cellForRowAtIndexPath(indexPath)
        let controller: TabViewController = segue.destinationViewController as! TabViewController
        controller.property = self.property
        controller.httpService = self.httpService
        controller.selectedBucket = self.buckets?[indexPath.row]
        controller.buckets = self.buckets
        controller.objectTitle = cell?.textLabel?.text
        controller.selectedRow = indexPath.row
    }
    
    // MARK: - Weather display support
    
    
    func weatherForecastReceived(notificattion: NSNotification) {
        if let weatherData = property?.configurationModel {
            if weatherData.WeatherValid {
                weatherReceived = true
                
                self.weatherViewController = self.storyboard?.instantiateViewControllerWithIdentifier("WeatherViewController") as? WeatherViewController
                self.weatherViewController!.property = property
                self.tableView.reloadData()
            }
        }
    }
    
    
    
    func maximizeWeatherView() {
        //@todo
        presentViewController(weatherViewController!, animated: true, completion: nil)
    }
    
    // MARK: - Display Utility Methods
    
    func initializeBuckets(configuration: [String : AnyObject]) {
        buckets!.removeAll()
        let airbucket = Bucket(children:[String](), type: BucketType.Air,title: "Air")
        let lightsbucket = Bucket(children:[String](), type: BucketType.Lights,title: "Lights")
        let featuresbucket = Bucket(children:[String](), type: BucketType.Features,title: "Features")
        let chemistrybucket = Bucket(children:[String](), type: BucketType.Chemistry,title: "Chemistry")
        let pumpsbucket = Bucket(children:[String](),type:BucketType.Pump,title: "Pumps")
        let schedulebucket = Bucket(children:[String](),type:BucketType.Schedule,title:"Schedule")
        let historybucket = Bucket(children:[String](),type:BucketType.History,title: "History")
        
        buckets.append(airbucket)
        buckets.append(lightsbucket)
        buckets.append(featuresbucket)
        buckets.append(chemistrybucket)
        buckets.append(pumpsbucket)
        buckets.append(schedulebucket)
        buckets.append(historybucket)
        
        for controlObject in (property!.configurationModel.ControlObjects) {
            let objectName:String = controlObject.0
            let objectParameters: NSMutableDictionary = controlObject.1
            
        
            if let type: String = objectParameters["OBJTYP"] as? String {
                switch type {
                case "PUMP":
                    pumpsbucket.addChild(objectName)
                    //print("Child object name for pumps is \(objectName)")
                    break
                default:
                    break
                }
            }
            
        
            if let type: String = objectParameters["OBJTYP"] as? String {
                switch type {
                case "HISTORY":
                    historybucket.addChild(objectName)
                    //print("Child object name for history is \(objectName)")
                    break
                default:
                    break
                }
            }
            
            //@here
            
            if let type: String = objectParameters["OBJTYP"] as? String {
                switch type {
                case "SCHED":
                    schedulebucket.addChild(objectName)
                    //print("Child object name for schedule is \(objectName)")
                    break
                default:
                    break
                }
            }

            
            if let subtyp: String = objectParameters["SUBTYP"] as? String {
                switch subtyp {
                case "AIR":
                    airbucket.addChild(objectName)
                    break
                case "ICHEM":
                    fallthrough
                case "ICHLOR":
                    chemistrybucket.addChild(objectName)
                    break
                    
                case "POOL":
                    if(objectParameters["OBJTYP"] as? String == "BODY"){
                        buckets.append(Bucket(children:[objectName], type: BucketType.Pool,title:(objectParameters["SNAME"] as? String)!))
                    }
                    break
                case "SPA":
                    if(objectParameters["OBJTYP"] as? String == "BODY"){
                        buckets.append(Bucket(children:[objectName], type: BucketType.Spa,title:(objectParameters["SNAME"] as? String)!))
                    }
                    break
                    
                default: // Double check that showmenu is a good qualifier for light and feature buckets
                    if let shomnu: String = objectParameters["SHOMNU"] as? String {
                        if (shomnu.containsString("l") || shomnu.containsString("e") ) {
                            lightsbucket.addChild(objectName)
                        }
                        if (shomnu.containsString("f")) {
                            featuresbucket.addChild(objectName)
                        }
                    }
                }
            }
        }
        //Order by Object Type
        buckets.sortInPlace({$0.getType().rawValue < $1.getType().rawValue})
        
        self.configurationReceived = true
        self.tableView.reloadData()
        property!.scheduleNotifications()
    }
    
    
    // MARK: - Selector Action methods
    
    func showmenu(sender:UIButton!) {
        self.frostedViewController.presentMenuViewController()
    }
    
    // MARK: - Notification methods
    
    // This is called once after a new configuration is loaded by the web socket open event.
    func configurationLoaded(notification: NSNotification) {
        
        self.stopActivityIndicator()
        self.initializeBuckets(self.property!.configurationModel.ConfigurationDictionary)
    }
    
    func statusChange(notification: NSNotification) {
        self.tableView.reloadData()
        
        refreshWeather()
    }
    
    func refreshWeather(){
        if let location = self.property!.configurationModel.ControlObjects["_5451"] {
            if let zip = location["ZIP"] as? String {
                if !zip.isEmpty && zip != self.lastZip{
                    self.lastZip = zip
                    self.httpService?.FetchWeather(zip)
                }
            }
        }
    }
    // MARK: - ActivityIndicatorView methods
    
    func startActivityIndicator() {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        let indicator: UIActivityIndicatorView = UIActivityIndicatorView()
        indicator.activityIndicatorViewStyle = .Gray
        indicator.hidesWhenStopped = true
        
        let screenWidth = self.view.bounds.width
        let screenHeight = self.view.bounds.height
        
        let centerAnchor = CGPoint(x:screenWidth/2, y:(screenHeight/2) - 200)
        
        indicator.center = centerAnchor
        indicator.startAnimating()
        self.indicator = indicator
        
        self.view.addSubview(indicator)
    }
    
    func stopActivityIndicator() {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        self.indicator.stopAnimating()
        self.indicator.removeFromSuperview()
    }
    
}
