//
//  InstallationDetailsViewController.swift
//  Pentair-iOs
//
//  Created by Dashon Howard on 2/23/15.
//  Copyright (c) 2015 Pentair Corporation. All rights reserved.
//

import Foundation
import Pentair

//
//  EditUserViewController.swift
//  Pentair-iOs
//
//  Created by Dashon Howard on 1/27/15.
//  Copyright (c) 2015 Pentair Corporation. All rights reserved.
//

import Foundation
import Pentair

class InstallationDetailsViewController : UIViewController {
    var installation:AnyObject!
    var httpService:HttpService?
    var timeZones = ["FST", "SST", "TST", "AKST", "PST", "MST", "CST", "EST", "BRT", "GST", "EGT", "GMT", "DFT", "CEST", "AST", "AMT", "PKT", "BST", "CXT", "CST", "EIT", "AEST", "BST", "FJT"]
    @IBOutlet weak var detailContainer: UIView!
    @IBOutlet weak var OwnerName: UILabel!
    @IBOutlet weak var OwnerPhone: UILabel!
    @IBOutlet weak var OwnerEmail: UILabel!
    @IBOutlet weak var PoolAddress1: UILabel!
    @IBOutlet weak var PoolAddress2: UILabel!
    @IBOutlet weak var TimeZone: UILabel!
    @IBOutlet weak var ConnectButton: UIButton!
    
    @IBOutlet weak var DeleteButton: UIButton!
    
    override func viewDidLoad() {
        setAccessibilityIdentifiers()
        view.accessibilityIdentifier = "InstallationDetailView"
        setData()
        setStyles()
    }
    
    func setData(){
        if let installation = installation{
            setDeleteBtnState(DeleteButton, canDelete: true)
            if((self.installation["AccessToken"] as! String).uppercaseString == "UFFFE" && self.installation["TotalAdmins"] as! Int == 1){
                setDeleteBtnState(DeleteButton, canDelete: false)
            }
            
            if let poolName = installation["PoolName"] as? String{
                self.title = poolName
            }
            
            self.OwnerName.text =  getStringOrDefault(installation["OwnerName"] as? String)
            self.OwnerPhone.text = getStringOrDefault(installation["Phone"] as? String)
            self.OwnerEmail.text = getStringOrDefault(installation["Email"] as? String)
            self.PoolAddress1.text = getStringOrDefault(installation["Address"] as? String)
            self.PoolAddress2.text = "--"
            if let city = installation["City"] as? String,
                let state = installation["State"] as? String,
                let zip = installation["Zip"] as? String{
                self.PoolAddress2.text = String(format: "%@, %@ %@", city, state, zip)
            }
            self.TimeZone.text = "--"
            if let timeZone = installation["TimeZone"] as? String{
                if let timeZoneNumber = Int(timeZone){
                    let offsetTimeZone = timeZoneNumber + 13
                    self.TimeZone.text = timeZones[offsetTimeZone - 1]
                }
            }
            if let isOnline = installation["OnLine"] as? Bool{
                setConnectBtnState(ConnectButton, isOnline: isOnline)
            }
        }
        detailContainer.hidden = false
        
    }
    
    func setAccessibilityIdentifiers(){
        //   DoneButton.accessibilityIdentifier = "DoneButton"
    }
    
    func setStyles(){
        //   DoneButton.setTitleColor(Green, forState: UIControlState())
    }
    
    @IBAction func didTouchLogin(sender: AnyObject) {
        
        self.performSegueWithIdentifier("showDashboard", sender: self)
    }
    
    @IBAction func didTouchDelete(sender: AnyObject) {
        let DeleteAlert = UIAlertController(title: "Remove this pool from my account?", message: "This Cannont Be Undone!", preferredStyle: UIAlertControllerStyle.Alert)
        
        DeleteAlert.addAction(UIAlertAction(title: "Confim", style: .Default, handler: { action in
            self.sendDeleteMessage()
        }))
        
        DeleteAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { action in
        }))
        
        self.presentViewController(DeleteAlert, animated: true, completion: nil)
        
    }
    
    func sendDeleteMessage(){
        httpService!.removeInstallationFromAccount(self.installation["InstallationId"] as! Int)
        self.goBack()
    }
    
    @IBAction func DidPressDone(sender: UIButton) {
        self.goBack()
    }
    
    
    func goBack(){
        self.navigationController?.popViewControllerAnimated(true)
    }
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if (segue.identifier == "showDashboard") {
            let vc: HomeTableViewController = segue.destinationViewController as! HomeTableViewController
            if let token = self.httpService!.authToken() {
                if let installationId = self.installation["InstallationId"] as? Int{
                    let host = baseConnection + "api/websocket/" + installationId.description + "?access-token=" + token
                    vc.property = Property(urlString: host, service: httpService!)
                }
            }
            vc.httpService = httpService
        }
    }
    
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "InstallationDetails", object: httpService)
    }
}