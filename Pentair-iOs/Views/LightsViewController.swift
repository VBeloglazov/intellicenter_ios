 //
 //  LightsViewController.swift
 //  Pentair-iOs
 //
 //  Created by Path Finder on 12/11/15.
 //  Copyright © 2015 Pentair Corporation. All rights reserved.
 //

import UIKit
import QuartzCore
import Pentair

class LightsViewController: UITableViewController {
    var property: Property?
    var lightGroups = [AnyObject]()
    var lights = [AnyObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        lightGroups = (property?.configurationModel.getAllLightGroups())!
        lights = (property?.configurationModel.getAllLights())!
        
        lights.sortInPlace({($0["LISTORD"] as! String) < ($1["LISTORD"] as! String)})
        lightGroups.sortInPlace({($0["LISTORD"] as! String) < ($1["LISTORD"] as! String)})
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LightsViewController.statusChange(_:)), name: "ConfigurationUpdate", object: property)
    }
    
    override func viewDidAppear(animated: Bool) {
        self.tableView.reloadData()
    }
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        var count: Int = 1
        
        if lightGroups.count > 0 {
            count = 2
        }
        
        return count
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count: Int = 0
        switch section {
        case 0:
            count = lights.count
            break
            
        case 1:
            count = lightGroups.count
            break
            
        default:
            break
        }
        
        return count
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        var title: String? = nil
        
        switch section {
        case 0:
            title = "Lights"
            break
            
        case 1:
            title = "Light Groups"
            break
            
        default:
            break
        }
        
        return title
    }
    override func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        let header: UITableViewHeaderFooterView = view as! UITableViewHeaderFooterView
        header.backgroundView?.backgroundColor =  UIColor(red:0.902,green:0.902, blue:0.902, alpha:1)
        header.textLabel!.textAlignment = .Center
        
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell!
        switch indexPath.section {
        case 0:
            var light: [String : AnyObject] = self.lights[indexPath.row] as! [String : AnyObject]
            
            let lightCell: LightTableViewCell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! LightTableViewCell
            
            if let objnam: String = light["OBJNAM"] as? String {
                lightCell.objNam = objnam
                if let status: String = self.property!.configurationModel.ControlObjects[objnam]!["STATUS"] as? String {
                    lightCell.onOffSwitch.on = (status == "ON") ? true : false
                }
                
                if (self.property!.configurationModel.hasColorCapabilities(objnam)){
                    let actParam = self.property!.configurationModel.getParam(objnam, param: "USE")
                    let lightCode = LightCode(enumKey:actParam)
                    lightCell.setLightMode(LightMode(lightCode: lightCode!))
                    lightCell.lightColorBar.hidden = false
                }else if (property!.configurationModel.isDimmable(objnam)){
                    if let dimmerValue = light["LIMIT"] as? NSString{
                        lightCell.setDimmer(dimmerValue.integerValue)
                    }else{
                        lightCell.setDimmer(nil)
                    }
                    lightCell.lightColorBar.hidden = false
                }else{
                    if (lightCell.lightColorBar != nil){
                        lightCell.lightColorBar.hidden = true
                    }
                }
            }
            
            lightCell.titleLabel!.text = light["SNAME"] as? String
            lightCell.onOffSwitch.tag = indexPath.row
            cell = lightCell
            break
            
        case 1:
            let lightGroup: [String : AnyObject] = self.lightGroups[indexPath.row] as! [String : AnyObject]
            
            let lightGroupCell: LightGroupTableViewCell = tableView.dequeueReusableCellWithIdentifier("GroupCell", forIndexPath: indexPath) as! LightGroupTableViewCell
            
            if let objnam: String = lightGroup["OBJNAM"] as? String {
                lightGroupCell.objNam = objnam
                if (self.property!.configurationModel.hasColorCapabilities(objnam)){
                    let actParam = self.property!.configurationModel.getParam(objnam, param: "USE")
                    let lightCode = LightCode(enumKey:actParam)
                    lightGroupCell.setLightMode(LightMode(lightCode: lightCode!))
                    lightGroupCell.lightColorBar.hidden = false
                }else if (property!.configurationModel.isDimmable(objnam)){
                    if let dimmerValue = lightGroup["LIMIT"] as? NSString{
                        lightGroupCell.setDimmer(dimmerValue.integerValue)
                    }else{
                        lightGroupCell.setDimmer(nil)
                    }
                    lightGroupCell.lightColorBar.hidden = false
                }else{
                    if (lightGroupCell.lightColorBar != nil){
                        lightGroupCell.lightColorBar.hidden = true
                    }
                }
            }
            
            lightGroupCell.titleLabel!.text = lightGroup["SNAME"] as? String
            lightGroupCell.onButton.tag = indexPath.row
            lightGroupCell.offButton.tag = indexPath.row
            cell = lightGroupCell
            break
            
        default:
            break
        }
        
        return cell
    }
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        var height:CGFloat = 65
        var light = [String : AnyObject]()
        
        switch indexPath.section {
        case 0:
            light = self.lights[indexPath.row] as! [String : AnyObject]
            break
        case 1:
            light = self.lightGroups[indexPath.row] as! [String : AnyObject]
            break
        default:
            break
        }
        
        if let objNam = light["OBJNAM"] as? String{
            if (self.property!.configurationModel.hasColorCapabilities(objNam) || self.property!.configurationModel.isDimmable(objNam)){
                height = 125
            }
        }
        
        return height
        
    }
    
    @IBAction func AllLightsOn(sender: UIButton) {
        let keyValues: [String: String] = ["STATUS": "ON"]
        let parameters = [KeyValue().setParametersObject("_A111", keyValues: keyValues)]
        
        self.property!.sendRequest(setParamList().buildSetParameters(parameters))
    }
    @IBAction func AllLightsOff(sender: UIButton) {
        let keyValues: [String: String] = ["STATUS": "OFF"]
        let parameters = [KeyValue().setParametersObject("_A110", keyValues: keyValues)]
        
        self.property!.sendRequest(setParamList().buildSetParameters(parameters))
    }

    @IBAction func turnLightOnOff(sender: UISwitch) {
        let cell: LightTableViewCell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: sender.tag, inSection: 0)) as! LightTableViewCell
        
        let keyValues: [String: String] = ["STATUS": sender.on ? "ON" : "OFF"]
        let parameters = [KeyValue().setParametersObject(cell.objNam!, keyValues: keyValues)]
        
        self.property!.sendRequest(setParamList().buildSetParameters(parameters))
    }
    
    // MARK: - Light Groups IBAction
    
    @IBAction func turnLightGroupOn(sender: UIButton) {
        let cell: LightGroupTableViewCell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: sender.tag, inSection: 1)) as! LightGroupTableViewCell
        
        let keyValues: [String: String] = ["STATUS": "ON"]
        let parameters = [KeyValue().setParametersObject(cell.objNam!, keyValues: keyValues)]
        
        self.property!.sendRequest(setParamList().buildSetParameters(parameters))
    }
    
    @IBAction func turnLightGroupOff(sender: UIButton) {
        let cell: LightGroupTableViewCell = self.tableView.cellForRowAtIndexPath(NSIndexPath(forRow: sender.tag, inSection: 1)) as! LightGroupTableViewCell
        
        let keyValues: [String: String] = ["STATUS": "OFF"]
        let parameters = [KeyValue().setParametersObject(cell.objNam!, keyValues: keyValues)]
        
        self.property!.sendRequest(setParamList().buildSetParameters(parameters))
    }
    
    
    // MARK: - Notification processing
    func statusChange(notification: NSNotification) {
        self.tableView.reloadData()
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        performSegueWithIdentifier("showLightDetails", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let indexPath: NSIndexPath = self.tableView.indexPathForSelectedRow!
        if let cell = self.tableView.cellForRowAtIndexPath(indexPath) as? LightTableViewCellProtocol{
            let controller = segue.destinationViewController as! LightDetailViewController
            controller.property = self.property
            controller.objNam = cell.objNam
        }
    }
}
