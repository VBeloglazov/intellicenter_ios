//
//  ViewController.swift
//  Pentair-iOs
//
//  Created by Jim Hewitt on 12/9/14.
//  Copyright (c) 2014 Pentair Corporation. All rights reserved.
//

import UIKit
import Foundation
import CoreData
import Pentair


class LoginViewController: UIViewController, UINavigationControllerDelegate {
    
    var error: NSError?
    var classIdentifier: CFStringRef = "Pentair"
    var email: String?
    var password: String?
    let httpService = HttpService()
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var chkRememberMe: UISwitch!
    @IBOutlet weak var lblLoginError: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var LoginButton: UIButton!
    @IBOutlet weak var LocalLoginButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicator.hidden = true
        lblLoginError.hidden = true
        
        self.LoginButton.layer.cornerRadius = 5
        
        //let passcode = AUTH_SERVICE.getPassword()
        
        let loginInfo: [String : AnyObject] = httpService.isLoginInfoAvailable()
        let loginStatus: Bool = loginInfo["status"] as! Bool
        if  loginStatus == true {
            self.email = loginInfo["userName"] as? String
            self.password = loginInfo["passcode"] as? String
            
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LoginViewController.loginEvent), name: "HttpLogin", object: httpService)
            
            httpService.login(self.email!, password: self.password!,saveLoginInfo: false)
        }
        
        setAccessibilityIdentifiers()
    }
    
    func setAccessibilityIdentifiers(){
        txtEmail.accessibilityIdentifier = "EmailField"
        txtPassword.accessibilityIdentifier = "PasswordField"
        chkRememberMe.accessibilityIdentifier = "RememverMeCheckBox"
        lblLoginError.accessibilityIdentifier = "LoginErrorLabel"
        LoginButton.accessibilityIdentifier = "LoginButton"
        LocalLoginButton.accessibilityIdentifier = "LocalLoginButton"
        view.accessibilityIdentifier = "LoginView"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // Hide the keyboard when not needed
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func btnLogin(sender: AnyObject) {
        self.view.endEditing(true)
        if (txtEmail.text!.isEmpty) {
            txtEmail.text = "Required"
            txtEmail.selectAll(sender)
        } else {
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LoginViewController.loginEvent), name: "HttpLogin", object: httpService)
            
            email = txtEmail.text
           // txtEmail.text = ""
            password = txtPassword.text
            txtPassword.text = ""
            activityIndicator.hidden = false
            activityIndicator.startAnimating()
            httpService.login(email!, password: password!, saveLoginInfo: chkRememberMe.on)
        }
    }
    
    func loginEvent() {
        activityIndicator.stopAnimating()
        activityIndicator.hidden = true
        
        if httpService.isUserLoggedIn() {
            lblLoginError.hidden = true
            lblLoginError.enabled = false
            
            self.performSegueWithIdentifier("showProperties", sender: self)
        } else {
            lblLoginError.hidden = false
            lblLoginError.enabled = true
        }
    }
    
    class func displayLoginResults(data: Int?) {
        print("results:")
        if (nil != data)
        {
            print(data)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "showProperties") {
            let vc: ManageInstallationsViewController = segue.destinationViewController as! ManageInstallationsViewController
            vc.httpService = httpService
        }
    }
    
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "HttpLogin", object: httpService)
        self.navigationController?.navigationBarHidden = false
    }
}

