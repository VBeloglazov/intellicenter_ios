//
//  ManageInstallationsViewController.swift
//  Pentair-iOs
//
//  Created by Dashon Howard on 2/23/15.
//  Copyright (c) 2015 Pentair Corporation. All rights reserved.
//
import Foundation
import UIKit
import Pentair
import Utility

// MARK: Local Constants for Nofifications
private let POOL_SELECTED = "poolSelected"
private let REMOTE_POOL = "remotePool"
private let LOCAL_POOL = "localPool"

class ManageInstallationsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate,InstallationDetailCellViewDelegate{
    
    
    @IBOutlet weak var filterSelectorButton: UIButton!
    @IBOutlet weak var InstallationTableView: UITableView!
    @IBOutlet var TableViewFooter:TableLoadingFooter!
    @IBOutlet weak var SearchButton: UIBarButtonItem!
    var titleLabel:UIView?
    
    var refreshControl = UIRefreshControl()
    var selectedInstallation : AnyObject?
    var selectedPoolName : String?
    var selectedFilter = InstallationConnectionStatus.All
    var searchString = ""
    var currentPage = 1
    var pageSize = 20
    var installations:[AnyObject] = [AnyObject]()
    var totalInstallations:Int?
    var loading = false
    var installationAccessType: NSNumber?
    
    var searchBar = UISearchBar(frame: CGRectMake(0, 0, 400, 44))
    var httpService:HttpService?
    let netServiceBrowser = Discovery(service: "Pentair")
    var host: String?
    var selectedInstallationName: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        //add refresh Control
        refreshControl.addTarget(self, action: #selector(ManageInstallationsViewController.refreshInstallations(_:)), forControlEvents: UIControlEvents.ValueChanged)
        InstallationTableView.addSubview(refreshControl)
        
        TableViewFooter.hidden = true
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ManageInstallationsViewController.installationsWereModified), name: "InstallationRemoved", object: httpService)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ManageInstallationsViewController.installationsDidUpdate(_:)), name: "InstallationsListUpdated", object: httpService)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ManageInstallationsViewController.localInstallationsDidUpdate(_:)), name: UTDataUpdatedNotification, object: netServiceBrowser)
        //  NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("installationsUpdateFailed"), name: "InstallationsListUpdateFailed", object: httpService)
        
        navigationController?.title = "Select Installation"
        titleLabel = navigationItem.titleView
        
        navigationItem.titleView = searchBar
        
        view.accessibilityIdentifier = "ManagePoolsView"
        SearchButton.accessibilityIdentifier = "Search_btn"
        
        searchBar.delegate = self
        searchBar.enablesReturnKeyAutomatically = false
        
        if let selectedIndex:Int = InstallationConnectionStatus.getIndex(selectedFilter){
            filterSelectorButton.setTitle(InstallationConnectionStatus.allStrings[selectedIndex], forState: .Normal)
        }
        
           NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.setPoolName(_:)), name: POOL_SELECTED, object: nil)
            
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.setPoolRemote(_:)), name: REMOTE_POOL, object: nil)
            
            NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.setPoolLocal(_:)), name: LOCAL_POOL, object: nil)
        
        hideSearch(self)
        refreshInstallations(self)
    }
    
    override func viewWillAppear(animated: Bool) {
        self.navigationController!.navigationBar.barTintColor =  UIColor.whiteColor()
       
        
       // self.InstallationTableView.reloadData()
    }
    
    override func viewDidAppear(animated: Bool) {
        //updateTableAppearance()
        // self.InstallationTableView.reloadData()
    }
    
    func updateTableAppearance() {
//        self.tableView.reloadData()
//        self.view.setNeedsLayout()
//        self.view.layoutIfNeeded()
    }

    @IBAction func showFilters(sender: UIButton) {
        hideSearch(sender)
        
        //use action sheet picker
        let selectedIndex:Int = InstallationConnectionStatus.getIndex(selectedFilter)!
        ActionSheetStringPicker(title: "Filter by", rows: InstallationConnectionStatus.allStrings, initialSelection: selectedIndex, target: self, successAction: #selector(ManageInstallationsViewController.didSelectFilter(_:)), cancelAction: nil, origin: sender).showActionSheetPicker()
    }
    
    @IBAction func addInstallation(sender: UIBarButtonItem) {
        hideSearch(self)
    }
    
    @IBAction func didPressLogout(sender: UIBarButtonItem) {
        self.httpService!.logOut()
        self.navigationController?.popViewControllerAnimated(true)
    }
    @IBAction func showSearch(sender: AnyObject) {
        if (!searchBar.hidden){
            searchBarSearchButtonClicked(searchBar)
        }else{
            navigationItem.titleView = searchBar
            searchBar.hidden = false
            searchBar.text = searchString
            searchBar.becomeFirstResponder()
        }
    }
    
    func hideSearch(sender:AnyObject){
        navigationItem.titleView = titleLabel
        searchBar.hidden = true
        searchBar.endEditing(true)
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Local Connections"
        default:
            return "Remote Connections"
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return netServiceBrowser.services.count
        case 1:
            return self.installations.count
        default:
            return 0
        }
    }
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerView(section)
    }
    func headerView(section: Int) -> UIView {
        let view = UIView(frame: CGRectMake(0, 0, self.view.frame.size.width, 40))
        view.backgroundColor = UIColor.whiteColor()
        let label = UILabel(frame: CGRectMake(0, 0, self.view.frame.size.width, 40))
        label.textColor = UIColor.blackColor()
        label.textAlignment = NSTextAlignment.Center
        label.font = UIFont(name: "Helvetica", size: 20.0)
        if 0 == section {
            label.text = "Local Connections"
        } else {
            label.text = "Remote Connections"
        }
        view.addSubview(label)
        view.accessibilityIdentifier = label.text! + "_SectionHeader"
        return view
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(36)
    }
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 175
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = InstallationTableView.dequeueReusableCellWithIdentifier("InstallationDetailCell", forIndexPath: indexPath) as! InstallationDetailCell
        cell.delegate = self
        cell.clear()
        
        switch (indexPath.section) {
        case 0:
            if (netServiceBrowser.services.count > indexPath.row){
                let localConnection = netServiceBrowser.services[indexPath.row]
                var poolNameLocal = localConnection.name
                cell.PoolNameLabel.text =  localConnection.name
                cell.ipAddress = localConnection.ipAddress
                cell.portNumber = localConnection.port
                cell.tag = indexPath.row
                setConnectBtnState(cell.ConnectButton, isOnline: true)
            }
            break
        case 1:
            let installation = installations[indexPath.row]
            var poolnameRemote = (installation["PoolName"] as? String)!
            cell.PoolNameLabel?.text = poolnameRemote
            cell.tag = indexPath.row
            
            cell.installationID = (installation["InstallationId"] as! Int).description
            if let isOnline = installation["OnLine"] as? Bool{
                setConnectBtnState(cell.ConnectButton, isOnline: isOnline)
            }
            if let address = installation["Address"] as? String{
                cell.PoolAddressLabel.text = address
            }
            if let city = installation["City"] as? String,
                let state = installation["State"] as? String,
                let zip = installation["Zip"] as? String{
                cell.PoolAddress2Label.text = String(format: "%@, %@ %@", city, state, zip)
                          }
        default:
            break
        }
        // cell.accessibilityIdentifier = cell.PoolNameLabel.text! + "_PropertyCell"
        return cell
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
//        if (maximumOffset - currentOffset) <= 40 {
//            fetchInstallations(currentPage+1)
//        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
          print("connectiontype for section should be \(selectedInstallation?.indexPath.section)")
        
        tableView.deselectRowAtIndexPath(indexPath, animated: false)
        if (indexPath.section == 1) {
          selectedInstallation = installations[indexPath.row]
           // print("Selected Installation is:  \(selectedInstallation)")
          //  self.selectedInstallationName = selectedInstallation!.name
            performSegueWithIdentifier("showInstallationDetails", sender: self)
        } else {
            performSegueWithIdentifier("showDashboard", sender: self)
        }
    }
    
    func setPoolName(notification: NSNotification) {
        //deal with notification.userInfo
        print(notification.userInfo)
        if let info = notification.userInfo as? Dictionary<String,String> {
            if info["poolName"] != nil {
                 self.selectedPoolName = info["poolName"]
            }
            else {
                print("no value for key\n")
            }
        }
        else {
            print("wrong userInfo type")
        }
        print("Received poolChangedNotification")
    }
    
    
    func setPoolLocal(notification: NSNotification) {
        print("Received pool is local notification")
        self.installationAccessType = 0
    }
    
    func setPoolRemote(notification: NSNotification) {
        print("Received pool is remote notification")
        self.installationAccessType = 1
    }

    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        
        if (segue.identifier == "showInstallationDetails") {
            
            let vc: InstallationDetailsViewController = segue.destinationViewController as! InstallationDetailsViewController
            vc.installation = self.selectedInstallation
            vc.httpService = httpService!
            
        } else if (segue.identifier == "showDashboard") {
            //let vc: DashboardViewController = segue.destinationViewController as! DashboardViewController
            let vc: HomeTableViewController = segue.destinationViewController as! HomeTableViewController
            
//            if(selectedInstallation?.indexPath.section == 1){
//                vc.connectionType = 1
//            } else {
//                vc.connectionType = 0
//            }
    
             // does not always have httpService here -- need to add a guard statement - will produce a crash
            
            vc.property = Property(urlString: host!, service: httpService!)
            
            vc.httpService = httpService
            vc.selectedPoolName = self.selectedPoolName
            //print("Selected Installation is:  \(selectedInstallation)")
            vc.selectedInstallation = self.selectedInstallation
            vc.installationAccessType = self.installationAccessType
        }
        
        hideSearch(self)
    }
    
    func didSelectFilter(selectedIndex: NSNumber){
        selectedFilter = InstallationConnectionStatus.allValues[selectedIndex.integerValue]
        filterSelectorButton.setTitle(InstallationConnectionStatus.allStrings[selectedIndex.integerValue], forState: .Normal)
        refreshInstallations(self)
    }
    
   func refreshInstallations(sender:AnyObject){
        InstallationTableView.backgroundView = nil
        installations = [AnyObject]()
        
        fetchInstallations(1)
        InstallationTableView.reloadData()
    }
    
    func fetchInstallations(page:Int){
        netServiceBrowser.reset()
        netServiceBrowser.searchForLocalProperties()
        if httpService!.isUserLoggedIn() {
            if (!loading && (installations.count < self.totalInstallations || 1 == page ||  self.totalInstallations == 0 )) {
                currentPage = page
                setLoadingState(true)
                httpService!.getInstallations(pageSize, page: page, filter: selectedFilter, searchTerm: searchString)
            }
        }
    }
    
    func installationsWereModified(){
        refreshInstallations(self)
    }
    
    func installationsDidUpdate(notification:NSNotification) {
        if let newData = notification.userInfo?["installations"] as? [AnyObject]{
            self.totalInstallations = newData.count
            updateInstallationsList(newData)
        }
        setLoadingState(false)
    }
    
    func localInstallationsDidUpdate(notification:NSNotification) {
        InstallationTableView.reloadData()
    }
    
    func updateInstallationsList(newData:[AnyObject]){
        if (newData.count > 0){
            for installation:AnyObject in newData {
                self.installations.append(installation)
            }
        }
        
        if( self.installations.count < 1){
            let label = UILabel(frame: CGRectMake(0, 0, 400, 20))
            label.textColor = UIColor.whiteColor()
            label.text = "No Pools Found!"
            InstallationTableView.backgroundView = label
        }
        InstallationTableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        hideSearch(searchBar)
        searchBar.text = searchString
    }
    
    func searchBarShouldEndEditing(searchBar: UISearchBar) -> Bool {
        searchBar.setShowsCancelButton(false, animated: true)
        return true
    }
    
    func searchBarShouldBeginEditing(searchBar: UISearchBar) -> Bool {
        searchBar.setShowsCancelButton(true, animated: true)
        return true
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchString =  searchBar.text!
        hideSearch(searchBar)
        refreshInstallations(self)
    }
    
    func setLoadingState(loading:Bool) {
        self.loading = loading
        if(!loading){
            refreshControl.endRefreshing()
            TableViewFooter.hidden = true
        }else if(!refreshControl.refreshing){
            TableViewFooter.hidden = false
        }
    }
    
    func remoteLogin(installationID: String){
        //@todo if no password, then crashes here
        if let token = self.httpService!.authToken() {
            host = baseConnection + "api/websocket/" + installationID + "?access-token=" + token
        }
        self.performSegueWithIdentifier("showDashboard", sender: self)
    }
    
    func localLogin(ipAddress: String, port:String){
        host = "http://" + ipAddress + ":" + port
        self.performSegueWithIdentifier("showDashboard", sender: self)
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.InstallationTableView.delegate = nil
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "InstallationRemoved", object: httpService)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "InstallationsListUpdated", object: httpService)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: UTDataUpdatedNotification, object: netServiceBrowser)
        // NSNotificationCenter.defaultCenter().removeObserver(self, name: "InstallationsListUpdateFailed", object: httpService)
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
}
