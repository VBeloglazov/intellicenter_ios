//
//  ManageUsersViewController.swift
//  Pentair-iOs
//
//  Created by Dashon Howard on 1/15/15.
//  Copyright (c) 2015 Pentair Corporation. All rights reserved.
//

import Foundation
import UIKit
import Pentair

public enum UserInvitationStatus : String {
    case All = "ALL", Accepted = "ACCEPTED", Unknown = "UNKNOWN",Bounced = "BOUNCED", Expired = "EXPIRED"
    
    static let allValues = [All,Accepted,Unknown,Bounced,Expired]
    static let allStrings = ["All","Accepted","No Response","Bounced","Expired"]
    
    static func getIndex(find:UserInvitationStatus) -> Int?{
        for (index, value) in allValues.enumerate() {
            if (value == find){
                return index
            }
        }
        return nil
    }
    
}

class ManageUsersViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    @IBOutlet weak var FilterSelectorButton: UIButton!
    
    @IBOutlet weak var UserTableView: UITableView!
    @IBOutlet weak var AddUserButton: UIButton!
    @IBOutlet weak var RemoveUserButton: UIButton!
    @IBOutlet weak var SearchButton: UIBarButtonItem!
    @IBOutlet var TableViewFooter:TableLoadingFooter!
    
    var titleLabel:UIView?
    
    var refreshControl = UIRefreshControl()
    var selectedUser : AnyObject?
    var selectedFilter = UserInvitationStatus.All
    var searchString = ""
    var currentPage = 1
    var pageSize = 20
    var checkboxSelected = Bool()
    var loading = false
    var property:Property?
    var configModel:ConfigurationModel?
    var searchBar = UISearchBar(frame: CGRectMake(0, 0, 400, 44))
    var checkboxButton : Checkbox!
    let imageSize : CGFloat = 28
    let gap : CGFloat = 10
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //add refresh Control
        refreshControl.addTarget(self, action: #selector(ManageUsersViewController.refreshUsers(_:)), forControlEvents: UIControlEvents.ValueChanged)
        UserTableView.addSubview(refreshControl)
        
        self.AddUserButton.layer.cornerRadius = 5
        self.RemoveUserButton.layer.cornerRadius = 5
        self.TableViewFooter.activityIndicator.hidesWhenStopped = true
        
        configModel = property?.configurationModel
        
        TableViewFooter.hidden = true

        view.accessibilityIdentifier = "ManageUsersView"
        //FilterButton.accessibilityIdentifier = "Filter_btn"
        //SearchButton.accessibilityIdentifier = "Search_btn"
        //FilterPanel.accessibilityIdentifier = "FilterPanel"
        
        navigationController?.title = "Manage Users"
        titleLabel = navigationItem.titleView
        navigationItem.titleView = searchBar
        
        SearchButton.accessibilityIdentifier = "Search_btn"
        
        searchBar.delegate = self
        searchBar.enablesReturnKeyAutomatically = false
        
        if let selectedIndex:Int = UserInvitationStatus.getIndex(selectedFilter){
            FilterSelectorButton.setTitle(UserInvitationStatus.allStrings[selectedIndex], forState: .Normal)
        }
        
        hideSearch(self)
        refreshUsers(self)
    }
    
    
    //@todo needs to refresh
    @IBAction func showFilters(sender: UIButton) {
        hideSearch(sender)
        
        //use action sheet picker
        let selectedIndex:Int = UserInvitationStatus.getIndex(selectedFilter)!
        ActionSheetStringPicker(title: "Filter by", rows: UserInvitationStatus.allStrings, initialSelection: selectedIndex, target: self, successAction: #selector(ManageUsersViewController.didSelectFilter(_:)), cancelAction: nil, origin: sender).showActionSheetPicker()
        
    }
    
    
    
    @IBAction func inviteUser(sender: UIButton) {
        performSegueWithIdentifier("showInviteUser", sender: self)
    }
    
    @IBAction func removeUser(sender: UIButton) {
        
        let DeleteAlert = UIAlertController(title: "Delete?", message: "This Cannont Be Undone!", preferredStyle: UIAlertControllerStyle.Alert)
        let NotifyAlert = UIAlertController(title: "Notify?", message: "Would you like an e-mail notice sent to this account to let them know they have been removed from this pool?", preferredStyle: UIAlertControllerStyle.Alert)
        
        
        DeleteAlert.addAction(UIAlertAction (title: "Ok", style: .Default, handler: { action in
            self.presentViewController(NotifyAlert, animated: true, completion: nil)
        }))
        
        DeleteAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { action in
        }))
        
        NotifyAlert.addAction(UIAlertAction(title: "Notify", style: .Default, handler: { action in
            self.sendDeleteMessage(true)
        }))
        
        NotifyAlert.addAction(UIAlertAction(title: "Do Not Notify", style: .Default, handler: { action in
            self.sendDeleteMessage(false)
        }))
        
        self.presentViewController(DeleteAlert, animated: true, completion: nil)
    }
    
    
    @IBAction func showSearch(sender: AnyObject) {
        if (!searchBar.hidden){
            searchBarSearchButtonClicked(searchBar)
        }else{
            navigationItem.titleView = searchBar
            searchBar.hidden = false
            searchBar.text = searchString
            searchBar.becomeFirstResponder()
        }
    }
    func hideSearch(sender:AnyObject){
        navigationItem.titleView = titleLabel
        searchBar.hidden = true
        searchBar.endEditing(true)
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    //UITableView delegate methods
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return configModel!.Users.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = IconCell()
        cell = UserTableView.dequeueReusableCellWithIdentifier("IconCell", forIndexPath: indexPath) as! IconCell
        
        if(indexPath.row < configModel!.Users.count){
        let user = configModel!.Users[indexPath.row]
        //cell.addSubview(self.radioButton)
        cell.titleLabel?.text = user["name"] as? String
        cell.iconView.layer.cornerRadius = cell.iconView.frame.size.width / 2
        cell.iconView.image = getStatusImage(user["status"] as! String)
        cell.iconView.clipsToBounds = true
        cell.checkbox.isChecked = false
        cell.accessibilityIdentifier = cell.titleLabel!.text! + "_UserCell"
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        }
        return cell
    }
    
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        if ((maximumOffset - currentOffset) <= 40 && (currentOffset > 0))  {
           //fetchUsers(currentPage+1)
        }
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        selectedUser = configModel!.Users[indexPath.row]
        performSegueWithIdentifier("showUserDetails", sender: self)
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if (segue.identifier == "showUserDetails") {
            let vc: EditUserViewController = segue.destinationViewController as! EditUserViewController
            vc.user = selectedUser!
            vc.property = property!
        }else if (segue.identifier == "showInviteUser") {
            let vc: AddUserViewController = segue.destinationViewController as! AddUserViewController
            vc.property = property!
        }
        
        hideSearch(self)
    }
    
    func didSelectFilter(selectedIndex: NSNumber){
        selectedFilter = UserInvitationStatus.allValues[selectedIndex.integerValue]
        FilterSelectorButton.setTitle(UserInvitationStatus.allStrings[selectedIndex.integerValue], forState: .Normal)
        refreshUsers(self)
    }
    
    func usersWereRefreshed(){
        currentPage = 1
        usersDidUpdate()
    }
    
    func usersDidUpdate(){
        updateUsersList()
        setLoadingState(false)
    }
    
    func usersWereModified(){
        refreshUsers(self)
    }
    
    func userWasInvited(){
        showNewUserAddedAlert()
    }
    
    
    func showNewUserAddedAlert(){
        let NewUserAddedAlert = UIAlertController(title: "New User Invited", message: "New user invitation has been sent", preferredStyle: UIAlertControllerStyle.Alert)
        
        NewUserAddedAlert.addAction(UIAlertAction(title: "Confim", style: .Default, handler: { action in
            self.usersWereModified()
        }))
        
        NewUserAddedAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { action in
        }))
        
        self.presentViewController(NewUserAddedAlert, animated: true, completion: nil)
    }
    
    
    func updateUsersList(){
        if (configModel!.Users.count < 1){
            let label = UILabel(frame: CGRectMake(0, 0, 400, 20))
            label.textColor = UIColor.whiteColor()
            label.text = "No Users Found!"
            UserTableView.backgroundView = label
        }
        UserTableView.reloadData()
    }
    
    func refreshUsers(sender:AnyObject){
        UserTableView.backgroundView = nil
        fetchUsers(1)
        UserTableView.reloadData()
    }
    
    func fetchUsers(page:Int){
        if (!loading && (configModel!.Users.count < configModel!.TotalUsers || 1 == page ||  configModel!.TotalUsers == 0 )) {
            currentPage = page
            setLoadingState(true)
            property!.getUsers(pageSize, page: page, filter: selectedFilter.rawValue, searchTerm: searchString)
        }
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        hideSearch(searchBar)
        searchBar.text = searchString
    }
    
    func searchBarShouldEndEditing(searchBar: UISearchBar) -> Bool {
        searchBar.setShowsCancelButton(false, animated: true)
        return true
    }
    
    func searchBarShouldBeginEditing(searchBar: UISearchBar) -> Bool {
        //var textField = searchBar.valueForKey("_searchField") as! UITextField
        searchBar.setShowsCancelButton(true, animated: true)
        return true
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchString =  searchBar.text!
        hideSearch(searchBar)
        refreshUsers(self)
    }
    
    func setLoadingState(loading:Bool) {
        self.loading = loading
        if (!loading){
            refreshControl.endRefreshing()
            TableViewFooter.activityIndicator.stopAnimating()
            TableViewFooter.hidden = true
        }else if (!refreshControl.refreshing){
            TableViewFooter.activityIndicator.stopAnimating()
            TableViewFooter.hidden = false
        }
    }
    
    func sendDeleteMessage(notify:Bool){
        for var i = 0; i < UserTableView.numberOfRowsInSection(0); i = i+1 {
            let indexPath = NSIndexPath(forRow: i, inSection: 0)
            if let cell =  UserTableView.cellForRowAtIndexPath(indexPath) as? IconCell{
                if (cell.selected)
                {
                    let user = configModel!.Users[i]
                    
                    if let userName = user["name"] as? String,
                        let userStatus = user["status"] as? String{
                        self.property!.deleteUser(userName, status:userStatus, notify: notify)
                    }
                    
                }
            }
        }
        refreshUsers(self)
    }
    override func viewWillAppear(animated: Bool) {
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ManageUsersViewController.usersDidUpdate), name: "UsersListUpdated", object: property)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ManageUsersViewController.usersWereModified), name: "UserModified", object: property)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ManageUsersViewController.userWasInvited), name: "UserInvited", object: property)
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "UsersListUpdated", object: property)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "UserModified", object: property)
    }
}
