//
//  MenuViewController.swift
//  Pentair-iOs
//
//  Created by Dashon Howard on 1/15/15.
//  Copyright (c) 2015 Pentair Corporation. All rights reserved.
//

import Foundation
import UIKit
import Pentair

enum MenuOptions : String {
    case ManageUsers = "Manage Users", ManageInstallations = "Manage Pools", SpaRemotes = "Spa-Side Remotes", VacationMode = "Vacation Mode", ViewNotifications = "View Notifications", LogOut = "Log Out"
    
    static let allValues = [ManageUsers, ManageInstallations, SpaRemotes, VacationMode,ViewNotifications, LogOut]
}
class MenuViewController: UITableViewController {
    var property:Property?
    var httpService:HttpService?
    var selectedInstallation:AnyObject?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.separatorColor = UIColor.blackColor()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.opaque = false
        self.tableView.backgroundColor = UIColor.clearColor()
        
        view.accessibilityIdentifier = "MenuView"
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MenuViewController.statusChange(_:)), name: "ConfigurationUpdate", object: property)
    }
    
    override func viewDidAppear(animated: Bool) {
        tableView.reloadData()
        
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return MenuOptions.allValues.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let menuItem = MenuOptions.allValues[indexPath.row]
        let cell = self.tableView.dequeueReusableCellWithIdentifier("MenuCell", forIndexPath: indexPath)
        
        cell.textLabel?.font  = UIFont(name: "Helvetica", size: 20.0)
        cell.backgroundColor = UIColor.clearColor()
        
        cell.textLabel?.text = menuItem.rawValue
        cell.textLabel?.textColor = UIColor.darkTextColor()
        
        cell.accessibilityIdentifier = MenuOptions.allValues[indexPath.row].rawValue + "_MenuCell"
        
        if (menuItem == MenuOptions.SpaRemotes){
            if (property!.configurationModel.areAllRemotesDisabled()){
                cell.textLabel?.text = "Enable " +  cell.textLabel!.text!
            }else{
                cell.textLabel?.text = "Disable " +  cell.textLabel!.text!
            }
        }
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        let menuItem = MenuOptions.allValues[indexPath.row]
        let navController = self.frostedViewController.contentViewController as! NavigationViewController
        
        switch menuItem {
        case MenuOptions.ManageUsers:
            let manageUsersViewController = self.storyboard!.instantiateViewControllerWithIdentifier("manageUsersController") as! ManageUsersViewController
            manageUsersViewController.property = property!
            navController.pushViewController(manageUsersViewController, animated: true)
            break
        case MenuOptions.ManageInstallations:
            let manageInstallationsViewController = self.storyboard!.instantiateViewControllerWithIdentifier("manageInstallationsController") as! ManageInstallationsViewController
            manageInstallationsViewController.httpService = httpService!
            navController.pushViewController(manageInstallationsViewController, animated: true)
            break
        case MenuOptions.LogOut:
            self.httpService!.logOut()
            self.performSegueWithIdentifier("showLogin", sender: self)
            break
        case MenuOptions.SpaRemotes:
            showSpaSideRemoteActionAlert()
            break
        case MenuOptions.VacationMode:
            let vacationModeViewController = self.storyboard!.instantiateViewControllerWithIdentifier("vacationModeController") as! VacationModeViewController
            vacationModeViewController.property = property!
            vacationModeViewController.selectedInstallation = self.selectedInstallation
            navController.pushViewController(vacationModeViewController, animated: true)
            break
        case MenuOptions.ViewNotifications:
            let notificationsViewController = self.storyboard!.instantiateViewControllerWithIdentifier("viewNotificationsController") as! NotificationsViewController
            notificationsViewController.property = property!
            navController.pushViewController(notificationsViewController, animated: true)
            break
        }
        self.frostedViewController.hideMenuViewController()
    }
    
    func showSpaSideRemoteActionAlert(){
        var status = "OFF"
        var text = "disable"
        if (property!.configurationModel.areAllRemotesDisabled()){
            status = "ON"
            text = "enable"
        }
        let NewUserAddedAlert = UIAlertController(title: "Spa Side Remotes", message: "Would you like " + text + " to all Spa-Side Remotes?", preferredStyle: UIAlertControllerStyle.Alert)
        
        NewUserAddedAlert.addAction(UIAlertAction(title: "Yes, " + text + " all", style: .Default, handler: { action in
            let remotes = self.property!.configurationModel.getAllRemotes()
            for remote in remotes{
                if let objnam = remote["OBJNAM"] as? String{
                    let keyValues: [String: String] = ["ENABLE": status]
                    let parameters = [KeyValue().setParametersObject(objnam, keyValues: keyValues)]
                    self.property!.sendRequest(setParamList().buildSetParameters(parameters))
                }
            }
        }))
        
        NewUserAddedAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { action in
        }))
        
        self.presentViewController(NewUserAddedAlert, animated: true, completion: nil)
    }
    
    
    // MARK: - Notification processing
    func statusChange(notification: NSNotification) {
        tableView.reloadData()
    }
}
