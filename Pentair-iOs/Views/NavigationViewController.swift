//
//  NavigationViewController.swift
//  Pentair-iOs
//
//  Created by Dashon Howard on 1/21/15.
//  Copyright (c) 2015 Pentair Corporation. All rights reserved.
//

import Foundation
class NavigationViewController : UINavigationController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //swipe to show menu
      //  super.view.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: "panGestureRecognized:"))
    }
    
    func panGestureRecognized (sender:UIPanGestureRecognizer){
        //if (self.topViewController!.isKindOfClass(DashboardViewController)) {
        if (self.topViewController!.isKindOfClass(HomeTableViewController)) {
           
            //Kill Keyboard
            self.view.endEditing(true)
            self.frostedViewController.view.endEditing(true)
            
            self.frostedViewController.panGestureRecognized(sender)
        }
    }
    
}