//
//  PopoverTableViewController.swift
//  Pentair-iOs
//
//  Created by Path Finder on 11/13/15.
//  Copyright © 2015 Pentair Corporation. All rights reserved.
//

import UIKit
import Pentair

class PopoverTableViewController: UITableViewController {

   // var delegate: DashboardViewController?
    var delegate: AnyObject?
    var userInfo: [String : AnyObject]?
    var heatModes: PickerData?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.heatModes = self.userInfo!["heatModes"] as? PickerData
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.heatModes?.pickerItems.count)!
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        let pickerItem = self.heatModes?.pickerItems[indexPath.row]
        
        cell.textLabel?.text = pickerItem!.name
        
        return cell
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
       // let cell = tableView.cellForRowAtIndexPath(indexPath)
        
//        if ((delegate?.respondsToSelector("popoverDidReturn:cell:userInfo:")) == true) {
////            delegate?.popoverDidReturn(indexPath.row, cell: cell!, userInfo: self.userInfo!)
//        } else {
//            NSLog("\(delegate?.description) does not respond to the method popoverDidReturn:")
//        }
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }

}
