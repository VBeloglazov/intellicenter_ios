 //
//  PumpsViewController.swift
//  Pentair-iOs
//
//  Created by CS on 05/27/2016
//  Copyright © 2015 Pentair Corporation. All rights reserved.
//

import UIKit
import QuartzCore
import Pentair
import CoreData

 var DarkGreen = UIColor(hue: 0.4278, saturation: 0.46, brightness: 0.49, alpha: 1.0).CGColor /* #437c64 */
 var BrightGreen = UIColor(hue: 0.3806, saturation: 0.18, brightness: 1, alpha: 1.0) /* #d1ffde */
 
//   detailContainer.layer.borderColor = UIColor.grayColor().CGColor


 extension CollectionType {
      func find(@noescape predicate: (Self.Generator.Element) throws -> Bool) rethrows -> Self.Generator.Element? {
        return try indexOf(predicate).map({self[$0]})
    }
 }

 class PumpsViewController: UIViewController,UITableViewDataSource, UITableViewDelegate {    
    var property: Property?
    var objNam: String!
    var param:String?
    var pumpGroups = [AnyObject]()
    //var pumps = [AnyObject]()
    var pumpsArray = [Pump]()
    var effectsArray = [PumpEffect]()
    var tmpCircuitsArray = [Circuit]()
    var circuitsArray = [Circuit]()
    var tmpPump = Pump?()
    var filteredPumpEffects = [PumpEffect]()
    var names = [AnyObject]()
    var selectedPump: Pump! = nil
    var disclosureTriangle: UIImageView!
    var configModel:ConfigurationModel?
    var animating: Bool = false
    var newInstallationSelected: Bool = true
    var pumplistTableViewIsDisplayed: Bool = false
    var pumpDetailsViewIsDisplayed: Bool = false
    var cellHeight:CGFloat! = 80
    var animationDuration: NSTimeInterval! = 0.5
    var maskBackgroundOpacity: CGFloat! = 0.3
    var maskBackgroundColor: UIColor! = UIColor.blackColor()
    var pumpCircuitEffects = [AnyObject]()
    var effects = [AnyObject]()
    //var effectsArray = [AnyObject]()
    var tmpDictionary = [String: AnyObject?]()
    var fontName = "Avenir"
    var currentLevel: Float = 0.0
    var minLevel:Float = 0.0
    var maxLevel:Float = 100.0
    var counter:Int = 0
    let arrowOffset:CGFloat = 5.0
    let offset:CGFloat = 5
    let indicatorStartingPoint:CGFloat = 115
    var selectedInstallation:AnyObject!
    
   // @IBOutlet weak var dropDownView: UIView!
    @IBOutlet weak var arrowImage: UIImageView!
    @IBOutlet weak var pumpDetailsView: UIView!
    @IBOutlet weak var selectedPumpLabel: UILabel!
    @IBOutlet weak var pumpNameLabel: UILabel!
    @IBOutlet weak var pumpSpeedLabel: UILabel!
    @IBOutlet weak var pumpFlowLabel: UILabel!
    @IBOutlet weak var pumpUseageLabel: UILabel!
    @IBOutlet weak var pumpEffectsTableView: UITableView!
    @IBOutlet weak var pumplistTableView: UITableView!
    @IBOutlet weak var pumpslistButton: UIButton!
    @IBOutlet weak var buttonBackgroundView: UIView!
    @IBOutlet weak var activepumpBackgroundView: UIView!
    @IBOutlet weak var pumpSpeedView: UIView!
    @IBOutlet weak var pumpFlowView: UIView!
    @IBOutlet weak var pumpUseageView: UIView!
    @IBOutlet weak var pumpSpeedTriangle: UIImageView!
    @IBOutlet weak var pumpFlowTriangle: UIImageView!
    @IBOutlet weak var pumpUseageTriangle: UIImageView!
    
    @IBOutlet weak var pumpSpeedViewContainer: UIView!
    @IBOutlet weak var pumpFlowViewContainer: UIView!
    @IBOutlet weak var pumpUseageViewContainer: UIView!
    @IBOutlet weak var flowRateTitleLabel: UILabel!
    @IBOutlet weak var powerUseageTitleLabel: UILabel!
    @IBOutlet weak var pumpSpeedTitleLabel: UILabel!
    
    weak var delegate: EffectsTableViewCellViewDelegate?

    
    //***************************************************************************
    
    // MARK: View Lifecycle
    
    //***************************************************************************
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //deleteCachedData()  - //@todo - we want to do this when  we receive a notification that the pumps have changed
        self.tmpCircuitsArray = createCircuitsArray()
        self.pumpsArray = self.fetchPumpsFromLocalCache()
        //print("Pump array count when fetch pumps is called is \(self.pumpsArray.count)")
        if (self.pumpsArray.count) == 0 {
            self.pumpsArray = self.fetchPumpsWithEffects()
            // @todo self.tmpCircuitsArray = createCircuitsArray()
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        self.pumplistTableView.hidden = true
        self.pumplistTableViewIsDisplayed = false
        let shadowPath = UIBezierPath(rect: activepumpBackgroundView.bounds)
        self.buttonBackgroundView.layer.cornerRadius = 8
        self.pumplistTableView.layer.cornerRadius = 5
        self.pumplistTableView.layer.masksToBounds = false
        self.pumplistTableView.layer.shadowColor = UIColor.blackColor().CGColor
        self.pumplistTableView.layer.shadowOffset = CGSize(width: 0, height: 0.5)
        self.pumplistTableView.layer.shadowOpacity = 0.2
        self.pumplistTableView.layer.shadowPath = shadowPath.CGPath
        self.pumpDetailsView.layer.cornerRadius = 8
        self.pumpDetailsView.layer.masksToBounds = false
        self.pumpDetailsView.layer.shadowColor = UIColor.blackColor().CGColor
        self.pumpDetailsView.layer.shadowOffset = CGSize(width: 0, height: 0.5)
        self.pumpDetailsView.layer.shadowOpacity = 0.2
        self.pumpDetailsView.layer.shadowPath = shadowPath.CGPath
        self.activepumpBackgroundView.layer.cornerRadius = 5
        self.activepumpBackgroundView.layer.masksToBounds = false
        self.activepumpBackgroundView.layer.shadowColor = UIColor.blackColor().CGColor
        self.activepumpBackgroundView.layer.shadowOffset = CGSize(width: 0, height: 0.5)
        self.activepumpBackgroundView.layer.shadowOpacity = 0.2
        self.activepumpBackgroundView.layer.shadowPath = shadowPath.CGPath
        
        self.hidePumpDetailsView()
        self.showPumpListView()
        self.counter = 0
    }
    
    override func viewDidAppear(animated: Bool) {
          //self.deletePreviousPumps()
        //@todo - set up notification to listen for updates and save in background
        
//        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PumpsViewController.statusChange(_:)), name: "ConfigurationUpdate", object: property)
    
        self.pumplistTableView.reloadData()
    }
    
    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func fetchPumpsWithEffects() -> [Pump]{
        var pumps:[AnyObject]
        var tmpPumps:[Pump] = []
        var localEffectsArray:[PumpEffect]
        pumps = (property?.configurationModel.getAllPumps())!  //any object
        //pumps.sortInPlace({$0.getType().rawValue < $1.getType().rawValue})
        
        //@todo - this really should only happen if they have
        // selected a different installation without logging out
        // please revisit
        deleteCachedData()
        
        localEffectsArray = createEffectsArray()
        
        for pump in pumps {
            let thisPump = Pump.MR_createEntity() as! Pump
            
            thisPump.identifier = NSUUID().UUIDString
            thisPump.objectName = pump["OBJNAM"]! as? String
            thisPump.state = pump["STATIC"]! as? String
            thisPump.listOrder = pump["LISTORD"]! as? String
            thisPump.pumpType = pump["SUBTYP"]! as? String
            thisPump.displayName = pump["SNAME"]! as? String
            thisPump.circuitName = pump["CIRCUIT"]! as? String
            thisPump.rpm = pump["RPM"]! as? String
            thisPump.gpm = pump["GPM"]! as? String
            thisPump.pwr = pump["PWR"]! as? String
            
            NSManagedObjectContext .MR_defaultContext() .MR_saveToPersistentStoreAndWait()
            tmpPumps.append(thisPump)
        }
        
        //sort the array in ascending order
        tmpPumps = tmpPumps.sort({ $0.listOrder < $1.listOrder })
        
        for pumpObj in tmpPumps {
            self.filteredPumpEffects = [PumpEffect]()
            self.filteredPumpEffects = localEffectsArray.filter({ return $0.parentObject == pumpObj.objectName })
        
            pumpObj.effects = Set(self.filteredPumpEffects)
            NSManagedObjectContext .MR_defaultContext() .MR_saveToPersistentStoreAndWait()
           // print("filtered effects (filtered local array) count for this pump is: \(self.filteredPumpEffects.count)")
           // print("effects when attacted to the pump are: \(pumpObj.effects)")
            }
        
           return tmpPumps
        }

 

    func createEffectsArray() -> [PumpEffect]{
        var tmpeffects:[AnyObject]
        var effectsArray:[PumpEffect] = []
        
        tmpeffects = (property?.configurationModel.getAllPumpEffects())!
        // print("tmpeffects (pull from the socket) count is: \(tmpeffects)")
        
        for effect in tmpeffects {
            let thisEffect = PumpEffect.MR_createEntity() as! PumpEffect
            
            thisEffect.boost = effect["BOOST"] as? String
            thisEffect.circuitName = effect["CIRCUIT"] as? String
            thisEffect.listOrder = effect["LISTORD"] as? String
            thisEffect.objectName = effect["OBJNAM"] as? String
            thisEffect.objectType = effect["OBJTYP"] as? String
            thisEffect.parentObject = effect["PARENT"] as? String
            thisEffect.select = effect["SELECT"] as? String
            thisEffect.speed = effect["SPEED"] as? String
            thisEffect.staticFlag = effect["STATIC"] as? String
        
            effectsArray.append(thisEffect)
           // print("unfiltered effectsArray (created local objects) count is: \(effectsArray)")
        }
        return effectsArray
    }
    
    func createCircuitsArray() -> [Circuit]{
        var tmpCircuits:[AnyObject]
        var circuitsArray:[Circuit] = []
        
        tmpCircuits = (property?.configurationModel.getAllCircuits())!
        //print("tmpCircuits (pull from the socket) count is: \(tmpCircuits.count)")
        //print("dump of tmpCircuits from config model \(dump(tmpCircuits))")
        
        for circuit in tmpCircuits {
            let thisCircuit = Circuit.MR_createEntity() as! Circuit
            
            thisCircuit.objectName = circuit["OBJNAM"] as? String
            thisCircuit.sName = circuit["SNAME"] as? String
            thisCircuit.statusFlag = circuit["STATUS"] as? String
            thisCircuit.objectType = circuit["OBJTYP"] as? String
            thisCircuit.subType = circuit["SUBTYP"] as? String
            thisCircuit.shoMenu = circuit["SHOMNU"] as? String
            thisCircuit.listOrder = circuit["LISTORD"] as? String
            thisCircuit.timOut = circuit["TIMOUT"] as? String

            NSManagedObjectContext .MR_defaultContext() .MR_saveToPersistentStoreAndWait()
            circuitsArray.append(thisCircuit)
            
        }
        return circuitsArray
    }



    func fetchCircuitWithName(targetCircuitName:String) -> Circuit {
        var fetchedObject:Circuit
        print("Target Circuit Name is \(targetCircuitName)")
        
        let tmpCircuits = self.fetchCircuitsFromLocalCache()
       // print("Temp Circuits array is: \(tmpCircuits.debugDescription)")
        
        if let i = tmpCircuits.indexOf({$0.objectName == targetCircuitName}) {
            fetchedObject =  tmpCircuits[i]
        } else {
            fetchedObject = Circuit.MR_createEntity() as! Circuit
        }
        return fetchedObject
    }
    
    func fetchActiveCircuit(targetCircuitName:String) -> Circuit {
        let circuitObj:Circuit = self.circuitsArray.find({$0.objectName == targetCircuitName})!
        return circuitObj
    }
    
    func fetchPrimingCircuit(targetCircuitName:String) -> Circuit {
        let circuitObj:Circuit = self.circuitsArray.find({$0.objectName == targetCircuitName})!
        return circuitObj
    }
    
    

//***************************************************************************
    
    // MARK: Caching / core data objects
    
//***************************************************************************
    
    func createLocalCachePumps() {
        var pumps = [Pump]()
        let pumpsFromConfig = (property?.configurationModel.getAllPumps())!
        //debug
        //print("dump of all pumps from config model \(dump(pumpsFromConfig))")
        
        for pump in pumpsFromConfig {
            
            let localPumpObj = Pump.MR_createEntity() as! Pump
            
            localPumpObj.identifier = NSUUID().UUIDString
            localPumpObj.objectName = pump["OBJNAM"]! as? String
            localPumpObj.state = pump["STATIC"]! as? String
            localPumpObj.listOrder = pump["LISTORD"]! as? String
            localPumpObj.pumpType = pump["SUBTYP"]! as? String
            localPumpObj.displayName = pump["SNAME"]! as? String
            localPumpObj.rpm = pump["RPM"]! as? String
            localPumpObj.gpm = pump["GPM"]! as? String
            localPumpObj.pwr = pump["PWR"]! as? String
            
            // get the circuit name
            let circutName = pump["CIRCUIT"]! as? String
            
            
            let tmpCircuit = self.fetchCircuitWithName(circutName!)
            //print("tmpCircuit  \(tmpCircuit.objectName)")
            // associate the circuit with the cached pump object
            localPumpObj.circuit = tmpCircuit
        
            //print("Effects array count when creating the pump  \(self.effectsArray.count)")
            
            let effectsForPump = self.effectsArray.filter({ return $0.parentObject == pump["OBJNAM"]! as? PumpEffect })  //String?
            
             NSManagedObjectContext .MR_defaultContext() .MR_saveToPersistentStoreAndWait()
        }
    }
    
    func createLocalCacheCircuits() {
        var tmpCircuits:[AnyObject]
        var localCircutsArray:[Circuit] = []
        
        tmpCircuits = (property?.configurationModel.getAllCircuits())!
        
        //debug
        print("dump of tmpCircuits from config model \(dump(tmpCircuits))")
        
        for circuit in tmpCircuits {
            let localCircuitObj = Circuit.MR_createEntity() as! Circuit
            
            localCircuitObj.objectName = circuit["OBJNAM"] as? String
            localCircuitObj.sName = circuit["SNAME"] as? String
            localCircuitObj.statusFlag = circuit["STATUS"] as? String
            localCircuitObj.objectType = circuit["OBJTYP"] as? String
            localCircuitObj.subType = circuit["SUBTYP"] as? String
            localCircuitObj.shoMenu = circuit["SHOMNU"] as? String
            localCircuitObj.listOrder = circuit["LISTORD"] as? String
            localCircuitObj.timOut = circuit["TIMOUT"] as? String
        }
        
            NSManagedObjectContext .MR_defaultContext() .MR_saveToPersistentStoreAndWait()
    }
    
    func createLocalCachePumpEffects() {
        var tmpeffects:[AnyObject]
        
        tmpeffects = (property?.configurationModel.getAllPumpEffects())!
        //print("tmpeffects (pull from the config) count is: \(tmpeffects)")
        
        for effect in tmpeffects {
            let localEffectObj = PumpEffect.MR_createEntity() as! PumpEffect
            
            let circutName = effect["CIRCUIT"] as? String             //changed from OBJNAM
            let tmpCircuit = self.fetchCircuitWithName(circutName!)
            
            localEffectObj.boost = effect["BOOST"] as? String
            localEffectObj.circuit = tmpCircuit
            
            localEffectObj.listOrder = effect["LISTORD"] as? String
            localEffectObj.objectName = effect["OBJNAM"] as? String
            localEffectObj.objectType = effect["OBJTYP"] as? String
            localEffectObj.parentObject = effect["PARENT"] as? String
            localEffectObj.select = effect["SELECT"] as? String
            localEffectObj.speed = effect["SPEED"] as? String
            localEffectObj.staticFlag = effect["STATIC"] as? String
        }
            NSManagedObjectContext .MR_defaultContext() .MR_saveToPersistentStoreAndWait()
    }
    
    
    func fetchPumpsFromLocalCache() -> [Pump] {
        let managedObjectContext = NSManagedObjectContext .MR_defaultContext()
        let fetchRequest = NSFetchRequest(entityName: "Pump")
        var pumpObjects = [Pump]()
        do {
            let results = try managedObjectContext.executeFetchRequest(fetchRequest)
            pumpObjects = results as! [Pump]
            //log debug
            for pump in pumpObjects {
                print("pump \(pump.objectName)")
            }
        } catch let error as NSError {
           //print("could not fetch local pump objects")
        }
        return pumpObjects
    }
    
    
    func fetchCircuitsFromLocalCache() -> [Circuit] {
        let managedObjectContext = NSManagedObjectContext .MR_defaultContext()
        let fetchRequest = NSFetchRequest(entityName: "Circuit")
        
        do {
            let results = try managedObjectContext.executeFetchRequest(fetchRequest)
            self.circuitsArray = results as! [Circuit]
            //log debug
            for circuit in self.circuitsArray {
               // print("circuit \(circuit.objectName)")
            }
        } catch let error as NSError {
            //print("could not fetch local circuit objects")
        }
        return self.circuitsArray
    }
    
    func fetchEffectsFromLocalCache() -> [PumpEffect] {
        let managedObjectContext = NSManagedObjectContext .MR_defaultContext()
        let fetchRequest = NSFetchRequest(entityName: "PumpEffect")
        
        do {
            let results = try managedObjectContext.executeFetchRequest(fetchRequest)
            self.effectsArray = results as! [PumpEffect]
            //log debug
            for effect in self.effectsArray {
               // print("effect \(effect.objectName)")
            }
        } catch let error as NSError {
           // print("could not fetch local effects objects")
        }
        return self.effectsArray
    }


// Delete cached objects and recreate
    func deleteCachedData(){
        Pump .MR_truncateAllInContext(NSManagedObjectContext .MR_defaultContext())
        PumpEffect.MR_truncateAllInContext(NSManagedObjectContext .MR_defaultContext())
        Circuit.MR_truncateAllInContext(NSManagedObjectContext .MR_defaultContext())
        
        //        //   Note:  We can use this if our target platform becomes iOS 9
        //        //        let fetchRequest = NSFetchRequest(entityName: "Pump")
        //        //        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        //        //
        //        //        do {
        //        //            try myPersistentStoreCoordinator.executeRequest(deleteRequest, withContext: myContext)
        //        //        } catch let error as NSError {
        //        //            // TODO: handle the error
        //        //        }

    }
    
 
//****************************************************************************

    // Mark: Notification Responses
    
//****************************************************************************
    
    
    func pumpsWereRefreshed(){
        //currentPage = 1
        pumpsDidUpdate()
    }
    
    func pumpsDidUpdate(){
        updatePumpsList()
        //setLoadingState(false)
    }
    
    func pumpsWereModified(){
        refreshPumps(self)
    }
    
    
    func updatePumpsList(){
        //        if (self.pumpsArray.count < 1){              //if (configModel!.Pumps.count < 1)
        //            let label = UILabel(frame: CGRectMake(0, 0, 400, 20))
        //            label.textColor = UIColor.whiteColor()
        //            label.text = "No Pumps Found!"
        //            pumplistTableView.backgroundView = label
        //        }
        pumplistTableView.reloadData()
    }
    
    func refreshPumps(sender:AnyObject){
        pumplistTableView.backgroundView = nil
        pumplistTableView.reloadData()
    }

    
//****************************************************************

    // MARK: UI Related Methods

//****************************************************************
    

    func showhidePumpsList() {
        if(self.pumpDetailsViewIsDisplayed)
        {
            self.pumpDetailsView.hidden = true
        }
        
        if (self.pumplistTableViewIsDisplayed) {
           // self.hidePumpListView()
        } else {
            self.showPumpListView()
        }
    }
    
    @IBAction func pumpsDropdownButtonPressed(sender: UIButton?) {
        self.showhidePumpsList()
    }
    
    func hidePumpDetailsView() {
        self.pumpDetailsView.hidden = true
        self.pumpDetailsViewIsDisplayed = false
    }
    
    
    func hidePumpSpeed() {
        self.pumpSpeedLabel.hidden = true
        self.pumpSpeedView.hidden = true
        self.pumpSpeedTriangle.hidden = true
        self.pumpSpeedViewContainer.hidden = true
        self.pumpSpeedTitleLabel.hidden = true
        
    }
    
    func hidePumpUseage() {
        self.pumpUseageLabel.hidden = true
        self.pumpUseageView.hidden = true
        self.pumpUseageTriangle.hidden = true
        self.pumpUseageViewContainer.hidden = true
        self.powerUseageTitleLabel.hidden = true
    }

    
    func hidePumpFlow() {
        self.pumpFlowLabel.hidden = true
        self.pumpFlowView.hidden = true
        self.pumpFlowTriangle.hidden = true
        self.pumpFlowViewContainer.hidden = true
        self.flowRateTitleLabel.hidden = true
    }
    
    func showPumpSpeed() {
        self.pumpSpeedLabel.hidden = false
        self.pumpSpeedView.hidden = false
        self.pumpSpeedTriangle.hidden = false
        self.pumpSpeedViewContainer.hidden = false
        self.pumpSpeedTitleLabel.hidden = false
    }
    
    func showPumpUseage() {
        self.pumpUseageLabel.hidden = false
        self.pumpUseageView.hidden = false
        self.pumpUseageTriangle.hidden = false
        self.pumpUseageViewContainer.hidden = false
        self.powerUseageTitleLabel.hidden = false
    }
    
    func showPumpFlow() {
        self.pumpFlowLabel.hidden = false
        self.pumpFlowView.hidden = false
        self.pumpFlowTriangle.hidden = false
        self.pumpFlowViewContainer.hidden = false
        self.flowRateTitleLabel.hidden = false
    }
    
    func animateEffectsGuage(guageView:UIView, triangeView:UIView, increment:CGFloat) {
        
    }
    
    func showPumpDetailsView() {
       // print("Selected Pump is  \(self.selectedPump.objectName)")
        print("GPM is  \(self.selectedPump.gpm)")
        print("RPM is  \(self.selectedPump.rpm)")
        print("Power is  \(self.selectedPump.pwr)")
        
        self.pumpDetailsView.hidden = false
        self.pumpDetailsViewIsDisplayed = true
        
        resetGaugesToZero()
        self.stylePumpDetailsView()
        
        //@todo remove hard coded values
        selectedPump.rpm = "50"
        selectedPump.gpm = "100"
        selectedPump.pwr = "250"
       
        self.pumpFlowLabel.text = self.selectedPump.gpm
        self.pumpSpeedLabel.text = self.selectedPump.rpm
        self.pumpUseageLabel.text = self.selectedPump.pwr
        
        //self.pumpNameLabel.attributedText = numberOfPumpsMutable titleStringMutable
        
        let fixedWidth = self.pumpDetailsView.frame.size.width
        self.pumpDetailsView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.max))
        let newSize = self.pumpDetailsView.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.max))
        var newFrame = self.pumpDetailsView.frame
        newFrame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        self.pumpDetailsView.frame = newFrame
        
        
        if let pumpType = self.selectedPump.pumpType {
            switch (pumpType) {
            case "SINGLE": // 'Single Speed'
                // per Keith - this has only one effect, not multiple
                // no number stepper
                // only show pump type, power value (?)
                self.hidePumpSpeed()
                self.hidePumpFlow()
                self.hidePumpUseage()
                self.selectedPumpLabel.text = "Single Speed"
                break;
            case "DUAL":  // 'Two Speed'
                // no number stepper
                // only show pump type, and power value
                self.hidePumpSpeed()
                self.hidePumpFlow()
                self.hidePumpUseage()
                self.selectedPumpLabel.text = "Two Speed"
                break;
            case "FLOW":  // 'Intelliflo VF'
                // has speed, power, and flow
                self.setupFlowView()
                break;
            case "SPEED": // 'Intelliflo VS'
                // has speed and power
                self.setUpSpeedView()
                break;
            case "VSF":  // none of these exist yet
                // flag UI elements for show/hide as appropriate
                break;
            default:
                //flag UI elements for show/hide as appropriate
                break;
            }
            
            print("pumpType is \(pumpType)")
            
        }

        self.pumpEffectsTableView.reloadData()
    }
    
    func resetGaugesToZero(){
        self.pumpFlowTriangle.frame.origin.x = self.indicatorStartingPoint - arrowOffset
        self.pumpSpeedTriangle.frame.origin.x = self.indicatorStartingPoint - arrowOffset
        self.pumpUseageTriangle.frame.origin.x = self.indicatorStartingPoint - arrowOffset
        self.pumpSpeedView.frame.size.width = 20
        self.pumpFlowView.frame.size.width = 20
        self.pumpUseageView.frame.size.width = 20
    }
    
    func stylePumpDetailsView(){
        
        let numberOfPumps = pumpsArray.count
        let numberOfSelectedPump = selectedPump.listOrder

        // Define string attributes
        let font1 = UIFont(name: "Arial", size: 16.0) ?? UIFont.systemFontOfSize(16.0)
        let font2 = UIFont(name: "Arial", size: 22.0) ?? UIFont.systemFontOfSize(22.0)
        let smallFont = [NSFontAttributeName:font1]
        let largeFont = [NSFontAttributeName:font2]
        let para = NSMutableAttributedString()
        
        // Create locally formatted strings
        let attrString1 = NSAttributedString(string:  "( " + numberOfSelectedPump! + " of ", attributes:smallFont)
        let attrString2 = NSAttributedString(string: String(numberOfPumps) + " )", attributes:smallFont)
        let attrString3 = NSAttributedString(string: " " + selectedPump.displayName!, attributes:largeFont)
        
        // Add locally formatted strings to paragraph
        para.appendAttributedString(attrString1)
        para.appendAttributedString(attrString2)
        para.appendAttributedString(attrString3)
        
        // Define paragraph styling
        let paraStyle = NSMutableParagraphStyle()
        paraStyle.firstLineHeadIndent = 5.0
        paraStyle.paragraphSpacingBefore = 5.0
        
        // Apply paragraph styles to paragraph
        para.addAttribute(NSParagraphStyleAttributeName, value: paraStyle, range: NSRange(location: 0,length: para.length))
        
        self.pumpNameLabel.attributedText = para
        self.pumpNameLabel.textAlignment = NSTextAlignment.Center
    }
    

    func setUpSpeedView() {
        self.showPumpSpeed()
        self.showPumpUseage()
        self.hidePumpFlow()
        self.pumpSpeedLabel.text = selectedPump.rpm
        self.pumpUseageLabel.text = selectedPump.pwr
        self.selectedPumpLabel.text = "Intelliflo VS"
        
        resetGaugesToZero()
        animateGuageView(self.pumpSpeedView, indicatorView: self.pumpSpeedTriangle)
        animateGuageView(self.pumpUseageView, indicatorView: self.pumpUseageTriangle)
    }
    
    
    func setupFlowView(){
        self.showPumpFlow()
        self.showPumpSpeed()
        self.showPumpUseage()
        self.pumpSpeedLabel.text = selectedPump.rpm
        self.pumpFlowLabel.text = selectedPump.gpm
        self.pumpUseageLabel.text = selectedPump.pwr
        self.selectedPumpLabel.text = "Intelliflo VF"
        
        resetGaugesToZero()
        animateGuageView(self.pumpFlowView, indicatorView: self.pumpFlowTriangle)
        animateGuageView(self.pumpSpeedView, indicatorView: self.pumpSpeedTriangle)
        animateGuageView(self.pumpUseageView, indicatorView: self.pumpUseageTriangle)
    }

    
    func animateGuageView(gagueView:UIView, indicatorView:UIImageView){
        
        var tmpValueFlow = Double(selectedPump.gpm!)!
        var tmpValueSpeed = Double(selectedPump.rpm!)!
        var tmpValueUseage = Double(selectedPump.pwr!)!  // SWIFT 2 allows the coversion of strings to double
        var viewSize = CGFloat(0)
        
        switch(gagueView.tag){
        case 0:
            viewSize =  CGFloat(tmpValueSpeed)
            
            if(viewSize == 0){
                viewSize = indicatorStartingPoint
            }
            
            moveGaugesToPosition(viewSize, triangleImage: indicatorView, gagueView: gagueView)
        case 1:
            viewSize =  CGFloat(tmpValueFlow)
            if(viewSize == 0){
                viewSize = indicatorStartingPoint
            }

            moveGaugesToPosition(viewSize, triangleImage: indicatorView, gagueView: gagueView)
        case 2:
            viewSize =  CGFloat(tmpValueUseage)
            if(viewSize == 0){
                viewSize = indicatorStartingPoint
            }

            moveGaugesToPosition(viewSize, triangleImage: indicatorView, gagueView: gagueView)
            
        default: break
            //nothing
        }
    }
    
    func moveGaugesToPosition(newposition:CGFloat, triangleImage:UIImageView, gagueView:UIView){
        print("new position is: \(newposition)")
        
    let animationDuration = 0.5
        
        UIView.animateWithDuration(
            self.animationDuration * 1.5,
            delay: 0,
            usingSpringWithDamping: 0.7,
            initialSpringVelocity: 0.5,
            options: [],
            animations: {
                gagueView.frame.size.width = newposition
                triangleImage.frame.origin.x = triangleImage.frame.origin.x + newposition
            }, completion: nil
        )
    }
    
    func calculatePercentage(paramValue:Float) -> CGFloat{
        let maxRange: Float = 100.0
        let perCent = 100*paramValue/maxRange
        var perCentCGFloat = CGFloat(0)
        
        if(paramValue > 0){
            perCentCGFloat =  CGFloat(perCent)
        }
        
        return perCentCGFloat
    }
    
    func hidePumpListView() {
        var frame: CGRect = self.pumplistTableView.frame
        frame.origin.y = frame.origin.y - 70
        self.pumplistTableView.frame = frame
       
        // Rotate arrow
        self.rotateArrowUp()
   
        UIView.animateWithDuration(
            self.animationDuration * 1.5,
            delay: 0,
            usingSpringWithDamping: 0.7,
            initialSpringVelocity: 0.5,
            options: [],
            animations: {
                self.pumplistTableView.frame.origin.y = CGFloat(-70)
            }, completion: nil
        )
        
        // Animation
        UIView.animateWithDuration(
            self.animationDuration,
            delay: 0,
            options: UIViewAnimationOptions.TransitionNone,
            animations: {
                }, completion: { _ in
                self.pumplistTableView.frame.origin.y = CGFloat(-70)
                self.pumplistTableView.hidden = true
                self.pumplistTableViewIsDisplayed = false
                
        })
    
    }
    
    func showPumpListView() {
        var frame: CGRect = self.pumplistTableView.frame
        frame.origin.y = frame.origin.y + 70
        self.pumplistTableView.frame = frame
        
        // Rotate arrow
        self.rotateArrowDown()
        self.pumplistTableView.hidden = false
        self.pumplistTableViewIsDisplayed = true
     
        // Reload data to dismiss highlight color of selected cell
        self.pumplistTableView.reloadData()
      
        UIView.animateWithDuration(
            self.animationDuration * 1.5,
            delay: 0,
            usingSpringWithDamping: 0.7,
            initialSpringVelocity: 0.5,
            options: [],
            animations: {
                self.pumplistTableView.frame.origin.y = CGFloat(70)
            }, completion: nil
        )

      // }
    }
    
    // Currently not being used
    func showNoPumpSelected() {
        UIGraphicsBeginImageContext(self.view.frame.size)
        UIImage(named: "NoPumpSelected")?.drawInRect(self.view.bounds)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.view.backgroundColor = UIColor(patternImage: image)
    }
    
    
  
    //MARK: UITableView delegate methods
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var count:Int?
        
        if tableView == self.pumplistTableView {
            count = self.pumpsArray.count
        }
        
        if tableView == self.pumpEffectsTableView {
           // count = 1
            if self.selectedPump != nil {
                print("selectedPump.effects.count is \(self.selectedPump.effects.count)")
                count = self.selectedPump.effects.count
            } else {
                count =  self.effectsArray.count
            }

        }
       return count!
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell:EffectsTableViewCell?
       
        
        if tableView == self.pumplistTableView {
             cell = tableView.dequeueReusableCellWithIdentifier("pumpsViewCell", forIndexPath: indexPath) as? EffectsTableViewCell
            // print("pumps array count in tableView \(self.pumpsArray.count)")
            self.selectedPump = self.pumpsArray[indexPath.row]
            cell!.effectsNameLabel.font = UIFont(name:fontName, size:22)
            cell!.effectsNameLabel.text = self.selectedPump.displayName
        }
        
        
        if tableView == self.pumpEffectsTableView {
             cell = tableView.dequeueReusableCellWithIdentifier("effectsViewCell", forIndexPath: indexPath) as? EffectsTableViewCell
            //print("Selected pump when effects are displayed is: \(self.selectedPump.displayName)")
            //print("Number of items for this pump is: \(self.selectedPump.effects.count)")
            //print("pumps effects array count in tableView \(self.selectedPump.effects.count)")
            
            // loop through the effects for the selected Pump
            
            var tmpConvenienceArray = self.selectedPump.effects.allObjects
            //print("tmpConvenienceArray is \(tmpConvenienceArray)")
            self.pumpEffectsTableView.hidden = false
            
            for _ in 0 ..< tmpConvenienceArray.count  {
                
                let tmpEffect = tmpConvenienceArray[indexPath.row] as! PumpEffect
                print("tmpEffect status is \(tmpEffect.staticFlag)")
                
                cell!.textLabel!.font = UIFont(name:fontName, size:22)
                
                let pumpEffect = property?.configurationModel.getItem(objNam!)
                cell!.controlObject = pumpEffect
                cell!.property = property!
                cell!.objNam = tmpEffect.objectName  // or can go pumpEffect. string "ObjNam" - is there any benefit in doing this ?
                
                cell!.stepper.autorepeat = true
                cell!.stepper.continuous = true

                cell!.stepper.backgroundColor = UIColor(hue: 222/360, saturation: 100/100, brightness: 59/100, alpha: 1.0) /* #002d96 */

                cell!.stepper.tintColor = UIColor .whiteColor()
                
               // cell!.stepper.decrementImageForState()
               // cell!.stepper.incrementImageForState()
                cell!.stepper.minimumValue = Double(cell!.minNextGenPumpType)
                cell!.stepper.maximumValue = Double(cell!.maxNextGenPumpType)
                
                if((tmpEffect.speed) != nil) {
                    cell!.stepperValue.text = tmpEffect.speed
                } else {
                    cell!.stepperValue.text = String(cell!.minNextGenPumpType)
                }
                
                cell!.oldValue = Double(cell!.stepper.value)
                
                var circuitName = tmpEffect.circuitName
                //var circuit = self.fetchCircuitWithName(tmpEffect.circuit!.objectName!)
                var circuit = self.fetchCircuitWithName(circuitName!)
                
                
                if let pumpType = self.selectedPump.pumpType {
                    switch(pumpType) {
                    case "SINGLE": //
                        cell!.stepper.hidden = true
                        cell!.stepperValue.hidden = true
                        cell!.SpeedTextLabel.hidden = true
                        cell!.pumpType = "SINGLE"
                        cell?.vsfPumpTypeView.hidden = true
                        break;
                    case "DUAL": //
                        cell!.stepper.hidden = true
                        cell!.stepperValue.hidden = true
                        cell!.SpeedTextLabel.hidden = true
                        cell!.pumpType = "DUAL"
                        cell?.vsfPumpTypeView.hidden = true
                        break;
                    case "FLOW": //
                        cell!.stepper.hidden = false
                        cell!.stepperValue.hidden = false
                        cell!.SpeedTextLabel.hidden = false
                        cell!.pumpType = "FLOW"
                        cell?.vsfPumpTypeView.hidden = true
                        break;
                    case "SPEED": //
                        cell!.stepper.hidden = false
                        cell!.stepperValue.hidden = false
                        cell!.SpeedTextLabel.hidden = false
                        cell!.pumpType = "SPEED"
                        cell?.vsfPumpTypeView.hidden = true
                        break;
                    case "VSF": //
                        cell!.stepper.hidden = false
                        cell!.stepperValue.hidden = false
                        cell!.SpeedTextLabel.hidden = false
                        cell!.pumpType = "VSF"
                        cell?.vsfPumpTypeView.hidden = false
                        break;
                        
                    default:
                        //flag UI elements for show/hide as appropriate
                        break;
                    }
                }
                
                cell!.effectsNameLabel.text = circuit.sName
                
                print("circuit.sName is \(circuit.sName)")
                    
               // cell!.controlObject =
                
                //print("self.selectedPump.circuit  \(self.selectedPump.circuit)")

                if(self.selectedPump.circuit == tmpEffect.circuit){
                    //print("there is a match for an active circuit")
                }
                
                if(tmpEffect.staticFlag == "ON") {
                    cell!.backingView.backgroundColor = BrightGreen
                    //cell!.backgroundColor = Green
                    cell!.backingView.layer.borderWidth = 1
                    cell!.backingView.layer.borderColor = DarkGreen
                } else {
                    cell!.backingView.backgroundColor = UIColor .whiteColor()
                }
               
            }
        }
        
        return cell!
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        if tableView == self.pumplistTableView {
            self.hidePumpDetailsView()
        //selectedPump = nil;
        self.selectedPump = self.pumpsArray[indexPath.row]
        self.hidePumpListView()
        self.pumpNameLabel.text = self.selectedPump.displayName
        self.showPumpDetailsView()
        }
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {

    }
    
    func didSelectFilter(selectedIndex: NSNumber){
       
    }
  
    
    func rotateArrowDown() {
        let animationDuration = 0.5
        UIView.animateWithDuration(animationDuration, animations: {[weak self] () -> () in
                self!.arrowImage.transform = CGAffineTransformRotate(self!.arrowImage.transform, 90 * CGFloat(M_PI/90))
            })
    }
    
    func rotateArrowUp() {
        let animationDuration = 0.5
        UIView.animateWithDuration(animationDuration, animations: {[weak self] () -> () in
            self!.arrowImage.transform = CGAffineTransformRotate(self!.arrowImage.transform, 45 * CGFloat(M_PI/45))
            })
    }
    
//    func positionArrow(position: CGFloat) {
//        
//        UIView.animateWithDuration(1, animations: {
//            self.pumpSpeedTriangle.frame.origin.x = position - (self.pumpSpeedTriangle.frame.size.width / 2)
//            if(self.pumpSpeedTriangle.frame.origin.x < 0 ){
//                self.pumpSpeedTriangle.frame.origin.x = 0
//            }
//            
////            if(self.pumpSpeedTriangle.frame.origin.x > self.frame.size.width - self.pumpSpeedTriangle.frame.size.width   ){
////                self.pumpSpeedTriangle.frame.origin.x = self.frame.size.width - self.pumpSpeedTriangle.frame.size.width
////            }
//            
//        })
//    }
    
    func getXPosition() -> CGFloat {
        return CGFloat((currentLevel - minLevel) / (maxLevel - minLevel)) *  self.pumpDetailsView.frame.size.width
    }

    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height
        
        if (maximumOffset - currentOffset) <= 40 {
            //fetchPumps(currentPage+1)
        }
    }
    
    // not currently used
    func fetchPumps(page:Int){
        //        if (!loading && (configModel!.Users.count < configModel!.TotalUsers || 1 == page ||  configModel!.TotalUsers == 0 )) {
        //            currentPage = page
        //            setLoadingState(true)
        //            property!.getPumps(pageSize, page: page, filter: selectedFilter.rawValue, searchTerm: searchString)
        //        }
    }
    
    func statusChange(notification: NSNotification) {
       // setValues()
    }
    

    
 }