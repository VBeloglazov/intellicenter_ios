//
//  RootViewController.swift
//  Pentair-iOs
//
//  Created by Dashon Howard on 1/21/15.
//  Copyright (c) 2015 Pentair Corporation. All rights reserved.
//

import Foundation
class RootViewController: REFrostedViewController {

     override func awakeFromNib() {
        self.contentViewController = self.storyboard?.instantiateViewControllerWithIdentifier("contentController") as! NavigationViewController
        self.menuViewController = self.storyboard?.instantiateViewControllerWithIdentifier("menuController") as! MenuViewController
        
        self.direction = REFrostedViewControllerDirection.Right
    }
} 