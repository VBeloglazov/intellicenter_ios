//
//  TabViewController.swift
//  Pentair-iOs
//
//  Created by Path Finder on 11/29/15.
//  Copyright © 2015 Pentair Corporation. All rights reserved.
//

import UIKit
import Pentair

class TabViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    var property: Property?
    var httpService: HttpService?
    var buckets:[Bucket] = []
    var objectTitle: String!
    var objnam: String!
    
    var selectedBucket: Bucket!
    var selectedRow: NSInteger!
    
    var currentViewController = UIViewController()
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var containerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        for (i , bucket) in self.buckets.enumerate(){
            if(bucket.getType() == BucketType.Air){
                buckets.removeAtIndex(i)
            }
        }
        showPage(selectedBucket, fromSegue: true)
    }
    
    override func viewDidAppear(animated: Bool) {
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(TabViewController.statusChange(_:)), name: "ConfigurationUpdate", object: property)
    }
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    // MARK: - ContainerView
    
    func displayContentViewController(controller: UIViewController) {
        self.addChildViewController(controller)
        controller.view.frame = self.containerView.bounds
        self.containerView.addSubview(controller.view)
        
        controller.didMoveToParentViewController(self)
        self.currentViewController = controller
    }
    
    func cycleFromViewController(oldVC: UIViewController, newVC: UIViewController) {
        self.currentViewController = newVC
        
        oldVC.willMoveToParentViewController(nil)
        addChildViewController(newVC)
        newVC.view.frame = oldVC.view.frame
        
        transitionFromViewController(oldVC, toViewController: newVC, duration: 0.25, options: .TransitionCrossDissolve, animations:{ () -> Void in
            // nothing needed here
            }, completion: { (finished) -> Void in
                oldVC.removeFromParentViewController()
                newVC.didMoveToParentViewController(self)
        })
        
    }
    
    // MARK: - UICollectionViewDataSource
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.buckets.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell: HomeDetailMenuCollectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("ColCell", forIndexPath: indexPath) as! HomeDetailMenuCollectionViewCell
        
        let bucket: Bucket = self.buckets[indexPath.row]
        cell.label.text = bucket.getTitle()
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegate
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let bucket: Bucket = self.buckets[indexPath.row]
        
        self.selectedRow = indexPath.row
        self.selectedBucket = bucket
        
        showPage(self.selectedBucket, fromSegue: false)
    }
    
    func showPage(bucket:Bucket, fromSegue:Bool){
        var selectedViewController = self.currentViewController
        self.objectTitle = bucket.getTitle()
        self.title = bucket.getTitle()
        
        switch (bucket.getType()) {
        case BucketType.Air:
            break
        case BucketType.Spa:
            fallthrough
            
        case BucketType.Pool:
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("BodyViewController") as? BodyViewController
            vc?.objNam = self.selectedBucket.getChildren()[0]
            vc?.property = self.property
            
            if let currentVC  = self.currentViewController as? BodyViewController{
                if currentVC.objNam == vc?.objNam {return}
            }
            
            selectedViewController = vc!
            break
            
        case BucketType.Pump:
            if self.currentViewController.isMemberOfClass(PumpsViewController.self){return}
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("PumpsViewController") as? PumpsViewController
            //vc?.objNam = self.selectedBucket.getChildren()[0]
            
            vc?.objNam = self.selectedBucket.getTitle()
            vc?.property = self.property
            
            // IMPORTANT DO NOT DELETE - this refreshes the locally cached data
            // for the pumps each time the user selects a different installation
            
            vc?.fetchPumpsWithEffects()
           
            
            if let currentVC  = self.currentViewController as? BodyViewController{
                if currentVC.objNam == vc?.objNam {return}
            }
            
            selectedViewController = vc!
            break
            
        case BucketType.History:
            if self.currentViewController.isMemberOfClass(HistoryViewController.self){return}
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("HistoryViewController") as? HistoryViewController
            //vc?.objNam = self.selectedBucket.getChildren()[0]
            
            vc?.objNam = self.selectedBucket.getTitle()
            vc?.property = self.property
            
            if let currentVC  = self.currentViewController as? BodyViewController{
                if currentVC.objNam == vc?.objNam {return}
            }
            
            selectedViewController = vc!
            break

        case BucketType.Schedule:
            if self.currentViewController.isMemberOfClass(ScheduleViewController.self){return}
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("ScheduleViewController") as? ScheduleViewController
            //vc?.objNam = self.selectedBucket.getChildren()[0]
            
            vc?.objNam = self.selectedBucket.getTitle()
            vc?.property = self.property
            
            if let currentVC  = self.currentViewController as? ScheduleViewController{
                if currentVC.objNam == vc?.objNam {return}
            }
            
            selectedViewController = vc!
            break
            
        case BucketType.Features:
            if self.currentViewController.isMemberOfClass(FeaturesViewController.self){return}
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("FeaturesViewController") as? FeaturesViewController
            vc?.property = self.property
            selectedViewController = vc!
            break
            
        case BucketType.Lights:
            if self.currentViewController.isMemberOfClass(LightsViewController.self){return}
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("LightsViewController") as? LightsViewController
            vc?.property = self.property
            selectedViewController = vc!
            break
            
        case BucketType.Chemistry:
            if self.currentViewController.isMemberOfClass(ChemistryViewController.self){return}
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("ChemistryViewController") as? ChemistryViewController
            vc?.property = self.property
            selectedViewController = vc!
            break
        default:
            break
        }
        
        if fromSegue == true {
            self.displayContentViewController(selectedViewController)
        } else {
            self.cycleFromViewController(self.currentViewController, newVC: selectedViewController)
        }
        
    }
    
    func statusChange(notification: NSNotification) {
    }
    
    func LightCellChosen() {
        performSegueWithIdentifier("showLightColorOptions", sender: self)
    }
}
