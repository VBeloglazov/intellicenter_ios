//
//  TableLoadingFilter.swift
//  Pentair-iOs
//
//  Created by Dashon Howard on 2/20/15.
//  Copyright (c) 2015 Pentair Corporation. All rights reserved.
//

import Foundation

class TableLoadingFooter : UIView {
    
    @IBOutlet var activityIndicator:UIActivityIndicatorView!
    
    override var hidden:Bool {
        get {
            return super.hidden
        }
        set(hidden) {
            super.hidden = hidden
        }
    }
    
    override func awakeFromNib() {
        self.activityIndicator.startAnimating()
    }
}
