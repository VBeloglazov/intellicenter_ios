//
//  LightTableViewCell.swift
//  Pentair-iOs
//
//  Created by Path Finder on 12/11/15.
//  Copyright © 2015 Pentair Corporation. All rights reserved.
//

import UIKit
import Pentair

class LightTableViewCell: UITableViewCell ,LightTableViewCellProtocol{
    var objNam: String?
    private var lightMode:LightMode?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var onOffSwitch: UISwitch!
    @IBOutlet weak var lightColorBar: LightColorBar!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setLightMode(LightMode(lightCode: .NONE))
    }
    
    func setLightMode(mode:LightMode){
        self.lightMode = mode
        
        lightColorBar.setLightMode(LightMode(lightCode: mode.code))
        lightColorBar.rightImage.hidden = true
    }
    func setDimmer(value:Int?){
        lightColorBar.setDimmer(value)
        lightColorBar.rightImage.hidden = true
    }
    
    func getLightMode() ->LightMode?{
        return self.lightMode
    }
}
