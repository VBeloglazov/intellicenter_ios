//
//  UserCell.swift
//  Pentair-iOs
//
//  Created by Dashon Howard on 1/19/15.
//  Copyright (c) 2015 Pentair Corporation. All rights reserved.
//

import Foundation
import UIKit

class IconCell: UITableViewCell,CheckboxViewDelegate {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var checkbox: Checkbox!
    
    func initCheckbox(){
        //checkbox = Checkbox(frame: CGRectMake(5, 5, 50, 30))
        checkbox.delegate = self
       // addSubview(checkbox)
    }
    
    func setDefaultFont(){
        titleLabel.textColor = UIColor.blackColor()
        titleLabel.font  = UIFont(name: "Helvetica", size: 16)
    }
    override func awakeFromNib() {
        setDefaultFont()
        initCheckbox()
        self.backgroundColor = UIColor.whiteColor()
    }
    
    func valueChanged(view: Checkbox, value: Bool) {
        self.setSelected(value, animated: false)
    }
    
}

class FilterCell: IconCell {
    
    override func setDefaultFont(){
        titleLabel.textColor = UIColor.blackColor()
        titleLabel.font  = UIFont(name: "Helvetica", size: 15.0)
    }
    override func awakeFromNib() {
        setDefaultFont()
        self.backgroundColor = UIColor.whiteColor()
    }
    
}

protocol InstallationDetailCellViewDelegate: class {
    func remoteLogin(installationID: String)
    func localLogin(ipAddress: String,port:String)
}

class InstallationDetailCell: UITableViewCell {
    
//    // MARK: enum constants
//    enum Section: Int {
//        case LOCAL_CONNECTION = 0
//        case REMOTE_CONNECTION = 1
//    }
    
    weak var delegate: InstallationDetailCellViewDelegate?
    var installationID:String?
    var ipAddress:String?
    var portNumber:String?
    var hasContentView:Bool?
    
    @IBOutlet weak var PoolNameLabel: UILabel!
    @IBOutlet weak var PoolAddressLabel: UILabel!
    @IBOutlet weak var PoolAddress2Label: UILabel!
    @IBOutlet weak var ConnectButton: UIButton!
    
    // MARK: Local Constants for Nofifications
    private let POOL_SELECTED = "poolSelected"
    private let REMOTE_POOL = "remotePool"
    private let LOCAL_POOL = "localPool"
    
    override func awakeFromNib() {
    }

    // here - make a notification
    @IBAction func LoginButtonTouched(sender: UIButton) {
        
        self.notifyPoolWasSelected()
        
        if let installationID = installationID{
            self.notifyPoolIsRemote()
            delegate?.remoteLogin(installationID)
        } else {
            self.notifyPoolIsLocal()
        }
        
        if let ipAddress = ipAddress,
        let portNumber = portNumber{
            delegate?.localLogin(ipAddress,port:portNumber)
        }
    }
    
    func notifyPoolWasSelected() {
         NSNotificationCenter.defaultCenter().postNotificationName(POOL_SELECTED, object: nil, userInfo: ["poolName": self.PoolNameLabel.text!]) //userInfo parameter has to be of type [NSObject : AnyObject]?
    }
    
    func notifyPoolIsRemote() {
        NSNotificationCenter.defaultCenter().postNotificationName(REMOTE_POOL, object: nil, userInfo: nil) //userInfo parameter has to be of type [NSObject : AnyObject]?
    }
    
    func notifyPoolIsLocal() {
        NSNotificationCenter.defaultCenter().postNotificationName(LOCAL_POOL, object: nil, userInfo: nil) //userInfo parameter has to be of type [NSObject : AnyObject]?
    }
    
    
    func clear() {
        self.PoolNameLabel.text = "--"
        self.PoolAddressLabel.text = "--"
        self.PoolAddress2Label.text = "--"
        installationID = nil
        ipAddress = nil
        portNumber = nil
    }
    
    override func prepareForReuse() {
        // Clear contentView
//        self.hasContentView = self.subviews.contains(self.contentView)
//        if((self.hasContentView) != nil){
//            self.contentView.removeFromSuperview()
//        }
    }
    
}