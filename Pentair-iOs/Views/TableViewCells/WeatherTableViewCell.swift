//
//  WeatherTableViewCell.swift
//  Pentair-iOs
//
//  Created by Path Finder on 11/28/15.
//  Copyright © 2015 Pentair Corporation. All rights reserved.
//

import UIKit

class WeatherTableViewCell: UITableViewCell {
    
    @IBOutlet weak var label: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
