//
//  WeatherViewController.swift
//  Pentair-iOs
//
//  Created by Jim Hewitt on 1/8/16.
//  Copyright © 2016 Pentair Corporation. All rights reserved.
//


import UIKit
import Pentair

class WeatherViewController: UIViewController {
    var property: Property?
    
    @IBOutlet var TodaysWeatherImage: UIImageView!
    @IBOutlet var CurrentTemperature: UILabel!
    @IBOutlet var TodaysWeatherDescription: UILabel!
    @IBOutlet var TodaysHi: UILabel!
    @IBOutlet var TodaysLo: UILabel!
    @IBOutlet var Sunrise: UILabel!
    @IBOutlet var Sunset: UILabel!
    @IBOutlet var Day1Image: UIImageView!
    @IBOutlet var Day1Day: UILabel!
    @IBOutlet var Day1HiTemp: UILabel!
    @IBOutlet var Day1LowTemp: UILabel!
    @IBOutlet var Day1Precipitation: UILabel!
    @IBOutlet var Day2Image: UIImageView!
    @IBOutlet var Day2Day: UILabel!
    @IBOutlet var Day2HiTemp: UILabel!
    @IBOutlet var Day2LowTemp: UILabel!
    @IBOutlet var Day2Precipitation: UILabel!
    @IBOutlet var Day3Image: UIImageView!
    @IBOutlet var Day3Day: UILabel!
    @IBOutlet var Day3HiTemp: UILabel!
    @IBOutlet var Day3LowTemp: UILabel!
    @IBOutlet var Day3Precipitation: UILabel!
    @IBOutlet var Day4Image: UIImageView!
    @IBOutlet var Day4Day: UILabel!
    @IBOutlet var Day4HiTemp: UILabel!
    @IBOutlet var Day4LowTemp: UILabel!
    @IBOutlet var Day4Precipitation: UILabel!
    @IBOutlet var Day5Image: UIImageView!
    @IBOutlet var Day5Day: UILabel!
    @IBOutlet var Day5HiTemp: UILabel!
    @IBOutlet var Day5LowTemp: UILabel!
    @IBOutlet var Day5Precipitation: UILabel!
    @IBOutlet var HeatIndex: UILabel!
    @IBOutlet var RainFall: UILabel!
    @IBOutlet var Humidity: UILabel!
    @IBOutlet var Pressure: UILabel!
    @IBOutlet var DewPoint: UILabel!
    @IBOutlet var CloudCover: UILabel!
    @IBOutlet var Wind: UILabel!
    @IBOutlet var UV: UILabel!
    @IBOutlet var arrowImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setValues()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(WeatherViewController.setValues), name: "ConfigurationUpdate", object: property)
    }

    
    override func viewWillDisappear(animated: Bool) {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func closeWeather(sender: AnyObject) {
       self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func viewTouched(sender: AnyObject) {
        print("View touched")
    }
    
    
    func rotateArrowDown() {
        let animationDuration = 0.5
        UIView.animateWithDuration(animationDuration, animations: {[weak self] () -> () in
            self!.arrowImage.transform = CGAffineTransformRotate(self!.arrowImage.transform, 90 * CGFloat(M_PI/90))
            })
    }
    
    func rotateArrowUp() {
        let animationDuration = 0.5
        UIView.animateWithDuration(animationDuration, animations: {[weak self] () -> () in
            self!.arrowImage.transform = CGAffineTransformRotate(self!.arrowImage.transform, 45 * CGFloat(M_PI/45))
            })
    }



    func setValues(){
        var timeZoneValid: Bool = false
        var timeZone: Int?
        let formatter = NSDateFormatter()
        self.Sunrise!.text = "--"
        self.Sunset!.text = "--"
        self.CurrentTemperature!.text = "--"
        self.TodaysHi!.text = "--"
        self.TodaysLo!.text = "--"
        self.HeatIndex!.text = "--"
        self.DewPoint!.text = "--"
        self.RainFall!.text = "--"
        self.Pressure!.text = "--"
        self.Wind!.text = "--"
        self.Humidity!.text = "--"
        self.CloudCover!.text = "--"
        self.UV!.text = "--"
        self.TodaysWeatherImage.image = UIImage(named:"iconw-na")
        self.Day1Image!.image = UIImage(named: "iconw-na")
        self.Day2Image!.image = UIImage(named: "iconw-na")
        self.Day3Image!.image = UIImage(named: "iconw-na")
        self.Day4Image!.image = UIImage(named: "iconw-na")
        self.Day5Image!.image = UIImage(named: "iconw-na")
        
        self.Day1Day!.text = "--"
        self.Day2Day!.text = "--"
        self.Day3Day!.text = "--"
        self.Day4Day!.text = "--"
        self.Day5Day!.text = "--"
        
        self.Day1HiTemp!.text = "--"
        self.Day2HiTemp!.text = "--"
        self.Day3HiTemp!.text = "--"
        self.Day4HiTemp!.text = "--"
        self.Day5HiTemp!.text = "--"
        
        self.Day1LowTemp!.text = "--"
        self.Day2LowTemp!.text = "--"
        self.Day3LowTemp!.text = "--"
        self.Day4LowTemp!.text = "--"
        self.Day5LowTemp!.text = "--"
        
        if let forecasts: [String: NSDictionary] = property!.configurationModel.Weather {
            let preferences: NSDictionary? = property!.configurationModel.ControlObjects["_5451"]
            
            
            let use24HourMode:Bool = property!.configurationModel.ControlObjects["_C10C"]?["CLK24A"] as? String == "HR24"
            let tz: Int = Int((property!.configurationModel.ControlObjects["_5451"]?["TIMZON"] as? String)!) ?? -24
            if tz > -24 {
                timeZoneValid = true
                timeZone = tz
            }
            if let todaysForcast = forecasts["0"] {
                let windDir = (todaysForcast["windDir"] as? String) ?? "--"
                if preferences!["MODE"] as? String == "METRIC" {
                    if let currentTemp = todaysForcast["avgTempC"] as? Int{
                        self.CurrentTemperature!.text = String(format: "%d", currentTemp)
                    }
                    if let todaysHi = todaysForcast["maxTempC"] as? Int{
                        self.TodaysHi!.text = String(format: "%d", todaysHi)
                    }
                    if let todaysLo = todaysForcast["minTempC"] as? Int{
                        self.TodaysLo!.text =  String(format: "%d", todaysLo)
                    }
                    if let fealsLike = todaysForcast["feelslikeC"] as? Int {
                        self.HeatIndex!.text = String(format: "%d°C", fealsLike)
                    }
                    if let dewPoint = todaysForcast["dewpointC"] as? Int{
                        self.DewPoint!.text = String(format: "%d°C", dewPoint)
                    }
                    if let rainFall = todaysForcast["precipMM"] as? Double {
                        self.RainFall!.text = String(format: "%.2f cm", rainFall)
                    }
                    if let pressure = todaysForcast["pressureMB"] as? Int {
                        self.Pressure!.text = String(format: "%d mb", pressure)
                    }
                    if let wind = todaysForcast["windSpeedKPH"] as? Int {
                        self.Wind!.text = String(format: "%d", wind) + " kph from " +  windDir
                    }
                } else {
                    if let currentTemp = todaysForcast["avgTempF"] as? Int{
                        self.CurrentTemperature!.text = String(format: "%d", currentTemp)
                    }
                    if let todaysHi = todaysForcast["maxTempF"] as? Int {
                        self.TodaysHi!.text =  String(format: "%d", todaysHi)
                    }
                    if let todaysLo = todaysForcast["minTempF"] as? Int {
                        self.TodaysLo!.text = String(format: "%d",  todaysLo)
                    }
                    if let fealsLike = todaysForcast["feelslikeF"] as? Int {
                        self.HeatIndex!.text = String(format: "%d°F",fealsLike)
                    }
                    if let dewPoint = todaysForcast["dewpointF"] as? Int {
                        self.DewPoint!.text = String(format: "%d°F", dewPoint)
                    }
                    if let rainFall = todaysForcast["precipIN"] as? Double {
                        self.RainFall!.text = String(format: "%.2f in", rainFall)
                    }
                    if let pressure = todaysForcast["pressureIN"] as? Int {
                        self.Pressure!.text = String(format: "%d in", pressure)
                    }
                    if let wind = todaysForcast["windSpeedMPH"] as? Int{
                        self.Wind!.text = String(format: "%d", wind) + " mph from " +  windDir
                    }
                }
                
                if timeZoneValid {
                    formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
                    formatter.timeZone = NSTimeZone(forSecondsFromGMT: timeZone! * 3600)
                    formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
                    
                    if let sunriseISO = todaysForcast["sunriseISO"] as? String{
                        if let sunrise = formatter.dateFromString(sunriseISO){
                            formatter.dateFormat =  use24HourMode ? "HH:mm" :"hh:mm a"
                            let time = formatter.stringFromDate(sunrise)
                            self.Sunrise!.text = time
                        }
                    }
                    
                    //@todo not showing up right now
                    if let sunsetISO = todaysForcast["sunsetISO"] as? String{
                        if let sunset = formatter.dateFromString(sunsetISO){
                            formatter.dateFormat =  use24HourMode ? "HH:mm" :"hh:mm a"
                            let time = formatter.stringFromDate(sunset)
                            self.Sunset!.text = time
                        }
                    }
                }
                
                self.TodaysWeatherDescription!.text = todaysForcast["weatherPrimary"] as? String
                if let humidity = todaysForcast["humidity"] as? Int{
                    self.Humidity!.text = String(format: "%d%%", humidity)
                }
                if let cloudCover = todaysForcast["sky"] as? Int{
                    self.CloudCover!.text = String(format: "%d%%", cloudCover)
                }
                if let uv = todaysForcast["uvi"] as? Int {
                    self.UV!.text = String(format: "%d out of 12", uv)
                }
                if let icon = todaysForcast["icon"] as? String{
                    let imageCurrent = icon.componentsSeparatedByString(".")
                    self.TodaysWeatherImage.image = UIImage(named: "iconw-" + imageCurrent[0])
                }
            }
            setFocastNew(self.Day1Image!,dayOfWeekLabel: self.Day1Day!,hiTempLabel: self.Day1HiTemp!, loTempLabel:self.Day1LowTemp!, forcast: forecasts["1"])
            
            setFocastNew(self.Day2Image!,dayOfWeekLabel: self.Day2Day!,hiTempLabel: self.Day2HiTemp!, loTempLabel:self.Day2LowTemp!, forcast: forecasts["2"])
            
            setFocastNew(self.Day3Image!,dayOfWeekLabel: self.Day3Day!,hiTempLabel: self.Day3HiTemp!, loTempLabel:self.Day3LowTemp!, forcast: forecasts["3"])
            
            setFocastNew(self.Day4Image!,dayOfWeekLabel: self.Day4Day!,hiTempLabel: self.Day4HiTemp!, loTempLabel:self.Day4LowTemp!, forcast: forecasts["4"])
            
            setFocastNew(self.Day5Image!,dayOfWeekLabel: self.Day5Day!,hiTempLabel: self.Day5HiTemp!, loTempLabel:self.Day5LowTemp!, forcast: forecasts["5"])
            
//            setFocast(self.Day2Image!,dayOfWeekLabel: self.Day2Day!,precipitationLabel: self.Day2Precipitation!, forcast: forecasts["2"])
//            setFocast(self.Day3Image!,dayOfWeekLabel: self.Day3Day!,precipitationLabel: self.Day3Precipitation!, forcast: forecasts["3"])
//            setFocast(self.Day4Image!,dayOfWeekLabel: self.Day4Day!,precipitationLabel: self.Day4Precipitation!, forcast: forecasts["4"])
//            setFocast(self.Day5Image!,dayOfWeekLabel: self.Day5Day!,precipitationLabel: self.Day5Precipitation!, forcast: forecasts["5"])
        }
    }
  
    
    private func setFocast(imageView:UIImageView, dayOfWeekLabel:UILabel, precipitationLabel:UILabel, forcast:AnyObject?){
        let formatter = NSDateFormatter()
        if let forcast = forcast{
            if let icon = forcast["icon"] as? String{
                let image1 = icon.componentsSeparatedByString(".")
                imageView.image = UIImage(named: image1.count > 1 ? "iconw-" + image1[0] : "iconw-na")
            }
            if let sunriseISO = forcast["sunriseISO"] as? String {
                formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
                formatter.timeZone = NSTimeZone.localTimeZone()
                formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
                if let day1 = formatter.dateFromString(sunriseISO){
                    formatter.dateFormat = "EEE"
                    dayOfWeekLabel.text = formatter.stringFromDate(day1)
                }
            }
            if let precip1 = forcast["pop"] as? Int{
                precipitationLabel.text = String(format: "%d%%", precip1)
            }
        }
    }
    
    private func setFocastNew(imageView:UIImageView, dayOfWeekLabel:UILabel, hiTempLabel:UILabel, loTempLabel:UILabel, forcast:AnyObject?){
        let formatter = NSDateFormatter()
        if let forcast = forcast{
            if let icon = forcast["icon"] as? String{
                let image1 = icon.componentsSeparatedByString(".")
                imageView.image = UIImage(named: image1.count > 1 ? "iconw-" + image1[0] : "iconw-na")
            }
            if let sunriseISO = forcast["sunriseISO"] as? String {
                formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
                formatter.timeZone = NSTimeZone.localTimeZone()
                formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
                if let day1 = formatter.dateFromString(sunriseISO){
                    formatter.dateFormat = "EEE"
                    dayOfWeekLabel.text = formatter.stringFromDate(day1)
                }
            }
            
            // print("forcast description \(forcast.debugDescription)")
        
            //@todo accomodate centigrade
            if let hiTemp = forcast["maxTempF"] as? Int{
                hiTempLabel.text = String(format: "%d%", hiTemp)
            }
            if let loTemp = forcast["minTempF"] as? Int{
                loTempLabel.text = String(format: "%d%", loTemp)
            }
        }
    }

}


