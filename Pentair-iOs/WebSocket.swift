//
//  WebSocket.swift
//  Pentair-iOS
//
//  Created by Jim Hewitt on 12/15/14.
//  Copyright (c) 2014 Pentair Corporation. All rights reserved.
//

import Foundation

class WebSocket: NSObject, SRWebSocketDelegate {
    var socket: SRWebSocket?
    var timer: NSTimer?

    init(urlString: String) {
        super.init()
        let url = NSURL(string: urlString)
        let request = NSURLRequest(URL: url!)
        socket = SRWebSocket(URLRequest: request)
        socket!.delegate = self
        socket!.open()
        timer = NSTimer.scheduledTimerWithTimeInterval(60 * 1, target: self, selector: Selector("pingFrame"), userInfo: nil, repeats: true)
    }
    
    func send(msg: String) {
        if (socket!.readyState.value == SR_OPEN.value) {
            socket!.send(msg)
        }
    }
    
    func close() {
        timer!.invalidate()
        if (socket!.readyState.value >= SR_CONNECTING.value)
        {
            socket!.close()
        }
        socket = SRWebSocket()
    }
    
    func pingFrame() {
        send("ping")
    }
    
    func webSocket(webSocket: SRWebSocket!, didReceiveMessage message: AnyObject!) {
        if let msg = message as? NSString {
            println("Message: \(msg)")
            if(!msg.containsString("pong")) {
                NSNotificationCenter.defaultCenter().postNotificationName("WsMessageEvent", object: self)
                NSNotificationCenter.defaultCenter().postNotificationName("WsMessageEvent", object: msg)
            }
        }
    }
    
    func webSocketDidOpen(webSocket: SRWebSocket!) {
        NSNotificationCenter.defaultCenter().postNotificationName("WsOpenEvent", object: self)
    }
    
    func webSocket(webSocket: SRWebSocket!, didCloseWithCode code: Int, reason: String!, wasClean: Bool) {
        NSNotificationCenter.defaultCenter().postNotificationName("WsCloseEvent", object: self)
    }
    
    func webSocket(webSocket: SRWebSocket!, didFailWithError error: NSError!) {
        NSNotificationCenter.defaultCenter().postNotificationName("WsErrorEvent", object: self)
    }
}