//
//  PentairiOSTester.swift
//  Pentair-iOs
//
//  Created by Path Finder on 7/6/15.
//  Copyright (c) 2015 Pentair Corporation. All rights reserved.
//

import UIKit
import XCTest
import Pentair
import Utility

enum UserInvitationStatus : String {
    case All = "ALL", Accepted = "ACCEPTED", Unknown = "UNKNOWN",Bounced = "BOUNCED", Expired = "EXPIRED"
    
    static let allValues = [All,Accepted,Unknown,Bounced,Expired]
    static let allStrings = ["All","Accepted","No Response","Bounced","Expired"]
    
    static func getIndex(find:UserInvitationStatus) -> Int?{
        for (index, value) in allValues.enumerate() {
            if (value == find){
                return index
            }
        }
        return nil
    }
}

class PentairiOSTester: NSObject {
    
    var expectation: XCTestExpectation!
    var timer: NSTimer!
    var resultDictionary = [String : AnyObject]()
    let httpService = HttpService()
    let netServiceBrowser: Discovery? = Discovery(service: "Pentair: ")
    var selectedFilter = UserInvitationStatus.All
    var searchString = ""
    var currentPage = 1
    var pageSize = 20
    var property: Property?
    var host: String?

    // MARK: - Unit Test methods
    
    func pentairLoginWithExpectation(expectation: XCTestExpectation!) {
        self.expectation = expectation
        self.login("ebacklin@pathf.com", password: "Password", selector: #selector(PentairiOSTester.loginEvent), notification: "HttpLogin")
    }
    
    func fetchInstallationsWithExpectation(expectation: XCTestExpectation!) {
        self.expectation = expectation
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PentairiOSTester.installationsDidUpdate(_:)), name: "InstallationsListUpdated", object: httpService)

        self.login("ebacklin@pathf.com", password: "Password", selector: #selector(PentairiOSTester.loginEventForInstallations), notification: "HttpLogin")
    }
    
    func fetchPoolUsersWithExpectation(expectation: XCTestExpectation!) {
        self.expectation = expectation
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PentairiOSTester.installationsDidUpdateWhileFetchingUsers(_:)), name: "InstallationsListUpdated", object: httpService)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PentairiOSTester.usersDidUpdate), name: "UsersListUpdated", object: property)

        self.login("ebacklin@pathf.com", password: "Password", selector: #selector(PentairiOSTester.loginEventForPoolUsers), notification: "HttpLogin")
    }
    
    // MARK: - Login Notifications
    
    func loginEvent() {
        let chkRememberMe: UISwitch! = UISwitch()
        
        chkRememberMe.on = true
        if self.httpService.isUserLoggedIn() {
            self.resultDictionary["status"] = true
            self.expectation.fulfill()
        } else {
            self.resultDictionary["status"] = false
            self.expectation.fulfill()
        }
        
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
        
    func loginEventForInstallations() {
        let chkRememberMe: UISwitch! = UISwitch()
        
        chkRememberMe.on = true
        if self.httpService.isUserLoggedIn() {
            self.resultDictionary["status"] = true
            self.fetchInstallations(self)
        } else {
            self.resultDictionary["status"] = false
            self.resultDictionary["error"] = "Unable to login."
            self.expectation.fulfill()
            NSNotificationCenter.defaultCenter().removeObserver(self)
        }
    }
    
    func loginEventForPoolUsers() {
        let chkRememberMe: UISwitch! = UISwitch()
        
        chkRememberMe.on = true
        if self.httpService.isUserLoggedIn() {
            self.resultDictionary["status"] = true
            self.fetchInstallations(self)
        } else {
            self.resultDictionary["status"] = false
            self.resultDictionary["error"] = "Unable to login."
            self.expectation.fulfill()
            NSNotificationCenter.defaultCenter().removeObserver(self)
        }
    }
    
    // MARK: - Installation Notifications

    func installationsDidUpdate(notification:NSNotification) {
        if let newData = notification.userInfo?["installations"] as? [AnyObject]{
            if let totalCount = Int((notification.userInfo?["totalInstallations"]?.description)!){
                self.resultDictionary["status"] = true
                self.resultDictionary["totalInstallations"] = totalCount
                self.resultDictionary["installations"] = newData
                NSNotificationCenter.defaultCenter().removeObserver(self)
                self.expectation.fulfill()
            } else {
                self.resultDictionary["status"] = false
                self.resultDictionary["error"] = "totalInstallations count = 0."
                NSNotificationCenter.defaultCenter().removeObserver(self)
                self.expectation.fulfill()
            }
        } else {
            self.resultDictionary["status"] = false
            self.resultDictionary["error"] = "installations data is nil."
            NSNotificationCenter.defaultCenter().removeObserver(self)
            self.expectation.fulfill()
        }
    }
    
    func installationsDidUpdateWhileFetchingUsers(notification:NSNotification) {
        if let newData = notification.userInfo?["installations"] as? [AnyObject]{
            if let totalCount = Int((notification.userInfo?["totalInstallations"]?.description)!){
                self.resultDictionary["status"] = true
                self.resultDictionary["totalInstallations"] = totalCount
                self.resultDictionary["installations"] = newData
                self.fetchUsers(1)
            } else {
                self.resultDictionary["status"] = false
                self.resultDictionary["error"] = "totalInstallations count = 0."
                NSNotificationCenter.defaultCenter().removeObserver(self)
                self.expectation.fulfill()
            }
        } else {
            self.resultDictionary["status"] = false
            self.resultDictionary["error"] = "installations data is nil."
            NSNotificationCenter.defaultCenter().removeObserver(self)
            self.expectation.fulfill()
        }
    }
    
    // MARK: - User Notifications

    func usersDidUpdate() {
        let totalCount: Int = property!.configurationModel.Users.count
        if totalCount > 0 {
            self.resultDictionary["status"] = true
            self.resultDictionary["totalUsers"] = totalCount
            self.resultDictionary["poolUser"] = property!.configurationModel.Users
            NSNotificationCenter.defaultCenter().removeObserver(self)
            self.expectation.fulfill()
        } else{
            self.resultDictionary["status"] = false
            self.resultDictionary["error"] = "total users count = 0."
            NSNotificationCenter.defaultCenter().removeObserver(self)
            self.expectation.fulfill()
        }
    }

    // MARK: - Utility methods
    
    func login(userName: String!, password: String!, selector: Selector!, notification: String?) {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: selector, name: notification, object: self.httpService)
        
        self.httpService.login(userName, password: password, saveLoginInfo: false)
        }
    
    func fetchInstallations(sender: AnyObject?) {
        if netServiceBrowser == nil {
            self.resultDictionary["status"] = false
            self.resultDictionary["error"] = "Discovery netServiceBrowser is nil."
            NSNotificationCenter.defaultCenter().removeObserver(self)
            self.expectation.fulfill()
        } else {
            netServiceBrowser!.reset()
            netServiceBrowser!.searchForLocalProperties()
            if self.httpService.isUserLoggedIn() {
                self.httpService.getInstallations()
            } else {
                self.resultDictionary["status"] = false
                self.resultDictionary["error"] = "User is not logged in."
                NSNotificationCenter.defaultCenter().removeObserver(self)
                self.expectation.fulfill()
            }
        }
    }
    
    func fetchUsers(page:Int){
        if let token = self.httpService.authToken() {
            let installations: [AnyObject] = self.resultDictionary["installations"] as! [AnyObject]
            let connection = installations[0]
            self.host = baseConnection + "api/websocket/" + String(connection["InstallationId"]!) + "?access-token=" + token
            self.property = Property(urlString: self.host!, service: httpService)
            self.property!.getUsers(pageSize, page: page, filter: selectedFilter.rawValue, searchTerm: searchString)
        } else {
            self.resultDictionary["status"] = false
            self.resultDictionary["error"] = "Invalid or nil authToken."
            NSNotificationCenter.defaultCenter().removeObserver(self)
            self.expectation.fulfill()
        }
    }

}
