//
//  Pentair_iOsTests.swift
//  Pentair-iOsTests
//
//  Created by Jim Hewitt on 12/9/14.
//  Copyright (c) 2014 Pentair Corporation. All rights reserved.
//

import UIKit
import XCTest
import Pentair

class Pentair_iOsTests: XCTestCase {
    
    var pentairiOSTester: PentairiOSTester!
    var didLogin: Bool = false
    var didFetchInstallations: Bool = false
    var didFetchUsers: Bool = false

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        self.pentairiOSTester = PentairiOSTester()
        self.didLogin = false
        self.didFetchInstallations = false
        self.didFetchUsers = false
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testPentairLogin() {
        // This is an example of a performance test case.
        self.measureBlock() {
            if self.didLogin == false {
                // Put the code you want to measure the time of here.
                let expectation: XCTestExpectation = self.expectationWithDescription("Login Expectation")
                
                self.pentairiOSTester.pentairLoginWithExpectation(expectation)
                
                self.waitForExpectationsWithTimeout(5.0, handler: { (error: NSError?) -> Void in
                    if error != nil {
                        NSLog("Timeout Error: \(error!.description)")
                    } else {
                        let resultsDictionary: [String : AnyObject]? = self.pentairiOSTester.resultDictionary
                        let status: Bool = resultsDictionary!["status"] as! Bool
                        
                        XCTAssertNotNil(resultsDictionary, "No values were returned.")
                        XCTAssertTrue(status, "Connection was not successful")
                        
                        self.didLogin = true
                        NSLog("Status returned:\n\(resultsDictionary!)")
                    }
                })
            }
        }
    }
    
    func testFetchInstallations() {
        // This is an example of a performance test case.
        self.measureBlock() {
            if self.didFetchInstallations == false {
                // Put the code you want to measure the time of here.
                let expectation: XCTestExpectation = self.expectationWithDescription("Login Expectation")
                
                self.pentairiOSTester.fetchInstallationsWithExpectation(expectation)
                
                self.waitForExpectationsWithTimeout(15.0, handler: { (error: NSError?) -> Void in
                    if error != nil {
                        NSLog("Timeout Error: \(error!.description)")
                    } else {
                        let resultsDictionary: [String : AnyObject]? = self.pentairiOSTester.resultDictionary
                        let status: Bool = resultsDictionary!["status"] as! Bool
                        
                        XCTAssertTrue(status, "Fetch Installation was not successful.")
                        XCTAssertNotNil(resultsDictionary, "No values were returned.")
                        
                        let newData: [AnyObject] = resultsDictionary!["installations"] as! [AnyObject]
                        XCTAssertNotNil(resultsDictionary, "No Installation values were returned.")
               
                        
                        let installation: AnyObject = newData[0]
                        XCTAssertNotNil(installation, "first installation = nil.")
                        
                        self.didFetchInstallations = true
                        NSLog("Installation poolName: \(installation["PoolName"])")
                        
                    }
                })
            }
        }
    }
    
    func testFetchPoolUsers() {
        // This is an example of a performance test case.
        self.measureBlock() {
            if self.didFetchUsers == false {
                // Put the code you want to measure the time of here.
                let expectation: XCTestExpectation = self.expectationWithDescription("Fetch Pool User Expectation")
                
                self.pentairiOSTester.fetchPoolUsersWithExpectation(expectation)
                
                self.waitForExpectationsWithTimeout(15.0, handler: { (error: NSError?) -> Void in
                    if error != nil {
                        NSLog("Timeout Error: \(error!.description)")
                    } else {
                        let resultsDictionary: [String : AnyObject]? = self.pentairiOSTester.resultDictionary
                        let status: Bool = resultsDictionary!["status"] as! Bool
                        
                        XCTAssertTrue(status, "Fetch PoolUser was not successful.")
                        XCTAssertNotNil(resultsDictionary, "No values were returned.")
                        
                        let newData: [AnyObject]? = resultsDictionary!["poolUser"] as! [AnyObject]?
                        XCTAssertNotNil(newData, "No PoolUser values were returned.")
                        
                        let totalCount: Int? = resultsDictionary!["totalUsers"] as! Int?
                        XCTAssertNotNil(totalCount, "totalUsers count = nil.")
                        
                        if newData != nil {
                            let poolUser: AnyObject = newData![0] as AnyObject
                            XCTAssertNotNil(poolUser, "first PoolUser = nil.")
                            
                            self.didFetchUsers = true
                            NSLog("PoolUser email: \(poolUser["email"])")
                            NSLog("PoolUser totalCount: \(totalCount!)")
                        }
                    }
                })
            }
        }
    }
    
}
