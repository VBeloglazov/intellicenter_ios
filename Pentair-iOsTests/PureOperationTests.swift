//
//  PureOperationTests.swift
//  CVTest
//
//  Created by Dietrich Kappe on 7/16/14.
//  Copyright (c) 2014 Dietrich Kappe. All rights reserved.
//

import Foundation

import XCTest
import CVTest


class PureOperationTests: XCTestCase {
    var onError : PureOperation.WorkConsumer = {
        (params : PureOperation.Hash) -> Void in
        let message = "message"
        println("Error message \(params[message])")
    }
    
    var onUpdate : PureOperation.WorkConsumer = {
        (params : PureOperation.Hash) -> Void in
        if let value = params["value"] {
            println("Value: \(value)")
        } else {
            println("***Value is invalid")
        }
        
    }
    
    var onFinish : PureOperation.WorkConsumer = {
        (params : PureOperation.Hash) -> Void in
        if let value = params["value"] {
            println("Finish Value: \(value)")
        }
    }
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCreate() {
        println("Starting create test")
        
        var done = false
        
        let finishMe : PureOperation.WorkConsumer = {
            (params : PureOperation.Hash) -> Void in
            if let value = params["value"] {
                println("Finish Value: \(value)")
            }
            done = true
        }
        
        let producer : PureOperation.WorkProducer = {
            (task : PureOperation, params : PureOperation.Hash) -> PureOperation.Hash in
            
            println("Running fib")
            
            var fib1 : NSDecimalNumber
            
            if let unwrapped = params["first"] {
                fib1 = unwrapped as NSDecimalNumber
            } else {
                fib1 = NSDecimalNumber(int : 0)
            }
            
            var fib2 : NSDecimalNumber
            
            if let unwrapped = params["second"] {
                fib2 = unwrapped as NSDecimalNumber
            } else {
                fib2 = NSDecimalNumber(int : 1)
            }
            
            for index in 1...5 {
                sleep(1)
                var temp : NSDecimalNumber = fib1.decimalNumberByAdding(fib2)
                fib1 = fib2
                fib2 = temp
                task.doUpdate(["value" : fib2])
            }
            return ["value" : fib2]
        }
        
        let worker = PureOperation(producer, finishMe, onError, onUpdate)
        
        worker.doTask(["first": NSDecimalNumber(int : 0), "second": NSDecimalNumber(int : 1)])
        
        var count = 8
        
        while (count > 0) {
            NSRunLoop.mainRunLoop().runMode(NSDefaultRunLoopMode,
                beforeDate: NSDate(timeIntervalSinceNow: 1.1))
            count--
            println("Decremented count \(count)")
        }
        
        XCTAssert(done, "Done was false.")

    }
    
    func testError() {
        var wasUpdated = false
        var wasError = false
        var wasFinished = false
        
        let finishMe : PureOperation.WorkConsumer = {
            (params : PureOperation.Hash) -> Void in
            wasFinished = true
        }
        
        let errorMe : PureOperation.WorkConsumer = {
            (params : PureOperation.Hash) -> Void in
            wasError = true
        }
        
        let updateMe : PureOperation.WorkConsumer = {
            (params : PureOperation.Hash) -> Void in
            wasUpdated = true
        }
        
        let producer : PureOperation.WorkProducer = {
            (task : PureOperation, params : PureOperation.Hash) -> PureOperation.Hash in
            task.reportError(["message":"something went wrong"])
            return [:]
        }
        
        let worker = PureOperation(producer, finishMe, errorMe, updateMe)
        worker.doTask([:])
        
        var count = 6
        
        while (count > 0) {
            NSRunLoop.mainRunLoop().runMode(NSDefaultRunLoopMode,
                beforeDate: NSDate(timeIntervalSinceNow: 0.2))
            count--
        }
        
        XCTAssertFalse(wasFinished, "Should not be finished.")
        XCTAssertFalse(wasUpdated, "Should not be updated.")
        XCTAssert(wasError, "Should have reported an error.")
    }
    
    func testCancelOldGeneration() {
        
        var finish1 = false
        var finish2 = false
        
        let finishMe : PureOperation.WorkConsumer = {
            (params : PureOperation.Hash) -> Void in
            finish1 = true
        }
        
        let finishMe2 : PureOperation.WorkConsumer = {
            (params : PureOperation.Hash) -> Void in
            finish2 = true
        }
        
        let producer : PureOperation.WorkProducer = {
            (task : PureOperation, params : PureOperation.Hash) -> PureOperation.Hash in
            println("*** In producer")
            task.nextGeneration()
            task.clearOldQ()
            println("*** Out producer")
            return [:]
        }
        
        let producer2 : PureOperation.WorkProducer = {
            (task : PureOperation, params : PureOperation.Hash) -> PureOperation.Hash in
            // do nothing
            println("*** In producer2")
            return [:]
        }
        
        let worker1 = PureOperation(producer, finishMe, onError, nil)
        let worker2 = PureOperation(producer2, finishMe2, onError, nil)
        
        worker1.doTask([:])
        worker2.doTask([:])
        
        var count = 20
        while (count > 0) {
            NSRunLoop.mainRunLoop().runMode(NSDefaultRunLoopMode,
                beforeDate: NSDate(timeIntervalSinceNow: 0.5))
            count--
        }
        
        XCTAssertFalse(finish2, "Should have cancelled the second task.")
        XCTAssert(finish1, "Should have finished the first task.")
    }
    
    func testBlankUpdate() {
        var wasUpdated = false
        
        let producer : PureOperation.WorkProducer = {
            (task : PureOperation, params : PureOperation.Hash) -> PureOperation.Hash in
            task.doUpdate([:])
            return [:]
        }
        
        let worker = PureOperation(producer, onFinish, onError, nil)
        
        worker.doTask([:])
        
        var count = 5
        while (count > 0) {
            NSRunLoop.mainRunLoop().runMode(NSDefaultRunLoopMode,
                beforeDate: NSDate(timeIntervalSinceNow: 0.5))
            count--
        }
        
        XCTAssertFalse(wasUpdated, "Should not have been updated.")
        
    }
}
