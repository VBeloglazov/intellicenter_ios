//
//  CommandExecute.swift
//  Pentair
//
//  Created by Jim Hewitt on 11/6/15.
//  Copyright © 2015 Path Finder. All rights reserved.
//

import Foundation

public class commandExecute: ExecuteMessage {
    public override init() { super.init()}
    
    public func buildExecuteCommand(method: String, objects: [String: String]) -> CommandMessage{
        self.command = MessageType.SEND_COMMAND_MESSAGE.rawValue
        self.method = method;
        self.arguments = objects;
        return self
    }
}