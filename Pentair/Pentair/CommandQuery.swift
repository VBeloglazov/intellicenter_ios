//
//  CommandQuery.swift
//  Pentair
//
//  Created by Jim Hewitt on 10/24/15.
//  Copyright © 2015 Path Finder. All rights reserved.
//

import Foundation
public class commandQuery: CommandMessage {
    var queryName: String!
    var arguments: String!
    
    func buildGetConfig() -> CommandMessage{
        self.command = MessageType.QUERY_COMMAND.rawValue
        self.queryName = QueryName.GET_CONFIGURATION.rawValue
        self.arguments = ""
        return self
    }
    
    func buildGetActiveStatusMessages() -> CommandMessage{
        self.command = MessageType.QUERY_COMMAND.rawValue
        self.queryName = QueryName.GET_ACTIVE_STATUS_MESSAGES.rawValue
        print ("Query name is \(self.queryName)")
        self.arguments = ""
        return self
    }
    
    
    func buildGetHistory() -> CommandMessage{
        self.command = MessageType.QUERY_COMMAND.rawValue
        self.queryName = QueryName.GET_HISTORY.rawValue
        self.arguments = ""
        return self
    }

}