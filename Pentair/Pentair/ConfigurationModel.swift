//
//  ConfigurationModel.swift
//  Pentair
//
//  Created by Jim Hewitt on 1/6/16.
//  Copyright © 2016 Path Finder. All rights reserved.
//
// This object holds the configuration dictionary and all objects assocated with the attached THW
// These get the objects from the control object


import Foundation


public class ConfigurationModel : NSObject {
    public var ConfigurationDictionary: [String: NSDictionary] = [String : NSDictionary]()
    public var ConfigurationHeirarchy: [AnyObject] = [AnyObject]()
    public var ActiveStatusMessagesDictionary: [String: NSDictionary] = [String : NSDictionary]()
    public var ActiveStatusMessagesHeirarchy: [AnyObject] = [AnyObject]()
    
    // answer ... was ConfigurationResponse
    //ControlObjects is like your Database - key is all the object names, so that it won't be duplicated
    // Can rename if we want to ...

    
    public var ControlObjects: [String : NSMutableDictionary] = [String : NSMutableDictionary]()  // object
    public var StatusMessages: [NSDictionary] = [NSDictionary]()  // object
    // @todo - are we using Weather, WeatherValid, and Users here?

    public var Weather: [String: NSDictionary] = [String : NSDictionary]()
    public var WeatherValid: Bool = false
    public var Users = [AnyObject]()
    public var Messages = [AnyObject]()
    public var TotalUsers:Int = 0
    public var Notifications = [AnyObject]()
    public var TotalNotifications:Int = 0
    
    public func getAllFeatures()-> [AnyObject]{
        var result = [AnyObject]()
        for (objNam,params) in ControlObjects{
            if(isfeature(objNam)){
                params["OBJNAM"] = objNam
                result.append(params)
                print("All features params \(params)")
            }
        }
        return result
    }
    
    public func getAllLights()-> [AnyObject]{
        var result = [AnyObject]()
        for (objNam,params) in ControlObjects{
            if(isLight(objNam)){
                params["OBJNAM"] = objNam
                result.append(params)
            }
        }
        return result
    }

    
    public func getAllBodies()-> [NSMutableDictionary]{
        var result = [NSMutableDictionary]()
        for (objNam,params) in ControlObjects{
            if(isBody(objNam)){
                params["OBJNAM"] = objNam
                result.append(params)
            }
        }
        return result
    }
    
    
    public func getAllRemotes()-> [NSMutableDictionary]{
        var result = [NSMutableDictionary]()
        for (objNam,params) in ControlObjects{
            if(isRemote(objNam)){
                params["OBJNAM"] = objNam
                result.append(params)
            }
        }
        return result
    }
    
    public func getAllLightGroups()-> [AnyObject]{
        var result = [AnyObject]()
        for (objNam,params) in ControlObjects{
            if(isLightGroup(objNam)){
                params["OBJNAM"] = objNam
                result.append(params)
            }
        }
        return result
    }
    
    public func getAllSecurityTokens()-> [AnyObject]{
        var result = [AnyObject]()
        for (objNam,params) in ControlObjects{
            if(isSecurityToken(objNam)){
                params["OBJNAM"] = objNam
                result.append(params)
            }
        }
        return result
    }
    
    
    public func getItem(objNam:String)-> NSMutableDictionary?{
        if let result = ControlObjects[objNam]{
            result["OBJNAM"] = objNam
            print("objName: \(objNam)")
            return result
        }
        return nil
    }
    
    public func getParam(objNam:String, param:String)-> String{
        var result = ""
        if let object = ControlObjects[objNam]{
            if let value = object[param] as? String{
                print("value: \(value)")
                result = value
            }
        }
        return result
    }
    
    
    public func getAllPumps()-> [AnyObject]{
        var result = [AnyObject]()
        for (objTyp,params) in ControlObjects{
            print("objTyp \(objTyp)")
            if(isPump(objTyp)){
                print("This is a pump")
                params["OBJTYP"] = objTyp
                result.append(params)
                print("pump params \(params)")
            }
        }
        return result
    }
    
    
    public func getAllPumpEffects()-> [AnyObject]{
        var result = [NSMutableDictionary]()
        for (objTyp,params) in ControlObjects{
            print("objTyp \(objTyp)")
            if(isPumpEffect(objTyp)){
                print("This is a pump effect")
                params["OBJTYP"] = objTyp
                result.append(params)
                print("pump effect params \(params)")
            }
        }
        return result
    }

    public func getAllCircuits()-> [AnyObject]{
        var result = [NSMutableDictionary]()
        for (objTyp,params) in ControlObjects{
            print("objTyp \(objTyp)")
            if(isCircuit(objTyp)){
               // print("This is a circuit")
                params["OBJTYP"] = objTyp
                result.append(params)
                print("params for the Circut \(params)")
            }
        }
        return result
    }
    
    public func getAllStatusMessages()-> [AnyObject]{
        var result = [AnyObject]()
        print("result \(result)")
        return result
    }
        
    public func getStatusMasterObject()-> [AnyObject]{
        var result = [AnyObject]()
        for (objNam,params) in ControlObjects{
            if(isStatusMaster(objNam)){
                params["OBJNAM"] = objNam
                result.append(params)
                print("params for the StatusMasterObject \(params)")
            }
        }
        return result
    }

    
//    public func getAllNotifications()-> [AnyObject]{
//        var result = [AnyObject]()
//        for (objTyp,params) in ControlObjects{
//            print("objTyp \(objTyp)")
//            if(isNotification(objTyp)){
//                params["OBJTYP"] = objTyp
//                result.append(params)
//                 print("this is a notification master")
//                 print("params for the Notifications \(params)")
//            }
//        }
//       return result
    
        // evoke case GET_ACTIVE_STATUS_MESSAGE = "GETACTIVESTATUSMESSAGE"
        // here
        
//        var result = [AnyObject]()
//        for (objTyp,params) in ControlObjects{
//            print("objTyp \(objTyp)")
//            if(isNotification(objTyp)){
//                print("This is a notification")
//                params["OBJTYP"] = objTyp
//                result.append(params)
//                print("status message notification params \(params)")
//            }
//        }
//        return result
//    }

    
    public func getObjectsBySubType(subtyp:String)-> [NSMutableDictionary]{
        var result = [NSMutableDictionary]()
        for (objNam,params) in ControlObjects{
            if(getParam(objNam,param: "SUBTYP") == subtyp){
                params["OBJNAM"] = objNam
                result.append(params)
            }
        }
        return result
    }
    
    public func getObjectsByType(type:String)-> [NSMutableDictionary]{
        var result = [NSMutableDictionary]()
        for (objNam,params) in ControlObjects{
            if(getParam(objNam,param: "OBJTYP") == type){
                params["OBJNAM"] = objNam
                result.append(params)
            }
        }
        return result
    }

    
    public func getChildren(objNam:String) -> [AnyObject]{
        var result = [AnyObject]()
        if let object = ControlObjects[objNam]{
            if let value = object["OBJLIST"]{
                result = value as! [AnyObject]
            }
        }
        return result
    }
    
    
    public func getIntelliChlor(objNam:String) -> AnyObject?{
        for obj:AnyObject in getChildren(objNam){
            var child = obj as! [String : AnyObject]
            if(isIntelliChlor(child["objnam"]!.description)){
                return child
            }
        }
        return nil
    }
    
    public func getIntelliChem(objNam:String) -> AnyObject?{
        for obj:AnyObject in getChildren(objNam){
            var child = obj as! [String : AnyObject]
            if(isIntelliChem(child["objnam"]!.description)){
                return child
            }
        }
        return nil
    }
    public func hasColorCapabilities(objNam:String) ->Bool{
        let usageFlags =  getParam(objNam,param: "USAGE")
        for flag in usageFlags.characters {
            let lightMode = LightMode(key:flag)
            
            if (lightMode.isColor){
                return true
            }
        }
        return false
    }
    
    public func hasCoolingCapabilities(objNam:String) -> Bool{
        return getParam(objNam,param: "OBJTYP") == "HEATER" && getParam(objNam,param: "SHOMNU").containsString("c")
    }
    
    
    public func hasHeatingCapabilities(objNam:String) -> Bool{
        return getParam(objNam,param: "OBJTYP") == "HEATER" && getParam(objNam,param: "SHOMNU").containsString("h")
    }
    
    public func isLastKnownTempEnabled() ->Bool {
        return getParam("_5451", param: "TEMPNC") == "ON"
    }
    
    public func getAllHeaters(bodyObjNam:String) -> [AnyObject]{
        var result = [AnyObject]()
        for obj:AnyObject in getChildren(bodyObjNam){
            var child = obj as! [String : AnyObject]
            if(isHeater(child["objnam"]!.description)){
                result.append(child)
            }
        }
        return result
    }
    
    public func isLight(objNam:String) -> Bool{
        return getParam(objNam,param: "OBJTYP") == "CIRCUIT" && getParam(objNam,param: "SHOMNU").containsString("l") && getParam(objNam,param: "SUBTYP") != "CIRCGRP" && getParam(objNam,param: "SUBTYP") != "LITSHO"
    }

    
    public func isLightGroup(objNam:String) -> Bool{
        return getParam(objNam,param: "OBJTYP") == "CIRCUIT" && getParam(objNam,param: "SHOMNU").containsString("l") && (getParam(objNam,param: "SUBTYP") == "CIRCGRP" || getParam(objNam,param: "SUBTYP") == "LITSHO")
    }
    
    public func isDimmable(objNam:String) -> Bool{
        return getParam(objNam,param: "OBJTYP") == "CIRCUIT" && getParam(objNam,param: "USAGE").containsString("D")
    }
    
    public func isPump(objTyp:String) -> Bool{
        return getParam(objTyp,param: "OBJTYP") == "PUMP"
    }
    
    public func isStatusMaster(objNam:String) -> Bool{
        return getParam(objNam,param: "OBJNAM") == "_57A7" //&&
            //getParam(objNam, param: "OBJNAM").containsString("t")   //_57A7
    }
    
    public func isStatusMessage(objTyp:String) -> Bool{
        return getParam(objTyp,param: "OBJTYP") == "STAMSG"
        print ("this is a status message")
        // && getParam(objNam, param: "OBJNAM").containsString("t")   //_57A7
    }
    
    public func isPumpEffect(objTyp:String) -> Bool{
      // print("this is a pump effect")
       return getParam(objTyp,param: "OBJTYP") == "PMPCIRC"
    }
    
    public func isCircuit(objTyp:String) -> Bool{
        // print("this is a circuit")
        return getParam(objTyp,param: "OBJTYP") == "CIRCUIT"
    }
    
    public func isfeature(objNam:String) -> Bool{
        return getParam(objNam,param: "OBJTYP") == "CIRCUIT" && getParam(objNam,param: "SHOMNU").containsString("f")
    }
    
    public func isChemistry(objNam:String) -> Bool{
        return getParam(objNam,param: "OBJTYP") == "CHEM"
    }
    
    public func isRemote(objNam:String) -> Bool{
        return getParam(objNam,param: "OBJTYP") == "REMOTE"
    }
    
    public func isSecurityToken(objNam:String) -> Bool{
        if objNam == "UFFFF" || objNam == "UFFFD" {
            return false
        }
        return getParam(objNam,param: "OBJTYP") == "PERMIT"
    }
    
    public func isHeater(objNam:String) -> Bool{
        return getParam(objNam,param: "OBJTYP") == "HEATER"
    }
    
    public func isBody(objNam:String) -> Bool{
        return getParam(objNam,param: "OBJTYP") == "BODY"
    }
    
    public func isAir(objNam:String) -> Bool{
        return getParam(objNam,param: "OBJNAM") == "_A135"
    }
    public func isIntelliChem(objNam:String) -> Bool{
        return (isChemistry(objNam) && getParam(objNam,param: "SUBTYP") == "ICHEM")
    }
    public func isIntelliChlor(objNam:String) -> Bool{
        return ( isChemistry(objNam) && getParam(objNam,param: "SUBTYP") == "ICHLOR")
    }
    
    public func areAllRemotesDisabled()-> Bool{
        var result = true
        for (objNam,params) in ControlObjects{
            if(isRemote(objNam)){
                if let enabled = params["ENABLE"] as? String where enabled == "ON"{
                    result = false
                }
            }
        }
        return result
    }
    
    public func isBodyOn(circuit:NSMutableDictionary) -> Bool{
        if let status = circuit["STATUS"] as? String {
            return ["OVRON","ON","DLYOFF"].contains(status.uppercaseString)
        }
        return false
    }
    
    public func isBodyOff(circuit:NSMutableDictionary) -> Bool{
        if let status = circuit["STATUS"] as? String {
            return ["OVROFF","OFF","DLYON"].contains(status.uppercaseString)
        }
        return false
    }
    
}