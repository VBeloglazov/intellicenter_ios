//
//  ExecuteMessage.swift
//  Pentair
//
//  Created by Jim Hewitt on 11/6/15.
//  Copyright © 2015 Path Finder. All rights reserved.
//

import Foundation
public class ExecuteMessage: CommandMessage {
    
    var method:String!
    var arguments=[:]
    public override init() { super.init()}
}
