//
//  GetParamList.swift
//  Pentair
//
//  Created by Jim Hewitt on 10/25/15.
//  Copyright © 2015 Path Finder. All rights reserved.
//

import Foundation

class getParamList: ObjectListMessage {
    override init() { super.init()}
    var condition:String?

    func buildRequestParamList() -> getParamList {
        self.command = MessageType.REQUEST_PARAM_LIST.rawValue
        return self
    }
    
    func buildRequestAllUsers() -> getParamList {
        self.command = MessageType.GET_USER_LIST.rawValue
        return self
    }
    
    func buildRequestSecurityTokens() -> getParamList {
        self.command = MessageType.GET_PARAM_LIST.rawValue
        self.objectList.append(KeyObject().buildRequestSecurityTokens())
        self.condition = "OBJTYP = PERMIT"
        return self
    }
    
    func buildRequestAllPumps() -> getParamList {
        self.command = MessageType.GET_PARAM_LIST.rawValue
        self.objectList.append(KeyObject().buildRequestPumps())
        self.condition = "OBJTYP = PUMP"
        return self
    }
    
    func buildRequestAllPumpEffects() -> getParamList {
        self.command = MessageType.GET_PARAM_LIST.rawValue
        self.objectList.append(KeyObject().buildRequestPumpEffects())
        self.condition = "OBJTYP = PMPCIRC"
        return self
    }
    
    func buildRequestCircuits() -> getParamList {
        self.command = MessageType.GET_PARAM_LIST.rawValue
        self.objectList.append(KeyObject().buildRequestCircuits())
        self.condition = "OBJTYP = CIRCUIT"
        return self
    }
    
//    func buildRequestSchedule() -> getParamList {
//        self.command = MessageType.GET_PARAM_LIST.rawValue
//        self.objectList.append(KeyObject().buildRequestSchedule())
//        self.condition = "OBJTYP = SCHED"
//        return self
//    }
    
    func buildRequestStatusMaster() -> getParamList {
        self.command = MessageType.GET_ACTIVE_STATUS_MESSAGES.rawValue
        self.objectList.append(KeyObject().buildRequestStatusMaster())
        //self.condition = "OBJNAM = _57A7"  //STAMSG  //_57A7
        print("status master object list: \(self.objectList.debugDescription)")
        return self
    }

//    func buildRequestPumpCircuits() -> getParamList {
//        self.command = MessageType.GET_PARAM_LIST.rawValue
//        self.objectList.append(KeyObject().buildRequestCircuits())
//        self.condition = "OBJTYP = CIRCUIT"
//        self.condition = "SUBTYP = PMPCIRC"
//        return self
//    }

    
    
//    func buildRequestDelayDetails(objName:String) -> getParamList {
//        self.command = MessageType.GET_PARAM_LIST.rawValue
//        self.objectList.append(KeyObject().buildRequestDelayDetails(objName))
//        return self
//    }
//
//    func buildRequestAllBodies() -> WebSocketRequest {
//        self.command = MessageType.GET_PARAM_LIST.rawValue
//        self.objectList.append(KeyObject().buildRequestAllBodies())
//        return self
//    }
//
//    func buildRequestAllCircuits() -> WebSocketRequest {
//        self.command = MessageType.GET_PARAM_LIST.rawValue
//        self.objectList.append(KeyObject().buildRequestAllCircuits())
//        return self
//    }
//
//    func buildRequestAllAirTemp() -> WebSocketRequest {
//            self.command = MessageType.GET_PARAM_LIST.rawValue
//        let keyOb = KeyObject().buildRequestAllAirTemp()
//            self.objectList.append(keyOb)
//            return self
//    }
//
//    func buildRequestAllHeaters() -> WebSocketRequest {
//            self.command = MessageType.GET_PARAM_LIST.rawValue
//            self.objectList.append(KeyObject().buildRequestAllHeaters())
//            return self
//    }
//
//    func buildRequestAllPumps() -> WebSocketRequest {
//        self.command = MessageType.GET_PARAM_LIST.rawValue
//        self.objectList.append(KeyObject().buildRequestAllPumps())
//        return self
//    }
//
//    func buildRequestAllValves() -> WebSocketRequest {
//        self.command = MessageType.GET_PARAM_LIST.rawValue
//        self.objectList.append(KeyObject().buildRequestAllValves())
//        return self
//    }
//
//    func buildRequestAllHeaterCombos() -> WebSocketRequest {
//            self.command = MessageType.GET_PARAM_LIST.rawValue
//            self.objectList.append(KeyObject().buildRequestAllHeaterCombos())
//            return self
//    }
//
    func buildRequestSystemPreferences() -> getParamList {
            self.command = MessageType.GET_PARAM_LIST.rawValue
            self.objectList.append(KeyObject().buildRequestSystemPrefs())
            return self
    }
    func buildRequestAir() -> getParamList {
        self.command = MessageType.GET_PARAM_LIST.rawValue
        self.objectList.append(KeyObject().buildRequestAirTemp())
        return self
    }
    func buildRequestFreezeCircuit() -> getParamList {
        self.command = MessageType.GET_PARAM_LIST.rawValue
        self.objectList.append(KeyObject().buildRequestFreezeCircuits())
        self.condition = "SUBTYP = FRZ"
        return self
    }
    func buildRequestRemotes() -> getParamList {
        self.command = MessageType.GET_PARAM_LIST.rawValue
        self.objectList.append(KeyObject().buildRequesRemotes())
        self.condition = "OBJTYP = REMOTE"
        return self
    }
}
