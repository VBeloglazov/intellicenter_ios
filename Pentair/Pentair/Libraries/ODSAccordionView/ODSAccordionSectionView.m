//
//  ODSSectionView.m
//  ODSAccordionView
//
//  Created by Johannes Seitz on 17/06/14.
//  Copyright (c) 2014 Johannes W. Seitz. All rights reserved.
//

#import "ODSAccordionSectionView.h"
#import "ODSAccordionSectionStyle.h"
#import "ODSArrowIcon.h"


#define BOUNDS_KEY_PATH NSStringFromSelector(@selector(bounds))
#define MARGIN 8

@implementation ODSAccordionSectionView {
    ODSAccordionSectionStyle *_sectionStyle;
    CGFloat _sectionViewAlpha;
    UIView *_infoView;
    ODSArrowIcon *_arrowIcon;
}
//UIView *_infoView;
-(instancetype)initWithTitle:(NSString *)sectionTitle
                     andView:(UIView *)sectionView
                sectionStyle:(ODSAccordionSectionStyle *)sectionStyle {
    self = [super init];
    if (self) {
        _sectionStyle = sectionStyle;
        _sectionView = sectionView;
        self.backgroundColor = _sectionStyle.backgroundColor;
        [self setClipsToBounds:YES];
        if (_sectionStyle.icon != nil)
        {
            _sectionStyle.headerMargin = _sectionStyle.headerMargin + _sectionStyle.iconSize.width;
        }
        [self makeHeader:sectionTitle];
        [self addIcon];
        [self addArrowIcon];
        
        [self addSubview: sectionView];
        [sectionView addObserver:self forKeyPath:BOUNDS_KEY_PATH options:0 context:nil];
        
        [self layoutIfNeeded];
        [self collapseSectionAnimated:NO];
    }
    return self;
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    if ([keyPath isEqualToString:BOUNDS_KEY_PATH] && self.expanded){
        self.height = [self expandedHeight];
    }
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

-(void)makeHeader:(NSString *)sectionTitle {
    _header = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [_header setTitleEdgeInsets:UIEdgeInsetsMake(40.0f, 0, 0.0f, 0.0f)];
    [_header setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [_header setContentVerticalAlignment:UIControlContentVerticalAlignmentTop];
    
    [_header setTitle:sectionTitle forState:UIControlStateNormal];
    [_header setTitleColor:_sectionStyle.headerTitleLabelTextColor forState:UIControlStateNormal];
    [_header setTitleColor:_sectionStyle.headerTitleLabelHighlightedTextColor forState:UIControlStateHighlighted];
    _header.backgroundColor = _sectionStyle.headerBackgroundColor;
    _header.titleLabel.font = _sectionStyle.headerTitleLabelFont;
    
    [_header addTarget:self action:@selector(toggleButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_header];
}

-(void)addIcon {
    if (_sectionStyle.icon != nil)
    {
        double verticalCenter = self.headerHeight / 2 - _sectionStyle.iconSize.height / 2;
        
        UIImageView *imgIcon = [[UIImageView alloc] initWithFrame:CGRectMake(10,verticalCenter,_sectionStyle.iconSize.width,_sectionStyle.iconSize.height)];
        UIImage *imgBody = _sectionStyle.icon;
        imgIcon.image = imgBody;
        [self.header addSubview:imgIcon];
    }
    
}

-(void)addArrowIcon {
    if (_sectionStyle.arrowVisible) {
        _arrowIcon = [[ODSArrowIcon alloc] initWithFrame:CGRectMake(0, 0, 30, 5)];
        _arrowIcon.color = _sectionStyle.arrowColor;
        
        _infoView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, _arrowIcon.frame.size.width, _arrowIcon.frame.size.height)];
        [_infoView addSubview:_arrowIcon];
    }
    else if(_sectionStyle.infoView != nil){
        _infoView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, _sectionStyle.infoView .frame.size.width, _sectionStyle.infoView .frame.size.height)];
        [_infoView addSubview:_sectionStyle.infoView];
    }
    [self.header addSubview:_infoView];
}

-(void)collapseSectionAnimated:(BOOL)animated {
    _expanded = NO;
    self.height = self.collapsedHeight;
    _sectionViewAlpha = 0.0;
    [self sectionViewAlphaChanged:animated];
    [_arrowIcon pointDownAnimated:animated];
}

-(void)expandSectionAnimated:(BOOL)animated {
    _expanded = YES;
    self.height = self.expandedHeight;
    _sectionViewAlpha = 1.0;
    [self sectionViewAlphaChanged:animated];
    [_arrowIcon pointUpAnimated:animated];
}

-(void)sectionViewAlphaChanged:(BOOL)animated {
    if (animated){
        [UIView animateWithDuration:0.5 animations:^{ _sectionView.alpha = _sectionViewAlpha; }];
    } else {
        _sectionView.alpha = _sectionViewAlpha;
    }
}

-(void)toggleButtonPressed:(id)sectionPressed {
    if (_expanded){
        [self collapseSectionAnimated:YES];
    } else {
        [self expandSectionAnimated:YES];
    }
    [self setNeedsLayout];
}

-(CGFloat)headerHeight {
    return _sectionStyle.headerHeight;
}

-(void)layoutSubviews {
    [super layoutSubviews];
    [self layoutHeader];
    [self layoutSection];
}

-(void)layoutHeader {
    _header.frame = CGRectMake(_header.frame.origin.x, _header.frame.origin.y, self.width, self.headerHeight);
    CGSize infoViewSize = _infoView.frame.size;
    double verticalCenter = self.headerHeight / 2 - infoViewSize.height / 2;
    if (_sectionStyle.headerStyle == ODSAccordionHeaderStyleLabelLeft){
        [_header setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        [_header setTitleEdgeInsets:UIEdgeInsetsMake(verticalCenter, _sectionStyle.headerMargin, 0, 0)];
        _infoView.frame = CGRectMake(self.width - infoViewSize.width - 10 , verticalCenter ,infoViewSize.width, infoViewSize.height);
    } else if (_sectionStyle.headerStyle == ODSAccordionHeaderStyleLabelRight){
        [_header setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [_header setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, _sectionStyle.headerMargin)];
        _infoView.frame = CGRectMake(_sectionStyle.headerMargin, verticalCenter,
                                     infoViewSize.width, infoViewSize.height);
    } else if (_sectionStyle.headerStyle == ODSAccordionHeaderStyleLabelCentered) {
        [_header setContentVerticalAlignment:UIControlContentVerticalAlignmentTop];
        [_header setTitleEdgeInsets:UIEdgeInsetsZero];
        _infoView.frame = CGRectMake(_header.center.x - (_infoView.frame.size.width / 2),
                                     _header.frame.size.height - _infoView.frame.size.height - _sectionStyle.headerMargin,
                                     _infoView.frame.size.width, _infoView.frame.size.height);
    }
}

-(void)dealloc {
    @try {
        [_sectionView removeObserver:self forKeyPath:BOUNDS_KEY_PATH];
    }
    @catch (NSException * __unused exception) {}
}

-(void)layoutSection {
    CGSize fittingSize = [_sectionView sizeThatFits:_sectionView.bounds.size];
    _sectionView.frame = CGRectMake(0, self.headerHeight, self.width, fittingSize.height);
}

-(void)autoHeight{
    if (_expanded) {
        self.height = self.expandedHeight;
    }else{
        self.height = self.collapsedHeight;
    }
}

-(CGFloat)expandedHeight {
    CGSize fittingSize = [_sectionView sizeThatFits:_sectionView.bounds.size];
    return fittingSize.height  + self.headerHeight;
}

-(CGFloat)collapsedHeight {
    return self.headerHeight;
}

-(CGFloat)width {
    return self.frame.size.width;
}

@end
