//
//  LightCode.swift
//  Pentair
//
//  Created by Dashon Howard on 2/12/16.
//  Copyright © 2016 Path Finder. All rights reserved.
//

import Foundation

public enum LightCode : String {
    
    case PARTY = "Party Mode"
    case ROMAN = "Romance"
    case CARIB = "Caribbean"
    case AMERCA = "American"
    case SSET = "Sunset"
    case ROYAL = "Royal"
    case WHITER = "IntelliBrite White"
    case GREENR = "IntelliBrite Green"
    case BLUER = "IntelliBrite Blue"
    case MAGNTAR = "IntelliBrite Magenta"
    case REDR = "IntelliBrite Red"
    case WHITE = "Sam White"
    case LITGRN = "Sam Light Green"
    case GREEN = "Sam Green"
    case AQUA = "Sam Aqua"
    case BLUE = "Sam Blue"
    case MAGNTA = "Sam Magenta"
    case RED = "Sam Red"
    case LITRED = "Sam Light Red"
    case ROTATE = "Rotate Start"
    case STOP = "Rotate Stop"
    case SYNC = "Color Sync"
    case SWIM = "Color Swim"
    case RECALL = "Recall"
    case SAMMOD = "Sam Mode"
    case THUMP = "Thumper"
    case MODE = "Mode"
    case RESET = "Reset"
    case HOLD = "Hold"
    case SMART = "Smart Start"
    case DIMMER = "White "
    case NONE = "--"
    
    static let allValues = [PARTY, ROMAN, CARIB, AMERCA, SSET, ROYAL,WHITER,GREENR,BLUER,MAGNTAR,REDR,WHITE,LITGRN,GREEN, AQUA, BLUE, MAGNTA, RED, LITRED, ROTATE, STOP, SYNC,SWIM, RECALL, SAMMOD, THUMP, MODE, RESET, HOLD, DIMMER, NONE]
    
//    public init?(enumKey:String){
//    self = NONE
//    for code in LightCode.allValues{
//    if String(code) == enumKey {
//    self = code
//        self.init(rawValue:code.rawValue)
//    }
//    }
//    return self
//    }
}
extension LightCode {
   public init?(enumKey:String){
        //self = NONE
        for code in LightCode.allValues{
            if String(code) == enumKey {
                self.init(rawValue:code.rawValue)
                return
            }
        }
        self.init(rawValue:NONE.rawValue)
        return
    }
    
}