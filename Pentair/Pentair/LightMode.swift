//
//  LightMode.swift
//  Pentair
//
//  Created by Dashon Howard on 2/12/16.
//  Copyright © 2016 Path Finder. All rights reserved.
//

import Foundation

public class LightMode : NSObject{
    public var colors = [UIColor.lightGrayColor()]
    public var code:LightCode!
    public var flag:Character?
    public var secondaryColor = UIColor.whiteColor()
    public var boarderColor = UIColor.clearColor()
    public var isColor = true
    override init() {
        super.init()
    }
    public init(lightCode:LightCode){
        super.init()
        self.code = lightCode
        setUp()
    }
    
    func setUp(){
        switch self.code! {
        case .SWIM:
            colors = makeColors([0x7fc99b,0x938fc6,0x74d0ed])
            isColor = false
            flag = "s"
            break
        case .PARTY:
            colors = makeColors([0x5DBF8C, 0xB8D979, 0xF08F67])
            flag = "y"
            break
        case .ROMAN:
            colors = makeColors([0xE7322F, 0xE08982, 0x7D77B7, 0x00AFAF, 0xE7322F])
            flag = "M"
            break
        case .CARIB:
            colors = makeColors([0x9ACC59, 0xE08982, 0xE7322F, 0x9ACC59, 0xE8882C, 0xF0C12F, 0xE7322F, 0x9ACC59])
            flag = "b"
            break
        case .AMERCA:
            colors = makeColors([0xFFF0DA, 0x006399, 0xFFF0DA, 0x006399, 0xD52E5F, 0xFFF0DA, 0x006399, 0xD52E5F, 0xFFF0DA])
            flag = "a"
            break
        case .SSET:
            colors = makeColors([ 0xFDBA56, 0xD3535C, 0x683B93, 0xEA832A, 0x7C73B4, 0xF6644E, 0xFDBF63])
            flag = "S"
            break
        case .ROYAL:
            colors = makeColors([0xFFF165, 0x1DA453, 0x2E3192])
            flag = "o"
            break
        case .WHITER:
            flag = "w"
            colors = makeColors([0xFFFFFF])
            secondaryColor = UIColor.blackColor()
            boarderColor = UIColor.blackColor()
            break
        case .WHITE:
            colors = makeColors([0xFFFFFF])
            secondaryColor = UIColor.blackColor()
            boarderColor = UIColor.blackColor()
            flag = "i"
            break
        case .DIMMER:
            colors = makeColors([0xFFFFFF])
            secondaryColor = UIColor.blackColor()
            boarderColor = UIColor.blackColor()
            flag = "D"
            isColor = false
            break
        case .GREEN:
            flag = "n"
            colors = makeColors([0x64a70b])
            break
        case .GREENR:
            colors = makeColors([0x64a70b])
            flag = "g"
            break
        case .BLUE:
            flag = "B"
            colors = makeColors([0x0c3471])
            break
        case .BLUER:
            colors = makeColors([0x0c3471])
            flag = "l"
            break
        case .MAGNTA:
            flag = "A"
            colors = makeColors([0xEC008B])
            break
        case .MAGNTAR:
            colors = makeColors([0xEC008B])
            flag = "T"
            break
        case .RED:
            flag = "R"
            colors = makeColors([0xff3519])
            break
        case .REDR:
            colors = makeColors([0xff3519])
            flag = "e"
            break
        case .LITGRN:
            colors = makeColors([0x9fcc14])
            flag = "G"
            break
        case .AQUA:
            colors = makeColors([0x5979bb])
            flag = "1"
            break
        case .LITRED:
            colors = makeColors([0xff4930])
            flag = "L"
            break
        case .NONE:
            isColor = false
            break
        default:
            isColor = false
            break
        }
    }
    
    public convenience init(key:Character){
        switch(key){
        case "u":
            self.init(lightCode:LightCode.THUMP)
            break
        case "m":
            self.init(lightCode: LightCode.MODE)
            break
        case "t":
            self.init(lightCode: LightCode.RESET)
            break
        case "h":
            self.init(lightCode: LightCode.HOLD)
            break
        case "Q":
            self.init(lightCode: LightCode.RECALL)
            break
        case "r":
            self.init(lightCode: LightCode.ROTATE)
            break
        case "p":
            self.init(lightCode: LightCode.STOP)
            break
        case "s":
            self.init(lightCode: LightCode.SWIM)
            break
        case "c":
            self.init(lightCode: LightCode.SYNC)
            break
        case "y":
            self.init(lightCode: LightCode.PARTY)
            break
        case "M":
            self.init(lightCode: LightCode.ROMAN)
            break
        case "b":
            self.init(lightCode: LightCode.CARIB)
            break
        case "a":
            self.init(lightCode: LightCode.AMERCA)
            break
        case "S":
            self.init(lightCode:LightCode.SSET)
            break
        case "o":
            self.init(lightCode:LightCode.ROYAL)
            break
        case "w":
            self.init(lightCode:LightCode.WHITER)
            break
        case "g":
            self.init(lightCode:LightCode.GREENR)
            break
        case "l":
            self.init(lightCode:LightCode.BLUER)
            break
        case "T":
            self.init(lightCode:LightCode.MAGNTAR)
            break
        case "e":
            self.init(lightCode:LightCode.REDR)
            break
        case "i":
            self.init(lightCode:LightCode.WHITE)
            break
        case "G":
            self.init(lightCode:LightCode.LITGRN)
            break
        case "n":
            self.init(lightCode:LightCode.GREEN)
            break
        case "q":
            self.init(lightCode:LightCode.AQUA)
            break
        case "B":
            self.init(lightCode:LightCode.BLUE)
            break
        case "A":
            self.init(lightCode:LightCode.MAGNTA)
            break
        case "R":
            self.init(lightCode:LightCode.RED)
            break
        case "L":
            self.init(lightCode:LightCode.LITRED)
            break
        case "d":
            self.init(lightCode:LightCode.SAMMOD)
            break
        case "E":
            self.init(lightCode:LightCode.SMART)
            break
        case "D":
            self.init(lightCode:LightCode.DIMMER)
            break
        default:
            self.init(lightCode:LightCode.NONE)
            break
        }
        self.flag = key
    }
    private func makeColors(hexColors:[Int]) -> [UIColor] {
        var newColors = [UIColor]()
        for color in hexColors{
            newColors.append(UIColor(hex: color))
        }
        return newColors
    }
}