//
//  Connections.swift
//  Pentair-iOs
//
//  Created by Jim Hewitt on 12/24/14.
//  Copyright (c) 2014 Pentair Corporation. All rights reserved.
//

import Foundation
import Utility

//public class Installation: Deserializable {
//    public var id: Int?
//    public var accessToken: String?
//    public var location: String?
//    public var onLine: Bool = false
//    public var phone: String?
//    public var poolName: String?
//    public var securityName: String?
//    public var address: String?
//    public var city: String?
//    public var state: String?
//    public var zip: String?
//    public var country: String?
//    public var ownerName: String?
//    public var ownerId: String?
//    public var email: String?
//    public var timeZone: Int?
//    public var totalAdmins: Int?
//    
//    required public init(data: [String: AnyObject]) {
//        id <-- data["InstallationId"]
//        accessToken <-- data["AccessToken"]
//        location <-- data["Location"]
//        onLine <-- data["OnLine"]
//        phone <-- data["Phone"]
//        poolName <-- data["PoolName"]
//        securityName <-- data["SecurityName"]
//        totalAdmins <-- data["TotalAdmins"]
//        ownerName <-- data["OwnerName"]
//        address <-- data["Address"]
//        city <-- data["City"]
//        state <-- data["State"]
//        country <-- data["Country"]
//        zip <-- data["Zip"]
//        email <-- data["Email"]
//        timeZone <-- data["TimeZone"]
//    }
//}
//
//public class Installations {
//    var installations: [Installation] = []
//    required public init(data: AnyObject) {
//            if let con = data as? [String: AnyObject] {
//                let installation = Installation(data: con)
//                installations.append(installation)
//            }
//    
//    }
//    required public init(data: [AnyObject]) {
//        for item in data {
//            if let con = item as? [String: AnyObject] {
//                let installation = Installation(data: con)
//                installations.append(installation)
//            }
//        }
//    }
//}

