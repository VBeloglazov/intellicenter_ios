//
//  CommandString.swift
//  Pentair-iOS
//
//  Created by Dashon Howard on 12/22/14.
//  Copyright (c) 2014 Pentair Corporation. All rights reserved.
//

import Foundation

public let kEnvironmentKey: String = "environment_type_preference"

// Message command types
public enum MessageType: String {
    case GET_PARAM_LIST = "GETPARAMLIST"
    case SET_PARAM_LIST = "SETPARAMLIST"
    case SEND_QUERY = "SENDQUERY"
    case REQUEST_PARAM_LIST = "REQUESTPARAMLIST"
    case RELEASE_PARAM_LIST = "RELEASEPARAMLIST"
    case SEND_PARAM_LIST = "SENDPARAMLIST"
    case NOTIFY_LIST = "NOTIFYLIST"
    case CLEAR_PARAM_LIST = "CLEARPARAMLIST"
    case UNKNOWN = "UNKNOWN"
    case GET_USER_LIST = "USERLIST"
    case EDIT_USER = "EDITUSER"
    case DELETE_USER = "DELETEUSER"
    case INVITE_USER = "INVITEUSER"
    case API_RESPONSE = "APIRESPONSE"
    case API_RESPONSE_HARDWARE = "APIRESPONSEHARDWARE"
    case QUERY_COMMAND = "GETQUERY"
    case WRITE_STATUS_MESSAGE = "WRITESTATUSMESSAGE"
    case SEND_COMMAND_MESSAGE = "SETCOMMAND"
    case CONFIGURATION = "CONFIGURATION"
    case GET_ACTIVE_STATUS_MESSAGES = "GETACTIVESTATUSMESSAGES"

}

// Query names
public enum QueryName: String {
    case GET_CONFIGURATION = "GETCONFIGURATION"
    case GET_HISTORY = "GETHISTORY"
    case GET_ACTIVE_STATUS_MESSAGES = "GETACTIVESTATUSMESSAGES"
  
    //@todo GET_SCHEDULE
}

// Command names
public enum CommandName: String {
    case SET_BODYSTATE = "SETBODYSTATE"
}

// Singleton objects and query object predicates
public enum ObjectName: String {
    case ALL = "ALL"
    case PREFERENCES = "_5451"
    case LOCATION = "_C10C"
    case TIME_SOURCE = "_C105"
    case SYSTEM_SENSE = "_A135"
    case STATUS_MASTER = "_57A7"
}

// Object types
public enum ObjectType: String {
    case BODY = "BODY"
    case HEATER = "HEATER"
    case PUMP = "PUMP"
    case PUMPEFFECT = "PMPCIRC"
    case HISTORY = "HISTORY"
    case VALVE = "VALVE"
    case SCHEDULE = "SCHED"
    case CIRCUIT = "CIRCUIT"
    case PERMIT = "PERMIT"
    case SENSE = "SENSE"
    case SYSTEM = "SYSTEM"
    case TIME = "SYSTIM"
    case STATUS = "STAMSG"
}

// List of required parameters for the given object type.
//public enum RequestKeys: String{
//    case KEY_BODY = "OBJNAM : SNAME : FILTER : TEMP : LOTMP : HITMP : OBJTYP : SUBTYP : HEATER : SHOMNU : LISTORD ? OBJTYP=BODY"
//    case KEY_CIRCUIT = "OBJNAM : SNAME : STATUS : OBJTYP : SUBTYP : SHOMNU : LISTORD : TIMOUT ? OBJTYP=CIRCUIT"
//    case KEY_SENSE_AIR = "OBJNAM : OBJTYP : PROBE : STATUS : SUBTYP : LISTORD"
//    case KEY_HEATER = "OBJAM : SNAME : PERMIT : STATUS : OBJTYP : SUBTYP : SHOMNU : READY : BODY : SHARE : LISTORD ? OBJTYP=HEATER"
//    case KEY_PUMP = "OBJAM : OBJTYP : SUBTYP ? OBJTYP=PUMP"
//    case KEY_VALVE = "OBJAM : OBJTYP : SUBTYP ? OBJTYP=VALVE"
//    case KEY_HEATER_COMBO = "OBJNAM : USE : ACT : HEATER : OBJTYP : SUBTYP : SHOMNU : LISTORD ? OBJTYP=HCOMBO"
//    case KEY_SYS_PREFS = "MODE : ZIP : TIMZON"
//    case KEY_SECURITY_TOKENS = "OBJNAM : SNAME ? OBJTYP=PERMIT"
//    case KEY_DELAY_DETAILS = "TIMOUT : SOURCE"
//}


public func getRequestKeys(ObjectType: String) -> AnyObject {
    switch ObjectType {
    case "BODY":
        return ["OBJNAM", "SNAME", "FILTER", "TEMP", "LOTMP", "HITMP", "OBJTYP", "SUBTYP", "SHOMNU", "LISTORD"]
    case "CIRCUIT":
        return ["OBJNAM", "SNAME", "STATUS", "OBJTYP", "SUBTYP", "SHOMNU", "LISTORD", "TIMOUT"]
    case "SENSE":
        return ["OBJNAM", "OBJTYP", "PROBE", "STATUS", "SUBTYP", "LISTORD"]
    case "SYSTEM":
        return ["MODE", "ZIP", "TIMZON"]
    case "PERMIT":
        return ["OBJNAM", "SNAME","OBJTYP", "PASSWRD", "ENABLE"]
    case "FRZ":
        return ["STATUS" , "SUBTYP" , "OBJTYP"]
    case "REMOTE":
        return ["ENABLE" , "SNAME" , "OBJTYP"]
    case "PUMP":
        return ["OBJNAM", "OBJTYP", "STATIC", "LISTORD", "SUBTYP", "HNAME", "SNAME", "CIRCUIT", "RPM", "GPM", "PWR", "STATUS"]
    case "PMPCIRC":
        return ["OBJNAM", "OBJTYP", "PARENT", "STATIC", "LISTORD", "CIRCUIT", "SPEED", "BOOST", "SELECT"]
    case "SCHED":
        return ["OBJNAM", "OBJTYP", "LISTORD", "CIRCUIT", "HNAME", "SNAME", "START","STOP","TIME","VACFLO"]
    case "STATUS_MASTER":
        return ["OBJNAM", "OBJREV", "OBJTYP", "STATIC", "RESET", "ACT", "BADGE","PARTY","ALARM","ROYAL","ALARM","SHOMNU"]
    case "STAMSG":
        return ["OBJNAM", "OBJREV", "OBJTYP", "PARENT", "MODE", "SNAME", "COUNT","PARTY","SINDEX","TIME","ALARM","SHOMNU"]
    default:
        break;
    }
    return []
}


