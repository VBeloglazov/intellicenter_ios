//
//  CommandMessage.swift
//  Pentair-iOS
//
//  Created by Dashon Howard on 12/22/14.
//  Copyright (c) 2014 Pentair Corporation. All rights reserved.
//

import Foundation
import Utility

public class CommandMessage :Serializable {
    
    public var command:String!
    public var messageID:String!
    override public init() { super.init()}
}


