//
//  RequestObject.swift
//  Pentair-iOS
//
//  Created by Dashon Howard on 12/22/14.
//  Copyright (c) 2014 Pentair Corporation. All rights reserved.
//

import Foundation
import Utility

class KeyObject : Serializable {

    var objnam: String!
    var keys: AnyObject!
    
    
//    func buildRequestAllBodies() -> KeyObject {
//        self.objnam = ObjectName.ALL.rawValue;
//        self.keys.append(RequestKeys.KEY_BODY.rawValue);
//        return self;
//    }
//    
//    func buildRequestAllCircuits() -> KeyObject {
//        self.objnam = ObjectName.ALL.rawValue;
//        self.keys.append(RequestKeys.KEY_CIRCUIT.rawValue);
//        return self;
//    }
    
    func buildRequestAirTemp() -> KeyObject {
        self.objnam = ObjectName.SYSTEM_SENSE.rawValue;
        self.keys = getRequestKeys("SENSE")
        return self;
    }
    
    func buildRequestSchedule() -> KeyObject {
        self.objnam = ObjectName.ALL.rawValue;
        self.keys = getRequestKeys("SCHED")
        return self;
    }
    
    func buildRequestStatusMessages() -> KeyObject {
        self.objnam = ObjectName.ALL.rawValue;
        self.keys = getRequestKeys("STAMSG")
        print("keys for status messages are \(keys)")
        return self;
    }
    
    func buildRequestStatusMaster() -> KeyObject {
        self.objnam = ObjectName.ALL.rawValue;
        self.keys = getRequestKeys("STATUS_MASTER")   //? STATUS
        print("keys for status master is \(keys)")
        return self;
    }
    
    func buildRequestAllPumps() -> KeyObject {
        self.objnam = ObjectName.ALL.rawValue;
        self.keys = getRequestKeys("PUMP")
        return self;
        }
    
    func buildRequestAllPumpEffects() -> KeyObject {
        self.objnam = ObjectName.ALL.rawValue;
        self.keys = getRequestKeys("PMPCIRC")
        return self;
    }


    
//
//    func buildRequestAllHeaters() -> KeyObject {
//        self.objnam = ObjectName.ALL.rawValue;
//        self.keys.append(RequestKeys.KEY_HEATER.rawValue);
//        return self;
//    }
//    
//    func buildRequestAllPumps() -> KeyObject {
//        self.objnam = ObjectName.ALL.rawValue;
//        self.keys.append(RequestKeys.KEY_PUMP.rawValue);
//        return self;
//    }
//    
//    func buildRequestAllValves() -> KeyObject {
//        self.objnam = ObjectName.ALL.rawValue;
//        self.keys.append(RequestKeys.KEY_VALVE.rawValue);
//        return self;
//    }
//    
//    func buildRequestAllHeaterCombos() -> KeyObject {
//        self.objnam = ObjectName.ALL.rawValue;
//        self.keys.append(RequestKeys.KEY_HEATER_COMBO.rawValue);
//        return self;
//    }
//    
    func buildRequestSystemPrefs() -> KeyObject {
        self.objnam = ObjectName.PREFERENCES.rawValue;
        self.keys = getRequestKeys("SYSTEM")
        return self;
    }
    
    func buildRequestHardwareDefinition() -> KeyObject {
        self.objnam = ObjectName.PREFERENCES.rawValue;
        self.keys = getRequestKeys("SYSTEM")
        return self;
    }
    
 //
//    func buildRequestLocation() -> KeyObject {
//        self.objnam = ObjectName.LOCATION.rawValue;
//        self.keys.append(RequestKeys.KEY_SYS_PREFS.rawValue);
//        return self;
//    }
//    
    
    func buildRequestPumps() -> KeyObject {
        self.objnam = ObjectName.ALL.rawValue;
        self.keys = getRequestKeys("PUMP")
        return self;
    }
    
    func buildRequestPumpEffects() -> KeyObject {
        self.objnam = ObjectName.ALL.rawValue;
        self.keys = getRequestKeys("PMPCIRC")
        return self;
    }
    
    func buildRequestCircuits() -> KeyObject {
        self.objnam = ObjectName.ALL.rawValue;
        self.keys = getRequestKeys("CIRCUIT")
        print("keys for circuits is \(keys)")
        return self;
    }
    
    func buildRequestSecurityTokens() -> KeyObject {
        self.objnam = ObjectName.ALL.rawValue;
        self.keys = getRequestKeys("PERMIT")
        return self;
    }
    func buildRequestFreezeCircuits() -> KeyObject {
        self.objnam = ObjectName.ALL.rawValue;
        self.keys = getRequestKeys("FRZ")
        return self;
    }
    func buildRequesRemotes() -> KeyObject {
        self.objnam = ObjectName.ALL.rawValue;
        self.keys = getRequestKeys("REMOTE")
        return self;
    }
//
//    func buildRequestDelayDetails(objName:String) -> KeyObject {
//        self.objnam = objName;
//        self.keys.append(RequestKeys.KEY_DELAY_DETAILS.rawValue);
//        return self;
//    }
}