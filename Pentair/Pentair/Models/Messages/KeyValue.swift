//
//  KeyObjectValue.swift
//  Pentair-iOS
//
//  Created by Jim Hewitt on 1/4/15.
//  Copyright (c) 2015 Pentair Corporation. All rights reserved.
//

import Foundation
import Utility

public class KeyValue : Serializable {
    
    var objnam:String!
    var params=[String: String]()
    
    public func setParametersObject(objnam: String, keyValues: [String: String]) -> KeyValue {
        self.objnam = objnam;
        self.params = keyValues;
        return self
    }
}
