//
//  KeyValueMessage.swift
//  Pentair-iOS
//
//  Created by Jim Hewitt on 1/4/15.
//  Copyright (c) 2015 Pentair Corporation. All rights reserved.
//

import Foundation
public class KeyValueMessage: CommandMessage {
    
    public var objectList=[KeyValue]()
    override public init() { super.init()}
}
