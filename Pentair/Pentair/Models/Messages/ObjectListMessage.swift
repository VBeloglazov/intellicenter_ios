//
//  ObjectListMessage.swift
//  Pentair-iOS
//
//  Created by Dashon Howard on 12/22/14.
//  Copyright (c) 2014 Pentair Corporation. All rights reserved.
//

import Foundation
class ObjectListMessage: CommandMessage {
    
    var objectList=[KeyObject]()
    override init() { super.init()}
}
