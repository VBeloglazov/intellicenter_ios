//
//  KeysMessage.swift
//  Pentair-iOs
//
//  Created by Dashon Howard on 1/27/15.
//  Copyright (c) 2015 Pentair Corporation. All rights reserved.
//

import Foundation
class ParamsMessage : CommandMessage {
    var params=[String:AnyObject]()
    
    
    override init() { super.init()}
    
     func buildMessage(command:String, params:[String:AnyObject]) -> ParamsMessage{
        self.command = command
        self.params = params
        return self
    }
}