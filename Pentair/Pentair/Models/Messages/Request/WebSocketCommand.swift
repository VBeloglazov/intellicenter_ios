//
//  WebSocketCommand.swift
//  Pentair-iOS
//
//  Created by Jim Hewitt on 1/4/15.
//  Copyright (c) 2015 Pentair Corporation. All rights reserved.
//

import Foundation

class WebSocketCommand : KeyValueMessage {
    override init() { super.init()}
    
    func buildSetParameters(objects: [KeyValue]) -> KeyValueMessage {
        self.command = MessageType.SET_PARAM_LIST.rawValue
        self.objectList = objects
        return self
    }
}
