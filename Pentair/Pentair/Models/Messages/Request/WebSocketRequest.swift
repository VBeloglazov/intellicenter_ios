//
//  WebSocketRequest.swift
//  Pentair-iOS
//
//  Created by Dashon Howard on 12/22/14.
//  Copyright (c) 2014 Pentair Corporation. All rights reserved.
//

import Foundation

class WebSocketRequest : ObjectListMessage {
     override init() { super.init()}
    
//    func buildRequestAllBodies() -> WebSocketRequest {
//        self.command = MessageType.GET_PARAM_LIST.rawValue
//        self.objectList.append(KeyObject().buildRequestAllBodies())
//        return self
//    }
//    
//    func buildRequestAllCircuits() -> WebSocketRequest {
//        self.command = MessageType.GET_PARAM_LIST.rawValue
//        self.objectList.append(KeyObject().buildRequestAllCircuits())
//        return self
//    }
//    
//    func buildRequestAllAirTemp() -> WebSocketRequest {
//            self.command = MessageType.GET_PARAM_LIST.rawValue
//        let keyOb = KeyObject().buildRequestAllAirTemp()
//            self.objectList.append(keyOb)
//            return self
//    }
//    
//    func buildRequestAllHeaters() -> WebSocketRequest {
//            self.command = MessageType.GET_PARAM_LIST.rawValue
//            self.objectList.append(KeyObject().buildRequestAllHeaters())
//            return self
//    }
//    
//    func buildRequestAllPumps() -> WebSocketRequest {
//        self.command = MessageType.GET_PARAM_LIST.rawValue
//        self.objectList.append(KeyObject().buildRequestAllPumps())
//        return self
//    }
//    
//    func buildRequestAllValves() -> WebSocketRequest {
//        self.command = MessageType.GET_PARAM_LIST.rawValue
//        self.objectList.append(KeyObject().buildRequestAllValves())
//        return self
//    }
//    
//    func buildRequestAllHeaterCombos() -> WebSocketRequest {
//            self.command = MessageType.GET_PARAM_LIST.rawValue
//            self.objectList.append(KeyObject().buildRequestAllHeaterCombos())
//            return self
//    }
    
    func buildRequestParamList() -> WebSocketRequest {
            self.command = MessageType.REQUEST_PARAM_LIST.rawValue
            return self
    }
    
//    func buildRequestSystemPreferences() -> WebSocketRequest {
//            self.command = MessageType.GET_PARAM_LIST.rawValue
//            self.objectList.append(KeyObject().buildRequestSystemPrefs())
//            return self
//    }
    
    func buildRequestAllUsers() -> WebSocketRequest {
        self.command = MessageType.GET_USER_LIST.rawValue
        return self
    }
    
    func buildRequestSecurityTokens() -> WebSocketRequest {
        self.command = MessageType.GET_PARAM_LIST.rawValue
        self.objectList.append(KeyObject().buildRequestSecurityTokens())
        return self
    }
    
    
    func buildRequestDelayDetails(objName:String) -> WebSocketRequest {
        self.command = MessageType.GET_PARAM_LIST.rawValue
        self.objectList.append(KeyObject().buildRequestDelayDetails(objName))
        return self
    }
}