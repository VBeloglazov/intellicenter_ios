//
//  HeaterControl.swift
//  Pentair-iOS
//
//  Created by Jim Hewitt on 1/5/15.
//  Copyright (c) 2015 Pentair Corporation. All rights reserved.
//

import Foundation

public class PickerData : NSObject, UIPickerViewDataSource {
    public var delegate: UIPickerViewDataSource?
    public var pickerItems = [PickerItem]()
    
    public func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    public func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerItems.count
    }
    
    public func getItem(objnam: String) -> String {
        var itemName: String = "Unknown"
        for item in pickerItems {
            if item.objnam! == objnam {
                itemName = item.name!
                break
            }
        }
        
        if itemName == "Unknown" {
       //     NSLog("getItem - objnam: \(objnam) itemName: \(itemName)")
        }
     //   NSLog("getItem - objnam: \(objnam) itemName: \(itemName)")
        return itemName
    }
    
    public func getItemRow(objnam: String) -> Int{
        for itemRow in 0...pickerItems.count - 1 {
            if pickerItems[itemRow].objnam == objnam{
                return itemRow
            }
        }
        return 0
    }
}

public class PickerItem : NSObject {
    public var objnam: String?
    public var name: String?
    
    public init(objnam: String, name: String) {
        self.objnam = objnam
        self.name = name
    }
}

