//
//  PoolUser.swift
//  Pentair-iOs
//
//  Created by Dashon Howard on 1/19/15.
//  Copyright (c) 2015 Pentair Corporation. All rights reserved.
//

import Foundation
import Utility

//public class PoolUser : Deserializable{
//    public var email:String?
//    public var name:String?
//    public var level:String?
//    public var status:String?
//    
//    required public init (data: [String: AnyObject]) {
//        email <-- data["email"]
//        name <-- data["name"]
//        level <-- data["level"]
//        status <-- data["status"]
//    }
//    
//    public init( name:String, email: String, level:String? , status:String?){
//        self.email = email
//        self.name = name
//        self.level = level
//        self.status = status
//    }
//    
//}
//
//public class userMessage : Deserializable{
//    public var command: String?
//    public var messageID: String?
//    public var users: [userItem]?
//    public var totalCount: Int?
//    
//    required public init(data: [String: AnyObject]) {
//        command <-- data["command"]
//        messageID <-- data["messageID"]
//        users <-- data["objectList"]
//        totalCount <-- data["totalCount"]
//    }
//    
//}
//
//public class userItem : Deserializable {
//    public var userName: String?
//    public var params: PoolUser?
//    
//    public required init(data: [String: AnyObject]) {
//        userName <-- data["objnam"]
//        params <-- data["params"]
//    }
//}
