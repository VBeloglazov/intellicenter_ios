//
//  Property.swift
//  Pentair-iOS
//
//  Created by Jim Hewitt on 12/17/14.
//  Copyright (c) 2014 Pentair Corporation. All rights reserved.
//
//  Property contains the property configuration data and status data.
//  It manages the web socket for the life time of the connection and
//  provides events via the NSNotificationCenter when changes occur to
//  the property's configuration or status data.
//
//  This object serves as a model for the property objects and manages
//  the communications with the THW

import Foundation
import SwiftWebSocket

public class Property : NSObject {
    public var securityTokens: [String : String] = [String : String]()
    public var statusMessages: [String : String] = [String : String]()
    public var bodyCount: Int = 0
    public var messageQueue = [String : MessageQueueItem]()
    public var configurationModel: ConfigurationModel = ConfigurationModel()
    public var socketError:ErrorType?
    public var timer: NSTimer?
    public var maxConnectionAttempts = 100
    
    
    private var ws: WebSocket?
    private var url: NSMutableURLRequest?
    private var httpService:HttpService?
    private var conntectionAttempt = 0
    private var connectionDelay = 0.0
    private var reOpenAfterClose = false
    public init(urlString: String, service: HttpService) {
        httpService = service;
        print("http service  \(httpService)")
        print("url  \(url)")
        super.init()
        var ssl: Bool = false
        let separators = NSCharacterSet(charactersInString: "?:=")
        let data:[String] = urlString.componentsSeparatedByCharactersInSet(separators)
        if (data.count > 3 && data[2] == "access-token") {
            self.url = NSMutableURLRequest(URL: NSURL(string: "wss:" + data[1])!)
            self.url!.addValue(data[3], forHTTPHeaderField: "Authorization")
            ssl = true
        } else {
            self.url = NSMutableURLRequest(URL: NSURL(string: "ws:" + data[1] + ":" + data[2] + "/")!)
        }
        self.securityTokens = [String : String]()
        self.ws = WebSocket()
        ws!.allowSelfSignedSSL = ssl
        ws!.event.open = {
            self.webSocketOpen()
        }
        ws!.event.error = { error in
            self.webSocketError(error)
        }
        ws!.event.message = { message in
            self.webSocketMessageReceived(message)
        }
        ws!.event.close = {_,_,_ in
            self.webSocketClosed()
        }
        ws!.open(request: self.url!)
    }
    
    public struct RequestStruct {
        var command: String
        var messageID: String
    }
    
    public func Close() {
        reOpenAfterClose = false
        ws!.close()
    }
    
    // The application has been restored, rebuild the model.
    public func restoreModel() {
        ws!.event.open = {
            self.configurationModel = ConfigurationModel()
        }
    }
    
    // process the received weather forecast
    public func processWeather(notification: NSNotification) {
        if let forecast = notification.userInfo?["data"] as? NSDictionary {
            let parsedMessage: MessageParser = MessageParser.sharedInstance
            parsedMessage.parseWeather(forecast, model: configurationModel)
            if (configurationModel.WeatherValid) {
                NSNotificationCenter.defaultCenter().postNotificationName("WeatherForecastAvailable", object: self)
            }
        } else {
            configurationModel.WeatherValid = false;
        }
    }
    
    
    // the application is going to be put in the background, disable the socket and timer.
    public func setInactive() {
        Close()
    }
    
    // Ping the server
    public func pingFrame() {
        ws!.send("ping")
    }
    
    public func scheduleNotifications()
    {
        let notifications = getParamList().buildRequestParamList()
        let keys = Array(configurationModel.ControlObjects.keys)
        for key in keys {
            let oName = key
            let objectDict: NSDictionary = configurationModel.ControlObjects[oName]!
            if let objType: String = objectDict.objectForKey("OBJTYP") as? String {
                
                switch objType {
                case "BODY":
                    let notification = KeyObject()
                    notification.objnam = oName
                    notification.keys = ["STATUS", "TEMP", "LOTMP", "HEATER", "HTSRC","LSTTMP"]
                    notifications.objectList.append(notification)
                    break
                case "REMOTE":
                    let notification = KeyObject()
                    notification.objnam = oName
                    notification.keys = ["ENABLE"]
                    notifications.objectList.append(notification)
                    break
                case "HEATER":
                    let notification = KeyObject()
                    notification.objnam = oName
                    notification.keys = ["STATUS", "SUBTYP", "PERMIT", "TIMOUT", "READY", "SHOMNU"]
                    notifications.objectList.append(notification)
                    break
                case "CIRCUIT":
                    let notification = KeyObject()
                    notification.objnam = oName
                    notification.keys = ["STATUS", "MODE", "LISTORD", "USAGE", "ACT", "LIMIT", "USE"]
                    notifications.objectList.append(notification)
                    break
                case "SCHED":
                    let notification = KeyObject()
                    notification.objnam = oName
                    notification.keys = ["OBJTYP","LISTORD","CIRCUIT", "HNAME", "SNAME", "DAY", "SINGLE", "START", "TIME", "STOP", "TIMOUT", "GROUP", "STATUS", "CLK24A", "HEATER", "HITMP", "LOTMP", "VOL", "SMTSRT", "VACFLO", "DNTSTP"]
                    notifications.objectList.append(notification)
                    break
                case "SENSE":
                    let notification = KeyObject()
                    notification.objnam = oName
                    notification.keys = ["OBJTYP","STATUS","PROBE"]
                    notifications.objectList.append(notification)
                    break
                case "PUMP":
                    let notification = KeyObject()
                    notification.objnam = oName
                    notification.keys = ["OBJTYP"]
                    notifications.objectList.append(notification)
                    break
                case "STAMSG":    //STATUS   @ IMPORTANT TO DO
                    let notification = KeyObject()
                    notification.objnam = oName
                    notification.keys = ["OBJTYP"]
                    notifications.objectList.append(notification)
                    break
                case "PMPCIRC":
                    let notification = KeyObject()
                    notification.objnam = oName
                    notification.keys = ["OBJTYP"]
                    notifications.objectList.append(notification)
                    break
                case "CHEM":
                    if let subType = objectDict.objectForKey("SUBTYP") as? String{
                        let notification = KeyObject()
                        notification.objnam = oName
                        
                        if (subType == "ICHLOR") {
                            notification.keys = ["SALT", "PRIM", "SEC", "SUPER", "TIMOUT"]
                        }else if (subType == "ICHEM") {
                            notification.keys = ["PHSET", "ORPSET", "PHTNK", "ORPTNK", "CALC", "CYACID", "ALK", "SALT", "ORPVAL", "PHVAL", "SINDEX", "PHLO", "PHHI", "ORPLO", "ORPHI"]
                        }
                        notifications.objectList.append(notification)
                    }
                    break
                    
                default:
                    NSLog("*** Not processed \(objType)")
                    break
                }
            }
        }
        let system = KeyObject()
        system.objnam = "_5451"
        system.keys = ["OBJTYP", "MODE", "AVAIL","TIMZON","ZIP","TEMPNC","VACFLO","VACTIM","START","STOP"]
        notifications.objectList.append(system)
        
        let adminToken = KeyObject()
        adminToken.objnam = "UFFFE"
        adminToken.keys = ["OBJTYP", "ENABLE"]
        notifications.objectList.append(adminToken)
        
        let clock = KeyObject()
        clock.objnam = "_C10C"
        clock.keys = ["OBJTYP", "DAY", "MIN", "OFFSET", "SNAME", "TIMZON", "SRIS", "SSET", "CLK24A", "LOCX", "LOCY", "ZIP", "DLSTIM"]
        notifications.objectList.append(clock)
        
        let message = KeyObject()
        message.objnam = "_57A7"
        message.keys = ["OBJNAM", "OBJREV", "OBJTYP", "STATIC", "RESET","ACT","BADGE", "PARTY","ALARM","ROYAL"]
        notifications.objectList.append(message)
        print("Notifications list \(notifications.debugDescription)")
        

        sendRequest(notifications)
    }
    
    //@ IMPORTANT Series of Build Requests
    
    public func loadConfiguration()->Future<[String]> {
        let loadSequence = [
            sendRequest(getParamList().buildRequestSystemPreferences()),
            sendRequest(getParamList().buildRequestAir()),
            sendRequest(getParamList().buildRequestFreezeCircuit()),
            sendRequest(getParamList().buildRequestRemotes()),
            sendRequest(getParamList().buildRequestAllPumps()), // does not have effects...
            sendRequest(getParamList().buildRequestAllPumpEffects()),
            sendRequest(getParamList().buildRequestAllUsers()),
            sendRequest(getParamList().buildRequestCircuits()),
            sendRequest(commandQuery().buildGetConfig()),
            // sendRequest(getParamList().buildRequestStatusMaster()),
            sendRequest(commandQuery().buildGetActiveStatusMessages()),
           // sendRequest(commandQuery().buildGetHardwareDefinition())
        ]
        
        return FutureUtils.sequence(loadSequence)
    }
    
    // Web socket opened, build the configuration for the attached THW
    public func webSocketOpen() {
        resetConnectionCounter()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "restoreModel:", name: "SetActiveState", object: self)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "setInactive:", name: "SetInactiveState", object: self)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "setInactive:", name: "SetTerminateState", object: self)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "processWeather:", name: "WeatherForecasts", object: httpService)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "getStatusMessages:", name: "RefreshStatusMessages", object: MessageParser.sharedInstance)
        //        timer = NSTimer.scheduledTimerWithTimeInterval(60 * 1, target: self, selector: Selector("pingFrame"), userInfo: nil, repeats: true)
        loadConfiguration()
            .onSuccess {
                results in
                if let location = self.configurationModel.ControlObjects["_5451"] {
                    if let zip = location["ZIP"] as? String {
                        if !zip.isEmpty {
                            self.httpService?.FetchWeather((location.valueForKey("ZIP") as? String)!)
                        }
                    }
                }
                NSNotificationCenter.defaultCenter().postNotificationName("ConfigurationLoaded", object: self)
            }
            .onFailure{
                error in
                print(error)
        }
    }
    
    // Message received from the THW or Web API, process it.
    private func webSocketMessageReceived(message: Any) {
        print("Message Received from webSocket \(message)")
        if let text = message as? String {
            if (text.containsString("pong")) {
                return;
            }
            
            let parsedMessage: MessageParser = MessageParser.sharedInstance
            NSLog("Received: \(text)")
            parsedMessage.parseMessage(text, model: configurationModel, messageQueue: messageQueue)
            
            //@todo
            if parsedMessage.Type == MessageType.NOTIFY_LIST || parsedMessage.Type == MessageType.WRITE_STATUS_MESSAGE {
                NSNotificationCenter.defaultCenter().postNotificationName("ConfigurationUpdate", object: self)
            }
            
            if let messageId = parsedMessage.Id {
                if let queuedMsg = messageQueue[messageId as String] {
                    if nil != parsedMessage.error {
                        queuedMsg.promise?.failure(NSError(domain: "Pentair", code: -1, userInfo: nil))
                    } else {
                        queuedMsg.promise?.trySuccess(text)
                    }
                }
            }
        }
    }
    
    // Web socket closed
    public func webSocketClosed() {
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "SetInactiveState", object: self)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "SetActiveState", object: self)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "SetTerminateState", object: self)
        NSNotificationCenter.defaultCenter().removeObserver(self, name: "RefreshStatusMessages", object: MessageParser.sharedInstance)
        if (reOpenAfterClose) {
            reConnect()
        }
    }
    
    // Web socket error
    public func webSocketError(error: ErrorType) {
        self.socketError = error;

        reOpenAfterClose = true
    }
    
    public func sendRequest(request: CommandMessage) -> Future<String>{
        request.messageID = NSUUID().UUIDString
        let json = request.toJsonString()
        NSLog("Sent: \(json)")
        return sendMessage(request.messageID, message:json as String)

        }
    
    public func sendStatusMessagesRequest(request: CommandMessage) -> Future<String>{
        request.messageID = NSUUID().UUIDString
        let json = request.toJsonString()
        NSLog("Sent: \(json)")
        return sendMessage(request.messageID, message:json as String)
        
    }
    
//    public func sendRequestAsString(request: RequestStruct) -> Future<String>{
//       // request.messageID = NSUUID().UUIDString
//       // // NSString(data: self.toJson(), encoding: NSUTF8StringEncoding)
//        request.messageID = NSUUID().UUIDString
//        
//        let json = request
//        NSLog("Sent: \(json)")
//        return sendMessage(request.messageID, message:json as String)
//    }

    
    public func sendMessage(messageId: String, message: String) -> Future<String>{
        let promise = Promise<String>()
        ws!.send(message)
        messageQueue[messageId] = MessageQueueItem(p:promise.future, msg:message)
        
        print("Promise.future  \(promise.future)")
        
        return promise.future
    }
    
    public func buildHeatModes(objnam: String) -> PickerData {
        let objectDict: NSDictionary = configurationModel.ConfigurationDictionary[objnam]!
        let objListArray: [NSDictionary] = objectDict.objectForKey("OBJLIST") as! [NSDictionary]
        let heatModes = PickerData()
        heatModes.pickerItems.append(PickerItem(objnam: "00000", name: "Off"))
        var hComboHeater: String?
        var hComboHeaterList = [String : AnyObject]()
        var hGenericHeater: String?
        var hGenericHeaterList = [String : AnyObject]()
        var heatGroupList = [String : AnyObject]()
        
        //@todo here
        for obj in objListArray { // get OBJLIST the configurationResponse using objnam of parent (pool)
            let keys = obj.allKeys
            for key in keys as! [String] {
                let object: NSDictionary = obj.objectForKey(key) as! NSDictionary
                if let objtyp = object.objectForKey("OBJTYP") as? String {
                    let body = object.objectForKey("BODY") as? String
                    let share = object.objectForKey("SHARE") as? String
                    let subtyp = object.objectForKey("SUBTYP") as? String
                    if objtyp == "HEATER" {
                        if body == objnam || share == objnam {
                            if subtyp == "HCOMBO" {
                                hComboHeater = key as String
                                hComboHeaterList[key] = object
                            } else if subtyp == "GENERIC" {
                                hGenericHeater = key as String
                                hGenericHeaterList[key] = object
                            }
                        }
                    } else if objtyp == "HCOMBO" {
                        heatGroupList[key] = object
                    }
                }
            }
        }
        
        if heatGroupList.count > 0 {
            var heaterType: String? = nil
            for key in Array(heatGroupList.keys) {
                let heater: NSDictionary = heatGroupList[key] as! NSDictionary
                if let type = heater.objectForKey("SUBTYP") as? String {
                    if type == "GENERIC" {
                        heatModes.pickerItems.append(PickerItem(objnam: key, name: "Heater"))
                    } else if type == "SOLAR" {
                        heatModes.pickerItems.append(PickerItem(objnam: key, name: "Solar Only"))
                        heaterType = "SOLAR"
                    } else if type == "HTPMP" {
                        heatModes.pickerItems.append(PickerItem(objnam: key, name: "Heat Pump Only"))
                        heaterType = "HTPMP"
                    } else if type == "ULTRA" {
                        heatModes.pickerItems.append(PickerItem(objnam: key, name: "Ultra Only"))
                    }
                }
            }
            
            if nil != hComboHeater && nil != heaterType {
                heatModes.pickerItems.append(PickerItem(objnam: hComboHeater!, name: heaterType == "SOLAR" ? "Solar Preferred" : "Heat Pump Preferred"))
            }
        } else {
            if nil != hGenericHeater {
                heatModes.pickerItems.append(PickerItem(objnam: hGenericHeater!, name: "Heater"))
            }
        }
        return heatModes
    }
    
    public func getUsers(pageSize:Int, page:Int, filter:String, searchTerm:String = "") -> Future<String>     {
        let params = ["PAGESIZE":pageSize.description,"PAGE":page.description,"FILTER":filter,"SEARCHTERM":searchTerm]
        return sendRequest(ParamsMessage().buildMessage(MessageType.GET_USER_LIST.rawValue, params: params)).onSuccess{results in
            NSNotificationCenter.defaultCenter().postNotificationName("UsersListUpdated", object: self)
            }.onFailure{ error in
                 NSNotificationCenter.defaultCenter().postNotificationName("UsersListUpdated", object: self)
                print(error)
        }
    }
    
    public func getUsers() -> Future<String> {
        return sendRequest(ParamsMessage().buildMessage(MessageType.GET_USER_LIST.rawValue , params: [String:String]())).onSuccess{results in
            NSNotificationCenter.defaultCenter().postNotificationName("UsersListUpdated", object: self)
            }.onFailure{ error in
                 NSNotificationCenter.defaultCenter().postNotificationName("UsersListUpdated", object: self)
                print(error)
        }
    }
    
    
    public func inviteUser(email:String, permGroup:String) -> Future<String> {
        let params = ["EMAIL":email,"PERMISSIONGROUP":permGroup]
        //@todo do we currently send the email at this point?
        return sendRequest(ParamsMessage().buildMessage(MessageType.INVITE_USER.rawValue , params: params)).onSuccess{ results in
            
            //@todo
            NSNotificationCenter.defaultCenter().postNotificationName("UserInvited", object: self)
            }.onFailure{ error in
                print(error)
        }
    }
    
    public func deleteUser(email:String, status:String, notify:Bool) -> Future<String> {
        let params:[String : AnyObject] = ["EMAIL":email,"STATUS":status,"NOTIFY":notify]
        return sendRequest(ParamsMessage().buildMessage(MessageType.DELETE_USER.rawValue , params: params)).onSuccess{ results in
            NSNotificationCenter.defaultCenter().postNotificationName("UserModified", object: self)
            }.onFailure{ error in
                NSNotificationCenter.defaultCenter().postNotificationName("UserModified", object: self)
                print(error)
        }
    }
    
    
    public func editUser(email:String, permGroup:String) -> Future<String> {
        let params = ["EMAIL":email,"PERMISSIONGROUP":permGroup]
        return sendRequest(ParamsMessage().buildMessage(MessageType.EDIT_USER.rawValue , params: params)).onSuccess{ results in
            NSNotificationCenter.defaultCenter().postNotificationName("UserModified", object: self)
            }.onFailure{ error in
                NSNotificationCenter.defaultCenter().postNotificationName("UserModified", object: self)
                print(error)
        }
    }
    
    public func getSecurityTokens() -> Future<String>{
        return sendRequest(getParamList().buildRequestSecurityTokens()).onSuccess{results in
            let parsedMessage: MessageParser = MessageParser.sharedInstance
            parsedMessage.parseMessage(results, model: self.configurationModel, messageQueue: self.messageQueue)
           
            NSNotificationCenter.defaultCenter().postNotificationName("ConfigurationUpdate", object: self)
            }.onFailure{ error in
                print(error)
        }
    }
    
    

    public func getStatusMessages(notification: NSNotification){
         getStatusMessages()
    }
    public func getStatusMessages() -> Future<String>{
        return sendRequest(commandQuery().buildGetActiveStatusMessages()).onSuccess{results in
            let parsedMessage: MessageParser = MessageParser.sharedInstance
            
          //  parsedMessage.parseMessage(results, model: self.configurationModel, messageQueue: self.messageQueue)
            NSNotificationCenter.defaultCenter().postNotificationName("ConfigurationUpdate", object: self)
            }.onFailure{ error in
                print(error)
        }
    }

    
    //    public func getRequestDelayDetails(objName:String) -> Future<String>{
    //        return sendRequest(getParamList().buildRequestDelayDetails(objName))
    //    }
    
    // MARK: Helper methods
    func reConnect(){
        if ws!.readyState == WebSocketReadyState.Open {
            print("Error Occured, closing socket manually. \(socketError)")
            ws!.close()
        } else {
            conntectionAttempt += 1
            if (conntectionAttempt <= maxConnectionAttempts){
                let delay = connectionDelay * Double(NSEC_PER_SEC)
                let dispatchTime = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
                
                dispatch_after(dispatchTime, dispatch_get_main_queue(), {
                    print("Reopening web socket. \(self.socketError)")
                    self.connectionDelay = 0.05 * Double(self.conntectionAttempt)
                    self.ws!.open(request: self.url!)
                })
            }else{
                print("Max Reconnection attempts Reached . \(socketError)")
            }
        }
    }
    
    func resetConnectionCounter(){
        connectionDelay = 0
        conntectionAttempt = 0
        socketError = nil
    }
    
    func objectNameforObjectType(objType: String!, objectName: String) -> [String]? {
        let object: [String: AnyObject]? = self.configurationModel.ConfigurationDictionary[objectName] as! [String: AnyObject]?
        let objectList: [NSDictionary] = object!["OBJLIST"] as! [NSDictionary]
        var results: [String]? = nil
        
        //let keys = Array(objectList.keys)
        for dict in objectList {
            let keys = dict.allKeys as! [String]
            for key in keys {
                let values: NSDictionary = dict.objectForKey(key) as! NSDictionary
                let type: String = values.objectForKey("OBJTYP") as! String
                if type == objType {
                    if results == nil {
                        results = [String]()
                    }
                    results?.append(key)
                }
            }
        }
        
        return results
    }
}



// Helper classes

public class MessageQueueItem {
    public var promise:Future<String>?
    public var time:NSDate
    public var message:String?
    
    public init(){time = NSDate()}
    
    public init(p:Future<String>, msg:String){
        self.promise = p
        self.time = NSDate()
        self.message = msg
    }
    
}



