//
//  Service.swift
//  Pentair-iOS
//
//  Created by Jim Hewitt on 12/12/14.
//  Copyright (c) 2014 Pentair Corporation. All rights reserved.
//

import UIKit
import Utility
import AFNetwork

public enum InstallationConnectionStatus : String {
    case All = "all", Connected = "Connected", NotConnected = "Off-line"
    
    public static let allValues = [All,Connected,NotConnected]
    public static let allStrings = ["All","Connected","Not Connected"]
    
    public static func getIndex(find:InstallationConnectionStatus) -> Int?{
        for (index, value) in allValues.enumerate() {
            if (value == find){
                return index
            }
        }
        return nil
    }
    
}

public var baseConnection: String = ""

public let AUTH_SERVICE = AuthService()

public class HttpService: AFURLConnectionOperation {
    public var userName: String? = nil
    public var passcode: String? = nil
    
    
    // MARK: - Login methods
    
    
    public func isUserLoggedIn() -> Bool {
        return AUTH_SERVICE.loggedIn()
    }
    
    public func isLoginInfoAvailable() -> [String : AnyObject] {
        var status: [String : AnyObject] = [String : AnyObject]()
        
        status["status"] = false
        
        if let passcode = AUTH_SERVICE.getPassword() {
            if let userName = AUTH_SERVICE.getUserName() {
                status["passcode"] = passcode
                status["userName"] = userName
                status["status"] = true
            }
        }
        
        return status
    }
    
    // Request a token from the API
    public func login(email: String, password: String,saveLoginInfo:Bool) {
        let parameters = ["username":email,"password":password,"grant_type": "password"]
        self.userName = email
        self.passcode = password

        AUTH_SERVICE.logOut()
        
        let manager = AFHTTPRequestOperationManager()
        manager.securityPolicy.allowInvalidCertificates = true
        manager.POST( baseConnection + "login", parameters: parameters,
            success: {
                (operation: AFHTTPRequestOperation!, responseObject: AnyObject!) in
                if let rsp = responseObject as? Dictionary<String, AnyObject> {
                    if let auth = rsp["access_token"] as? String {AUTH_SERVICE.authToken = auth}
                    if let t = rsp["token_type"] as? String! {AUTH_SERVICE.type = t}
                    if let e = rsp["expires_in"] as? Int {AUTH_SERVICE.expiration = e}
                    NSNotificationCenter.defaultCenter().postNotificationName("HttpLogin", object: self)
                    AUTH_SERVICE.userName = email
                    
                    //SaveLogin
                    if saveLoginInfo {
                        AUTH_SERVICE.saveLogin(self.userName!, Password: self.passcode!)
                        self.userName = nil
                        self.passcode = nil
                    }
                }
            },
            failure: {
                (operation: AFHTTPRequestOperation!, error: NSError!) in
                NSNotificationCenter.defaultCenter().postNotificationName("HttpLogin", object: self)
        })
    }
    
    public func logOut() {
        AUTH_SERVICE.logOut()
    }
    
    // MARK: - Authorization methods
    
    public func authToken() -> String? {
        return AUTH_SERVICE.authToken
    }
    
    public func authorizedUserName() -> String? {
        return AUTH_SERVICE.userName
    }
    
    // MARK: - Installation Detail methods
    
    
    public func getInstallations(pageSize:Int, page:Int, filter:InstallationConnectionStatus, searchTerm:String = ""){
        var parameters: [String:String] = ["pageSize":pageSize.description,"page":page.description,"filter":filter.rawValue]
        
        if (!searchTerm.isEmpty){
            parameters["term"] = searchTerm
        }
        
        let manager = AFHTTPRequestOperationManager()
        manager.securityPolicy.allowInvalidCertificates = true
        manager.requestSerializer.setValue(AUTH_SERVICE.tokenPayload() as String, forHTTPHeaderField: "Authorization")
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Accept")
        
        manager.GET( baseConnection + "api/installations", parameters: parameters,
            success: {
                (operation: AFHTTPRequestOperation!, responseObject: AnyObject!) in
                if let data = responseObject as? Array<AnyObject> {
                    let installationData:[String:NSObject]? = ["installations":data]
                    NSNotificationCenter.defaultCenter().postNotificationName("InstallationsListUpdated", object: self, userInfo: installationData)
                }
            },
            failure: {
                (operation: AFHTTPRequestOperation!, error: NSError!) in
                let errorDict:[String:NSObject]?
                
                if let operationResponse = operation.response {
                    errorDict = ["statusCode" : operationResponse.statusCode, "error" : error]
                }else {
                    errorDict = ["error" : error]
                }
                NSNotificationCenter.defaultCenter().postNotificationName("InstallationsListUpdateFailed", object: self, userInfo: errorDict)
        })
    }
    
    public func FetchWeather(zip: String) {
        let parameters: [String:String] = ["zip":zip]
        let manager = AFHTTPRequestOperationManager()
        manager.securityPolicy.allowInvalidCertificates = true
        manager.requestSerializer.setValue(AUTH_SERVICE.tokenPayload() as String, forHTTPHeaderField: "Authorization")
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Accept")
        manager.GET( baseConnection + "api/WeatherForecast", parameters: parameters,
            success: {
                (operation: AFHTTPRequestOperation!, responseObject: AnyObject!) in
                let data = responseObject as? NSObject
                let forecasts:[String: NSObject]? = ["data":nil == data ? "" : data!]
                NSNotificationCenter.defaultCenter().postNotificationName("WeatherForecasts", object: self, userInfo: forecasts)
            },
            failure: {
                (operation: AFHTTPRequestOperation!, error: NSError!) in
                
        })
    }
    
    // Request property list
    public func getInstallations() {
        getInstallations(20, page: 1, filter: InstallationConnectionStatus.All)
    }
    
    public func removeInstallationFromAccount(installationId:Int){
        let parameters: [String:String] = ["PropertyId":installationId.description]
        
        let manager = AFHTTPRequestOperationManager()
        manager.securityPolicy.allowInvalidCertificates = true
        manager.requestSerializer.setValue(AUTH_SERVICE.tokenPayload(), forHTTPHeaderField: "Authorization")
        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Accept")
        
        manager.POST(baseConnection + "api/Account/RemoveInstallation", parameters: parameters,
            success: {
                (operation: AFHTTPRequestOperation!, responseObject: AnyObject!) in
                NSNotificationCenter.defaultCenter().postNotificationName("InstallationRemoved", object: self)
            },
            failure: {
                (operation: AFHTTPRequestOperation!, error: NSError!) in
                NSNotificationCenter.defaultCenter().postNotificationName("InstallationRemoveFailed", object: self)
        })
    }
}
