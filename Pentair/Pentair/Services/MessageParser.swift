//
//  Messaging.swift
//  Pentair-iOS
//
//  Created by Jim Hewitt on 12/17/14.
//  Copyright (c) 2014 Pentair Corporation. All rights reserved.
//

import Foundation
import Utility

extension CollectionType {
    func toDictionary<K, V>
        (transform:(element: Self.Generator.Element) -> (key: K, value: V)?) -> [K : V] {
        var dictionary = [K : V]()
        for e in self {
            if let (key, value) = transform(element: e) {
                dictionary[key] = value
            }
        }
        return dictionary
    }
}

public class MessageParser: NSObject {
    static let sharedInstance = MessageParser()
    
    public var Id: NSString?
    public var Type = MessageType.UNKNOWN
    public var error: NSError?
    public var messagesArray : Array<String>!
    
  
    public func parseMessage(message: AnyObject, model: ConfigurationModel, messageQueue: [String : MessageQueueItem]) {
        
        let jsonData = message.dataUsingEncoding(NSUTF8StringEncoding)
        do {
            if let jsonObject: [String: AnyObject]! = try NSJSONSerialization.JSONObjectWithData(jsonData!, options: NSJSONReadingOptions.MutableContainers) as? [String: AnyObject] {
                let response: String? = jsonObject!["response"] as? String
                
                print("Response is:   \(response)")
                
                let messageId: String? = jsonObject!["messageID"] as? String
                self.Id = messageId
                
                if let messageType = jsonObject!["command"] as? String {
                    switch messageType.uppercaseString {
                        
                    case "SENDPARAMLIST":
                        Type = .SEND_PARAM_LIST
                        self.parseSendParamList(jsonObject, model: model, response: response)
                        break
                        
                    case "SENDQUERY":
                        if let queueItem: MessageQueueItem = messageQueue[messageId!]! as MessageQueueItem {
                            if let queryType = parseQueryType(queueItem) {
                                switch queryType {
                                case "GETCONFIGURATION":
                                    Type = .CONFIGURATION
                                    self.parseSendQuery(jsonObject, model: model, response: response)
                                    break
                                case "GETACTIVESTATUSMESSAGES":
                                    print("Made it into Message Parser")
                                    Type = .GET_ACTIVE_STATUS_MESSAGES
                                    self.parseStatusMessages(jsonObject, model: model, response: response)
                                    break
                                default:
                                    break
                                }
                            }
                        }
                        break
                        
                    case "NOTIFYLIST":
                        Type = .NOTIFY_LIST
                        self.parseNotifyList(jsonObject, model: model, response: response)
                        break
                        
                    case "APIRESPONSE":
                        Type = .API_RESPONSE
                        self.parseUserList(jsonObject, model:model, response: response)
                        break
                    default:
                        break
                    }
                }
            } else {
                print("Parsing error: \(error)")
            }
        } catch {
            print("Error")
        }
    }
    
    public func parseWeather(message: NSDictionary, model: ConfigurationModel) {
        var response: AnyObject?
        model.WeatherValid = false
        model.Weather = [String : NSDictionary]()
        
        for (Key, Value) in message {
            if let item = Key as? NSString {
                switch item.uppercaseString {
                case "RESPONSE":
                    response = Value
                    break
                case "SUCCESS":
                    model.WeatherValid = (Value as? Bool)!
                default:
                    break
                }
            }
        }
        if (model.WeatherValid) {
            if let data = response as? [NSDictionary] {
                if let forecasts = data[0]["periods"] as! [NSDictionary]? {
                    for (index, element) in forecasts.enumerate() {
                        model.Weather[index.description] = element
                    }
                }
                
            }
        }
    }
    

    
    // MARK: - Parse SENDQUERY
    
    func parseQueryType(request: MessageQueueItem) -> String? {
        let jsonData = request.message!.dataUsingEncoding(NSUTF8StringEncoding)
        var queryType:String?
        do {
            if let jsonObject: [String: AnyObject]! = try NSJSONSerialization.JSONObjectWithData(jsonData!, options: NSJSONReadingOptions.MutableContainers) as? [String: AnyObject] {
                queryType = jsonObject!["queryName"] as? String
            }
        } catch {
            
        }
        return queryType
    }
    
    func parseSendQuery(jsonObject: [String: AnyObject]!, model: ConfigurationModel!, response: String!) {
        if response == "200" {
            
            model.ConfigurationHeirarchy = jsonObject!["answer"] as! [AnyObject]
            for dict: NSDictionary in model.ConfigurationHeirarchy as! [NSDictionary] {
                let params: NSMutableDictionary = dict["params"] as! NSMutableDictionary
                
                model.ConfigurationDictionary[dict["objnam"] as! String] = params
                
                parseObject(dict as! [String : AnyObject], model: model)
            }
            model.ConfigurationDictionary["_A135"] = defaultAirObject()
            
        } else {
            print("Invalid response \(response)")
        }
    }
    
    
    func defaultAirObject() -> NSDictionary {
        let dict: NSMutableDictionary = NSMutableDictionary()
        dict.setObject("SENSE", forKey: "OBJTYP")
        dict.setObject("AIR", forKey: "SUBTYP")
        dict.setObject("All Lights", forKey: "SNAME")
        dict.setObject("<null>", forKey: "PROBE")
        dict.setObject("<null>", forKey: "STATUS")
        
        return dict
    }
    
    // MARK: - Parse SENDPARAMLIST
    
    func parseSendParamList(jsonObject: [String: AnyObject]!, model: ConfigurationModel!, response: String?) {
        if response != nil {
            if response == "200" {
                parseNotifyList(jsonObject, model: model, response: response)
            }
        }
    }
    
    // IMPORTANT: TODO
    // MARK: - Parse NOTIFYLIST
    
    func parseNotifyList(jsonObject: [String: AnyObject]!, model: ConfigurationModel, response: String?) {
        if response != nil {
            if (response != "200") {
                print("Invalid command type")
                return
            }
        }
        let objectList: [AnyObject]! = jsonObject!["objectList"] as! [AnyObject]
        
        for jsonObject: NSDictionary in objectList as! [NSDictionary] {
            parseObject(jsonObject as! [String : AnyObject], model:model)
        }
    }
    
    func parseUserList(jsonObject: [String: AnyObject]!,model: ConfigurationModel!, response: String?) {
        if response != nil {
            if (response != "200") {
                print("Invalid command type")
                return
            }
        }
        
        model.TotalUsers = 0
        model.Users.removeAll()
        
        if let totalUsers = jsonObject!["totalCount"] as? Int
        {
            model.TotalUsers = totalUsers
        }
        
        if let objectList: [NSDictionary] = jsonObject!["objectList"] as? [NSDictionary]{
        
        for jsonObject: NSDictionary in objectList{
            if let params: NSMutableDictionary = jsonObject["params"] as? NSMutableDictionary{
               model.Users.append(params)

            }

        }
    }
}

    func parseStatusMessages(jsonObject: [String: AnyObject]!,model: ConfigurationModel!, response: String?) {
        if response != nil {
            if (response != "200") {
                print("Invalid command type")
                return
            }
        }

        if let objectList: [NSDictionary] = jsonObject!["answer"] as? [NSDictionary]{
            model.StatusMessages.removeAll()
            for answer: NSDictionary in objectList{
                if let params: NSMutableDictionary = answer["params"] as? NSMutableDictionary{
                   
                    params["OBJNAM"] = answer["objnam"]
                    model.StatusMessages.append(params)
                }
            }
        }
    }

 
    func toDictionary<E, K, V>(
        array:       [E],
        transformer: (element: E) -> (key: K, value: V)?)
        -> Dictionary<K, V>
    {
        return array.reduce([:]) {
            (var dict, e) in
            if let (key, value) = transformer(element: e)
            {
                dict[key] = value
            }
            return dict
        }
    }
    
//    private func convertArrayToDictionary(testArray:NSArray) -> NSDictionary{
//        reduce(enumerate(testArray), [String:NSArray]()) { (var dict, enumeration) in
//            dict["\(enumeration.index)"] = enumeration.element as! NSArray
//            return dict
//        }
//    }
    
    private func parseObject (jsonObject: [String: AnyObject]!, model: ConfigurationModel!){
        let objectName: String = jsonObject["objnam"] as! String
        print("objectName is \(objectName)")
        
        let params: NSMutableDictionary = jsonObject["params"] as! NSMutableDictionary
        
        var controlObject = model.ControlObjects[objectName]
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if objectName == "_57A7"{
            print("It was _57A7")
           if let val = params["PARTY"] {
            if(controlObject == nil || controlObject?["PARTY"] as? String != params["PARTY"] as? String){
                print("It was Party")
                NSNotificationCenter.defaultCenter().postNotificationName("RefreshStatusMessages", object: self, userInfo: nil)
                }
             }
        }
    
       
        if (controlObject == nil) {
            model.ControlObjects[objectName] = NSMutableDictionary()
            controlObject = model.ControlObjects[objectName]
        }
        for (paramsKey, paramsValue) in params {     // one param with list of values
            controlObject!.setObject(paramsValue, forKey: paramsKey as! String)
        }
        if let objectList: [AnyObject] = controlObject!["OBJLIST"] as? [AnyObject]{
            for childObject: NSDictionary in objectList as! [NSDictionary] {
                parseObject(childObject as! [String : AnyObject], model:model)
            }
        }
    }
    
    
    private func captureStatusMessages(statusString:String){
        
    }
}