//
//  PureOperation.swift
//  CVTest
//
//  Created by Dietrich Kappe on 7/16/14.
//  Copyright (c) 2014 Dietrich Kappe. All rights reserved.
//

// Cool idea, but this doesn't seem to be used anywhere -- double check on it

import Foundation

let _PureQSharedInstance = PureOperation.PureQ()

protocol PureTask {
    var generation : Int { get set }
    func cancel()
}

class PureOperation : NSOperation, PureTask {
    
    
    class PureQ {
        var _mainQ : NSOperationQueue
        var _opQ : NSOperationQueue
        var _generation = 0
        
        init() {
            _mainQ = NSOperationQueue.mainQueue()
            _opQ = NSOperationQueue()
            _opQ.maxConcurrentOperationCount = 1
        }
        
        class var sharedInstance : PureQ {
            return _PureQSharedInstance
        }
        
        func incrementGeneration() {
            _generation++
        }
        
        func cancelOpQ() {
            for op in _opQ.operations {
                if op is PureOperation {
                    op.cancel()
                }
                if op is PureBlock {
                    op.cancel()
                }
            }
        }
        
        func cancelMainQ() {
            for op in _mainQ.operations {
                if op is PureOperation {
                    op.cancel()
                }
                if op is PureBlock {
                    op.cancel()
                }
            }
        }
        
        func cancelAllQ() {
            cancelOpQ()
            cancelMainQ()
        }
        
        func cancelOldOpQ() {
            for op in _opQ.operations {
                let pt: PureTask = op as! PureTask
                
                if pt is PureOperation {
                    if pt.generation < _generation {
                        pt.cancel()
                    }
                }
                
                if pt is PureBlock {
                    if pt.generation < _generation {
                        pt.cancel()
                    }
                }
            }
        }
        
        func cancelOldMainQ() {
            for op in _mainQ.operations {
                let pt: PureTask = op as! PureTask
                if pt is PureOperation {
                    if pt.generation < _generation {
                        pt.cancel()
                    }
                }
                if pt is PureBlock {
                    if pt.generation < _generation {
                        pt.cancel()
                    }
                }
            }
        }
        
        func cancelOldAllQ() {
            cancelOldOpQ()
            cancelOldMainQ()
        }
        
        func scheduleBlock(block : () -> ()) {
            _opQ.addOperationWithBlock(block)
        }
        
        func scheduleBlockOnMain(block : () -> ()) {
            _mainQ.addOperationWithBlock(block)
        }
        
        func setGeneration(obj : AnyObject) {
            if let task = obj as? PureBlock {
                task.generation = _generation
            } else if let task2 = obj as? PureOperation {
                task2.generation = _generation
            }
        }
        
        func scheduleOp(op : NSOperation) {
            setGeneration(op)
            _opQ.addOperation(op)
        }
        
        func scheduleOpOnMain(op : NSOperation) {
            setGeneration(op)
            _mainQ.addOperation(op)
        }
    }
    
    class PureBlock : NSOperation, PureTask {
        var _block : SimpleBlock
        
        var generation : Int
        
        init(block : SimpleBlock) {
            _block = block
            generation = 0
        }
        
        override func main() {
            self._block()
        }
    }
    
    typealias Hash = Dictionary<String, Any>
    typealias WorkConsumer = (Hash) -> Void
    typealias WorkProducer = (PureOperation, Hash) -> (Hash)
    typealias SimpleBlock = () -> ()
    
    var generation : Int
    var _operation : WorkProducer
    var _update : WorkConsumer?
    var _finisher : WorkConsumer
    var _error : WorkConsumer
    var _Q : PureQ
    var _payload : Hash?
    
    var _wasError = false
    
    init(operation: WorkProducer, finisher : WorkConsumer, error : WorkConsumer, update : WorkConsumer?) {
        _operation = operation
        _finisher = finisher
        _error = error
        _update = update
        //assert(_operation != nil)
        //assert(_finisher != nil)
        //assert(_error != nil)
        _Q = PureQ.sharedInstance
        generation = 0
    }
    
    func doTask(payload : Hash) {
        _payload = payload
        assert(_payload != nil)

        _Q._opQ.addOperation(self)
    }
    
    override func main() {
        let result : Hash = _operation(self, _payload!)
        if (!_wasError) {
            let finishBlock : PureBlock = PureBlock(block: {
                self._finisher(result)
            })
            _Q.scheduleOpOnMain(finishBlock)
        }
    }
    
    func clearQ() {
        _Q.cancelAllQ()
    }
    
    func clearOldQ() {
        _Q.cancelOldOpQ()
    }
    
    func clearOldMainQ() {
        _Q.cancelOldMainQ()
    }
    
    func clearOldOpQ() {
        _Q.cancelOldOpQ()
    }
    
    func nextGeneration() {
        _Q.incrementGeneration()
    }
    
    func reportError(payload : Hash) {
        _wasError = true
        
        let errorBlock : PureBlock = PureBlock(block: {
            self._error(payload)
        })
        _Q.scheduleOpOnMain(errorBlock)

    }
    
    func doUpdate(payload : Hash) {
        if ((_update != nil) && !self.cancelled) {
            let updateBlock : PureBlock = PureBlock(block: {
                self._update!(payload)
            })
            _Q.scheduleOpOnMain(updateBlock)
        }
    }
}