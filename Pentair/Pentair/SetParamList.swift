//
//  SetParamList.swift
//  Pentair
//
//  Created by Jim Hewitt on 10/25/15.
//  Copyright © 2015 Path Finder. All rights reserved.
//

import Foundation

public class setParamList: KeyValueMessage {
    override public init() { super.init()}
    
    public func buildSetParameters(objects: [KeyValue]) -> KeyValueMessage {
        self.command = MessageType.SET_PARAM_LIST.rawValue
        self.objectList = objects
        return self
    }
}
