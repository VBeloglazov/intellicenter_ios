//
//  StringExtentions.swift
//  Pentair
//
//  Created by Dashon Howard on 3/31/16.
//  Copyright © 2016 Path Finder. All rights reserved.
//

import Foundation

extension String {
    func toDouble() -> Double? {
        return (self as NSString).doubleValue
    }
    func toFloat() -> Float? {
        return (self as NSString).floatValue
    }
    func toInt() -> Int? {
        return (self as NSString).integerValue
    }
}
