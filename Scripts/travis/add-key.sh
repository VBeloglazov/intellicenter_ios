#!/bin/sh
KEY_CHAIN=ios-build.keychain
security create-keychain -p travis $KEY_CHAIN
security default-keychain -s $KEY_CHAIN
security unlock-keychain -p travis $KEY_CHAIN
security set-keychain-settings -t 3600 -u $KEY_CHAIN

echo "***Adding certs for archive"

echo $APPLE_CER | base64 -D > ./Scripts/travis/apple.cer
echo $DIST_CER | base64 -D > ./Scripts/travis/dist.cer
echo $CERT_P12 | base64 -D > ./Scripts/travis/dist.p12

security import ./scripts/travis/apple.cer -k ~/Library/Keychains/ios-build.keychain -T /usr/bin/codesign
security import ./scripts/travis/dist.cer -k ~/Library/Keychains/ios-build.keychain -T /usr/bin/codesign
security import ./scripts/travis/dist.p12 -k ~/Library/Keychains/ios-build.keychain -P $KEY_PASSWORD -T /usr/bin/codesign
mkdir -p ~/Library/MobileDevice/Provisioning\ Profiles
cp ./Scripts/travis/profile/* ~/Library/MobileDevice/Provisioning\ Profiles/