#!/bin/sh

echo "Upload to hockeyapp"
if [[ "$TRAVIS_PULL_REQUEST" != "false" ]]; then
  echo "This is a pull request. No deployment will be done."
  exit 0
fi
if [[ "$TRAVIS_BRANCH" != "release" ]]; then
  echo "Testing on a branch other than release. No deployment will be done."
  exit 0
fi

# Thanks @djacobs https://gist.github.com/djacobs/2411095

PROVISIONING_PROFILE="$HOME/Library/MobileDevice/Provisioning Profiles/$PROFILE_UUID.mobileprovision"
RELEASE_DATE=`date '+%Y-%m-%d %H:%M:%S'`
OUTPUTDIR="$PWD/build/Release-iphoneos"

echo "********************"
echo "*     Signing      *"
echo "********************"
xcrun -log -sdk iphoneos PackageApplication "$OUTPUTDIR/$APPNAME.app" -o "$OUTPUTDIR/$APPNAME.ipa" -sign "$DEVELOPER_NAME" -embed "$PROVISIONING_PROFILE"

RELEASE_NOTES="Build: $TRAVIS_BUILD_NUMBER\nUploaded: $RELEASE_DATE"

zip -r -9 "$OUTPUTDIR/$APPNAME.app.dSYM.zip" "$OUTPUTDIR/$APPNAME.app.dSYM"

echo "********************"
echo "*    Uploading     *"
echo "********************"
#curl http://testflightapp.com/api/builds.json \
#  -F file="@$OUTPUTDIR/$APPNAME.ipa" \
#  -F dsym="@$OUTPUTDIR/$APPNAME.app.dSYM.zip" \
#  -F api_token="$API_TOKEN" \
#  -F team_token="$TEAM_TOKEN" \
#  -F distribution_lists='Internal' \
#  -F notes="$RELEASE_NOTES" -v

curl \
  -F status="1" \
  -F notify="0" \
  -F notes="$RELEASE_NOTES" \
  -F notes_type="0" \
  -F ipa="@$OUTPUTDIR/$APPNAME.ipa" \
  -F dsym="@$OUTPUTDIR/$APPNAME.app.dSYM.zip" \
  -H "X-HockeyAppToken: $HOCKEY_TOKEN" \
  -v \
  https://rink.hockeyapp.net/api/2/apps/1234567890abcdef1234567890abcdef/app_versions/upload