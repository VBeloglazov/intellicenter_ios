//
//  AppPreferences.swift
//  ChildPanePreference
//
//  Created by Gene Backlin on 3/18/15.
//  Copyright (c) 2015 Gene Backlin. All rights reserved.
//

import UIKit
import Foundation

public class AppPreferences: NSObject {
    var keys: [String]?
    var preferences: NSDictionary?
    
    public convenience init(preferenceKeys: [String]?) {
        self.init()
        keys = preferenceKeys
        preferences = self.getPreferencesWithKeyArray(keys)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "defaultsChanged:", name: NSUserDefaultsDidChangeNotification, object: nil)
        /*
        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
        [center addObserver:self
        selector:@selector(defaultsChanged:)
        name:NSUserDefaultsDidChangeNotification
        object:nil];
        */

    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    public func getPreferencesWithKeyArray(keys: [String]!) -> NSDictionary? {
        let settings: NSDictionary? = self.bundleSettings()
        let settingKeys = settings?.allKeys as! [String]
        var localDefaults: [String : AnyObject]! = [String : AnyObject]()
        
        for prefItem in settingKeys {
            let tempDict = settings?.objectForKey(prefItem) as! NSDictionary
            var defaultValue: AnyObject?
            
            if tempDict.objectForKey("Type") as! String == "PSMultiValueSpecifier" {
                defaultValue = NSUserDefaults.standardUserDefaults().objectForKey(prefItem)
                if defaultValue == nil {
                    defaultValue = tempDict.objectForKey("DefaultValue")
                }
                NSLog("defaultValue: \(defaultValue!)")
            } else {
                defaultValue = tempDict.objectForKey("DefaultValue")
            }
            localDefaults[prefItem] = tempDict
        }
        NSUserDefaults.standardUserDefaults().registerDefaults(localDefaults! as [String : AnyObject])
        NSUserDefaults.standardUserDefaults().synchronize()
        
        return localDefaults
    }
    
    func bundleSettings() -> NSDictionary? {
        let pathStr: NSString! = NSBundle.mainBundle().bundlePath
        let settingsBundlePath: NSString = pathStr.stringByAppendingPathComponent("Settings.bundle")
        let finalPath: NSString! = settingsBundlePath.stringByAppendingPathComponent("Root.plist")
        let plistDict: NSMutableDictionary? = NSMutableDictionary()
        
        let settingsDict = NSDictionary(contentsOfFile: finalPath! as String)
        let array: NSMutableArray? = settingsDict?.objectForKey("PreferenceSpecifiers") as? NSMutableArray
        
        for tempDict in array! {
            if (tempDict.objectForKey("Key") != nil) {
                let typeValueStr: String? = tempDict.objectForKey("Type") as? String
                if typeValueStr == "PSChildPaneSpecifier" {
                    let keyValue: String? = tempDict.objectForKey("Key") as? String
                    let keyValueStr: String? = "\(keyValue!).plist"
                    let plist: String? = settingsBundlePath.stringByAppendingPathComponent(keyValueStr!)
                    let childSettings = NSDictionary(contentsOfFile: plist!)
                    let childArray: NSArray? = childSettings?.objectForKey("PreferenceSpecifiers") as? NSArray
                    
                    if childArray != nil {
                        for tempChildDict in childArray! {
                            array?.addObject(tempChildDict)
                            if (tempChildDict.objectForKey("Key") != nil) {
                                plistDict?.setObject(tempChildDict, forKey: tempChildDict.objectForKey("Key") as! String)
                            }
                        }
                    }
                } else {
                    plistDict?.setObject(tempDict, forKey: tempDict.objectForKey("Key") as! String)
                }
            }
        }
        
        return plistDict;
    }
    
    override public func valueForKey(key: String) -> AnyObject? {
        var keyValue: String? = NSUserDefaults.standardUserDefaults().stringForKey(key)
        let tempDict: NSDictionary? = self.preferences?.objectForKey(key) as? NSDictionary
        
        if tempDict?.objectForKey("Type") as? String! == "PSMultiValueSpecifier" {
            if keyValue == nil {
                keyValue = tempDict?.objectForKey("DefaultValue") as? String
            }
        } else if keyValue == nil {
            let type = tempDict?.objectForKey("Type") as! NSString
            if type == "PSToggleSwitchSpecifier" {
                let isTrue: Bool = Bool(tempDict?.objectForKey("DefaultValue") as! Int)
                keyValue = isTrue ? "1" : "0"
            } else if type == "PSTextFieldSpecifier" {
                keyValue = tempDict?.objectForKey("DefaultValue") as? String
            } else {
                keyValue = tempDict?.objectForKey("DefaultValue") as? String
            }
        }
        
        return keyValue
    }
    
    func defaultsChanged(notification: NSNotification) {
        NSUserDefaults.standardUserDefaults().synchronize()
        let defaults: NSUserDefaults = notification.object as! NSUserDefaults
        defaults.synchronize()
    }
    
    public class func setBuildVersion() {
        let appInfo = NSBundle.mainBundle().infoDictionary
        let shortVersionString = appInfo!["CFBundleShortVersionString"] as! String
        let bundleVersion      = appInfo!["CFBundleVersion"] as! String
        let versionString = shortVersionString + "." + bundleVersion
        
        let date = NSDate()
        let formattedDate = NSDateFormatter.localizedStringFromDate(date, dateStyle: .ShortStyle, timeStyle: .ShortStyle)
        let buildDateString = "\(formattedDate)"
        
        let timeInterval = Int(date.timeIntervalSince1970)
        let hexTimeInterval = String(timeInterval, radix: 16)
        let valhex = hexTimeInterval.substringFromIndex((hexTimeInterval.characters.indices.minElement()!).advancedBy(3)).uppercaseString
        
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(versionString, forKey: "application_version")
        defaults.setObject(buildDateString, forKey: "application_build_date")
        defaults.setObject(valhex, forKey: "application_build_number")
        defaults.synchronize()
    }
}
