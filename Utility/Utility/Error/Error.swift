//
//  Error.swift
//  Utility
//
//  Created by Path Finder on 5/14/15.
//  Copyright (c) 2015 PathFinder Software. All rights reserved.
//

import UIKit

public class Error: NSObject {
   
    public class func createError(key: String!, reason: String!, suggestion: String!, code: Int) -> NSError {
        let userInfo: [String : String] = [NSLocalizedDescriptionKey: key,
            NSLocalizedFailureReasonErrorKey : reason,
            NSLocalizedRecoverySuggestionErrorKey : suggestion]
        
        return  NSError(domain: NSPOSIXErrorDomain, code: code, userInfo: userInfo)
    }

    public class func createError(key: String!, reason: String!, suggestion: String!, underlyingError: NSError?, code: Int) -> NSError {
        var error: NSError?
        let userInfo: [String : AnyObject] = [NSLocalizedDescriptionKey: key,
            NSLocalizedFailureReasonErrorKey : reason,
            NSLocalizedRecoverySuggestionErrorKey : suggestion,
            NSUnderlyingErrorKey: underlyingError!]
        
        error = NSError(domain: NSPOSIXErrorDomain, code: code, userInfo: userInfo)
        
        return error!
    }
    

}

