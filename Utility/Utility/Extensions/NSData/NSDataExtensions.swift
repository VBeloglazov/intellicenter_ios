//
//  NSDataExtensions.swift
//  Utility
//
//  Created by Path Finder on 5/21/15.
//  Copyright (c) 2015 PathFinder Software. All rights reserved.
//

import UIKit

extension NSData {
    // Return hexadecimal string representation of NSData bytes
    // Usage: let decodedData = data?.hexadecimalString
    
    @objc(kdj_hexadecimalString)
    public var hexadecimalString: NSString {
        var bytes = [UInt8](count: length, repeatedValue: 0)
        var counter = 0
        getBytes(&bytes, length: length)
        
        let hexString = NSMutableString()
        for byte in bytes {
            if counter < 2 {
                switch counter {
                case 0:
                    NSLog("0 byte = \(byte)") // 30 - N
                    break
                    
                case 1:
                    NSLog("1 byte = \(byte)") // 39 - W
                    break
                    
                default:
                    break
                }
            } else {
                hexString.appendFormat("%02x", UInt(byte))
            }
            counter++
        }
        
        return NSString(string: hexString)
    }
}
