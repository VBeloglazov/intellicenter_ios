//
//  StringExtensions.swift
//  Utility
//
//  Created by Path Finder on 5/22/15.
//  Copyright (c) 2015 PathFinder Software. All rights reserved.
//

import UIKit

extension String {
    
    public func padding(fieldLength: Int) -> String {
        var formatedString: String = ""
        formatedString += self
        
        for _ in 1...(fieldLength - self.characters.count) {
            formatedString += " "
        }
        
        return formatedString
    }
    
    public func padding(fieldLength: Int, paddingChar: String) -> String {
        var formatedString: String = ""
        formatedString += self
        
        for _ in 1...(fieldLength - self.characters.count) {
            formatedString += paddingChar
        }
        
        return formatedString
    }
}
