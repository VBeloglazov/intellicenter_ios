//
//  JsonSerializer.swift
//  Pentair-iOS
//
//  Created by Dashon Howard on 12/22/14.
//  Copyright (c) 2014 Pentair Corporation. All rights reserved.
//
import Foundation

extension Array {
    func toDictionary() -> [NSDictionary!] {
        var dictionaryArray=[NSDictionary]()
        for item in self{
            if item is Serializable{
                let sItem = item as? Serializable
                dictionaryArray.append(sItem!.toDictionary())
            }
        }
        return dictionaryArray
    }
}


public class Serializable : NSObject{
    public func toJson() -> NSData! {
        do {
            return try NSJSONSerialization.dataWithJSONObject(self.toDictionary(), options:NSJSONWritingOptions(rawValue: 0))
        } catch _ {
            return nil
        }
    }
    
    public func toJsonString() -> NSString! {
        return NSString(data: self.toJson(), encoding: NSUTF8StringEncoding)
    }
    
    public func toDictionary() -> NSDictionary {
        let propertiesDictionary = NSMutableDictionary()
        
        for name in getNames() {
            let propValue : AnyObject! = self.valueForKey(name);
            
            if propValue is Serializable {
                propertiesDictionary.setValue((propValue as! Serializable).toDictionary(), forKey: name)
            } else if propValue is Array<Serializable> {
                var subArray = Array<NSDictionary>()
                for item in (propValue as! Array<Serializable>) {
                    subArray.append(item.toDictionary())
                }
                propertiesDictionary.setValue(subArray, forKey: name)
            } else if propValue is NSData {
                propertiesDictionary.setValue((propValue as! NSData).base64EncodedStringWithOptions([]), forKey: name)
            } else if propValue is Bool {
                propertiesDictionary.setValue((propValue as! Bool).boolValue, forKey: name)
            } else if propValue is NSDate {
                let date = propValue as! NSDate
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateFormat = "Z"
                let dateString = NSString(format: "/Date(%.0f000%@)/", date.timeIntervalSince1970, dateFormatter.stringFromDate(date))
                propertiesDictionary.setValue(dateString, forKey: name)
            } else {
                propertiesDictionary.setValue(propValue, forKey: name)
            }
        }
        return propertiesDictionary
    }
    
    
    public func getNames() -> [String]{
        var target: AnyClass  = classForCoder
        var result:[String] = propertyNames(target)
        
        while target.superclass() != nil {
            target = target.superclass()!
            
            let clz: NSObject.Type = target as! NSObject.Type
            let con = clz.init()
            if (con is Serializable){
                result = result + propertyNames(target)
            }
        }
        return result
    }
    
    public func propertyNames(target:AnyClass!) -> [String] {
        var names: [String] = []
        var count: UInt32 = 0
        // Uses the Objc Runtime to get the property list
        let properties = class_copyPropertyList(target, &count)
        for var i = 0; i < Int(count); ++i {
            let property: objc_property_t = properties[i]
            let name: String = String.fromCString(property_getName(property))!
            names.append(name)
        }
        free(properties)
        return names
    }
    
    public override init() { super.init()}
}
