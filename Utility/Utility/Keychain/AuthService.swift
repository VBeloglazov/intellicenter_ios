//
//  KeychainAccess.swift
//  Pentair-iOS
//
//  Created by Jim Hewitt on 12/31/14.
//  Copyright (c) 2014 Pentair Corporation. All rights reserved.
//

import UIKit
import Security;

struct KeychainConstants {
    static var klass: String { return toString(kSecClass) }
    static var classGenericPassword: String { return toString(kSecClassGenericPassword) }
    static var attrAccount: String { return toString(kSecAttrAccount) }
    static var valueData: String { return toString(kSecValueData) }
    static var returnData: String { return toString(kSecReturnData) }
    static var matchLimit: String { return toString(kSecMatchLimit) }
    static var userName: String { return "UserName" }
    static var password: String { return "Password" }
    
    private static func toString(value: CFStringRef) -> String {
        return (value as String) ?? ""
    }
}

public class AuthService: NSObject {
    
    public var serviceName: String?
    public var authToken: String?
    public var type: String?
    public var expiration: Int?
    public var userName:String?
    
    // Login status
    public func loggedIn() -> Bool {
        if let token = self.authToken {
            //return count(self.authToken!) > 0
            return token.characters.count > 0
        }
        return false
    }
    
    // Retrieve the authentication token
    public func tokenPayload() -> String {
        return self.type! + " " + self.authToken!
    }
    
    public func logOut() -> Bool{
        self.authToken = nil
        return (self.remove(KeychainConstants.userName) && self.remove(KeychainConstants.password))
    }
    public func saveLogin(Username:String, Password:String) -> Bool{
        return (self.setKey(KeychainConstants.userName, value: Username) && self.setKey(KeychainConstants.password, value: Password))
    }
    
    public func getUserName()-> String?{
        return get(KeychainConstants.userName) ?? userName!
    }
    
    public func getPassword()-> String? {
        return get(KeychainConstants.password)
    }
    
    public func setKey(key: String, value: String) -> Bool {
        if let currentData = value.dataUsingEncoding(NSUTF8StringEncoding) {
            return setDataKey(key, value: currentData)
        }
        
        return false
    }
    
    public func setDataKey(key: String, value: NSData) -> Bool {
        let query = [
            KeychainConstants.klass       : KeychainConstants.classGenericPassword,
            KeychainConstants.attrAccount : key,
            KeychainConstants.valueData   : value ]
        
        SecItemDelete(query as CFDictionaryRef)
        
        let status: OSStatus = SecItemAdd(query as CFDictionaryRef, nil)
        
        return status == noErr
    }
    
    public func get(key: String) -> String? {
        if let currentData = getData(key) {
            if let currentString = NSString(data: currentData,
                encoding: NSUTF8StringEncoding) as? String {
                    
                    return currentString
            }
        }
        
        return nil
    }
    
    public func getData(key: String) -> NSData? {
        let query = [
            KeychainConstants.klass       : KeychainConstants.classGenericPassword,
            KeychainConstants.attrAccount : key,
            KeychainConstants.returnData  : kCFBooleanTrue,
            KeychainConstants.matchLimit  : kSecMatchLimitOne ]
        
        var dataTypeRef :AnyObject?
        let status: OSStatus = SecItemCopyMatching(query, &dataTypeRef)
        
        if status == errSecSuccess {
            if let currentDataTypeRef = dataTypeRef {
                return currentDataTypeRef as? NSData
            }
        }
        
        return nil
    }
    
    public func remove(key: String) -> Bool {
        let query = [
            KeychainConstants.klass       : kSecClassGenericPassword,
            KeychainConstants.attrAccount : key ]
        
        let status: OSStatus = SecItemDelete(query as CFDictionaryRef)
        
        return status == noErr
    }
    
    public func clear() -> Bool {
        let query = [ kSecClass as String : kSecClassGenericPassword ]
        
        let status: OSStatus = SecItemDelete(query as CFDictionaryRef)
        
        return status == noErr
    }
}

