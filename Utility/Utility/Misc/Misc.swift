//
//  Misc.swift
//
//
//  Created by Path Finder on 6/11/15.
//
//

import UIKit

public class Misc: NSObject {
    var result: Bool = false;
    
    public class func makeHex(prefix: String, status: String, parameterArray: [String]?, length: Int) -> [UInt8] {
        var hexValue = self.stringToHex(prefix)
        let command = UInt8(Int(status)!)
        let pad = UInt8(0)
        
        hexValue += [command]
        
        if parameterArray != nil {
            _ = parameterArray?.count
            for param in parameterArray! {
                let result = numberToHex(param)
                switch result {
                case let .Success(value):
                    hexValue += value;
                case let .Failure(s):
                    // TODO handle error
                    print(s)
                }
            }
        }
        
        for _ in hexValue.count...length-1 {
            hexValue += [pad]
        }
        
        return hexValue
    }
    
    public class func makeHex(prefix: String, status: String, parameters: String?, length: Int) -> [UInt8] {
        var hexValue = self.stringToHex(prefix)
        let command = UInt8(Int(status)!)
        let pad = UInt8(0)
        
        hexValue += [command]
        
        if parameters != nil {
            if (parameters!).characters.count > 4 {
                _ = self.hexStringToHex(parameters!)
            } else {
                let result = numberToHex(parameters!)
                switch result {
                case let .Success(value):
                    hexValue += self.prependToLength(value, len: 4);
                case let .Failure(s):
                    print (s);
                }
            }
        }
        
        for _ in hexValue.count...length-1 {
            hexValue += [pad]
        }
        
        return hexValue
    }
    
    public class func stringToHex(str: String) -> [UInt8] {
        _ = str.utf8.count
        var bytes = [UInt8]()
        
        for c in str.utf8 {
            bytes += [c]
        }
        
        return bytes
    }
    
    enum INumberToHexResult {
        case Success([UInt8])
        case Failure(NSString)
    }
    
    private class func numberToHex(str: String) -> INumberToHexResult {
        var bytes = [UInt8]()
        var value = Int(str)
        
        if (value >= (2 << 7)) {
            if (value >= (2 << 15)) {
                if (value >= (2 << 23)) {
                    guard (value < (2 << 31)) else {
                        return .Failure("Error in numberToHex conversion");
                    }
                    
                    bytes += [UInt8(value! / (2 << 23))]
                    value! = value! - (2 << 23)
                }
                bytes += [UInt8(value! / (2 << 15))]
                value! = value! - (2 << 15)
            }
            bytes += [UInt8(value! / (2 << 7))]
            value! = value! - (2 << 7)
        }
        bytes += [UInt8(value! % (2 << 7))]
        return .Success(bytes)
    }
    
    enum IHexToNumberResult {
        case Success(Int)
        case Failure(NSString)
    }
    
    private class func hexToNumber(str: [UInt8]) -> IHexToNumberResult {
        var value = 0
        var index = 0
        
        if str.count > 1 {
            index += 1
            if str.count > 2 {
                index += 1
                if str.count > 3 {
                    index += 1
                    guard (str.count <= 4) else {
                        return .Failure("Error in hexToNumber conversion")
                    }
                    value += (2 << 23) * Int(str[0])
                }
                value += (2 << 15) * Int(str[index-2])
            }
            value += (2 << 7) * Int(str[index-1])
        }
        value += Int(str[index])
        return .Success(value)
    }
    
    public class func padToLength(val: [UInt8], len: Int) -> [UInt8] {
        var bytes = [UInt8]()
        bytes += val
        
        for _ in val.count..<len {
            bytes += [UInt8(0)]
        }
        return bytes
    }
    
    public class func prependToLength(val: [UInt8], len: Int) -> [UInt8] {
        let size = val.count
        var bytes = [UInt8]()
        
        for _ in size..<len {
            bytes += [UInt8(0)]
        }
        bytes += val
        return bytes
    }
    
    enum IHexStringToHexResult {
        case Success([UInt8])
        case Failure(NSString)
    }
    
    private class func hexStringToHex(str: String) -> IHexStringToHexResult {
        var bytes = [UInt8]()
        var complete: Bool = false
        var nibble: UInt8 = 0x00
        
        for c in str.utf8 {
            if c >= 0x30 && c <= 0x39 {
                nibble |= c - 0x30
            }
            else if (c >= 0x41 && c <= 0x46) {
                nibble |= c - 0x37
            }
            else {
                guard (c >= 0x61 && c <= 0x66) else {
                    return .Failure("Error in hexStringToHex conversion")
                }
                nibble |= c - 0x57
            }
            if complete {
                bytes += [nibble]
                complete = false
            }
            else {
                nibble = nibble << 4
                complete = true
            }
        }
        return .Success(bytes)
    }
    
    public class func timeStampToStringArray() -> [String] {
        let calendar = NSCalendar.currentCalendar()
        let now = NSDate()
        var timestamp: [String] = [String]()
        
        let year = String(calendar.component(.Year, fromDate: now))
        
        let indexLL = year.startIndex.advancedBy(2)
        
        let yearLL = year.substringToIndex(indexLL)
        var yearHH = year
        yearHH.removeRange(Range<String.Index>(start: yearHH.startIndex, end:yearHH.startIndex.advancedBy(2)))
        
        let month = String(calendar.component(.Month, fromDate: now))
        let day = String(calendar.component(.Day, fromDate: now))
        let weekday = String(calendar.component(.Weekday, fromDate: now))
        let hour = String(calendar.component(.Hour, fromDate: now))
        let minute = String(calendar.component(.Minute, fromDate: now))
        let second = String(calendar.component(.Second, fromDate: now))
        
        timestamp.append(yearLL)
        timestamp.append(yearHH)
        timestamp.append(month)
        timestamp.append(day)
        timestamp.append(weekday)
        timestamp.append(hour)
        timestamp.append(minute)
        timestamp.append(second)
        
        return timestamp
    }
}
