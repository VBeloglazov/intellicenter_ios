//: Playground - noun: a place where people can play

import UIKit

public class Misc: NSObject {
    
    public class func makeHex(prefix: String, status: String, parameters: [String]?, length: Int) -> [UInt8] {
        var hexValue = self.stringToHex(prefix)
        let command = UInt8(Int(status)!)
        let pad = UInt8(0)
        
        hexValue += [command]
        
        if parameters != nil {
            let count = parameters?.count
            for param in parameters! {
                hexValue += self.numberToHex(param)
            }
        }
        
        for index in hexValue.count...length-1 {
            hexValue += [pad]
        }
        
        return hexValue
    }
    
    public class func makeHex(prefix: String, status: String, parameters: String?, length: Int) -> [UInt8] {
        var hexValue = self.stringToHex(prefix)
        let command = UInt8(Int(status)!)
        let pad = UInt8(0)
        
        hexValue += [command]
        
        if parameters != nil {
            if (parameters!).characters.count > 4 {
                
            } else {
                let hexParameters = self.numberToHex(parameters!)
                hexValue += self.prependToLength(hexParameters, len: 4)
            }
        }
        
        for index in hexValue.count...length-1 {
            hexValue += [pad]
        }
        
        return hexValue
    }
    
    public class func stringToHex(str: String) -> [UInt8] {
        let length = str.utf8.count
        var bytes = [UInt8]()
        
        for c in str.utf8 {
            bytes += [c]
        }
        
        return bytes
    }
    
    public class func hexStringToHex(str: String) -> [UInt8] {
        let error: NSError?
        var bytes = [UInt8]()
        var complete: Bool = false
        var nibble: UInt8 = 0x00
        
        for c in str.utf8 {
            if c >= 0x30 && c <= 0x39 {
                nibble |= c - 0x30
            }
            else if (c >= 0x41 && c <= 0x46) {
                nibble |= c - 0x37
            }
            else if (c >= 0x61 && c <= 0x66) {
                nibble |= c - 0x57
            }
            else {
                NSException.raise("Error in hexStringToHex conversion", format:"Error: %@", arguments: getVaList([error ?? "nil"]))
            }
            if complete {
                bytes += [nibble]
                complete = false
            }
            else {
                nibble = nibble << 4
                complete = true
            }
        }
        return bytes
    }
    
    public class func numberToHex(str: String) -> [UInt8] {
        let error: NSError?
        var bytes = [UInt8]()
        var value = Int(str)
        
        if (value >= (2 << 7)) {
            if (value >= (2 << 15)) {
                if (value >= (2 << 23)) {
                    if (value >= (2 << 31)) {
                        NSException.raise("Error in numberToHex conversion", format:"Error: %@", arguments: getVaList([error ?? "nil"]))
                    }
                    bytes += [UInt8(value! / (2 << 23))]
                    value! = value! - (2 << 23)
                }
                bytes += [UInt8(value! / (2 << 15))]
                value! = value! - (2 << 15)
            }
            bytes += [UInt8(value! / (2 << 7))]
            value! = value! - (2 << 7)
        }
        bytes += [UInt8(value! % (2 << 7))]
        return bytes
    }
    
    public class func hexToNumber(str: [UInt8]) -> Int {
        let error: NSError?
        var value = 0
        var index = 0
        
        if str.count > 1 {
            index += 1
            if str.count > 2 {
                index += 1
                if str.count > 3 {
                    index += 1
                    if str.count > 4 {
                        NSException.raise("Error in hexToNumber conversion", format:"Error: %@", arguments: getVaList([error ?? "nil"]))
                    }
                    value += (2 << 23) * Int(str[0])
                }
                value += (2 << 15) * Int(str[index-2])
            }
            value += (2 << 7) * Int(str[index-1])
        }
        value += Int(str[index])
        return value
    }
    
    public class func padToLength(val: [UInt8], len: Int) -> [UInt8] {
        var bytes = [UInt8]()
        bytes += val
        
        for i in val.count..<len {
            bytes += [UInt8(0)]
        }
        return bytes
    }
    
    public class func prependToLength(val: [UInt8], len: Int) -> [UInt8] {
        let size = val.count
        var bytes = [UInt8]()
        
        for i in size..<len {
            bytes += [UInt8(0)]
        }
        bytes += val
        return bytes
    }

    public class func timeStampToStringArray() -> [String] {
        let calendar = NSCalendar.currentCalendar()
        let now = NSDate()
        var timestamp: [String] = [String]()
        
        let year = String(calendar.component(.Year, fromDate: now))
        
        let indexLL = year.startIndex.advancedBy(2)
        
        let yearLL = year.substringToIndex(indexLL)
        var yearHH = year
        yearHH.removeRange(Range<String.Index>(start: yearHH.startIndex, end:yearHH.startIndex.advancedBy(2)))
        
        let month = String(calendar.component(.Month, fromDate: now))
        let day = String(calendar.component(.Day, fromDate: now))
        let weekday = String(calendar.component(.Weekday, fromDate: now))
        let hour = String(calendar.component(.Hour, fromDate: now))
        let minute = String(calendar.component(.Minute, fromDate: now))
        let second = String(calendar.component(.Second, fromDate: now))
        
        timestamp.append(yearLL)
        timestamp.append(yearHH)
        timestamp.append(month)
        timestamp.append(day)
        timestamp.append(weekday)
        timestamp.append(hour)
        timestamp.append(minute)
        timestamp.append(second)
        
        return timestamp
    }
    

}

let timestamp = Misc.timeStampToStringArray()
let ts1 = Misc.makeHex("NW", status: "1", parameters: timestamp, length: 22)

let yearLL = Misc.numberToHex("20")
let yearHH = Misc.numberToHex("15")
let month = Misc.numberToHex("6")
let day = Misc.numberToHex("12")
let dayOfWeek = Misc.numberToHex("5")
let hours = Misc.numberToHex("9")
let minutes = Misc.numberToHex("57")
let seconds = Misc.numberToHex("5")

let key = "6bba236332f223e2a4b1ccbf73b12033"

let val1 = Misc.numberToHex("532")
let val2 = Misc.makeHex("NW", status: "1", parameters: "9999", length: 22)
let val3 = Misc.padToLength(val1, len: 4)
let val4 = Misc.prependToLength(val1, len: 4)
let val5 = Misc.hexToNumber(val1)
let num1 = 2 << 3
let num2 = 2 << 7
let num3 = 2 << 11
let num4 = 2 << 15

let hexString = Misc.hexStringToHex(key)

