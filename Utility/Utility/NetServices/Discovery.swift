//
//  discovery.swift
//  Pentair-iOS
//
//  Created by Jim Hewitt on 12/8/14.
//  Copyright (c) 2014 Pentair Corporation. All rights reserved.
//

import Foundation

public let UTDataUpdatedNotification: String! = "DataUpdated"

public class Discovery: NSObject, NSNetServiceBrowserDelegate, NSNetServiceDelegate {
    public var serviceType: String
    public var serviceBrowser: NSNetServiceBrowser
    public var serviceList: [NSNetService]
    public var services: [LocalService]
    
    var serviceForSearch: String!
    
    public override init() {
        self.serviceType = "_http._tcp."
        self.serviceBrowser = NSNetServiceBrowser()
        self.serviceList = [NSNetService]()
        self.services = [LocalService]()
        super.init()
        self.serviceBrowser.delegate = self
    }
    
    public convenience init(service: String!) {
        self.init()
        self.serviceForSearch = service
    }
    
    public func netServiceBrowser(aNetServiceBrowser: NSNetServiceBrowser, didFindService aNetService: NSNetService, moreComing: Bool) {
        serviceList.append(aNetService)
        NSLog("Found: \(aNetService)")
        if !moreComing {
            update()
        }
    }
    
    public func netServiceBrowser(aNetServiceBrowser: NSNetServiceBrowser, didNotSearch errorDict: [String : NSNumber]) {
        NSLog("Search was not successful. Error code: \(errorDict[NSNetServicesErrorCode])")
    }
    
    public func netServiceBrowser(aNetServiceBrowser: NSNetServiceBrowser, didRemoveService aNetService: NSNetService, moreComing: Bool) {
        let iService = serviceList.indexOf(aNetService)
        if iService != nil {
            serviceList.removeAtIndex(iService!)
        }
        let subArray = services.filter({$0.netService == aNetService})
        for item in subArray {
            let iSharedPoint = services.indexOf(item)
            if iSharedPoint != nil {
                services.removeAtIndex(iSharedPoint!)
            }
        }
        NSLog("Became unavailable: \(aNetService)")
        if !moreComing {
            update()
        }
    }
    
    func update() {
        for service in serviceList {
            service.delegate = self
            service.resolveWithTimeout(5)
        }
    }
    
    public func netServiceDidResolveAddress(sender: NSNetService) {
        //service.addresses - array containing NSData objects, each of which contains an appropriate
        //sockaddr structure that you can use to connect to the socket
        for addressBytes in sender.addresses!
        {
            var inetAddress : sockaddr_in!
            var inetAddress6 : sockaddr_in6?
            //NSData’s bytes returns a read-only pointer (const void *) to the receiver’s contents.
            //var bytes: UnsafePointer<()> { get }
            let inetAddressPointer = UnsafePointer<sockaddr_in>(addressBytes.bytes)
            //Access the underlying raw memory
            inetAddress = inetAddressPointer.memory
            
            if inetAddress.sin_family == __uint8_t(AF_INET) //Note: explicit convertion (var AF_INET: Int32 { get } /* internetwork: UDP, TCP, etc. */)
            {
            }
            else
            {
                if inetAddress.sin_family == __uint8_t(AF_INET6) //var AF_INET6: Int32 { get } /* IPv6 */
                {
                    let inetAddressPointer6 = UnsafePointer<sockaddr_in6>(addressBytes.bytes)
                    inetAddress6 = inetAddressPointer6.memory
                    inetAddress = nil
                }
                else
                {
                    inetAddress = nil
                }
            }
            var ipString : UnsafePointer<Int8>?
            var portString = String()
            
            let ipStringBuffer = UnsafeMutablePointer<Int8>.alloc(Int(INET6_ADDRSTRLEN))
            if (inetAddress != nil)
            {
                var addr = inetAddress.sin_addr
                ipString = inet_ntop(Int32(inetAddress.sin_family),
                                     &addr,
                                     ipStringBuffer,
                                     __uint32_t(INET6_ADDRSTRLEN))
                
                let port = UInt16(bigEndian: inetAddress.sin_port)
                portString = (Int(port) as NSNumber).stringValue
            } else {
                if (inetAddress6 != nil)
                {
                    var addr  = inetAddress6!.sin6_addr
                    ipString = inet_ntop(Int32(inetAddress6!.sin6_family),
                                         &addr,
                                         ipStringBuffer,
                                         __uint32_t(INET6_ADDRSTRLEN))
                    let port = UInt16(bigEndian: inetAddress6!.sin6_port)
                    portString = (Int(port) as NSNumber).stringValue
                }
            }
            if (ipString != nil)
            {
                
                // Returns `nil` if there is a malformed service type or ip address
                let ip = String.fromCString(ipString!)
                if ip != nil {
                    NSLog("\(sender.name)(\(sender.type)) - \(ip!)")
                    if (sender.name.isEmpty)
                    {
                        continue
                    }
                    let serviceName = sender.name
                    let location = serviceName.rangeOfString("Pentair")
                    if (location != nil)
                    {
                        // strip off the Pentair:
                        let args = serviceName.substringWithRange(Range<String.Index>(start: serviceName.startIndex.advancedBy(7), end: serviceName.endIndex))
                        if let nameIndex = args.rangeOfString("-n"),
                            let idIndex = args.rangeOfString("-i"){
                            guard case let id = args.substringWithRange(Range<String.Index>(start: idIndex.startIndex.advancedBy(2), end: nameIndex.startIndex)) else {
                             continue
                            }
                            let name = args.substringWithRange(Range<String.Index>(start: nameIndex.startIndex.advancedBy(2), end: args.endIndex))
                            let trimmedName = name.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
                            var found = false;
                            // Is this a duplicate entry?
                            for var i = 0; i < services.count; i++ {
                                
                                if (services[i].name == trimmedName) {
                                    found = true;
                                    continue
                                }
                            }
                            if (!found) {
                                services.append(LocalService(netService: sender, name: trimmedName, id: id, ipAddress: ip!, portValue: portString))
                                NSNotificationCenter.defaultCenter().postNotificationName(UTDataUpdatedNotification, object: self)
                            }
                        }
                    }
                }
            }
            // Deallocate the pointer memory.
            ipStringBuffer.dealloc(Int(INET6_ADDRSTRLEN))
        }
    }
    
    public func netService(sender: NSNetService, didNotResolve errorDict: [String : NSNumber]) {
        NSLog("\(sender.name) did not resolve: \(errorDict[NSNetServicesErrorCode]!)")
    }
    
    public func searchForLocalProperties() {
        serviceBrowser.searchForServicesOfType(serviceType, inDomain: "")
    }
    
    public func reset() {
        serviceBrowser.stop()
        for service in serviceList {
            service.stop()
        }
        serviceList.removeAll()
        services.removeAll()
    }
}

// Helper class
public class LocalService: Equatable {
    public var netService: NSNetService
    public var name: String
    public var Id: String
    public var ipAddress: String
    public var port: String
    
    public init(netService: NSNetService, name: String, id: String, ipAddress: String, portValue: String) {
        self.netService = netService
        self.name = name
        self.Id = id
        self.ipAddress = ipAddress
        self.port = portValue
    }
}

public func == (lhs: LocalService, rhs: LocalService) -> Bool {
    return (lhs.netService == rhs.netService) && (lhs.name == rhs.name) && (lhs.ipAddress == rhs.ipAddress) && (lhs.port == rhs.port)
}