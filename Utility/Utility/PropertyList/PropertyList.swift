//
//  PropertyList.swift
//  
//
//  Created by Path Finder on 6/9/15.
//
//

import UIKit

public class PropertyList: NSObject {
    
    public class func dictionaryFromPropertyList(filename: NSString) -> NSDictionary? {
        var result: NSDictionary! = nil
        let fname: NSString = NSString(format: "%@.plist", filename)
        let rootPath: NSString = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as NSString
        let bundlePath: NSString = rootPath.stringByAppendingPathComponent(fname as String)
        
        let aData: NSData? = NSData(contentsOfFile: bundlePath as String)
        if (aData != nil) {
            result = NSKeyedUnarchiver.unarchiveObjectWithData(aData!) as! NSDictionary
        }
        
        return result
    }
    
    public class func writePropertyListFromDictionary(filename: NSString, plistDict: NSDictionary) -> Bool {
        let fname: NSString = NSString(format: "%@.plist", filename)
        let rootPath: NSString = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] as NSString
        let bundlePath: NSString = rootPath.stringByAppendingPathComponent(fname as String)
        
        let aData: NSData = NSKeyedArchiver.archivedDataWithRootObject(plistDict)
        
        return aData.writeToFile(bundlePath as String, atomically: true)
    }
}
